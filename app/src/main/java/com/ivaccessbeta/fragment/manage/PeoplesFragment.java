package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.PeopleAdapter;
import com.ivaccessbeta.model.FacilityListModel;
import com.ivaccessbeta.model.PeopleModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class PeoplesFragment extends Fragment implements AsynchTaskListner {
    public RecyclerView rvPeople;
    public PeopleAdapter mAdapter;
    public ArrayList<PeopleModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static PeoplesFragment instance;
    public EditText edtSearch;
    public View view;
    public PeopleModel peopleModel;
    public String emailId, token, userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_peoples, container, false);
        linearJobs.setVisibility(View.GONE);
        instance = this;
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");

//        emailId = App.uEmail;
//        token = App.uToken;
//        userId = App.uID;

        setHasOptionsMenu(true);
        rvPeople = view.findViewById(R.id.rvPeople);
        relativeAdd = view.findViewById(R.id.relative_add);
        relativeAdd.setOnClickListener(view -> {

            AddPeopleFragment addPeopleFragment = new AddPeopleFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("value", 0);
            addPeopleFragment.setArguments(bundle);
            ((MainActivity)getActivity()).changeFragment(addPeopleFragment, true);

        });

        edtSearch = view.findViewById(R.id.edt_search);


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }

            }
        });
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // menu.getItem(1).setVisible(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getPeopleDetail();
    }

    private void getPeopleDetail() {
        list.clear();
        new CallRequest(instance).getPeople(emailId, token);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getPeople:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {

                            JSONArray jObject = jObj.getJSONArray("data");

                            for (int i = 0; jObject.length() > i; i++) {
                                peopleModel = new PeopleModel();
//                                peopleModel = (PeopleModel) jParsar.parseJson(jObject.getJSONObject(i), new PeopleModel());
                                peopleModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), PeopleModel.class);
                                list.add(peopleModel);

                                JSONArray facilityJArray = jObject.getJSONObject(i).getJSONArray("Facilities");

                                System.out.println("facility size===" + facilityJArray.length());
                                for (int j = 0; j < facilityJArray.length(); j++) {
                                    FacilityListModel facilityListModel = new FacilityListModel();
//                                    facilityListModel = (FacilityListModel) jParsar.parseJson(facilityJArray.getJSONObject(j), new FacilityListModel());
                                    facilityListModel = LoganSquare.parse(facilityJArray.getJSONObject(j).toString(), FacilityListModel.class);
                                    peopleModel.facilityList.add(facilityListModel);
                                }
                            }
                            mAdapter = new PeopleAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvPeople.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvPeople.setLayoutManager(mLayoutManager);
                            rvPeople.setItemAnimator(new DefaultItemAnimator());
                            rvPeople.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case deletePeople:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("People Delete Successfully", getContext());
                            getPeopleDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}