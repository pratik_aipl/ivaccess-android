package com.ivaccessbeta.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.BuildConfig;
import com.ivaccessbeta.R;
import com.ivaccessbeta.model.AdminDetailModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class SplashActivity extends AppCompatActivity implements AsynchTaskListner {

    private static int SPLASH_TIME_OUT = 2000;
    public SharedPreferences shared;
    public SplashActivity instance;
    AdminDetailModel detailModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        instance = this;
        shared = getSharedPreferences(getApplication().getPackageName(), 0);

        TextView txtVersion = findViewById(R.id.txtVersion);
        txtVersion.setText("Version " + BuildConfig.VERSION_NAME);

        new Handler().postDelayed(() -> {
            MySharedPref.MySharedPref(getApplicationContext());
            Intent i = null;
            if (MySharedPref.getisLogin()) {
                App.password = MySharedPref.getString(getApplicationContext(), App.PASSWORD, "");
                App.passwordChange = MySharedPref.getString(getApplicationContext(), App.IS_PASSWORD_CHANGE, "");
                Utils.hideProgressDialog();
                if (App.passwordChange.equalsIgnoreCase(Constant.PASSWORD_NOT_CHANGE)) {
                    i = new Intent(SplashActivity.this, ChangePasswordActivity.class);
                } else {
                    i = new Intent(SplashActivity.this, MainActivity.class);
                }
            } else {
                i = new Intent(SplashActivity.this, LoginActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("intent", 1);
                i.putExtras(bundle);
            }
            startActivity(i);
            finish();

        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case login:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {

                            JSONObject jObject = jObj.getJSONObject("data");
                            detailModel = LoganSquare.parse(jObject.toString(),AdminDetailModel.class);
                            String uEmail = detailModel.getEmail();
                            String uToken = detailModel.getApp_token();
                            String uID = detailModel.getId();

                            MySharedPref.setString(instance, App.EMAIL, uEmail);
                            MySharedPref.setString(instance, App.APP_TOKEN, uToken);
                            MySharedPref.setString(instance, App.LOGGED_USER_ID, uID);
                            MySharedPref.setisLogin(true);

                            Intent intent = new Intent(instance, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Utils.handleJSonAPIError(jObj, instance);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
