package com.ivaccessbeta.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ivaccessbeta.fragment.profile.AboutProfileFragment;
import com.ivaccessbeta.fragment.profile.EmployeeDocProfileFragment;
import com.ivaccessbeta.fragment.profile.EquipmentsProfileFragment;
import com.ivaccessbeta.fragment.profile.FacilitiesProfileFragment;
import com.ivaccessbeta.fragment.profile.HistoryProfileFragment;
import com.ivaccessbeta.fragment.profile.JobScheduleProfileFragment;
import com.ivaccessbeta.fragment.profile.ScheduleProfileFragment;

/**
 * Created by User on 22-03-2018.
 */

public class ProfileTabsPagerAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;

    public ProfileTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ScheduleProfileFragment tab1 = new ScheduleProfileFragment();
                return tab1;
            case 1:
                JobScheduleProfileFragment tab2 = new JobScheduleProfileFragment();
                return tab2;
            case 2:
                HistoryProfileFragment tab3 = new HistoryProfileFragment();
                return tab3;
            case 3:
                AboutProfileFragment tab4 = new AboutProfileFragment();
                return tab4;
            case 4:
                FacilitiesProfileFragment tab5 = new FacilitiesProfileFragment();
                return tab5;
            case 5:
                EmployeeDocProfileFragment tab6 = new EmployeeDocProfileFragment();
                return tab6;
            case 6:
                EquipmentsProfileFragment tab7 = new EquipmentsProfileFragment();
                return tab7;

            default:
                return null;
        }
    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 7;
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
