package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 01-03-2018.
 */

@JsonObject
public class EquipmentsModel implements Serializable {

    @JsonField
    public String EquimentID, EquipmentName, EquipmentCode, EquipmentDetails;

    public String getEquimentID() {
        return EquimentID;
    }

    public void setEquimentID(String equimentID) {
        EquimentID = equimentID;
    }

    public String getEquipmentName() {
        return EquipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        EquipmentName = equipmentName;
    }

    public String getEquipmentDetails() {
        return EquipmentDetails;
    }

    public void setEquipmentDetails(String equipmentDetails) {
        EquipmentDetails = equipmentDetails;
    }

    public String getEquipmentCode() {
        return EquipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        EquipmentCode = equipmentCode;
    }
}
