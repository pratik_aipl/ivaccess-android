package com.ivaccessbeta.utils;

import android.view.View;

/**
 * Created by User on 19-02-2018.
 */

public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
