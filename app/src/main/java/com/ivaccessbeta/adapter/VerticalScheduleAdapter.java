package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.PeofileJobScheduleModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class VerticalScheduleAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PeofileJobScheduleModel> mList;
    private SparseIntArray listPosition = new SparseIntArray();
    private HorizontalSchedualAdapter.OnItemClickListener mItemClickListener;
    private Context mContext;

    public VerticalScheduleAdapter(Context mContext, ArrayList<PeofileJobScheduleModel> list) {
        this.mContext = mContext;
        this.mList = list;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {

        private RecyclerView mRecyclerView;
        private TextView txtDate,txtDay,txtMonth;

        public CellViewHolder(View itemView) {
            super(itemView);
            mRecyclerView = itemView.findViewById(R.id.recyclerView);
            txtDate= itemView.findViewById(R.id.txtDate);
            txtDay= itemView.findViewById(R.id.txtDay);
            txtMonth= itemView.findViewById(R.id.txtMonth);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_schedule_vertical, viewGroup, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
                cellViewHolder.mRecyclerView.setHasFixedSize(true);
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
//                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                cellViewHolder.mRecyclerView.setLayoutManager(layoutManager);
                HorizontalSchedualAdapter adapter = new HorizontalSchedualAdapter(mContext,mList.get(position).subArray);
                cellViewHolder.mRecyclerView.setAdapter(adapter);
                getDateMMYYFormate(position,cellViewHolder.txtDate,cellViewHolder.txtDay,cellViewHolder.txtMonth);
                adapter.SetOnItemClickListener(mItemClickListener);
                int lastSeenFirstPosition = listPosition.get(position, 0);

                if (lastSeenFirstPosition >= 0) {
                    cellViewHolder.mRecyclerView.scrollToPosition(lastSeenFirstPosition);
                }
                break;
            }
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        final int position = viewHolder.getAdapterPosition();
        CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
        LinearLayoutManager layoutManager = ((LinearLayoutManager) cellViewHolder.mRecyclerView.getLayoutManager());
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
        listPosition.put(position, firstVisiblePosition);

        super.onViewRecycled(viewHolder);
    }


    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }


    // for both short and long click
    public void SetOnItemClickListener(final HorizontalSchedualAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private void getDateMMYYFormate(int position ,TextView txt,TextView day,TextView month) {
        try{
        String CurrentStringEnd =mList.get(position).getJob_date();
        String[] separated = CurrentStringEnd.split("-");
        final String s1End = separated[1];
        String sEndMonth = null;
        System.out.println("spilits" + s1End);

        if (s1End.equalsIgnoreCase("01")) {
            sEndMonth = "January ";
        } else if (s1End.equalsIgnoreCase("02")) {
            sEndMonth = "February ";
        } else if (s1End.equalsIgnoreCase("03")) {
            sEndMonth = "March ";
        } else if (s1End.equalsIgnoreCase("04")) {
            sEndMonth = "April ";
        } else if (s1End.equalsIgnoreCase("05")) {
            sEndMonth = "May ";
        } else if (s1End.equalsIgnoreCase("06")) {
            sEndMonth = "June ";
        } else if (s1End.equalsIgnoreCase("07")) {
            sEndMonth = "July ";
        } else if (s1End.equalsIgnoreCase("08")) {
            sEndMonth = "August ";
        } else if (s1End.equalsIgnoreCase("09")) {
            sEndMonth = "September ";
        } else if (s1End.equalsIgnoreCase("10")) {
            sEndMonth = "October ";
        } else if (s1End.equalsIgnoreCase("11")) {
            sEndMonth = "November ";
        } else if (s1End.equalsIgnoreCase("12")) {
            sEndMonth = "December ";
        }

        String asubstringEnd = CurrentStringEnd.substring(8, 10);

        String date = mList.get(position).getJob_date();
        System.out.println("date from api---" + date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dayOfTheWeek = (String) DateFormat.format("EEEE",myDate);
       // txt.setText(Html.fromHtml(" <font color=\"#000000\">"+(firstThree(dayOfTheWeek)+""+"</font><font color=\"#3F51B5\"> <big>"+"" + asubstringEnd + "</big></font>" + "<font color=\"#000000\">"+"     "+firstThree(sEndMonth))+"</font>"));

       txt.setText(asubstringEnd);
       day.setText(firstThree(dayOfTheWeek));
       month.setText(firstThree(sEndMonth));
        //tabLayout.addTab(tabLayout.newTab().setText(asubstringEnd + "-" + firstThree(sEndMonth) + " " + dayOfTheWeek));

        }catch (Exception e){
        e.printStackTrace();}
    }
    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }
}
