package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.checks.CheckFragment;
import com.ivaccessbeta.listners.CheckListener;
import com.ivaccessbeta.model.CheckChildModel;
import com.ivaccessbeta.model.CheckGroupModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CheckCustomExpandableListAdapter extends BaseExpandableListAdapter {

    private static final String TAG = "CheckCustomExpandableLi";
    private Context context;
    private ArrayList<CheckGroupModel> groupArray;
    public CheckListener checkListener;
    public CheckFragment checkFragment;
    public List<CheckGroupModel> searchFilterList = new ArrayList<>();

    public String roleId;

    public CheckCustomExpandableListAdapter(Context context, ArrayList<CheckGroupModel> groupArray, CheckFragment checkFragment) {
        this.context = context;
        this.groupArray = groupArray;
        this.checkFragment = checkFragment;
        this.checkListener = checkFragment;
        this.searchFilterList.addAll(groupArray);
    }

    @Override
    public CheckChildModel getChild(int groupPos, int childPos) {
        return this.groupArray.get(groupPos).getChildList().get(childPos);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int groupPos, final int childPos, boolean isLastChild, View convertView, ViewGroup parent) {
//        if (convertView == null) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.check_list_child, null);
//        }
        final EditText edtItemMemo;
        final EditText edtAmount;

        TextView expandedListTxtCheckNo = convertView
                .findViewById(R.id.txtCheckNo);
        TextView expandedLisTxtCheckDate = convertView
                .findViewById(R.id.txtCheckDate);
        TextView expandedLisTxt1 = convertView
                .findViewById(R.id.txt1);
        TextView expandedLisTxtPrice = convertView
                .findViewById(R.id.txt_price);
        edtItemMemo = convertView
                .findViewById(R.id.edtItemMemo);
        edtAmount = convertView
                .findViewById(R.id.edtAmount);
        ImageView ivPlush = convertView
                .findViewById(R.id.ivPlush);
        roleId = MySharedPref.getString(context, App.ROLEID, "");
        //ImageView ivEdit = convertView.findViewById(R.id.iv_edit);
        ImageView ivCancel = convertView.findViewById(R.id.iv_cancel);
        CheckChildModel cModel = getChild(groupPos, childPos);
        if (cModel.getCheckStatus().equalsIgnoreCase(Constant.BLUE_BG)) {
            expandedListTxtCheckNo.setBackgroundResource(R.drawable.dynamic_round);
            GradientDrawable gd = (GradientDrawable) expandedListTxtCheckNo.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#4C85BA"));
            gd.setCornerRadii(new float[]{20, 20, 20, 20, 20, 20, 20, 20});
            gd.setStroke(2, Color.parseColor("#4C85BA"), 5, 0);
        } else if (cModel.getCheckStatus().equalsIgnoreCase(Constant.RED_BG)) {
            expandedListTxtCheckNo.setBackgroundResource(R.drawable.dynamic_round);
            GradientDrawable gd = (GradientDrawable) expandedListTxtCheckNo.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#e41c1c"));
            gd.setCornerRadii(new float[]{20, 20, 20, 20, 20, 20, 20, 20});
            gd.setStroke(2, Color.parseColor("#e41c1c"), 5, 0);
        }

        if (!cModel.getJobNo().equals(""))
            expandedListTxtCheckNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkListener.onJobNoClick(groupPos, childPos, v);
                }
            });


//        expandedLisTxtCheckDate.setText(cModel.getCheckDate());
        Utils.converMMddyyyyFormate(cModel.getCompletedDate(), expandedLisTxtCheckDate);
        expandedLisTxt1.setText(cModel.getPatientName() + " " + cModel.getServiceName());
        if (isLastChild) {
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                edtItemMemo.setVisibility(View.GONE);
                edtAmount.setVisibility(View.GONE);
                ivPlush.setVisibility(View.GONE);
            } else {
                edtItemMemo.setVisibility(View.VISIBLE);
                edtAmount.setVisibility(View.VISIBLE);
                ivPlush.setVisibility(View.VISIBLE);
            }
            if (cModel.getQuickBookSync().equalsIgnoreCase("s")) {
                edtItemMemo.setVisibility(View.GONE);
                edtAmount.setVisibility(View.GONE);
                ivPlush.setVisibility(View.GONE);
            }
        } else {
            edtItemMemo.setVisibility(View.GONE);
            edtAmount.setVisibility(View.GONE);
            ivPlush.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(cModel.getJobNo())) {
            expandedLisTxt1.setText(cModel.getNote());
            expandedListTxtCheckNo.setText(" - ");
        } else {
            expandedListTxtCheckNo.setText(cModel.getJobNo());
        }
        expandedLisTxtPrice.setText(cModel.getCheckAmount());
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) || roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            ivCancel.setVisibility(View.INVISIBLE);
        }
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkListener.onDeletePerticularChildClick(groupPos, childPos, v);
            }
        });
        ivPlush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String memoNote = edtItemMemo.getText().toString();
                String memoAmount = edtAmount.getText().toString();

                if (TextUtils.isEmpty(memoAmount)) {
                    Utils.showToast("Please Enter Amount", context);
                } else {
                    checkListener.onAddMemoChildClick(groupPos, childPos, v, memoNote, memoAmount);
                }
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.groupArray.get(listPosition).getChildList().size();
    }


    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        groupArray.clear();
        if (charText.length() == 0) {
            groupArray.addAll(searchFilterList);
        } else {
            for (CheckGroupModel bean : searchFilterList) {
                if (bean.getFullname().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    groupArray.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.groupArray.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupArray.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        //  String listTitle = (String) getGroup(groupPosition);
//        if (convertView == null) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.check_list_group, null);
//        }
        TextView listTitleTxtDate = convertView
                .findViewById(R.id.txtDate);
        TextView listTitleTxtName = convertView
                .findViewById(R.id.txtName);
        TextView listTitleTxtPrice = convertView
                .findViewById(R.id.txtPrice);
        ImageView listAction = convertView.findViewById(R.id.ivAction);
        LinearLayout llMain = convertView.findViewById(R.id.llMain);
        listTitleTxtDate.setTypeface(null, Typeface.BOLD);


        Utils.converMMddyyyyFormate(groupArray.get(groupPosition).getCheckDate(), listTitleTxtDate);
        Log.d(TAG, "getGroupView: " + groupArray.get(groupPosition).getCheckDate());
//        listTitleTxtDate.setText(groupArray.get(groupPosition).getCheckDate());

        listTitleTxtName.setText(groupArray.get(groupPosition).getFullname());
        listTitleTxtPrice.setText(groupArray.get(groupPosition).getAmount());

        if (groupArray.get(groupPosition).getCheckStatus().equalsIgnoreCase(Constant.BLUE_BG)) {
            llMain.setBackgroundColor(Color.parseColor("#4C85BA"));
        } else if (groupArray.get(groupPosition).getCheckStatus().equalsIgnoreCase(Constant.RED_BG)) {
            llMain.setBackgroundColor(Color.parseColor("#e41c1c"));
        }
        listAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkListener.onOpenPopupListClick(groupPosition, v, groupArray.get(groupPosition));
            }
        });

        LinearLayout linearNote = convertView.findViewById(R.id.linear_note);
        LinearLayout linearPaymentNote = convertView.findViewById(R.id.linear_payment_note);

        if (!isExpanded && (linearNote.getChildCount() + linearPaymentNote.getChildCount()) == 0) {
            for (int j = 0; j < groupArray.get(groupPosition).getNoteList().size(); j++) {
                LayoutInflater layoutInflater2 = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater2.inflate(R.layout.invoice_note_list, null);
                linearNote.addView(view);
                TextView txtDate = view.findViewById(R.id.txtDate);
                TextView txtNote = view.findViewById(R.id.txtNote);
                ImageView ivClose = view.findViewById(R.id.ivClose);
                Log.d(TAG, "getGroupView: 1 1 " + groupArray.get(groupPosition).getNoteList().get(j).getCreatedOn());
//                Utils.converMMddyyyyFormate(groupArray.get(groupPosition).getNoteList().get(j).getCreatedOn(),txtDate);
                txtDate.setText(groupArray.get(groupPosition).getNoteList().get(j).getCreatedOn());

                final int finalJ = j;
                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkListener.onDeleteNote(groupPosition, finalJ, v);
                    }
                });
                Log.d(TAG, "getGroupView: note --> " + groupArray.get(groupPosition).getNoteList().get(j).getNote());
                txtNote.setText(groupArray.get(groupPosition).getNoteList().get(j).getNote());
            }
//
//            for (int k = 0; k < groupArray.get(groupPosition).getNotePaymentList().size(); k++) {
//
//                LayoutInflater layoutInflater2 = (LayoutInflater) this.context.
//                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                final View view = layoutInflater2.inflate(R.layout.invoice_payment_note_list, null);
//                linearPaymentNote.addView(view);
//                TextView txtDate = view.findViewById(R.id.txtDate);
//                TextView txtNote = view.findViewById(R.id.txtNote);
//                TextView txtRFNo = view.findViewById(R.id.txtRfNo);
//                TextView txtPayment=view.findViewById(R.id.txtPayment);
//                ImageView ivClose=view.findViewById(R.id.ivClose);
//                final int finalK = k;
//                ivClose.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        checkListener.onDeletePaymentNote(groupPosition, finalK, v);
//                    }
//                });
//                txtPayment.setText("Payment: $"+groupArray.get(groupPosition).getNotePaymentList().get(k).getAmount());
//                txtDate.setText(groupArray.get(groupPosition).getNotePaymentList().get(k).getCreatedOn());
//                txtNote.setText(groupArray.get(groupPosition).getNotePaymentList().get(k).getNote());
//                txtRFNo.setText("Ref No: "+groupArray.get(groupPosition).getNotePaymentList().get(k).getRefNo());
//            }

        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}