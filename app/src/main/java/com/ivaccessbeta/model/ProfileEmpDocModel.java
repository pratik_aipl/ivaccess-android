package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonObject
public class ProfileEmpDocModel implements Serializable {

    @JsonField
    public String DOCID, DocumentName, DEID, UserID, DocumentStatus, FilePath, Expired, NewUploaded, isUploaded, DocumentExpire = "", Pdf = "";

    @JsonField(name = "expired")
    ArrayList<String> expiredDocArray;

    @JsonField(name = "FileName")
    ArrayList<String> FileName;

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getNewUploaded() {
        return NewUploaded;
    }

    public void setNewUploaded(String newUploaded) {
        NewUploaded = newUploaded;
    }

    public String getPdf() {
        return Pdf;
    }

    public void setPdf(String pdf) {
        Pdf = pdf;
    }

    public String getExpired() {
        return Expired;
    }


    public ArrayList<String> getExpiredDocArray() {
        return expiredDocArray;
    }

    public void setExpiredDocArray(ArrayList<String> expiredDocArray) {
        this.expiredDocArray = expiredDocArray;
    }

    public ArrayList<String> getFileName() {
        return FileName;
    }

    public void setFileName(ArrayList<String> fileName) {
        FileName = fileName;
    }

    public void setExpired(String expired) {
        Expired = expired;
    }

    public String getDocumentExpire() {
        return DocumentExpire;
    }

    public void setDocumentExpire(String documentExpire) {
        DocumentExpire = documentExpire;
    }

    public String getDOCID() {
        return DOCID;
    }

    public void setDOCID(String DOCID) {
        this.DOCID = DOCID;
    }

    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String documentName) {
        DocumentName = documentName;
    }

    public String getDEID() {
        return DEID;
    }

    public void setDEID(String DEID) {
        this.DEID = DEID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getDocumentStatus() {
        return DocumentStatus;
    }

    public void setDocumentStatus(String documentStatus) {
        DocumentStatus = documentStatus;
    }

    public String getFilePath() {
        return FilePath;
    }

    public void setFilePath(String filePath) {
        FilePath = filePath;
    }


}
