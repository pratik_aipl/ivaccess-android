package com.ivaccessbeta.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.listners.LogOnClick;
import com.ivaccessbeta.model.LogHistoryModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 19-03-2018.
 */

public class LogHistoryAdapter extends RecyclerView.Adapter<LogHistoryAdapter.MyViewHolder> {

    public ArrayList<LogHistoryModel> list;
    public List<LogHistoryModel> searchFilterList = new ArrayList<>();
    public Activity context;
    public String emailId, token, userId;
    LogOnClick logOnClick;

    public LogHistoryAdapter(ArrayList<LogHistoryModel> logList, JobDetailFragment jobDetailFragment) {
        this.context = jobDetailFragment.getActivity();
        this.list = logList;
        this.searchFilterList.addAll(list);
        logOnClick = jobDetailFragment;
    }


    public LogHistoryAdapter(ArrayList<LogHistoryModel> List, Activity context) {
        this.context = context;
        this.list = List;
        this.searchFilterList.addAll(list);
        logOnClick = (LogOnClick) context;
    }

    @Override
    public LogHistoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_log_history, parent, false);
        return new LogHistoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LogHistoryAdapter.MyViewHolder holder, final int position) {
        LogHistoryModel model = list.get(position);
        holder.txtJobStatus.setText(Html.fromHtml(" <font color=\"#3F51B5\">" + model.getTitle() + "</font><font color=\"#000000\"> <small>" + "by " + model.getFirst_name() + " " + model.getLast_name() + "</small></font>"));
        holder.txtDateAndTime.setText(model.getCreatedOn());
        String note = model.getNote();
        holder.txtNote.setVisibility(View.GONE);
        if (model.getTitle().equalsIgnoreCase("Added Check") && !note.equalsIgnoreCase("")) {
            holder.txtNo.setVisibility(View.VISIBLE);
            holder.txtNo.setText(note);
        } else if (model.getTitle().equalsIgnoreCase("Added Invoice") && !note.equalsIgnoreCase("")) {
            holder.txtNo.setVisibility(View.VISIBLE);
            holder.txtNo.setText(note);
        } else {
            holder.txtNo.setVisibility(View.GONE);
            if (note.equalsIgnoreCase("")) {
                holder.txtNote.setVisibility(View.GONE);
            } else {
                holder.txtNote.setVisibility(View.VISIBLE);
                holder.txtNote.setText(note);
            }
        }
        holder.txtNo.setOnClickListener(view -> logOnClick.callLogClick(position, list.get(position)));


        dateConvertFormate(holder.txtDateAndTime, model.getCreatedOn(), "");
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtJobStatus, txtDateAndTime, txtNote, txtNo;

        public MyViewHolder(View view) {
            super(view);
            // name = view.findViewById(R.id.txtName);
            txtJobStatus = view.findViewById(R.id.txtJobStatus);
            txtDateAndTime = view.findViewById(R.id.txtDateTime);
            txtNote = view.findViewById(R.id.txtNote);
            txtNo = view.findViewById(R.id.txtNo);
        }

    }

    private void dateConvertFormate(TextView txt, String Date, String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = null;
        try {
            date = sdf.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat formatDate = new SimpleDateFormat("dd");
        DateFormat formatMonth = new SimpleDateFormat("MM");
        DateFormat formatYear = new SimpleDateFormat("yyyy");
        DateFormat formatAmPm = new SimpleDateFormat("hh:mm a");

        String finalDate = formatDate.format(date);
        String finalMonth = formatMonth.format(date);
        String finalHour = formatAmPm.format(date);
        String finalYear = formatYear.format(date);


        txt.setText(msg + "" + finalHour + " " + finalMonth + "-" + finalDate + "-" + finalYear);
    }
}
