package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.model.PeofileJobScheduleModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;


/**
 * Created by User on 23-03-2018.
 */

public class HorizontalSchedualAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<PeofileJobScheduleModel> mList;
    private OnItemClickListener mItemClickListener;
    Context mContext;

    public HorizontalSchedualAdapter(Context mContext, ArrayList<PeofileJobScheduleModel> list) {
        this.mList = list;
        this.mContext = mContext;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView txtHospitalName, txtFacliterName, txtTimeTo, txtTimeFrom, txtJobStatus;
        public RelativeLayout relativeSchedule;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtHospitalName = itemView.findViewById(R.id.txtHospitalName);
            txtFacliterName = itemView.findViewById(R.id.txtFacliterName);
            txtTimeTo = itemView.findViewById(R.id.txtTimeTo);
            txtTimeFrom = itemView.findViewById(R.id.txtTimeFrom);
            txtJobStatus = itemView.findViewById(R.id.txtJobStatus);
            relativeSchedule = itemView.findViewById(R.id.relativeSchedule);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getLayoutPosition());
            }

            String jobid= mList.get(getLayoutPosition()).getJobID();
            System.out.println("job id===="+jobid);
            JobDetailFragment fragment = new JobDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString("jobNo", jobid);
            fragment.setArguments(bundle);
            ((MainActivity)mContext).changeFragment(fragment, true);
        }

        @Override
        public boolean onLongClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemLongClick(view, getLayoutPosition());
                return true;
            }
            return false;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {

        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_horizontal_schedule, viewGroup, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;
                cellViewHolder.txtHospitalName.setText("" + mList.get(position).getFacilityName());
                cellViewHolder.txtFacliterName.setText("" + mList.get(position).getFirst_name() + " " + mList.get(position).getLast_name());
                cellViewHolder.txtJobStatus.setText("" + mList.get(position).getJobStatusName());

                    cellViewHolder.relativeSchedule.setBackgroundColor(Color.parseColor(mList.get(position).getJobStatusColor()));

                // cellViewHolder.txtJobStatus.setText(""+mList.get(position).getJo());

//                cellViewHolder.txtTimeFrom.setText(mList.get(position).getDueTimeDate());
//                cellViewHolder.txtTimeTo.setText(mList.get(position).getCreatedOn());

                convertTo24FormateFrom(cellViewHolder.txtTimeFrom, mList.get(position).getDueTimeDate());
                convertTo24FormateFrom(cellViewHolder.txtTimeTo, mList.get(position).getCreatedOn());
                break;
            }
        }
    }

    private void convertTo24FormateFrom(TextView txt, String timeStr) {


        StringTokenizer tk = new StringTokenizer(timeStr);
        String date = tk.nextToken();
        String time = tk.nextToken();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        Date dt;
        try {
            dt = sdf.parse(time);
            System.out.println("Time Display: " + sdfs.format(dt)); // <-- I got result here
            txt.setText(sdfs.format(dt));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        SimpleDateFormat date12Format = new SimpleDateFormat("HH:mm");
//
//        SimpleDateFormat date24Format = new SimpleDateFormat("hh:mm aa");
//
//        try {
//            String fromTime24 = date24Format.format(date12Format.parse(mList.get(position).getDueTimeDate()));
//            System.out.println("fromTime24==="+fromTime24);
//
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    // for both short and long click
    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}