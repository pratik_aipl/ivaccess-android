package com.ivaccessbeta.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ivaccessbeta.App;


/**
 * Created by empiere-php on 11/8/2017.
 */

public  class MySharedPref {
    public App app;
    String isFirst;
    public  static SharedPreferences shared;
    public static SharedPreferences.Editor et;



  public String getFirst_name() {
        return date;
    }

    public String date = "first_name";
    public String nextdate = "date";
    public String password = "pass";


    public static String isLogin="login";


    public static boolean getisLogin() {
        return shared.contains(isLogin) && shared.getBoolean(isLogin, false);

    }

    public static void setisLogin(boolean alarmname) {
        et.putBoolean(isLogin,alarmname);
        et.commit();

    }

    public String getPassword() {
        return shared.contains(password) ? shared.getString(password, "") : "";

    }

    public void setPassword(String passwordname) {
        et.putString(password,passwordname);
        et.commit();
    }


    public String getNextdate() {
        //return nextdate;
        return shared.contains(nextdate) ? shared.getString(nextdate, "") : "";

    }

    public String getdate() {
        return shared.contains(date) ? shared.getString(date, "") : "";
    }

    public void setNextdate(String nextdatename) {
        et.putString(nextdate,nextdatename);
        et.commit();
    }
    public void setdate(String name) {
        et.putString(date, name);
        et.commit();
    }

    public static void MySharedPref(Context ct) {

        shared = ct.getSharedPreferences(App.myPref, 0);
        et = shared.edit();
    }


   public MySharedPref(App app) {
    }

    public String getIsFirstTime() {

        return String.valueOf(shared.contains(isFirst) && shared.getBoolean(isFirst, false));
    }

    public void setIsFirstTime(boolean isfirst) {
        et.putBoolean(isFirst, isfirst);
        et.commit();
    }


    public static String getString(Context context, final String key, final String value) {
        SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
        String data = sh.getString(key, value);
        return data;
    }

    public static void SharedPrefClear(final Context context) {
        SharedPreferences.Editor sha = PreferenceManager.getDefaultSharedPreferences(context).edit();
        sha.clear();
        sha.apply();
        sha.commit();
    }

    public static void setString(Context context, final String key, final String value) {
        SharedPreferences.Editor sha = PreferenceManager.getDefaultSharedPreferences(context).edit();
        sha.putString(key, value);
        sha.commit();
    }
}
