package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 01-03-2018.
 */

@JsonObject
public class PeopleModel implements Serializable {

    @JsonField
   public String id, role_id,first_name,last_name,username,password,email,activated,banned,ban_reason,new_password_key,new_password_requested,
    new_email,new_email_key,last_login,app_token,created,modified,USERDETAILID,UserID,RegionID,QB_Company,MobileNumber,AlternateNumber,
            EstimatedPICCSupplies,EstimatedMidSupplies,Facilities,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,DirectoryNote;

    public ArrayList<FacilityListModel> facilityList = new ArrayList<FacilityListModel>();

    public ArrayList<FacilityListModel> getFacilityList() {
        return facilityList;
    }

    public void setFacilityList(ArrayList<FacilityListModel> facilityList) {
        this.facilityList = facilityList;
    }

    public String getDirectoryNote() {
        return DirectoryNote;
    }

    public void setDirectoryNote(String directoryNote) {
        DirectoryNote = directoryNote;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getBanned() {
        return banned;
    }

    public void setBanned(String banned) {
        this.banned = banned;
    }

    public String getBan_reason() {
        return ban_reason;
    }

    public void setBan_reason(String ban_reason) {
        this.ban_reason = ban_reason;
    }

    public String getNew_password_key() {
        return new_password_key;
    }

    public void setNew_password_key(String new_password_key) {
        this.new_password_key = new_password_key;
    }

    public String getNew_password_requested() {
        return new_password_requested;
    }

    public void setNew_password_requested(String new_password_requested) {
        this.new_password_requested = new_password_requested;
    }

    public String getNew_email() {
        return new_email;
    }

    public void setNew_email(String new_email) {
        this.new_email = new_email;
    }

    public String getNew_email_key() {
        return new_email_key;
    }

    public void setNew_email_key(String new_email_key) {
        this.new_email_key = new_email_key;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    public String getApp_token() {
        return app_token;
    }

    public void setApp_token(String app_token) {
        this.app_token = app_token;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getUSERDETAILID() {
        return USERDETAILID;
    }

    public void setUSERDETAILID(String USERDETAILID) {
        this.USERDETAILID = USERDETAILID;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getRegionID() {
        return RegionID;
    }

    public void setRegionID(String regionID) {
        RegionID = regionID;
    }

    public String getQB_Company() {
        return QB_Company;
    }

    public void setQB_Company(String QB_Company) {
        this.QB_Company = QB_Company;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getAlternateNumber() {
        return AlternateNumber;
    }

    public void setAlternateNumber(String alternateNumber) {
        AlternateNumber = alternateNumber;
    }

    public String getEstimatedPICCSupplies() {
        return EstimatedPICCSupplies;
    }

    public void setEstimatedPICCSupplies(String estimatedPICCSupplies) {
        EstimatedPICCSupplies = estimatedPICCSupplies;
    }

    public String getEstimatedMidSupplies() {
        return EstimatedMidSupplies;
    }

    public void setEstimatedMidSupplies(String estimatedMidSupplies) {
        EstimatedMidSupplies = estimatedMidSupplies;
    }

    public String getFacilities() {
        return Facilities;
    }

    public void setFacilities(String facilities) {
        Facilities = facilities;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
