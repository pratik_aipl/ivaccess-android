package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class SearchData {

    @JsonField
    String JobState;
    @JsonField
    String Regions;
    @JsonField
    String Persons;
    @JsonField
    String Facilities;
    @JsonField
    String Statuses;
    @JsonField
    String ShortBy;



    public SearchData() {
    }

    public SearchData(String jobState, String shortBy, String regions, String persons, String facilities, String statuses) {
        JobState = jobState;
        ShortBy = shortBy;
        Regions = regions;
        Persons = persons;
        Facilities = facilities;
        Statuses = statuses;
    }



    public String getJobState() {
        return JobState;
    }

    public void setJobState(String jobState) {
        JobState = jobState;
    }

    public String getShortBy() {
        return ShortBy;
    }

    public void setShortBy(String shortBy) {
        ShortBy = shortBy;
    }

    public String getRegions() {
        return Regions;
    }

    public void setRegions(String regions) {
        Regions = regions;
    }

    public String getPersons() {
        return Persons;
    }

    public void setPersons(String persons) {
        Persons = persons;
    }

    public String getFacilities() {
        return Facilities;
    }

    public void setFacilities(String facilities) {
        Facilities = facilities;
    }

    public String getStatuses() {
        return Statuses;
    }

    public void setStatuses(String statuses) {
        Statuses = statuses;
    }
}
