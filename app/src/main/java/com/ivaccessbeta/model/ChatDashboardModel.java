package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class ChatDashboardModel implements Serializable {

    @JsonField
    public String ChatID = "", ThreadID = "", GroupID = "", Message = "", SenderID = "", ReceiverID = "", SendingDate = "", ChatStatus = "", ReceiverStatus = "",
            ChatName = "", ProfileImage = "", PenddingView = "";

    public String getChatID() {
        return ChatID;
    }

    public void setChatID(String chatID) {
        ChatID = chatID;
    }

    public String getThreadID() {
        return ThreadID;
    }

    public void setThreadID(String threadID) {
        ThreadID = threadID;
    }

    public String getGroupID() {
        return GroupID;
    }

    public void setGroupID(String groupID) {
        GroupID = groupID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getSendingDate() {
        return SendingDate;
    }

    public void setSendingDate(String sendingDate) {
        SendingDate = sendingDate;
    }

    public String getChatStatus() {
        return ChatStatus;
    }

    public void setChatStatus(String chatStatus) {
        ChatStatus = chatStatus;
    }

    public String getReceiverStatus() {
        return ReceiverStatus;
    }

    public void setReceiverStatus(String receiverStatus) {
        ReceiverStatus = receiverStatus;
    }

    public String getChatName() {
        return ChatName;
    }

    public void setChatName(String chatName) {
        ChatName = chatName;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getPenddingView() {
        return PenddingView;
    }

    public void setPenddingView(String penddingView) {
        PenddingView = penddingView;
    }
}