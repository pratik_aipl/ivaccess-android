package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 11-04-2018.
 */

@JsonObject
public class CheckGroupModel implements Serializable {

    @JsonField
    public String id,fullname,VoucherNo,CheckDate,Amount,CheckStatus,pdf_url,QuickBookSync;

    public String getQuickBookSync() {
        return QuickBookSync;
    }

    public void setQuickBookSync(String quickBookSync) {
        QuickBookSync = quickBookSync;
    }

    public ArrayList<CheckNoteModel> NoteList = new ArrayList<CheckNoteModel>();
    public ArrayList<CheckPaymentNoteModel> NotePaymentList = new ArrayList<CheckPaymentNoteModel>();
    public String getCheckStatus() {
        return CheckStatus;
    }

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    public ArrayList<CheckNoteModel> getNoteList() {
        return NoteList;
    }

    public void setNoteList(ArrayList<CheckNoteModel> noteList) {
        NoteList = noteList;
    }

    public ArrayList<CheckPaymentNoteModel> getNotePaymentList() {
        return NotePaymentList;
    }

    public void setNotePaymentList(ArrayList<CheckPaymentNoteModel> notePaymentList) {
        NotePaymentList = notePaymentList;
    }

    public void setCheckStatus(String checkStatus) {
        CheckStatus = checkStatus;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCheckDate() {
        return CheckDate;
    }

    public void setCheckDate(String checkDate) {
        CheckDate = checkDate;
    }

    public ArrayList<CheckChildModel> childList = new ArrayList<CheckChildModel>();

    public ArrayList<CheckChildModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<CheckChildModel> childList) {
        this.childList = childList;
    }

    public String getVoucherNo() {
        return VoucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        VoucherNo = voucherNo;
    }
}


