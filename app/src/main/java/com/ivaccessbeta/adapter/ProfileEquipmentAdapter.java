package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.model.ProfileEquipmentModel;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 22-03-2018.
 */

public class ProfileEquipmentAdapter extends RecyclerView.Adapter<ProfileEquipmentAdapter.MyViewHolder> {
    public ArrayList<ProfileEquipmentModel> list;
    public List<ProfileEquipmentModel> searchFilterList = new ArrayList<>();
    public Context context;

    @Override
    public ProfileEquipmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_profile_equipment, parent, false);
        return new ProfileEquipmentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProfileEquipmentAdapter.MyViewHolder holder, final int position) {
        ProfileEquipmentModel model = list.get(position);
        holder.name.setText(model.getEquipmentName());
        if (model.getStock() != 0) {
            holder.txtCount.setText("" + model.getStock());
        } else {
            holder.txtCount.setText("");
        }

    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (ProfileEquipmentModel bean : searchFilterList) {
                if (bean.getEquipmentName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ProfileEquipmentAdapter(Context context, ArrayList<ProfileEquipmentModel> featuredJobList) {
        this.list = featuredJobList;
        this.context = context;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, txtCount;
        ImageView  ivEdit;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.txtEquipmentName);
            txtCount = view.findViewById(R.id.txtCount);
        }


    }

}
