package com.ivaccessbeta.fragment.Directory;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.DirectoryTabsPagerAdapter;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.Utils;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DirectoryFragment extends Fragment {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public ImageView ivBack;
    public DirectoryTabsPagerAdapter adapter;
    public TextView txtTitle;
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_directory, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        MainActivity.toolbar_title.setText("DIRECTORY");
        ((MainActivity)getActivity()).showToolbarAction();
        viewPager =view. findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.tabs);
        txtTitle=view.findViewById(R.id.txtTitle);
        ivBack=view.findViewById(R.id.ivBack);

        tabLayout.addTab(tabLayout.newTab().setText("BY REGIONS "));
        tabLayout.addTab(tabLayout.newTab().setText("BY NAME"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        App.checkStateSchedule=true;
        adapter = new DirectoryTabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        MainActivity.toolbar_title.setText("DIRECTORY");
        ((MainActivity)getActivity()).showToolbarAction();
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }
}

