package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 01-03-2018.
 */

@JsonObject
public class FacilitesModel implements Serializable {

    @JsonField
    public String FacilityID, FacilityName, PrimaryRegion, DefaultBillTo, Phone, Address, Address2, City, State, Zip, note, IvAccessSupply;

    public FacilitesModel() {
    }

    public FacilitesModel(String id) {
        this.FacilityID = id;
    }

    public String getIvAccessSupply() {
        return IvAccessSupply;
    }

    public void setIvAccessSupply(String ivAccessSupply) {
        IvAccessSupply = ivAccessSupply;
    }

    public String getFacilityID() {
        return FacilityID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setFacilityID(String facilityID) {
        FacilityID = facilityID;
    }

    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }

    public String getPrimaryRegion() {
        return PrimaryRegion;
    }

    public void setPrimaryRegion(String primaryRegion) {
        PrimaryRegion = primaryRegion;
    }

    public String getDefaultBillTo() {
        return DefaultBillTo;
    }

    public void setDefaultBillTo(String defaultBillTo) {
        DefaultBillTo = defaultBillTo;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }
}
