package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 01-03-2018.
 */
@JsonObject
public class ServieTypeModel implements Serializable{

    @JsonField
    public String ServiceID,ServiceName,EquipmentID,ServiceRate;

    public String getServiceID() {
        return ServiceID;
    }

    public void setServiceID(String serviceID) {
        ServiceID = serviceID;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getEquipmentID() {
        return EquipmentID;
    }

    public void setEquipmentID(String equipmentID) {
        EquipmentID = equipmentID;
    }

    public String getServiceRate() {
        return ServiceRate;
    }

    public void setServiceRate(String serviceRate) {
        ServiceRate = serviceRate;
    }
}
