package com.ivaccessbeta.model;

import android.content.ClipData;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 13-04-2018.
 */
@JsonObject
public class NotifyModel {

    @JsonField
    public String id,FullName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

}
