package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 11-04-2018.
 */
@JsonObject
public class CheckChildModel implements Serializable {

    @JsonField
    public String id,CheckID,QuickBookSync,JobID,CheckAmount,CheckDate,CompletedDate,CreatedBy,CreatedOn,JobNo,PatientName,ServiceName,Note,CheckStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckStatus() {
        return CheckStatus;
    }

    public String getQuickBookSync() {
        return QuickBookSync;
    }

    public void setQuickBookSync(String quickBookSync) {
        QuickBookSync = quickBookSync;
    }

    public void setCheckStatus(String checkStatus) {
        CheckStatus = checkStatus;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getCheckID() {
        return CheckID;
    }

    public void setCheckID(String checkID) {
        CheckID = checkID;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getCheckAmount() {
        return CheckAmount;
    }

    public void setCheckAmount(String checkAmount) {
        CheckAmount = checkAmount;
    }

    public String getCheckDate() {
        return CheckDate;
    }

    public void setCheckDate(String checkDate) {
        CheckDate = checkDate;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getJobNo() {
        return JobNo;
    }

    public void setJobNo(String jobNo) {
        JobNo = jobNo;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getCompletedDate() {
        return CompletedDate;
    }

    public void setCompletedDate(String completedDate) {
        CompletedDate = completedDate;
    }
}
