package com.ivaccessbeta.model;

/**
 * Created by User on 06-02-2018.
 */

public class DrawerModel {
    int icon;
    String name;

    public DrawerModel( String name) {
        this.icon = icon;
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
