package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.BillToModel;
import com.ivaccessbeta.model.BillToServiceModel;
import com.ivaccessbeta.App;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by User on 26-04-2018.
 */

public class BillToServicesAdapter extends RecyclerView.Adapter<BillToServicesAdapter.MyViewHolder> {

    public ArrayList<BillToServiceModel> list = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;
    public BillToModel model;
    public RecyclerView recyclerView;
    BillToServiceModel billToServiceModel;

    @Override
    public BillToServicesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_billto_service, parent, false);
        return new BillToServicesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BillToServicesAdapter.MyViewHolder holder, final int position) {

        holder.txtServiceTitle.setText(list.get(position).getServiceName() + "");
        holder.edtService.setText(list.get(position).getRate());
    }

    public void getServiceRate() {
        billToServiceModel = new BillToServiceModel();
        String rate;
        JSONObject obj = null;
        JSONArray jsonArray = null;
        int childCount = recyclerView.getChildCount();

        jsonArray = new JSONArray();
        for (int j = 0; j < childCount; j++) {
            recyclerView.getLayoutManager().scrollToPosition(j);
            try {
                if (recyclerView.findViewHolderForLayoutPosition(j) instanceof BillToServicesAdapter.MyViewHolder) {
                    BillToServicesAdapter.MyViewHolder childHolder = (BillToServicesAdapter.MyViewHolder) recyclerView.findViewHolderForLayoutPosition(j);
                    rate = childHolder.edtService.getText().toString();
                    obj = new JSONObject();
                    try {
                        obj.put(list.get(j).getServiceID(), rate);
                        jsonArray.put(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        App.billToSreviceRate = jsonArray.toString();
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public BillToServicesAdapter(ArrayList<BillToServiceModel> List, RecyclerView recyclerView) {
        this.list = List;
        this.recyclerView = recyclerView;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtServiceTitle, edtService;

        public MyViewHolder(View view) {
            super(view);
            txtServiceTitle = view.findViewById(R.id.txtServiceTitle);
            edtService = view.findViewById(R.id.edtService);
        }


    }

}
