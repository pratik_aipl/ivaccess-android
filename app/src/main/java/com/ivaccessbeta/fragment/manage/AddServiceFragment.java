package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.EquipmentsModel;
import com.ivaccessbeta.model.ServieTypeModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddServiceFragment extends Fragment implements AsynchTaskListner {
    public EditText edtServicetName;
    public View view;
    public Button btnSave, btnCancel;
    public String serviceName;
    public AddServiceFragment instance;
    public String emailId, token, userId, serviceId, equipmentId = null, serviceRate;
    public int value;
    public Spinner spEquipment;
    public ArrayList<EquipmentsModel> equipmentList = new ArrayList<>();
    public ArrayList<String> equipmentStringList = new ArrayList<String>();
    public ArrayAdapter aa;
    public int equipmentPos = 0;
    public EditText edtServicetRate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add, container, false);
        MainActivity.toolbar_title.setText("Add Service Type");
        ((MainActivity) getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        instance = this;
        setHasOptionsMenu(true);
        value = getArguments().getInt("value");
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        edtServicetName = view.findViewById(R.id.edtServicetName);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        spEquipment = view.findViewById(R.id.spEquipment);
        edtServicetRate = view.findViewById(R.id.edtServicetRate);

        if (value == 1) {
            MainActivity.toolbar_title.setText("Update Service Type");
            ServieTypeModel servieTypeModel = (ServieTypeModel) getArguments().getSerializable("ServiceModel");
            serviceName = servieTypeModel.getServiceName();
            serviceId = servieTypeModel.getServiceID();
            equipmentId = servieTypeModel.getEquipmentID();
            serviceRate = servieTypeModel.getServiceRate();
            edtServicetName.setText(serviceName);
            edtServicetRate.setText(serviceRate);
        }
        getEquipmentsDetail();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceName = edtServicetName.getText().toString();
                serviceRate = edtServicetRate.getText().toString();
                if (TextUtils.isEmpty(serviceName)) {
                    Utils.showToast("Please Enter Service Name", getContext());
                } else if (TextUtils.isEmpty(serviceRate)) {
                    Utils.showToast("Please Enter Service Rate", getContext());
                } else if (TextUtils.isEmpty(equipmentId)) {
                    Utils.showToast("Please Select Equipment.", getContext());
                } else {
                    if (value == 0) {
                        new CallRequest(instance).addService(emailId, token, serviceName, userId, equipmentId, serviceRate);
                    } else if (value == 1) {
                        new CallRequest(instance).updateService(emailId, token, serviceName, userId, equipmentId, serviceId, serviceRate);
                    }
                }
            }
        });
        spEquipment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.disableKeyboard(getContext(), view);
                try {
                    Utils.disableKeyboard(getContext(), view);
                    equipmentId = equipmentList.get(position - 1).getEquimentID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                equipmentId = null;
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    public void getEquipmentsDetail() {
        new CallRequest(instance).getEquipment(emailId, token);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addService:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Service Add Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateService:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Service Update Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getEquipment:
                    Utils.hideProgressDialog();
                    try {
                        equipmentList.clear();
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                EquipmentsModel equipmentsModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),EquipmentsModel.class);
                                equipmentList.add(equipmentsModel);
                                equipmentStringList.add(equipmentList.get(i).getEquipmentName());
                                if (equipmentId != null) {
                                    if (equipmentId.equalsIgnoreCase(equipmentsModel.getEquimentID())) {
                                        equipmentPos = i + 1;
                                    }
                                }
                            }
                            aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, equipmentStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spEquipment.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.euipment_spinner_row_nothing_selected,
                                    getActivity()));
                            spEquipment.setSelection(equipmentPos);

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
