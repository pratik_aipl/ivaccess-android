package com.ivaccessbeta.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.ContractorAdapter;
import com.ivaccessbeta.fragment.profile.MyProfileFragment;
import com.ivaccessbeta.listners.ContractorSpinnerListener;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.ItemOffsetDecoration;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class ContractorListFragment extends Fragment implements AsynchTaskListner, ContractorSpinnerListener {
    public View view;
    public ContractorListFragment instance;
    public static TextView txtTitle;
    public ArrayList<ContractorModel> list = new ArrayList<>();
    public ContractorModel contractorModel;
    public RecyclerView rvSchDetail;

    public static String emailId, token, userId, roleId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_schedule, container, false);
        instance = this;
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        //txtTitle.setText("ContractorList");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        MainActivity.toolbar_title.setText("ContractorList");
        ((MainActivity) getActivity()).showToolbarAction();
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        rvSchDetail = view.findViewById(R.id.rvSchDetail);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen.margin_10);

        rvSchDetail.addItemDecoration(new ItemOffsetDecoration(spacing));
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        Utils.allMenuHide(menu);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        new CallRequest(instance).getConractorActivity(emailId, token, "");
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContractorActivity:
                    Utils.hideProgressDialog();

                    try {
                        list.clear();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            list.addAll(LoganSquare.parseList(jObject.toString(), ContractorModel.class));

                            rvSchDetail.setHasFixedSize(false);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            rvSchDetail.setLayoutManager(layoutManager);
                            rvSchDetail.setAdapter(new ContractorAdapter(list, instance));

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    break;

            }
        }

    }

    @Override
    public void viewSchedule(View v, int position) {
        MyProfileFragment myProfileFragment = new MyProfileFragment();
        Bundle b = new Bundle();
        b.putInt("value", 1);
        b.putString("contractorId", list.get(position).getId());
        // b.putSerializable("ContractorModel", contractorModel);
        myProfileFragment.setArguments(b);
        ((MainActivity)getActivity()).changeFragment(myProfileFragment, true);
        // MainActivity.changeFragment(new MyProfileFragment(), true);
    }
}
