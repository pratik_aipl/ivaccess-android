package com.ivaccessbeta.fragment.Directory;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 17-04-2018.
 */

public class ByNameAdapter extends RecyclerView.Adapter<ByNameAdapter.MyViewHolder> {

    public ArrayList<ByNameModel> list;
    public Context context;
    public   String emailId,token,userId,roleId;
    public List<ByNameModel> searchFilterList=new ArrayList<>();
    @Override
    public ByNameAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_byname, parent, false);

        return new ByNameAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ByNameAdapter.MyViewHolder holder, final int position) {
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(context, App.ROLEID, "");
        ByNameModel model = list.get(position);
        holder.name.setText(model.getFullName());
        holder.email.setText(model.getEmail());
        holder.moNo.setText(model.getMobileNumber());
        holder.moNo2.setText(model.getAlternateNumber());
        holder.note.setText(model.getDirectoryNote());
        if(model.getRole_id().equals(App.ADMIN_ROLE_ID)){
            holder.email.setVisibility(View.VISIBLE);
        }else {
            holder.email.setVisibility(View.GONE);
        }
    }
    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (ByNameModel bean : searchFilterList) {
                if (bean.getFullName().toLowerCase(Locale.getDefault()  )
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ByNameAdapter(Context context, ArrayList<ByNameModel> List) {
        this.context = context;
        this.list = List;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,email,moNo,note,moNo2;
        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.txtName);
            email = view.findViewById(R.id.txtEmail);
            moNo = view.findViewById(R.id.txtMoNo);
            moNo2 = view.findViewById(R.id.txtMoNo2);
            note = view.findViewById(R.id.txtNote);

        }
    }

}
