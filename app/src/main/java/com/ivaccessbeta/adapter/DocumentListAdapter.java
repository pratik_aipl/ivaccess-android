package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.listners.DocumentViewClickListener;
import com.ivaccessbeta.model.JobDetailsModel;
import com.ivaccessbeta.utils.Constant;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.MyViewHolder> {

    public ArrayList<JobDetailsModel> list;
    public List<JobDetailsModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;
    public View itemView;
    public DocumentViewClickListener itemListener;
    public int groupPos, childPos,position;

    @Override
    public DocumentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.document_listitem, parent, false);
        return new DocumentListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DocumentListAdapter.MyViewHolder holder, final int position) {
        String imageURL = "http://ivaccess.blenzabi.com/" + list.get(position).getDocumentPath() + list.get(position).getDocumentName();

        if (imageURL != null) {
            Picasso.with(context).load(imageURL)
                    .placeholder(R.drawable.nopic)
                    .error(R.drawable.nopic)
//                    .rotate(270)
                    .into(holder.imageDoc);
        }
        if (list.get(position).getDocumentStatus().equals(Constant.DOCUMENT_APPROVE)) {
            holder.ivApprove.setVisibility(View.VISIBLE);
        } else if (list.get(position).getDocumentStatus().equals(Constant.DOCUMENT_DENY)) {
            holder.ivDeny.setVisibility(View.VISIBLE);
        }
        if (list.get(position).getIsPrint().equals(Constant.DOCUMENT_ISPRINT)) {
            holder.ivPrint.setVisibility(View.VISIBLE);
        }
        holder.imageDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemListener.recyclerViewListClicked(v, position, groupPos, childPos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public DocumentListAdapter(ArrayList<JobDetailsModel> List, HomeJobDetailAdapter fragment,  Context context, int groupPos) {
        this.list = List;
        this.searchFilterList.addAll(list);
        this.itemListener = fragment;
        this.context = context;
        this.groupPos = groupPos;
    }

    public DocumentListAdapter(ArrayList<JobDetailsModel> List, JobDetailFragment fragment) {
        this.list = List;
        this.searchFilterList.addAll(list);
        this.itemListener = fragment;
        this.context = fragment.getActivity();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imageDoc, ivApprove, ivDeny, ivPrint;

        public MyViewHolder(View view) {
            super(view);
            imageDoc = view.findViewById(R.id.ivDocImage);
            ivApprove = view.findViewById(R.id.ivApprove);
            ivDeny = view.findViewById(R.id.ivDeny);
            ivPrint = view.findViewById(R.id.ivPrint);
        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewListClicked(v, this.getPosition(), groupPos, childPos);
        }
    }
}
