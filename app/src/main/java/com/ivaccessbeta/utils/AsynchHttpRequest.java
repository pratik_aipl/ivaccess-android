package com.ivaccessbeta.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class AsynchHttpRequest {
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public Constant.POST_TYPE post_type;
    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

    public AsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showAlert("No Internet, Please try again later", ft.getActivity());
        }
    }

    public Context ct;


    public AsynchHttpRequest(Context ct, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ct;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ct;
        this.post_type = post_type;
        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ct);
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showAlert("No Internet, Please try again later", ct);
        }
    }

    class MyRequest extends AsyncTask<Map<String, String>, Void, String> {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));
            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        map[0].remove("url");
                        List<String> values = new ArrayList<String>(map[0].values());
                        List<String> keys = new ArrayList<String>(map[0].keySet());
                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();
                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }
                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);
                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        System.out.println("Responce" + "====>" + responseBody);
                        return responseBody;

                    case POST_WITH_JSON:
                        String result = "";
                        httpClient = new DefaultHttpClient();
                        HttpPost httpPost = new HttpPost(map[0].get("url"));
                        nameValuePairs = new ArrayList<NameValuePair>();

                        nameValuePairs.add(new BasicNameValuePair("userid", map[0].get("userid")));
                        nameValuePairs.add(new BasicNameValuePair("flexid", map[0].get("flexid")));
                        nameValuePairs.add(new BasicNameValuePair("joindate", map[0].get("joindate")));
                        nameValuePairs.add(new BasicNameValuePair("flexamt", map[0].get("flexamt")));
                        nameValuePairs.add(new BasicNameValuePair("qty", map[0].get("qty")));
                        nameValuePairs.add(new BasicNameValuePair("userpaymentdtlid", map[0].get("userpaymentdtlid")));
                        nameValuePairs.add(new BasicNameValuePair("pay_type", map[0].get("pay_type")));
                        nameValuePairs.add(new BasicNameValuePair("cardno", map[0].get("cardno")));
                        nameValuePairs.add(new BasicNameValuePair("ex_date", map[0].get("ex_date")));
                        nameValuePairs.add(new BasicNameValuePair("que_info", map[0].get("que_info")));
                        nameValuePairs.add(new BasicNameValuePair("cvv", map[0].get("cvv")));
                        nameValuePairs.add(new BasicNameValuePair("flexname", map[0].get("flexname")));
                        nameValuePairs.add(new BasicNameValuePair("new_amount", map[0].get("new_amount")));

                        nameValuePairs.add(new BasicNameValuePair("data", map[0].get("json")));

                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                        httpResponse = httpClient.execute(httpPost);
                        httpEntity = httpResponse.getEntity();
                        result = EntityUtils.toString(httpEntity, "UTF-8");
                        System.out.println("Responce ::" + result);

                        return result;

                    case POST:
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        HttpPost postRequest = new HttpPost(map[0].get("url"));
                        DefaultHttpClient httpclient = new DefaultHttpClient();
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        for (String key : map[0].keySet()) {
                            System.out.println(key + "====>" + map[0].get(key));
                            nameValuePairs.add(new BasicNameValuePair(key, map[0].get(key)));
                        }
                        postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                        System.out.println("Final URI::" + postRequest.getEntity().toString());
                        HttpResponse response = httpclient.execute(postRequest);
                        httpEntity = response.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        return responseBody;
                    case POST_WITH_IMAGE:
                        httpClient = new DefaultHttpClient();
                        postRequest = new HttpPost(map[0].get("url"));
                        reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });
                        BasicHttpContext localContext = new BasicHttpContext();
                        for (String key : map[0].keySet()) {
                            System.out.println();
                            System.out.println(key + "====>" + map[0].get(key));
                            if (matchKeysForImages(key)) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "image/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart(key, fbody);
                                    }
                                }
                            } else if (matchKeyForDocs(key)) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "application/pdf";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart(key, fbody);
                                    }
                                }
                            } else {
                                if (map[0].get(key) == null) {
                                    reqEntity.addPart(key, new StringBody(""));
                                } else {
                                    reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                }
                            }
                        }
                        postRequest.setEntity(reqEntity);
                        HttpResponse responses = httpClient.execute(postRequest, localContext);
                        BufferedReader reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(),
                                StandardCharsets.UTF_8));
                        String sResponse;
                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }
                        System.out.println("Responce ::" + responseBody);
                        return responseBody;

                    case POST_WITH_IMAGE_MULTIPAL:
                        httpClient = new DefaultHttpClient();
                        postRequest = new HttpPost(map[0].get("url"));
                        reqEntity = new MyCustomMultiPartEntity(new MyCustomMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                            }
                        });
                        localContext = new BasicHttpContext();
                        for (String key : map[0].keySet()) {
                            System.out.println();
                            System.out.println(key + "====>" + map[0].get(key));
                            if (matchKeysForImages(key)) {

                                if (key.equals("document_file")) {
                                    if (map[0].get(key) != null) {
                                        if (map[0].get(key).length() > 1) {
                                            String images = map[0].get(key);
                                            if (images.contains(",")) {
                                                ArrayList<String> aList = new ArrayList(Arrays.asList(images.split(",")));
                                                for (int i = 0; i < aList.size(); i++) {

                                                    String type = "image/png";
                                                    File f = new File(aList.get(i));
                                                    FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                                    System.out.println(key + "====>" + map[0].get(key));
                                                    reqEntity.addPart("document_file[" + i + "]", fbody);
                                                }
                                            } else {
                                                String type = "image/png";
                                                File f = new File(map[0].get(key));
                                                FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                                reqEntity.addPart("document_file[0]", fbody);
                                            }

                                        }
                                    }
                                } else if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "image/png";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart(key, fbody);
                                    }
                                }
                            } else if (matchKeyForDocs(key)) {
                                if (map[0].get(key) != null) {
                                    if (map[0].get(key).length() > 1) {
                                        String type = "application/pdf";
                                        File f = new File(map[0].get(key));
                                        FileBody fbody = new FileBody(f, f.getName(), type, "UTF-8");
                                        reqEntity.addPart(key, fbody);
                                    }
                                }
                            } else {
                                if (map[0].get(key) == null) {
                                    reqEntity.addPart(key, new StringBody(""));
                                } else {
                                    reqEntity.addPart(key, new StringBody(map[0].get(key)));
                                }
                            }
                        }
                        postRequest.setEntity(reqEntity);
                        responses = httpClient.execute(postRequest, localContext);
                        reader = new BufferedReader(new InputStreamReader(responses.getEntity().getContent(), StandardCharsets.UTF_8));
                        while ((sResponse = reader.readLine()) != null) {
                            responseBody = responseBody + sResponse;
                        }
                        System.out.println("Responce ::" + responseBody);
                        return responseBody;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            aListner.onTaskCompleted(result, request);
            super.onPostExecute(result);
        }


        public boolean matchKeysForImages(String key) {
      /*  profile_picture_1
                profile_picture_2
        profile_picture_3
                profile_picture_4
        profile_picture_5*/

            return key.equalsIgnoreCase("profile_picture") ||
                    key.equalsIgnoreCase("profile_picture_2") ||
                    key.equalsIgnoreCase("profile_picture_3") ||
                    key.equalsIgnoreCase("profile_picture_4") ||
                    key.equalsIgnoreCase("profile_picture_5") ||
                    key.equalsIgnoreCase("profile_picture") ||
                    key.equalsIgnoreCase("user_image") ||
                    key.equalsIgnoreCase("event_image") ||
                    key.equalsIgnoreCase("critiques_image") ||
                    key.equalsIgnoreCase("classified_image") ||
                    key.equalsIgnoreCase("lerning_image") ||
                    key.equalsIgnoreCase("video") ||
                    key.equalsIgnoreCase("fund_image") ||
                    key.equalsIgnoreCase("project_image") ||
                    key.equalsIgnoreCase("company_image") ||
                    key.equalsIgnoreCase("company_image") ||
                    key.equalsIgnoreCase("candidate_image") ||
                    key.equalsIgnoreCase("profile_img") ||
                    key.contains("document_file") ||


                    key.equalsIgnoreCase("photo_id") || key.equalsIgnoreCase("profile_pic");
        }
    }

    public boolean matchKeyForDocs(String key) {
      /*  profile_picture_1
                profile_picture_2
        profile_picture_3
                profile_picture_4
        profile_picture_5*/

        return key.equalsIgnoreCase("job_doc") ||
                key.equalsIgnoreCase("profile_picture_2") ||
                key.equalsIgnoreCase("profile_picture_3") ||
                key.equalsIgnoreCase("profile_picture_4") ||
                key.equalsIgnoreCase("profile_picture_5") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("user_image") ||
                key.equalsIgnoreCase("event_image") ||
                key.equalsIgnoreCase("critiques_image") ||
                key.equalsIgnoreCase("classified_image") ||
                key.equalsIgnoreCase("lerning_image") ||
                key.equalsIgnoreCase("video") ||
                key.equalsIgnoreCase("fund_image") ||
                key.equalsIgnoreCase("project_image") ||
                key.equalsIgnoreCase("company_image") ||

                key.equalsIgnoreCase("candidate_image") ||
                key.equalsIgnoreCase("candidate_image") ||


                key.equalsIgnoreCase("photo_id") || key.equalsIgnoreCase("profile_pic");
    }
}
