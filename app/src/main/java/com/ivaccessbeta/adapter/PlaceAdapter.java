package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.manage.AddPlacefragment;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.ivaccessbeta.fragment.manage.PlaceFragment.instance;

/**
 * Created by User on 27-03-2018.
 */

public class PlaceAdapter  extends RecyclerView.Adapter<PlaceAdapter.MyViewHolder> {

    public ArrayList<PlaceModel> list;
    public List<PlaceModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId,token,userId;
    public PlaceModel model ;
    @Override
    public PlaceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_region, parent, false);

        return new PlaceAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlaceAdapter.MyViewHolder holder, final int position) {

       holder.name.setText(list.get(position).getPlacesName());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailId= MySharedPref.getString(context, App.EMAIL,"");
                token=MySharedPref.getString(context, App.APP_TOKEN,"");
                userId=MySharedPref.getString(context, App.LOGGED_USER_ID,"");
                doDelete(position);
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AddPlacefragment addPlacefragment=new AddPlacefragment();
                Bundle args = new Bundle();
                args.putInt("value", 1);
                args.putSerializable("PlaceModel",list.get(position));
                addPlacefragment.setArguments(args);
                ((MainActivity)context).changeFragment(addPlacefragment,true);
            }
        });

    }
    private void doDelete(final int position) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deletePlace(emailId,token,userId,list.get(position).getPlacesID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }
    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (PlaceModel bean : searchFilterList) {
                if (bean.getPlacesName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public PlaceAdapter(Context context, ArrayList<PlaceModel> List) {
        this.context = context;
        this.list = List;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView ivDelete,ivEdit;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.txtName);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivEdit=view.findViewById(R.id.ivEdit);
        }


    }

}
