package com.ivaccessbeta.fragment.notify;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ivaccessbeta.activity.MainActivity.drawer;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNotifyFragment extends Fragment implements AsynchTaskListner{

    public EditText edtMessage;
    public Button btnSend,btnCancel;
    public String message;
    public AddNotifyFragment instance;
    public String emailId, token, userId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view;
        view= inflater.inflate(R.layout.fragment_add_notify, container, false);
        instance=this;
        setHasOptionsMenu(true);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        edtMessage=view.findViewById(R.id.edtMessage);
        btnSend=view.findViewById(R.id.btnSend);
        btnCancel=view.findViewById(R.id.btnCancel);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               message= edtMessage.getText().toString();
                if(message.equals("")){
                    Utils.showToast("Please Enter Message",getContext());
                }
                else {
                    sendNotification();
                }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        MainActivity.toolbar_title.setText("NOTIFY");
        ((MainActivity)getActivity()).showToolbarAction();
        return view;
    }

    private void sendNotification() {
        new CallRequest(instance).sendNotify(emailId,token,userId,message,App.notifyPersonId);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);


    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {

                case sendNotify:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"),getActivity());
                            App.notifyPersonId="";
                            getActivity().onBackPressed();
                            getActivity().onBackPressed();

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();

                    break;


            }
        }
    }
}
