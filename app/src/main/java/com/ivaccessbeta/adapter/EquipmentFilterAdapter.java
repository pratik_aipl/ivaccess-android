package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.ContractorModel;

import java.util.ArrayList;

/**
 * Created by User on 14-04-2018.
 */

//public class EquipmentFilterAdapter extends RecyclerView.Adapter<EquipmentFilterAdapter.MyViewHolder> {
//
//    public ArrayList<ContractorModel> list;
//    public Context context;
//    public String emailId, token, userId;
//    public JobDetailsModel documentListModel;
//    public View itemView;
//
//    public ArrayList<Integer> checkedArray = new ArrayList<>();
//    @Override
//    public EquipmentFilterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.layout_select_contractors, parent, false);
//
//
//        return new EquipmentFilterAdapter.MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(EquipmentFilterAdapter.MyViewHolder holder, final int position) {
//        holder.cbContractor.setText(list.get(position).getFullName());
//
//        holder.cbContractor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
////                App.indexContractorList.clear();
////                String id =list.get(position).getId();
//                if (isChecked) {
////                    if(!App.indexContractorList.contains(id)){
////                        App.indexContractorList.add(id);
////                    }
////                    if(!checkedArray.contains(position)){
////                        checkedArray.add(position);
////                    }
////                } else  {
////                    if(App.indexContractorList.contains(id)){
////                        App.indexContractorList.remove(id);
////                    }
////                    try{
////                        if(checkedArray.contains(position)){
////                            checkedArray.remove(position);
////                        }
////                    }catch (IndexOutOfBoundsException e){
////                        e.printStackTrace();
////                    }
////                }
//            }
//        });
//    }
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//    public EquipmentFilterAdapter(ArrayList<ContractorModel> List) {
//        this.list = List;
//
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//
//        public RadioButton cbContractor;
//
//        public MyViewHolder(View view) {
//            super(view);
//            cbContractor = view.findViewById(R.id.checkBox);
//        }
//
//
//    }
//
//}
public class EquipmentFilterAdapter extends ArrayAdapter<ContractorModel> {

    public ArrayList<ContractorModel> list = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;
    public ContractorModel listModel;

    public EquipmentFilterAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ContractorModel> list) {
        super(context, resource, list);
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = infalInflater.inflate(R.layout.layout_select_contractors, parent, false);
        RadioButton radioButton = view.findViewById(R.id.checkBox);

        radioButton.setText( list.get(position).getFullName());

        return view;
    }
}