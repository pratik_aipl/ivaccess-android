package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.ContractorListFragment;
import com.ivaccessbeta.listners.ContractorSpinnerListener;
import com.ivaccessbeta.model.ContractorModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 01-03-2018.
 */

public class ContractorAdapter extends RecyclerView.Adapter<ContractorAdapter.MyViewHolder> {

    public ArrayList<ContractorModel> list;

    public List<ContractorModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;
    public ContractorModel model;
    private static ContractorSpinnerListener itemListener;

    @Override
    public ContractorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_contactor_item, parent, false);
        return new ContractorAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContractorAdapter.MyViewHolder holder, final int position) {

        holder.txtContractorName.setText(list.get(position).getFullName());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemListener.viewSchedule(v, position);
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public ContractorAdapter(ArrayList<ContractorModel> List, ContractorListFragment context) {
        this.list = List;
        itemListener = context;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtContractorName, txtViewSchedule;

        public MyViewHolder(View view) {
            super(view);

            txtContractorName = view.findViewById(R.id.txtContractorName);

        }


    }

}
