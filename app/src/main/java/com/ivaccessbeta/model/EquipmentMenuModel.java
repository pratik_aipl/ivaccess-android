package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 14-04-2018.
 */

@JsonObject
public class EquipmentMenuModel implements Serializable {

    @JsonField
    public String EquimentOrderID,EquimentID,ContractorID,Quantity,Status,Notes,CreatedBy,CreatedOn,FullName,
            EquipmentOrderStatusName,EquipmentOrderStatusColor,EquipmentName,EquipmentCode,Total;

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getEquipmentCode() {
        return EquipmentCode;
    }

    public void setEquipmentCode(String equipmentCode) {
        EquipmentCode = equipmentCode;
    }

    public String getEquipmentName() {
        return EquipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        EquipmentName = equipmentName;
    }

    public String getEquipmentOrderStatusColor() {
        return EquipmentOrderStatusColor;
    }

    public void setEquipmentOrderStatusColor(String equipmentOrderStatusColor) {
        EquipmentOrderStatusColor = equipmentOrderStatusColor;
    }

    public String getEquipmentOrderStatusName() {
        return EquipmentOrderStatusName;
    }

    public void setEquipmentOrderStatusName(String equipmentOrderStatusName) {
        EquipmentOrderStatusName = equipmentOrderStatusName;
    }

    public String getEquimentOrderID() {
        return EquimentOrderID;
    }

    public void setEquimentOrderID(String equimentOrderID) {
        EquimentOrderID = equimentOrderID;
    }

    public String getEquimentID() {
        return EquimentID;
    }

    public void setEquimentID(String equimentID) {
        EquimentID = equimentID;
    }

    public String getContractorID() {
        return ContractorID;
    }

    public void setContractorID(String contractorID) {
        ContractorID = contractorID;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }
}
