package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.BillToServicesAdapter;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.BillToModel;
import com.ivaccessbeta.model.BillToServiceModel;
import com.ivaccessbeta.model.QBModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

public class AddBillToFragment extends Fragment implements AsynchTaskListner {
    private static final String TAG = "AddBillToFragment";
    public EditText edtBillToName, edtPhone, edtAddress, edtAddress2, edtCity, edtState, edtZip, edtCustomInvoiceInfi;
    public Spinner spQBCompany;
    public Button btnSave, btnCancel;
    public String qb, emailId, token, userId, billToName, phone, address, address2, city, zip, state, invoiceInfo, billId, qbCompany;
    // piccRate, midlineRate, cashfloRate, pivwutRate,
//            consultRate, portAccessRate, educationRate, powerGlidRate, pivRate, picc2, picc3, dcRate, billId, qbCompany;
    public AddBillToFragment instance;
    public RegionModel regionModel;
    public int value;
    public RecyclerView rvServices;
    public ArrayList<BillToServiceModel> serviceList = new ArrayList<>();
    public ArrayList<QBModel> qbList = new ArrayList<>();
    public ArrayList<BillToServiceModel> serviceEditList = new ArrayList<BillToServiceModel>();
    public BillToServicesAdapter mAdapter;
    public BillToModel billToModel;
    public BillToServiceModel billToServiceModel2;
    public ArrayList<String> qbStringList = new ArrayList<>();
    public int qbPos;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_bill_to, container, false);
        MainActivity.toolbar_title.setText("Add Bill To");
        ((MainActivity)getActivity()).showToolbarAction();
        instance = this;

        setHasOptionsMenu(true);
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        linearJobs.setVisibility(View.GONE);
        edtBillToName = view.findViewById(R.id.edtBillName);
        spQBCompany = view.findViewById(R.id.spinnerQbCompany);
        edtPhone = view.findViewById(R.id.edtPhone);
        edtAddress = view.findViewById(R.id.edtAddress);
        edtAddress2 = view.findViewById(R.id.edtAddress2);
        edtCity = view.findViewById(R.id.edtCity);
        edtZip = view.findViewById(R.id.edtZip);
        edtState = view.findViewById(R.id.edtState);
        edtCustomInvoiceInfi = view.findViewById(R.id.edtCustomInvoiceInfi);
        rvServices = view.findViewById(R.id.rvServices);

        new CallRequest(instance).qbCompany(emailId,token);

        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);


        value = getArguments().getInt("value");
        if (value == 1) {
            MainActivity.toolbar_title.setText("Update BillTo");
            billToModel = (BillToModel)getArguments().getSerializable("BillToModel");
         //   billToServiceModel2=(BillToServiceModel)getArguments().getSerializable("BillToServiceModel");
            billId = billToModel.getBillToID();
            billToName = billToModel.getName();
            qbCompany = billToModel.getQBCompany();
           // primaryRegion = getArguments().getString("regionId");
            phone = billToModel.getPhone();
            address =billToModel.getAddress();
            address2 = billToModel.getAddress2();
            city = billToModel.getCity();
            state = billToModel.getState();
            zip = billToModel.getZip();
//            educationRate = getArguments().getString("educationRate");
            invoiceInfo = billToModel.getCustomInvoiceInfo();
            serviceEditList = billToModel.billToServiceList;
            mAdapter = new BillToServicesAdapter(serviceEditList, rvServices);
            System.out.println("service size=="+ serviceEditList.size());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            rvServices.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            rvServices.setLayoutManager(mLayoutManager);
            rvServices.setItemAnimator(new DefaultItemAnimator());
            rvServices.setAdapter(mAdapter);

            edtBillToName.setText(billToName);
            edtPhone.setText(phone);
            edtAddress.setText(address);
            edtAddress2.setText(address2);
            edtCity.setText(city);
            edtState.setText(state);
            edtZip.setText(zip);
            edtCustomInvoiceInfi.setText(invoiceInfo);
        }else {
            getServiceBillTo();
        }
        spQBCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getActivity(), view);
                try {
                    qb = qbList.get(i - 1).getID();


                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        btnSave.setOnClickListener(view12 -> {
            billToName = edtBillToName.getText().toString().trim();
            phone = edtPhone.getText().toString().trim();
            address = edtAddress.getText().toString().trim();
            address2 = edtAddress2.getText().toString().trim();
            city = edtCity.getText().toString().trim();
            zip = edtZip.getText().toString().trim();
            state = edtState.getText().toString().trim();
            invoiceInfo = edtCustomInvoiceInfi.getText().toString().trim();
            mAdapter.getServiceRate();

            if (billToName.equals("")) {
                Utils.showToast("Enter Bill Name", getContext());
            } else if (phone.equals("")) {
                Utils.showToast("Enter  Number", getContext());
            } else if (address.equals("")) {
                Utils.showToast("Enter  Address", getContext());
            } else if (city.equals("")) {
                Utils.showToast("Enter City", getContext());
            } else if (zip.equals("")) {
                Utils.showToast("Enter Zip Code", getContext());
            } else if (state.equals("")) {
                Utils.showToast("Enter State", getContext());
            }else if (qb.equals("")) {
                Utils.showToast("Please Select QB Company", getActivity());
            }
            else {
                if (value == 0) {
                    new CallRequest(instance).addBillTo(emailId, token, billToName, qb, phone, address, address2, city, state,
                            zip, invoiceInfo, App.billToSreviceRate, userId);
                } else if (value == 1) {
                    new CallRequest(instance).updateBillTo(emailId, token, billToName, qb, phone, address, address2, city, state,
                            zip, invoiceInfo, App.billToSreviceRate, userId, billId);
                }
            }
        });

        btnCancel.setOnClickListener(view1 -> getActivity().onBackPressed());

        return view;
    }

    private void getServiceBillTo() {
        new CallRequest(instance).getBillToServiceList(emailId, token);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addBillTo:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Bill Add Succesfully", getContext());
                            getActivity().onBackPressed();

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateBillTo:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Bill Update Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getBillToServiceList:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            serviceEditList.clear();
                                serviceList.addAll(LoganSquare.parseList(jObject.toString(),BillToServiceModel.class));
                            System.out.println("list size==== " + serviceList.size());
                            mAdapter = new BillToServicesAdapter(serviceList, rvServices);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvServices.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvServices.setLayoutManager(mLayoutManager);
                            rvServices.setItemAnimator(new DefaultItemAnimator());
                            rvServices.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
                case qbCompanyList:
                    qbStringList.clear();
                    qbList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                             QBModel   qbModel = new QBModel();
                                qbModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),QBModel.class);
                                qbList.add(qbModel);
                                qbStringList.add(qbList.get(i).getName());
                                if (qb != null) {
                                    if (qb.equalsIgnoreCase(qbModel.getID())) {
                                        qbPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, qbStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spQBCompany.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.qb_spinner_row_nothing_selected,
                                    getActivity()));
                            spQBCompany.setSelection(qbPos);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}