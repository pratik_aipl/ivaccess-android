package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.ProfileScheduleVerticalAdapter;
import com.ivaccessbeta.model.ProfileScheduleModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.ItemOffsetDecoration;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class ScheduleProfileFragment extends Fragment implements AsynchTaskListner {
    public ArrayList<ProfileScheduleModel> subList = new ArrayList<>();
    public RecyclerView rvSchDetail;
    public ScheduleProfileFragment instance;
    public String emailId, token, userId;

    //    public  JsonParserUniversal jParsar;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_schedule_profile, container, false);
        setHasOptionsMenu(true);

        instance = this;
//        jParsar = new JsonParserUniversal();
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        rvSchDetail = view.findViewById(R.id.rvSchDetail);
        final int spacing = getResources().getDimensionPixelOffset(R.dimen.margin_10);

        rvSchDetail.addItemDecoration(new ItemOffsetDecoration(spacing));

        try {
            if (App.contractorId.equalsIgnoreCase("")) {
                userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
            } else {
                userId = App.contractorId;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return view;
    }

    private void getSchedule() {

        new CallRequest(instance).getProfileSchedule(emailId, token, userId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getSchedule();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getProfileSchedule:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            subList.clear();
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject objDate = jObject.getJSONObject(i);
                                ProfileScheduleModel jobStatusModel = new ProfileScheduleModel();
//                                jobStatusModel = (ProfileScheduleModel) jParsar.parseJson(jObject.getJSONObject(i), new ProfileScheduleModel());
                                jobStatusModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), ProfileScheduleModel.class);// jParsar.parseJson(, new ProfileScheduleModel());


                                JSONArray jArray = objDate.getJSONArray("schedual_list");
                                for (int j = 0; jArray.length() > j; j++) {
                                    ProfileScheduleModel jobStatusModelSchedule = new ProfileScheduleModel();
//                                    jobStatusModelSchedule = (ProfileScheduleModel) jParsar.parseJson(jArray.getJSONObject(j), new ProfileScheduleModel());
                                    jobStatusModel.subArray.add(LoganSquare.parse(jArray.getJSONObject(j).toString(), ProfileScheduleModel.class));

                                }
                                subList.add(jobStatusModel);

                            }
                            rvSchDetail.setHasFixedSize(false);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            rvSchDetail.setLayoutManager(layoutManager);
                            ProfileScheduleVerticalAdapter adapter = new ProfileScheduleVerticalAdapter(getActivity(),subList);
                            rvSchDetail.setAdapter(adapter);


                            //jobStatusAdapter = new JobStatusAdapter(jobStatusList);


                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

            }
        }
    }

}
