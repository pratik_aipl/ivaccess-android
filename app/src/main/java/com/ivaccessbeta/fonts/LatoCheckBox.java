package com.ivaccessbeta.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by empiere-php on 10/31/2017.
 */

@SuppressLint("AppCompatCustomView")
public class LatoCheckBox extends CheckBox {


    public LatoCheckBox(Context context) {
        super(context);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Lato-Medium.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public LatoCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Lato-Medium.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public LatoCheckBox(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Lato-Medium.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}
