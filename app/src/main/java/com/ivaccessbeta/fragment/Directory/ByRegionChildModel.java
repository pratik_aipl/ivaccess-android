package com.ivaccessbeta.fragment.Directory;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 17-04-2018.
 */
@JsonObject
class ByRegionChildModel {
    @JsonField
    public String id,FullName,email,MobileNumber,AlternateNumber,DirectoryNote,role_id;

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getAlternateNumber() {
        return AlternateNumber;
    }

    public void setAlternateNumber(String alternateNumber) {
        AlternateNumber = alternateNumber;
    }

    public String getDirectoryNote() {
        return DirectoryNote;
    }

    public void setDirectoryNote(String directoryNote) {
        DirectoryNote = directoryNote;
    }
}
