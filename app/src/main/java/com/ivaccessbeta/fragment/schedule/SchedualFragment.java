package com.ivaccessbeta.fragment.schedule;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.SchedualTabsPagerAdapter;
import com.ivaccessbeta.fragment.AddPushDeviderFragment;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.model.SchedualDateCalanderModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;


public class SchedualFragment extends Fragment implements AsynchTaskListner {
    public View view;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static SchedualTabsPagerAdapter adapter;
    public static SchedualFragment instance;
    public String userId, roleId;
    public static String emailId, token;
    //    public SchedualDateCalanderModel schedualDateCalanderModel;
    public static Date myDate = null;
    public static int selectedPosition;
    public ContractorModel contractorModel;
    public static String asubstringEnd;

    public PlaceModel placeModel;

    public RegionModel regionModel;
    public ArrayAdapter aaPersons, aaRegion, aaFacility;
    public ArrayList<SchedualDetailFragment> mScheduleFragments;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_schedual, container, false);
        viewPager = view.findViewById(R.id.viewpager);
        linearJobs.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        instance = this;
        MainActivity.toolbar_title.setText("");
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        tabLayout = view.findViewById(R.id.tabs);
        getConractor();
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        return view;
    }

    public static void getSchedualHeader() {
        try {
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
            new CallRequest(instance).getSchedual(emailId, token, App.nextPreDate);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem();
    }

    private void getRegion() {
        try {
            App.regionList.clear();
            App.regionStringList.clear();
            new CallRequest(instance).getRegion(emailId, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPlace() {
        try {
            App.placeList.clear();
            App.placeStringList.clear();
            new CallRequest(instance).getPlace(emailId, token, "");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getConractor() {
        App.personList.clear();
        App.contactorStringList.clear();
        new CallRequest(instance).getConractor(emailId, token, "");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_schedual, menu);  // Use filter.xml from step 1
        menu.findItem(R.id.refrash).setVisible(false);
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            menu.findItem(R.id.addPushDevider).setVisible(false);
        }
        if (roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            menu.findItem(R.id.addPushDevider).setVisible(false);
            menu.findItem(R.id.addSchedual).setVisible(false);
        }
        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            menu.findItem(R.id.addPushDevider).setVisible(false);
            menu.findItem(R.id.addSchedual).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.addSchedual) {

            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                ((MainActivity)getActivity()).changeFragment(new AddNotAvailableFragment(), true);
            } else {
                MainActivity.toolbar_title.setText("Add Not Available Schedule");
                ((MainActivity) getActivity()).showToolbarAction();
                ((MainActivity)getActivity()).changeFragment(new AddAvailableNotAvailableScheduleFragment(), true);
            }

//            Intent i = new Intent(getContext(), AddScheduleActivity.class);
//            startActivity(i);

            return true;
        }
        if (id == R.id.drawer) {
            drawer.openDrawer(Gravity.RIGHT);
            return true;
        }
        if (id == R.id.refrash) {
            getConractor();
            return true;
        }
        if (id == R.id.addPushDevider) {
            ((MainActivity)getActivity()).changeFragment(new AddPushDeviderFragment(), true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getSchedual:
                    App.ScheduleFragmentList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {

                            if (adapter != null) {
                                tabLayout.removeAllTabs();
                                adapter.notifyDataSetChanged();
                                viewPager.setAdapter(adapter);
                                viewPager.invalidate();
                            }
                            JSONArray jObject = jObj.getJSONArray("data");
                            Date todayDate = Calendar.getInstance().getTime();
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                            String todayDateString = formatter.format(todayDate);
                            int selectedPos = 0;
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject jObjData = jObject.getJSONObject(i);
                                App.ScheduleFragmentList.add(LoganSquare.parse(jObjData.toString(), SchedualDateCalanderModel.class));
                                getDateMMYYFormate(i);
                                if (App.title.equalsIgnoreCase(Constant.SCHEDULE_ADD) || App.title.equalsIgnoreCase(Constant.SCHEDULE_UPDATE) || App.isFromNotificationFragment) {
                                    if (App.ScheduleFragmentList.get(i).getDate().equalsIgnoreCase(App.scheduleDate))
                                        selectedPos = i;
                                } else if (App.ScheduleFragmentList.get(i).getDate().equalsIgnoreCase(todayDateString)) {
                                    selectedPos = i;
                                } else if (App.ScheduleFragmentList.get(i).getDate().equalsIgnoreCase(App.nextPreDate + "-01")) {
                                    selectedPos = i;
                                }
                            }

                            adapter = new SchedualTabsPagerAdapter
                                    (getChildFragmentManager(), tabLayout.getTabCount());
                            viewPager.setAdapter(adapter);
                            viewPager.setCurrentItem(selectedPos);
                            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    viewPager.setCurrentItem(tab.getPosition());
                                    SchedualTabsPagerAdapter adapter = (SchedualTabsPagerAdapter) viewPager.getAdapter();
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }
                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {

                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {

                                }
                            });
                            adapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();

                    break;

                case getContractor:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                contractorModel = new ContractorModel();
                                contractorModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), ContractorModel.class);
                                App.personList.add(contractorModel);
                                App.contactorStringList.add(contractorModel.getFullName());
                            }
                            try {
                                aaPersons = new ArrayAdapter(getActivity(), R.layout.spinner_text, App.contactorStringList);
                                aaPersons.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getPlace();
                    break;
                case getPlace:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                placeModel = LoganSquare.parse(ob.toString(),PlaceModel.class);
                                App.placeList.add(placeModel);
                                App.placeStringList.add(placeModel.getPlacesName());
                            }
                            aaFacility = new ArrayAdapter(getContext(), R.layout.spinner_text, App.placeStringList);
                            aaFacility.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getRegion();

                    break;

                case getRegion:
                    //    Utils.hideProgressDialog();
                    try {

                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                regionModel = LoganSquare.parse(ob.toString(),RegionModel.class);
                                App.regionList.add(regionModel);
                                App.regionStringList.add(regionModel.getRegionName());
                            }
                            try {
                                aaRegion = new ArrayAdapter(getContext(), R.layout.spinner_text, App.regionStringList);
                                aaRegion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Utils.hideProgressDialog();

                    getSchedualHeader();
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    public static void getDateMMYYFormate(int position) {
        String CurrentStringEnd = App.ScheduleFragmentList.get(position).getDate();
        String[] separated = CurrentStringEnd.split("-");
        final String s1End = separated[1];
        String sEndMonth = null;

        if (s1End.equalsIgnoreCase("01")) {
            sEndMonth = "January ";
        } else if (s1End.equalsIgnoreCase("02")) {
            sEndMonth = "February ";
        } else if (s1End.equalsIgnoreCase("03")) {
            sEndMonth = "March ";
        } else if (s1End.equalsIgnoreCase("04")) {
            sEndMonth = "April ";
        } else if (s1End.equalsIgnoreCase("05")) {
            sEndMonth = "May ";
        } else if (s1End.equalsIgnoreCase("06")) {
            sEndMonth = "June ";
        } else if (s1End.equalsIgnoreCase("07")) {
            sEndMonth = "July ";
        } else if (s1End.equalsIgnoreCase("08")) {
            sEndMonth = "August ";
        } else if (s1End.equalsIgnoreCase("09")) {
            sEndMonth = "September ";
        } else if (s1End.equalsIgnoreCase("10")) {
            sEndMonth = "October ";
        } else if (s1End.equalsIgnoreCase("11")) {
            sEndMonth = "November ";
        } else if (s1End.equalsIgnoreCase("12")) {
            sEndMonth = "December ";
        }
        asubstringEnd = CurrentStringEnd.substring(8, 10);

        String date = App.ScheduleFragmentList.get(position).getDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            myDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dayOfTheWeek = (String) DateFormat.format("EEEE", myDate);
        tabLayout.addTab(tabLayout.newTab().setText(asubstringEnd + "-" + firstThree(sEndMonth) + " " + dayOfTheWeek));


    }

    public static String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }
}
