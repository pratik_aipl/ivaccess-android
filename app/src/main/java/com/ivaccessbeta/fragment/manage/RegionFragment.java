package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.RegionAdapter;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class RegionFragment extends Fragment implements AsynchTaskListner {
    public RecyclerView rvRegion;
    public RegionAdapter mAdapter;
    public ArrayList<RegionModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static RegionFragment instance;
    public EditText edtSearch;
    public RegionModel regionModel;
    public String emailId,token;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_region, container, false);
        linearJobs.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        instance = this;

         emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
         token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        relativeAdd = view.findViewById(R.id.relative_add);
        edtSearch = view.findViewById(R.id.edt_search);
        rvRegion = view.findViewById(R.id.rvRegion);

        relativeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddRegionFragment addRegionFragment=new AddRegionFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("value",0);
                addRegionFragment.setArguments(bundle);
                ((MainActivity)getActivity()).changeFragment(addRegionFragment, true);
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }
            }
        });
        return view;
    }

    private void getRegionDetail() {
        list.clear();


//        emailId = App.uEmail;
//        token = App.uToken;

        new CallRequest(instance).getRegion(emailId, token);
    }


    @Override
    public void onResume() {
        super.onResume();

    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        getRegionDetail();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getRegion:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {

                            JSONArray jObject = jObj.getJSONArray("data");
                                list.addAll(LoganSquare.parseList(jObject.toString(),RegionModel.class));
                            mAdapter = new RegionAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvRegion.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvRegion.setItemAnimator(new DefaultItemAnimator());
                            rvRegion.setLayoutManager(mLayoutManager);
                            rvRegion.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        }  else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteRegion:
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Delete Region Successfully", getContext());
                            getRegionDetail();
                        }  else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
