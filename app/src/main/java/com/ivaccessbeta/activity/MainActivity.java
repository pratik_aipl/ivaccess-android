package com.ivaccessbeta.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.crashlytics.android.Crashlytics;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.DrawerAdapter;
import com.ivaccessbeta.chat.ChatDashboardFragment;
import com.ivaccessbeta.chat.ChatFragment;
import com.ivaccessbeta.fonts.LatoTextViewSemiBold;
import com.ivaccessbeta.fragment.AddJobActivity;
import com.ivaccessbeta.fragment.ChangePasswordFragment;
import com.ivaccessbeta.fragment.ContractorListFragment;
import com.ivaccessbeta.fragment.Directory.DirectoryFragment;
import com.ivaccessbeta.fragment.equipment.EquipmentMenuFragment;
import com.ivaccessbeta.fragment.HomeFragmentNew;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.fragment.ManageFragment;
import com.ivaccessbeta.fragment.checks.CheckFragment;
import com.ivaccessbeta.fragment.flotingNotification.FloatingViewControlFragment;
import com.ivaccessbeta.fragment.invoice.InvoiceFragment;
import com.ivaccessbeta.fragment.notify.NotifyFragment;
import com.ivaccessbeta.fragment.profile.EditProfileFragment;
import com.ivaccessbeta.fragment.profile.EmployeeDocProfileFragment;
import com.ivaccessbeta.fragment.profile.MyProfileFragment;
import com.ivaccessbeta.fragment.schedule.NewScheduleFragment;
import com.ivaccessbeta.model.ChatDashboardModel;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.DrawerModel;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.model.JobStateModel;
import com.ivaccessbeta.model.JobStatusModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.model.ShortByModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

import static com.ivaccessbeta.fragment.flotingNotification.services.ChatHeadService.iconView;
import static com.ivaccessbeta.fragment.flotingNotification.services.ChatHeadService.tv_notification_count;
import static com.ivaccessbeta.fragment.flotingNotification.services.CustomFloatingViewService.iconViewChat;
import static com.ivaccessbeta.fragment.flotingNotification.services.CustomFloatingViewService.tv_notification_count_chat;
import static com.ivaccessbeta.utils.Utils.showProgressDialog;

public class MainActivity extends AppCompatActivity implements AsynchTaskListner {

    public static FragmentManager manager;
    public static ActionBar actionBar;
    public static Toolbar toolbar;
    public static LatoTextViewSemiBold toolbar_title;
    public static ImageView ivBack;
    public static DrawerLayout drawer;
    public static ListView lvDrawer;
    public Button btnLogout;
    public ArrayList<DrawerModel> drawerList = new ArrayList<>();
    public static LinearLayout linearJobs;
    public static TextView txtTitle;
    public String roleId;
    public static ImageView ivPublish;
    public String emailId, token, userId;
    public ContractorModel contractorModel;

    public ArrayList<ContractorModel> listPerson = new ArrayList<>();
    public ArrayList<JobStatusModel> statusList = new ArrayList<>();
    public ArrayList<FacilitesModel> facilityList = new ArrayList<>();
    public ArrayList<RegionModel> regionList = new ArrayList<>();
    public ArrayList<ShortByModel> shortByList = new ArrayList<>();
    public ArrayList<JobStateModel> jobStateList = new ArrayList<>();

    public ArrayList<String> facillityStringList = new ArrayList<>();
    public ArrayList<String> regionStringList = new ArrayList<>();
    public ArrayList<String> shortByStringList = new ArrayList<>();
    public ArrayList<String> jobstateStringList = new ArrayList<>();
    public ArrayList<String> statusStringList = new ArrayList<String>();
    public ArrayList<String> contactorArrayList = new ArrayList<>();

    public static int pos;
    public DrawerAdapter adapter;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS};
    int PERMISSION_ALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);

        }
        toolbar = findViewById(R.id.toolbar);
        ivBack = findViewById(R.id.ivBack);
        toolbar_title = findViewById(R.id.toolbar_title);
        manager = getSupportFragmentManager();
        actionBar = getSupportActionBar();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawer = findViewById(R.id.drawer_layout);
        lvDrawer = findViewById(R.id.lv_drawer);
        btnLogout = findViewById(R.id.btn_logout);
        txtTitle = findViewById(R.id.txtTitle);
        ivPublish = findViewById(R.id.ivPublish);
        linearJobs = findViewById(R.id.linearJobs);

        btnLogout.setOnClickListener(view -> doLogout());

        emailId = MySharedPref.getString(MainActivity.this, App.EMAIL, "");
        token = MySharedPref.getString(MainActivity.this, App.APP_TOKEN, "");
        userId = MySharedPref.getString(MainActivity.this, App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(MainActivity.this, App.ROLEID, "");

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            setDrawerForContractor();
        } else if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            setDrawerForManager();
        } else if (roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            seDrawerForAdmin();
        } else {
            setDrawerSuperAdmin();
        }
        linearJobs.setVisibility(View.GONE);
        getJobStatus();
    }

    public void showToolbarAction() {
        ivBack.setVisibility(View.VISIBLE);

    }

    public static void hideToolbarAction() {
        ivBack.setVisibility(View.GONE);
        txtTitle.setText("JOBS");
        toolbar_title.setText("");
    }

    private void seDrawerForAdmin() {
        final Bundle bundle = new Bundle();
        drawerList.clear();
        drawerList.add(new DrawerModel("Jobs"));
        drawerList.add(new DrawerModel("SCHEDULES"));
        drawerList.add(new DrawerModel("EQUIPMENTS"));
        drawerList.add(new DrawerModel("Checks"));
        drawerList.add(new DrawerModel("Invoices"));
        drawerList.add(new DrawerModel("Change Password"));


        adapter = new DrawerAdapter(getApplicationContext(), R.layout.layout_drawer, drawerList);
        lvDrawer.setAdapter(adapter);

        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pos = i;
                if (i == 0) {
                    changeFragment(new HomeFragmentNew(), false);
                    drawer.closeDrawers();
                    disableKeyBoard();
                } else if (i == 1) {
                    try {
                        NewScheduleFragment sh = (NewScheduleFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new NewScheduleFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new NewScheduleFragment(), true);
                    }

                } else if (i == 2) {
                    try {
                        EquipmentMenuFragment sh = (EquipmentMenuFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                            changeFragment(new EquipmentMenuFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        changeFragment(new EquipmentMenuFragment(), true);

                    }
                } else if (i == 3) {
                    try {
                        CheckFragment sh = (CheckFragment) manager.findFragmentById(R.id.frame);
                        bundle.putString(Constant.chkDate, "");
                        bundle.putString(Constant.chkId, "");
                        sh.setArguments(bundle);
                        if (!(sh != null && sh.isVisible())) {
                            CheckFragment checkFragment = new CheckFragment();
                            checkFragment.setArguments(bundle);
                            changeFragment(checkFragment, true);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CheckFragment checkFragment = new CheckFragment();
                        checkFragment.setArguments(bundle);

                        changeFragment(checkFragment, true);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    }

                } else if (i == 4) {

                    try {
                        InvoiceFragment sh = (InvoiceFragment) manager.findFragmentById(R.id.frame);
                        bundle.putString(Constant.InvoiceNo, "");

                        if (!(sh != null && sh.isVisible())) {
                            InvoiceFragment invoiceFragment = new InvoiceFragment();
                            invoiceFragment.setArguments(bundle);
                            changeFragment(invoiceFragment, true);
                        } else {
                            drawer.closeDrawers();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        InvoiceFragment invoiceFragment = new InvoiceFragment();
                        invoiceFragment.setArguments(bundle);
                        changeFragment(invoiceFragment, true);

                    }

                } else if (i == 5) {
                    try {
                        ChangePasswordFragment sh = (ChangePasswordFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ChangePasswordFragment(), true);
                            drawer.closeDrawers();
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ChangePasswordFragment(), true);
                        drawer.closeDrawers();
                    }
                }

            }
        });

        lvDrawer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setEnabled(false);
                view.setClickable(false);
                lvDrawer.setSelector(new ColorDrawable(0));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setDrawerForManager() {
        final Bundle bundle = new Bundle();
        drawerList.clear();
        drawerList.add(new DrawerModel("Jobs"));
        drawerList.add(new DrawerModel("SCHEDULES"));
        drawerList.add(new DrawerModel("Checks"));
        drawerList.add(new DrawerModel("Change Password"));

        adapter = new DrawerAdapter(getApplicationContext(), R.layout.layout_drawer, drawerList);
        lvDrawer.setAdapter(adapter);
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pos = i;
                if (i == 0) {
                    changeFragment(new HomeFragmentNew(), false);
                    drawer.closeDrawers();
                    disableKeyBoard();
                } else if (i == 1) {
                    try {
                        NewScheduleFragment sh = (NewScheduleFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new NewScheduleFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new NewScheduleFragment(), true);
                    }

                } else if (i == 2) {
                    try {
                        CheckFragment sh = (CheckFragment) manager.findFragmentById(R.id.frame);
                        bundle.putString(Constant.chkDate, "");
                        bundle.putString(Constant.chkId, "");

                        if (!(sh != null && sh.isVisible())) {
                            CheckFragment checkFragment = new CheckFragment();
                            checkFragment.setArguments(bundle);

                            changeFragment(checkFragment, true);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        CheckFragment checkFragment = new CheckFragment();
                        checkFragment.setArguments(bundle);

                        e.printStackTrace();

                        changeFragment(checkFragment, true);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    }
                } else if (i == 3) {
                    try {
                        ChangePasswordFragment sh = (ChangePasswordFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ChangePasswordFragment(), true);
                            drawer.closeDrawers();
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ChangePasswordFragment(), true);
                        drawer.closeDrawers();
                    }
                }
            }
        });
    }

    private void setDrawerForContractor() {
        final Bundle bundle = new Bundle();
        drawerList.clear();
        drawerList.add(new DrawerModel("Jobs"));
        drawerList.add(new DrawerModel("SCHEDULES"));
        drawerList.add(new DrawerModel("EQUIPMENTS"));
        drawerList.add(new DrawerModel("Checks"));
        drawerList.add(new DrawerModel("My Profile"));
        drawerList.add(new DrawerModel("Change Password"));
        drawerList.add(new DrawerModel("Chat"));
        adapter = new DrawerAdapter(getApplicationContext(), R.layout.layout_drawer, drawerList);
        lvDrawer.setAdapter(adapter);

        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    changeFragment(new HomeFragmentNew(), false);
                    drawer.closeDrawers();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                } else if (i == 1) {
                    try {
                        NewScheduleFragment sh = (NewScheduleFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new NewScheduleFragment(), true);
                            lvDrawer.getChildAt(i).setEnabled(false);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new NewScheduleFragment(), true);
                        lvDrawer.getChildAt(i).setEnabled(false);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                    }
                } else if (i == 2) {
                    try {
                        EquipmentMenuFragment sh = (EquipmentMenuFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                            changeFragment(new EquipmentMenuFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        changeFragment(new EquipmentMenuFragment(), true);

                    }
                } else if (i == 3) {
                    try {
                        CheckFragment sh = (CheckFragment) manager.findFragmentById(R.id.frame);
                        bundle.putString(Constant.chkDate, "");
                        bundle.putString(Constant.chkId, "");

                        if (!(sh != null && sh.isVisible())) {
                            CheckFragment checkFragment = new CheckFragment();
                            checkFragment.setArguments(bundle);

                            changeFragment(checkFragment, true);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CheckFragment checkFragment = new CheckFragment();
                        checkFragment.setArguments(bundle);

                        changeFragment(checkFragment, true);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    }


                } else if (i == 4) {
                    try {
                        MyProfileFragment sh = (MyProfileFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            MyProfileFragment myProfileFragment = new MyProfileFragment();
                            Bundle b = new Bundle();
                            b.putInt("value", 0);
                            myProfileFragment.setArguments(b);
                            changeFragment(myProfileFragment, true);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        MyProfileFragment myProfileFragment = new MyProfileFragment();
                        Bundle b = new Bundle();
                        b.putInt("value", 0);
                        myProfileFragment.setArguments(b);
                        changeFragment(myProfileFragment, true);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    }
                } else if (i == 5) {
                    try {
                        ChangePasswordFragment sh = (ChangePasswordFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ChangePasswordFragment(), true);
                            drawer.closeDrawers();
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ChangePasswordFragment(), true);
                        drawer.closeDrawers();
                    }
                } else if (i == 6) {
                    try {
                        ChatDashboardFragment sh = (ChatDashboardFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ChatDashboardFragment(), true);
                            drawer.closeDrawers();
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ChatDashboardFragment(), true);
                        drawer.closeDrawers();
                    }
                }
            }
        });
    }

    private void setDrawerSuperAdmin() {
        final Bundle bundle = new Bundle();
        drawerList.clear();
        drawerList.add(new DrawerModel("Jobs"));
        drawerList.add(new DrawerModel("SCHEDULES"));
        drawerList.add(new DrawerModel("EQUIPMENTS"));
        drawerList.add(new DrawerModel("Checks"));
        drawerList.add(new DrawerModel("Invoices"));
        drawerList.add(new DrawerModel("Manage"));
        drawerList.add(new DrawerModel("Directory"));
        drawerList.add(new DrawerModel("Notify"));
        drawerList.add(new DrawerModel("Change Password"));
        drawerList.add(new DrawerModel("Chat"));
        drawerList.add(new DrawerModel("Contractor List"));
        adapter = new DrawerAdapter(getApplicationContext(), R.layout.layout_drawer, drawerList);
        lvDrawer.setAdapter(adapter);
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                pos = i;

                if (i == 0) {
                    changeFragment(new HomeFragmentNew(), false);
                    drawer.closeDrawers();
                    disableKeyBoard();
                } else if (i == 1) {
                    try {
//                        NewScheduleFragment sh = (NewScheduleFragment) manager.findFragmentById(R.id.frame);
//
//                        if (!(sh != null && sh.isVisible())) {
                        changeFragment(new NewScheduleFragment(), true);
//                        } else {
//                            drawer.closeDrawers();
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new NewScheduleFragment(), true);
                    }

                } else if (i == 2) {
                    try {
                        EquipmentMenuFragment sh = (EquipmentMenuFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                            changeFragment(new EquipmentMenuFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        changeFragment(new EquipmentMenuFragment(), true);

                    }
                } else if (i == 3) {

                    try {
                        CheckFragment sh = (CheckFragment) manager.findFragmentById(R.id.frame);
                        bundle.putString(Constant.chkDate, "");
                        bundle.putString(Constant.chkId, "");

                        if (!(sh != null && sh.isVisible())) {
                            CheckFragment checkFragment = new CheckFragment();
                            checkFragment.setArguments(bundle);

                            changeFragment(checkFragment, true);
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CheckFragment checkFragment = new CheckFragment();
                        checkFragment.setArguments(bundle);

                        changeFragment(checkFragment, true);
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    }

                } else if (i == 4) {

                    try {
                        InvoiceFragment sh = (InvoiceFragment) manager.findFragmentById(R.id.frame);
                        bundle.putString(Constant.InvoiceNo, "");
                        if (!(sh != null && sh.isVisible())) {
                            InvoiceFragment invoiceFragment = new InvoiceFragment();
                            invoiceFragment.setArguments(bundle);
                            changeFragment(invoiceFragment, true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        InvoiceFragment invoiceFragment = new InvoiceFragment();
                        invoiceFragment.setArguments(bundle);
                        changeFragment(invoiceFragment, true);
                    }
                } else if (i == 5) {

                    try {
                        ManageFragment sh = (ManageFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ManageFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ManageFragment(), true);
                    }

                } else if (i == 6) {
                    try {
                        DirectoryFragment sh = (DirectoryFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new DirectoryFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new DirectoryFragment(), true);
                    }
                } else if (i == 7) {
                    try {
                        NotifyFragment sh = (NotifyFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new NotifyFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new NotifyFragment(), true);
                    }

                } else if (i == 8) {
                    try {
                        ChangePasswordFragment sh = (ChangePasswordFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ChangePasswordFragment(), true);
                            drawer.closeDrawers();
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ChangePasswordFragment(), true);
                        drawer.closeDrawers();
                    }
                } else if (i == 9) {
                    try {
                        ChatDashboardFragment sh = (ChatDashboardFragment) manager.findFragmentById(R.id.frame);
                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ChatDashboardFragment(), true);
                            drawer.closeDrawers();
                        } else {
                            drawer.closeDrawers();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ChatDashboardFragment(), true);
                        drawer.closeDrawers();
                    }
                } else if (i == 10) {
                    try {
                        ContractorListFragment sh = (ContractorListFragment) manager.findFragmentById(R.id.frame);

                        if (!(sh != null && sh.isVisible())) {
                            changeFragment(new ContractorListFragment(), true);
                        } else {
                            drawer.closeDrawers();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        changeFragment(new ContractorListFragment(), true);

                    }
                }
            }
        });

        lvDrawer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                view.setEnabled(false);
                view.setClickable(false);
                lvDrawer.setSelector(new ColorDrawable(0));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void doLogout() {
        final MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                .title("Really Logout?")
                .content("Do you want to logout?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        iconView.setVisibility(View.GONE);
                        iconViewChat.setVisibility(View.GONE);

                        MySharedPref.setString(MainActivity.this, App.EMAIL, "");
                        MySharedPref.setString(MainActivity.this, App.APP_TOKEN, "");
                        MySharedPref.setString(MainActivity.this, App.LOGGED_USER_ID, "");
                        MySharedPref.setString(MainActivity.this, App.PASSWORD, "");
                        MySharedPref.setString(MainActivity.this, App.ROLEID, "");
                        MySharedPref.setString(MainActivity.this, App.NAME, "");
                        MySharedPref.setString(MainActivity.this, App.REGION_ID, "");
                        MySharedPref.setString(MainActivity.this, App.IS_PASSWORD_CHANGE, "");
                        MySharedPref.setisLogin(false);
                        MySharedPref.SharedPrefClear(getApplicationContext());

                        App.multiSelectPlaceId = "";
                        App.multiSelectRegionId = "";
                        App.multiSelectPersonId = "";
                        App.contractorId = "";
                        App.password = "";
//                        App.uEmail = "";

                        MySharedPref.setisLogin(false);
                        MySharedPref.SharedPrefClear(getApplicationContext());

                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("intent", 2);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    public void changeFragment(Fragment fragment, boolean doAddToBackStack) {
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
            setBackButton();
        } else {
//            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        transaction.commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        // transaction.commit();
    }

    public void disableKeyBoard() {
        //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    public void setBackButton() {
        hideToolbarAction();
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (drawer != null)
            drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        toolbar.setVisibility(View.VISIBLE);
        hideToolbarAction();
        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawers();
            return;
        }
        if ((App.title.equalsIgnoreCase(Constant.JOB_EDITED) ||
                App.title.equalsIgnoreCase(Constant.JOB_NOTE) ||
                App.title.equalsIgnoreCase(Constant.JOB_MARKARRIVAL) ||
                App.title.equalsIgnoreCase(Constant.JOB_DOC_APPROVE) ||
                App.title.equalsIgnoreCase(Constant.JOB_DOC_UPLOAD) ||
                App.title.equalsIgnoreCase(Constant.JOB_ADD) ||
                App.title.equalsIgnoreCase(Constant.JOB_NOTIFICATION)) ||
                App.title.equalsIgnoreCase(Constant.JOB_EQUIPMENT_STATUS) ||
                App.title.equalsIgnoreCase(Constant.JOB_STATUS) ||
                App.title.equalsIgnoreCase(Constant.EQUIPMENT_ADD) ||
                App.title.equalsIgnoreCase(Constant.SCHEDULE_ADD) ||
                App.title.equalsIgnoreCase(Constant.SCHEDULE_UPDATE) ||
                App.title.equalsIgnoreCase(Constant.DOCUMENT_EXPIRED) ||
                App.title.equalsIgnoreCase(Constant.JOB_DOSE_NOT_ACKNLOG) ||
                App.title.equalsIgnoreCase(Constant.Message)) {
            changeFragment(new HomeFragmentNew(), false);
            App.title = "";
            App.threadID = "";
        }
        if (App.isFromNotificationFragment || App.isFromChatFragment) {
            iconView.setVisibility(View.VISIBLE);
            iconViewChat.setVisibility(View.VISIBLE);
        }
        App.threadID = "";
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() == 1) {
            manager.popBackStack();
            disableKeyBoard();
        } else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            setBackButton();
            disableKeyBoard();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            iconView.setVisibility(View.GONE);
                            iconViewChat.setVisibility(View.GONE);
                            finish();
                            moveTaskToBack(true);
                        }
                    }).create().show();
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_icon, menu);
//        if (menu instanceof MenuBuilder) {
//            try {
//                Method m = menu.getClass().getDeclaredMethod(
//                        "setOptionalIconsVisible", Boolean.TYPE);
//                m.setAccessible(true);
//                m.invoke(menu, true);
//            } catch (NoSuchMethodException e) {
//                Log.e("", "onMenuOpened", e);
//            } catch (Exception e) {
//                throw new RuntimeException(e);
//            }
//        }


        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (iconView != null)
            iconView.setVisibility(View.VISIBLE);
        if (iconViewChat != null)
            iconViewChat.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.drawer) {
            drawer.openDrawer(Gravity.RIGHT);
            disableKeyBoard();
            return true;
        }

        if (id == R.id.addJob) {

            AddJobActivity fragment = new AddJobActivity();
            Bundle bundle = new Bundle();
            bundle.putInt("value", 0);
            fragment.setArguments(bundle);
            changeFragment(fragment, true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (App.homeAddDoc) {

            try {
                Log.i("TAG", "Main activity : " + resultCode);
                HomeFragmentNew homeFragment = (HomeFragmentNew) getSupportFragmentManager().findFragmentById(R.id.frame);
                if (homeFragment != null && homeFragment.isVisible()) {
                    homeFragment.onActivityResult(requestCode, resultCode, data);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (data != null) {
            if (App.checkEmpDocState = true) {
                try {
                    EmployeeDocProfileFragment employeeDocProfileFragment = (EmployeeDocProfileFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (employeeDocProfileFragment != null && employeeDocProfileFragment.isVisible()) {
                        employeeDocProfileFragment.onActivityResult(requestCode, resultCode, data);
                    }
                    App.checkEmpDocState = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (App.checkEditProfileState = true) {
                try {
                    EditProfileFragment editFragment = (EditProfileFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (editFragment != null && editFragment.isVisible()) {
                        editFragment.onActivityResult(requestCode, resultCode, data);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }

    private void getRegion() {
        new CallRequest(MainActivity.this).getRegionActivity(emailId, token);
    }

    private void getJobState() {
        new CallRequest(MainActivity.this).getJobStateActivity(emailId, token);
    }

    private void getFacility() {
        new CallRequest(MainActivity.this).getFacilityActivity(emailId, token);
    }

    private void getJobStatus() {
        new CallRequest(MainActivity.this).getJobStatusActivity(emailId, token, userId);
    }

    private void getShortBy() {
        new CallRequest(MainActivity.this).getShortByActivity(emailId, token, userId);
    }

    private void getPersons() {
        new CallRequest(MainActivity.this).getConractorActivityHome(emailId, token, "");
    }

    private void get_job_filter() {
        showProgressDialog(MainActivity.this);
        new CallRequest(MainActivity.this).get_job_filter(emailId, token, userId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (iconView != null)
            iconView.setVisibility(View.VISIBLE);
        if (iconViewChat != null)
            iconViewChat.setVisibility(View.VISIBLE);
        if (!Constant.isMap) {
            android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.container, FloatingViewControlFragment.newInstance());
            ft.commit();
            new CallRequest(MainActivity.this).get_user_pendding_count_notification(emailId, token, userId);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (!Constant.isMap) {
            if (!App.isServiceRunning) {
                android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.add(R.id.container, FloatingViewControlFragment.newInstance());
                ft.commit();
            }
        }
        if (iconView != null)
            iconView.setVisibility(View.GONE);
        if (iconViewChat != null)
            iconViewChat.setVisibility(View.GONE);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getJobStatus:
                    try {
                        statusList.clear();
                        statusStringList.clear();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                JobStatusModel jobStatusModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), JobStatusModel.class);
//                                jobStatusModel = (JobStatusModel) jParsar.parseJson(jObject.getJSONObject(i), new JobStatusModel());
                                statusList.add(jobStatusModel);
                                statusStringList.add(jobStatusModel.getJobStatusName());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    get_job_filter();
                    break;
                case get_user_pendding_count_notification:
                    try {
                        Constant.isMap = true;
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            int count = jObj.getInt("data");
                            if (count > 0) {
                                try {
                                    tv_notification_count.setVisibility(View.VISIBLE);
                                    tv_notification_count.setText(count + "");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    tv_notification_count.setVisibility(View.GONE);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }


                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        e.printStackTrace();
                    }
                    new CallRequest(MainActivity.this).getChatHistoryActivity(emailId, token, userId);
                    break;
                case getChatHistory:
                    Utils.hideProgressDialog();
                    try {
                        int count = 0;
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray array = jObj.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject ob = array.getJSONObject(i);
//                                ChatDashboardModel  chatModel = (ChatDashboardModel) jParsar.parseJson(ob, new ChatDashboardModel());
                                ChatDashboardModel chatModel = LoganSquare.parse(ob.toString(), ChatDashboardModel.class);// jParsar.parseJson(ob, new ChatDashboardModel());
                                count += Integer.parseInt(chatModel != null ? !TextUtils.isEmpty(chatModel.getPenddingView()) ? chatModel.getPenddingView() : "0" : "0");
                            }
                            if (count > 0) {
                                if (tv_notification_count_chat != null) {
                                    tv_notification_count_chat.setVisibility(View.VISIBLE);
                                    tv_notification_count_chat.setText(count + "");
                                }
                            } else {
                                if (tv_notification_count_chat != null)
                                    tv_notification_count_chat.setVisibility(View.GONE);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), MainActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case get_job_filter:
                    listPerson.clear();
                    contactorArrayList.clear();

                    statusList.clear();
                    statusStringList.clear();

                    facilityList.clear();
                    facillityStringList.clear();
                    regionList.clear();
                    regionStringList.clear();
                    shortByList.clear();
                    shortByStringList.clear();
                    jobStateList.clear();
                    jobstateStringList.clear();

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {

                            JSONObject jData = jObj.getJSONObject("data");
                            JSONArray jContractor = jData.getJSONArray("contractor_list");
                            for (int i = 0; jContractor.length() > i; i++) {
                                contractorModel = new ContractorModel();
//                                contractorModel = (ContractorModel) jParsar.parseJson(jContractor.getJSONObject(i), new ContractorModel());
                                contractorModel = LoganSquare.parse(jContractor.getJSONObject(i).toString(), ContractorModel.class);
                                listPerson.add(contractorModel);
                                contactorArrayList.add(contractorModel.getFullName());
                            }
                            JSONArray jJobStatus = jData.getJSONArray("job_status_list");

                            for (int i = 0; jJobStatus.length() > i; i++) {
                                JobStatusModel jobStatusModel = new JobStatusModel();
                                jobStatusModel = LoganSquare.parse(jJobStatus.getJSONObject(i).toString(), JobStatusModel.class);
//                                jobStatusModel = (JobStatusModel) jParsar.parseJson(jJobStatus.getJSONObject(i), new JobStatusModel());
                                statusList.add(jobStatusModel);
                                statusStringList.add(jobStatusModel.getJobStatusName());
                            }
                            JSONArray jFacility = jData.getJSONArray("facility_list");
                            for (int i = 0; i < jFacility.length(); i++) {
                                JSONObject ob = jFacility.getJSONObject(i);
                                FacilitesModel facilityModel = LoganSquare.parse(ob.toString(), FacilitesModel.class);
                                facilityList.add(facilityModel);
                                facillityStringList.add(facilityModel.getFacilityName());
                            }
                            JSONArray jJobState = jData.getJSONArray("job_state_list");
                            for (int i = 0; i < jJobState.length(); i++) {
                                JSONObject ob = jJobState.getJSONObject(i);
                                JobStateModel jobStateModel = LoganSquare.parse(ob.toString(), JobStateModel.class);
                                jobStateList.add(jobStateModel);
                                jobstateStringList.add(jobStateModel.getName());
                            }

                            JSONArray jRegion = jData.getJSONArray("region_list");
                            for (int i = 0; i < jRegion.length(); i++) {
                                JSONObject ob = jRegion.getJSONObject(i);
                                RegionModel regionModel = LoganSquare.parse(ob.toString(), RegionModel.class);
                                regionList.add(regionModel);
                                regionStringList.add(regionModel.getRegionName());
                            }
                            JSONArray jJobSort = jData.getJSONArray("job_sort_by_list");
                            for (int i = 0; i < jJobSort.length(); i++) {
                                JSONObject ob = jJobSort.getJSONObject(i);
                                ShortByModel shortByModel = LoganSquare.parse(ob.toString(), ShortByModel.class);
                                shortByList.add(shortByModel);
                                shortByStringList.add(shortByModel.getName());
                            }
                            try {
                                if (App.title.equalsIgnoreCase(Constant.JOB_EDITED) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_NOTE) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_MARKARRIVAL) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_DOC_UPLOAD) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_DOC_APPROVE) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_ADD) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_NOTIFICATION) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_STATUS) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_DOSE_NOT_ACKNLOG)) {
                                    JobDetailFragment fragment = new JobDetailFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("jobNo", App.jobId);
                                    fragment.setArguments(bundle);
                                    changeFragment(fragment, true);
                                } else if (App.title.equalsIgnoreCase(Constant.JOB_EQUIPMENT_STATUS) || App.title.equalsIgnoreCase(Constant.EQUIPMENT_ADD)) {
                                    changeFragment(new EquipmentMenuFragment(), true);
                                } else if (App.title.equalsIgnoreCase(Constant.SCHEDULE_ADD) || App.title.equalsIgnoreCase(Constant.SCHEDULE_UPDATE)) {
                                    changeFragment(new NewScheduleFragment(), true);
                                } else if (App.title.equalsIgnoreCase(Constant.DOCUMENT_EXPIRED)) {
                                    if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                                        MyProfileFragment myProfileFragment = new MyProfileFragment();
                                        Bundle b = new Bundle();
                                        b.putInt("value", 0);
                                        myProfileFragment.setArguments(b);
                                        changeFragment(myProfileFragment, true);
                                    } else {
                                        MyProfileFragment myProfileFragment = new MyProfileFragment();
                                        Bundle b = new Bundle();
                                        b.putInt("value", 1);
                                        b.putString("contractorId", App.contractorId);
                                        // b.putSerializable("ContractorModel", contractorModel);
                                        myProfileFragment.setArguments(b);
                                        changeFragment(myProfileFragment, true);
                                    }
                                } else {
                                    changeFragment(new HomeFragmentNew(), false);
                                }
                            } catch (IllegalStateException e) {
                                Utils.hideProgressDialog();
                                e.printStackTrace();
                            }


                        } else {

                            try {
                                Utils.handleJSonAPIError(jObj, getApplicationContext());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case getContractor:
                    listPerson.clear();
                    contactorArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                ContractorModel   contractorModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), ContractorModel.class);
                                listPerson.add(contractorModel);
                                contactorArrayList.add(contractorModel.getFullName());
                            }
                        } else {
                            try {
                                Utils.handleJSonAPIError(jObj, getApplicationContext());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getFacility();
                    break;
                case getFacilityActivity:
                    try {
                        facilityList.clear();
                        facillityStringList.clear();
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                FacilitesModel facilityModel = LoganSquare.parse(ob.toString(), FacilitesModel.class);
                                facilityList.add(facilityModel);
                                facillityStringList.add(facilityModel.getFacilityName());
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getApplicationContext());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getRegion();
                    break;

                case getRegion:
                    try {
                        regionList.clear();
                        regionStringList.clear();
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                RegionModel regionModel = LoganSquare.parse(ob.toString(), RegionModel.class);
                                regionList.add(regionModel);
                                regionStringList.add(regionModel.getRegionName());
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getApplicationContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getShortBy();

                    break;
                case getShortBy:
                    try {
                        shortByList.clear();
                        shortByStringList.clear();
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                ShortByModel shortByModel = LoganSquare.parse(ob.toString(), ShortByModel.class);
                                shortByList.add(shortByModel);
                                shortByStringList.add(shortByModel.getName());
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getApplicationContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    getJobState();
                    break;
                case getJobState:
                    try {
                        jobStateList.clear();
                        jobstateStringList.clear();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                JobStateModel jobStateModel = LoganSquare.parse(ob.toString(), JobStateModel.class);
                                jobStateList.add(jobStateModel);
                                jobstateStringList.add(jobStateModel.getName());
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getApplicationContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (App.title.equalsIgnoreCase(Constant.JOB_EDITED) ||
                                App.title.equalsIgnoreCase(Constant.JOB_NOTE) ||
                                App.title.equalsIgnoreCase(Constant.JOB_MARKARRIVAL) ||
                                App.title.equalsIgnoreCase(Constant.JOB_DOC_UPLOAD) ||
                                App.title.equalsIgnoreCase(Constant.JOB_DOC_APPROVE) ||
                                App.title.equalsIgnoreCase(Constant.JOB_ADD) ||
                                App.title.equalsIgnoreCase(Constant.JOB_NOTIFICATION) ||
                                App.title.equalsIgnoreCase(Constant.JOB_STATUS) ||
                                App.title.equalsIgnoreCase(Constant.JOB_DOSE_NOT_ACKNLOG)) {
                            JobDetailFragment fragment = new JobDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("jobNo", App.jobId);
                            fragment.setArguments(bundle);
                            changeFragment(fragment, true);
                        } else if (App.title.equalsIgnoreCase(Constant.JOB_EQUIPMENT_STATUS) || App.title.equalsIgnoreCase(Constant.EQUIPMENT_ADD)) {
                            changeFragment(new EquipmentMenuFragment(), true);
                        } else if (App.title.equalsIgnoreCase(Constant.SCHEDULE_ADD) || App.title.equalsIgnoreCase(Constant.SCHEDULE_UPDATE)) {
                            changeFragment(new NewScheduleFragment(), true);
                        } else if (App.title.equalsIgnoreCase(Constant.DOCUMENT_EXPIRED)) {
                            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                                MyProfileFragment myProfileFragment = new MyProfileFragment();
                                Bundle b = new Bundle();
                                b.putInt("value", 0);
                                myProfileFragment.setArguments(b);
                                changeFragment(myProfileFragment, true);
                            } else {
                                MyProfileFragment myProfileFragment = new MyProfileFragment();
                                Bundle b = new Bundle();
                                b.putInt("value", 1);
                                b.putString("contractorId", App.contractorId);
                                // b.putSerializable("ContractorModel", contractorModel);
                                myProfileFragment.setArguments(b);
                                changeFragment(myProfileFragment, true);
                            }
                        } else if (App.title.equalsIgnoreCase(Constant.Message)) {
                            changeFragment(new ChatFragment(), true);
                        } else {
                            changeFragment(new HomeFragmentNew(), false);
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }

                    break;
            }

        }
    }

    @Override
    protected void onDestroy() {
        deleteCache(this);
        super.onDestroy();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
            deleteDir(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), ".IVACESS"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

}
