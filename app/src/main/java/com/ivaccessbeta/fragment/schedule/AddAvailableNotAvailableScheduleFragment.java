package com.ivaccessbeta.fragment.schedule;

import android.annotation.SuppressLint;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.AddJobAndNotAvailableTabsPagerAdapter;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.Utils;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

public class AddAvailableNotAvailableScheduleFragment extends Fragment {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public ImageView ivBack;
    public AddJobAndNotAvailableTabsPagerAdapter adapter;
    public TextView txtTitle;
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_available_not_available_schedule, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        MainActivity.toolbar_title.setText("SCHEDULE");
        ((MainActivity)getActivity()).showToolbarAction();
        viewPager =view. findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.tabs);
        txtTitle=view.findViewById(R.id.txtTitle);
        ivBack=view.findViewById(R.id.ivBack);

        tabLayout.addTab(tabLayout.newTab().setText("New Schedule "));
        tabLayout.addTab(tabLayout.newTab().setText("Not Available"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        App.checkStateSchedule=true;
        adapter = new AddJobAndNotAvailableTabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }
}
