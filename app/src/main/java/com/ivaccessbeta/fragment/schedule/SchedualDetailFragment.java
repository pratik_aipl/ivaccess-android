package com.ivaccessbeta.fragment.schedule;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.NotAvailableScheduleAdapter;
import com.ivaccessbeta.adapter.ScheduleDetailAdapter;
import com.ivaccessbeta.listners.NotAvailableScheduleListener;
import com.ivaccessbeta.listners.ScheduleListener;
import com.ivaccessbeta.model.ScheduleDetailModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MultiSelectionSpinnerSchedule;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.ivaccessbeta.activity.MainActivity.ivPublish;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.txtTitle;
import static com.ivaccessbeta.App.nextPreDate;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.NOT_AVAILABLE;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.PERSONS;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.PLACE;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.REGIONS;

public class SchedualDetailFragment extends Fragment implements AsynchTaskListner, MultiSelectionSpinnerSchedule.OnMultipleItemsSelectedListener, AdapterView.OnItemSelectedListener, ScheduleListener, NotAvailableScheduleListener {

    public RecyclerView rvSchedualDetail, rvScheduleNotAvailable;
    public ScheduleDetailAdapter scheduleDetailAdapter;
    public NotAvailableScheduleAdapter notAvailableScheduleAdapter;
    public ArrayList<ScheduleDetailModel> listSchedule = new ArrayList<>();
    public ArrayList<ScheduleDetailModel> listNotAvailableSchedule = new ArrayList<>();
    public SchedualDetailFragment instance;
    public static final String POS = "pos";
    public RecyclerView.LayoutManager mLayoutManager, layoutManager;
    public Dialog dialog;
    public static String typeSchedule;
    public TextView txtJobState, txtShortBy, txtPersons, txtNotAvailable,txtPlace, txtStatuses, txtRegion, txtJobAcknologe, txtNoteAvailable,txtLable,txtNote;
    public int tabPostion = 0;
    public String emailId, token, userId, multiSelecteStatusId, roleId;
    public MultiSelectionSpinnerSchedule spRegion, spPerson, spPlace, spNoteAvailable;
    public View viewPerson;
    public Calendar calander;
    public int year;
        public static Fragment newInstance(int pos) {
        Bundle bundle = new Bundle();
        bundle.putInt(POS, pos);
        SchedualDetailFragment fragment = new SchedualDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_schedual_detail, container, false);
        setHasOptionsMenu(true);
        instance = this;
        linearJobs.setVisibility(View.VISIBLE);
        txtTitle.setText("SCHEDULE");
        ivPublish.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");

        tabPostion = getArguments().getInt(POS);
        rvSchedualDetail = view.findViewById(R.id.rvSchedualDetail);
        rvScheduleNotAvailable = view.findViewById(R.id.rvSNotAvailableDetail);
        txtLable=view.findViewById(R.id.txtLable);
        mLayoutManager = new LinearLayoutManager(getActivity());
        layoutManager = new LinearLayoutManager(getActivity());
        rvSchedualDetail.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvSchedualDetail.setLayoutManager(mLayoutManager);


        linearJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobSearchDialog();
            }
        });
        return view;
    }

    public void showJobSearchDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.schedule_search_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnOK = dialog.findViewById(R.id.btnOk);
        Button dialogClear = dialog.findViewById(R.id.btnClear);
        final TextView txtMonth = dialog.findViewById(R.id.txt_month);
        LinearLayout llPrevious = dialog.findViewById(R.id.llPrevious);
        TextView txtToday = dialog.findViewById(R.id.txtToday);
        TextView txt_month = dialog.findViewById(R.id.txt_month);
        RelativeLayout relativeNextPre = dialog.findViewById(R.id.relativeNextPre);
        LinearLayout llNext = dialog.findViewById(R.id.llNext);

        //=====contractor login next previous visibility gone=====
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txt_month.setVisibility(View.GONE);
            relativeNextPre.setVisibility(View.GONE);
        }
        //============================================
        long date = System.currentTimeMillis();
        calander = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM  yyyy");
        final SimpleDateFormat month = new SimpleDateFormat("MM");
        final SimpleDateFormat yearFormate = new SimpleDateFormat("yyyy");
        if(App.yearMonth.equalsIgnoreCase("")){
            year= (calander.get(Calendar.YEAR));
        }else {
            try {
                year = Integer.parseInt(yearFormate.format(dateFormat.parse(App.yearMonth)));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (App.monthNo == 0) {
            App.monthNo = Integer.parseInt(month.format(date));
        }

        if (App.yearMonth.equalsIgnoreCase("")) {
            App.yearMonth = dateFormat.format(date);
            txtMonth.setText(App.yearMonth);
        } else {
            txtMonth.setText(App.yearMonth);
        }
        txtToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.nextPreDate = "";
                long date = System.currentTimeMillis();
                calander = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM  yyyy");
                App.monthNo = Integer.parseInt(month.format(date));
                year= Integer.parseInt(yearFormate.format(date));
                App.yearMonth = dateFormat.format(date);
                txtMonth.setText(App.yearMonth);
            }
        });
        llPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String month = null;
                String monthNo = null;
                App.monthNo = App.monthNo - 1;
                if (App.monthNo == 1) {
                    monthNo = "01";
                    month = "January ";
                } else if (App.monthNo == 2) {
                    monthNo = "02";
                    month = "February ";
                } else if (App.monthNo == 3) {
                    monthNo = "03";
                    month = "March ";
                } else if (App.monthNo == 4) {
                    monthNo = "04";
                    month = "April ";
                } else if (App.monthNo == 5) {
                    monthNo = "05";
                    month = "May ";
                } else if (App.monthNo == 6) {
                    monthNo = "06";
                    month = "June ";
                } else if (App.monthNo == 7) {
                    monthNo = "07";
                    month = "July ";
                } else if (App.monthNo == 8) {
                    monthNo = "08";
                    month = "August ";
                } else if (App.monthNo == 9) {
                    monthNo = "09";
                    month = "September ";
                } else if (App.monthNo == 10) {
                    monthNo = "10";
                    month = "October ";
                } else if (App.monthNo == 11) {
                    monthNo = "11";
                    month = "November ";
                } else if (App.monthNo == 12) {
                    monthNo = "12";
                    month = "December ";
                } else {
                    App.monthNo = -1;
                    monthNo = "12";
                    month = "December";
                }

                if(App.monthNo==-1){
                   // monthNo="12";
                    int minusYear= year-1;
                    System.out.println("minus year==="+minusYear);
                    nextPreDate = minusYear + "-" + monthNo;
                    year= minusYear;
                   // month="December";
                    App.yearMonth = month + " " + minusYear;
                    App.monthNo=12;
                }else {
                    nextPreDate = year + "-" + monthNo;
                    App.yearMonth = month + " " + year;
                }
                txtMonth.setText(App.yearMonth);
            }
        });
        llNext.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                String month = null;
                String monthNo = null;
                App.monthNo = App.monthNo + 1;
                if (App.monthNo == 1) {
                    monthNo = "01";
                    month = "January ";
                } else if (App.monthNo == 2) {
                    monthNo = "02";
                    month = "February ";
                } else if (App.monthNo == 3) {
                    monthNo = "03";
                    month = "March ";
                } else if (App.monthNo == 4) {
                    monthNo = "04";
                    month = "April ";
                } else if (App.monthNo == 5) {
                    monthNo = "05";
                    month = "May ";
                } else if (App.monthNo == 6) {
                    monthNo = "06";
                    month = "June ";
                } else if (App.monthNo == 7) {
                    monthNo = "07";
                    month = "July ";
                } else if (App.monthNo == 8) {
                    monthNo = "08";
                    month = "August ";
                } else if (App.monthNo == 9) {
                    monthNo = "09";
                    month = "September ";
                } else if (App.monthNo == 10) {
                    monthNo = "10";
                    month = "October ";
                } else if (App.monthNo == 11) {
                    monthNo = "11";
                    month = "November ";
                } else if (App.monthNo == 12) {
                    monthNo = "12";
                    month = "December ";
                } else {
                    App.monthNo = -1;
                    monthNo = "1";
                    month = "January";
                }
//                nextPreDate = calander.get(Calendar.YEAR) + "-" + monthNo;
//                App.yearMonth = month + " " + calander.get(Calendar.YEAR);

                if(App.monthNo==-1){
                    // monthNo="12";
                    int minusYear= year+1;
                    System.out.println("Plush year==="+minusYear);
                    nextPreDate = minusYear + "-" + monthNo;
                    year= minusYear;
                    // month="December";
                    App.yearMonth = month + " " + minusYear;
                    App.monthNo=1;

                }else {
                    nextPreDate = year + "-" + monthNo;
                    App.yearMonth = month + " " + year;
                }
                txtMonth.setText(App.yearMonth);
            }
        });
        dialogClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long date = System.currentTimeMillis();
                calander = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM  yyyy");
                App.yearMonth = dateFormat.format(date);
                txtMonth.setText(App.yearMonth);

                if (!App.regionStringList.isEmpty()) {
                    spRegion.clearSelection();
                    txtRegion.setVisibility(View.VISIBLE);
                    App.indexListRegionSchedule.clear();
                    App.multiSelectRegionId = "";
                }
                if (!App.placeStringList.isEmpty()) {
                    spPlace.clearSelection();
                    txtPlace.setVisibility(View.VISIBLE);
                    App.indexListPlaceSchedule.clear();
                    App.multiSelectPlaceId = "";
                }
//                if(!statusStringList.isEmpty()) {
//                    spJobStatus.clearSelection();
//                }

                if (!App.contactorStringList.isEmpty()) {
                    spPerson.clearSelection();
                    txtPersons.setVisibility(View.VISIBLE);
                    App.indexListPersonSchedule.clear();
                    //contactorArrayList.clear();
                    App.multiSelectPersonId = "";
                }
                if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                    txtPersons.setVisibility(View.GONE);
                    spPerson.setVisibility(View.GONE);
                    viewPerson.setVisibility(View.GONE);
                    App.multiSelectPersonId = userId;
                }
//                spNoteAvailable.setItems(App.notAvailableList, Constant.FILTER_TYPE.NOT_AVAILABLE);
//                spNoteAvailable.setSelection(new int[]{0});
//                spNoteAvailable.setSelection(App.notAvailableList);

                App.multiSelectedNotAvailable = "Approve,Pending,Deny";
//                for (int i = 1; i <3 ; i++) {
//                    App.indexListNotAvailable.add(i);
//                }

                App.indexListNotAvailable.clear();
                spNoteAvailable.clearSelection();
                txtNotAvailable.setVisibility(View.VISIBLE);
                App.nextPreDate = "";
                txtMonth.setText(App.yearMonth);


            }
        });
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String month = null;
                SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
                month = month_date.format(calander.getTime());

                String checkMonthYear = month + "-" + calander.get(Calendar.YEAR);
                if (!checkMonthYear.equalsIgnoreCase(nextPreDate)) {
                    SchedualFragment.getSchedualHeader();
                    SchedualFragment.adapter.notifyDataSetChanged();
                } else {
                    getSchedualDetail(true);
                }
                dialog.dismiss();

            }
        });

        spPlace = dialog.findViewById(R.id.spPlaces);
        spPerson = dialog.findViewById(R.id.spPerson);
        spRegion = dialog.findViewById(R.id.spRegion);
        spNoteAvailable = dialog.findViewById(R.id.spNoteAvailable);
        txtPersons = dialog.findViewById(R.id.txtPersons);
        txtPlace = dialog.findViewById(R.id.txtPlace);
        txtRegion = dialog.findViewById(R.id.txtRegion);
        viewPerson = dialog.findViewById(R.id.viewPerson);
        txtNotAvailable=dialog.findViewById(R.id.txtNotAvailable);
        dialog.show();

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtPersons.setVisibility(View.GONE);
            spPerson.setVisibility(View.GONE);
            viewPerson.setVisibility(View.GONE);
            App.multiSelectPersonId = userId;
        }
        spPlace.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                typeSchedule = "place";
                return false;
            }
        });
        spRegion.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                typeSchedule = "region";
                return false;
            }
        });
        spPerson.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                typeSchedule = "person";
                return false;
            }
        });
        spNoteAvailable.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                typeSchedule = "notAvailable";
                return false;
            }
        });


        spPerson.setItems(App.contactorStringList, Constant.FILTER_TYPE.PERSONS);
        spPerson.setSelection(new int[]{0});
        spPerson.setListener(this);
        spPerson.clearSelection();

        spPlace.setItems(App.placeStringList, Constant.FILTER_TYPE.PLACE);
        spPlace.setSelection(new int[]{0});
        spPlace.clearSelection();
        spPlace.setListener(this);

        spRegion.setItems(App.regionStringList, Constant.FILTER_TYPE.REGIONS);
        spRegion.setSelection(new int[]{0});
        spRegion.clearSelection();
        spRegion.setListener(this);

        spNoteAvailable.setItems(App.notAvailableList, Constant.FILTER_TYPE.NOT_AVAILABLE);
        spNoteAvailable.setSelection(new int[]{0});
        spNoteAvailable.setListener(this);
        spNoteAvailable.setSelection(App.notAvailableList);

        if (!TextUtils.isEmpty(App.multiSelectRegionId)) {
            int[] array = new int[App.indexListRegionSchedule.size()];
            int counter = 0;
            for (Integer myInt : App.indexListRegionSchedule) {
                array[counter++] = myInt;
            }
            spRegion.setSelection(array);
            txtRegion.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(App.multiSelectPlaceId)) {
            int[] array = new int[App.indexListPlaceSchedule.size()];
            int counter = 0;
            for (Integer myInt : App.indexListPlaceSchedule) {
                array[counter++] = myInt;
            }
            spPlace.setSelection(array);
            txtPlace.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(App.multiSelectPersonId)) {
            int[] array = new int[App.indexListPersonSchedule.size()];
            int counter = 0;
            for (Integer myInt : App.indexListPersonSchedule) {
                array[counter++] = myInt;
            }
            spPerson.setSelection(array);
            txtPersons.setVisibility(View.GONE);
        }
        if (App.indexListNotAvailable.size() != 0) {
            txtNotAvailable.setVisibility(View.GONE);
            int[] array = new int[App.indexListNotAvailable.size()];
            int counter = 0;
            for (Integer myInt : App.indexListNotAvailable) {
                array[counter++] = myInt;
            }
            spNoteAvailable.setSelection(array);
        }
        if(App.indexListNotAvailable.size()==0){
            spNoteAvailable.clearSelection();
            txtNotAvailable.setVisibility(View.VISIBLE);
        }

    }

    //    public void getSchedualHeader() {
//        App.ScheduleFragmentList.clear();
//        System.out.println("schedule listSchedule size==="+App.ScheduleFragmentList.size());
//        new CallRequest(instance).getSchedual(emailId, token,nextPreDate);
//    }
    public boolean isSearch;

    private void getSchedualDetail(boolean isSearch) {
        this.isSearch = isSearch;
        try {
            int currentTab = tabPostion;
            if (isSearch) {
                if (tabPostion > 1) {
                    currentTab = tabPostion - 1;
                }
            }
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                App.multiSelectPersonId = userId;
            }
            App.tabCount=currentTab;
            new CallRequest(instance).getSchedualDetail(emailId, token, App.ScheduleFragmentList.get(currentTab).getDate(), App.multiSelectRegionId, App.multiSelectPersonId, App.multiSelectPlaceId, App.multiSelectedNotAvailable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getSchedualDetail(false);

        //  getPlace();
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);

            switch (request) {
                case getSchedualDetail:
                    listSchedule.clear();
                    listNotAvailableSchedule.clear();
                    //   Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                ScheduleDetailModel scheduleDetailModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),ScheduleDetailModel.class);
                                if (scheduleDetailModel.getAvailableSchedule().equalsIgnoreCase("1")) {
                                    listSchedule.add(scheduleDetailModel);
                                } else {
                                    listNotAvailableSchedule.add((scheduleDetailModel));
                                }
                            }

                            if(listNotAvailableSchedule.size()==0){
                                txtLable.setVisibility(View.GONE);
                            }else {
                                txtLable.setVisibility(View.VISIBLE);
                            }

                            scheduleDetailAdapter = new ScheduleDetailAdapter(listSchedule, instance);
                            rvSchedualDetail.setItemAnimator(new DefaultItemAnimator());
                            rvSchedualDetail.setAdapter(scheduleDetailAdapter);
                            scheduleDetailAdapter.notifyDataSetChanged();

                            rvScheduleNotAvailable.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvScheduleNotAvailable.setLayoutManager(layoutManager);
                            notAvailableScheduleAdapter = new NotAvailableScheduleAdapter(listNotAvailableSchedule, instance);
                            rvScheduleNotAvailable.setItemAnimator(new DefaultItemAnimator());
                            rvScheduleNotAvailable.setAdapter(notAvailableScheduleAdapter);
                            notAvailableScheduleAdapter.notifyDataSetChanged();

                            if (this.isSearch)
                                //view pager adapter notifyDataSetChange
                                SchedualFragment.adapter.notifyDataSetChanged();
                        }
                        else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case deleteSchedule:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            SchedualFragment.adapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case approveDenyNotAvailableSchedule:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            SchedualFragment.adapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case copyToSchedule:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            SchedualFragment.adapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {

        if (filter_type == PERSONS) {

            App.indexListPersonSchedule = indices;
            // App.  multiSelectPersonId = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            String selectedNames = "";
            for (Integer i : indices) {
                statusIDArray.add(App.personList.get(i).getId());
                selectedNames += App.personList.get(i).getFullName() + ", ";
            }
            txtPersons.setVisibility(View.GONE);

            App.multiSelectPersonId = android.text.TextUtils.join(",", statusIDArray);
            if (App.multiSelectPersonId.equals("")) {
                txtPersons.setVisibility(View.VISIBLE);
            }
        } else if (filter_type == PLACE) {

            App.indexListPlaceSchedule = indices;
            //   App.  multiSelectPlaceId = "";

            ArrayList<String> statusIDArray = new ArrayList<>();
            String selectedNames = "";
            for (Integer i : indices) {
                statusIDArray.add(App.placeList.get(i).getPlacesID());
                selectedNames += App.placeList.get(i).getPlacesName() + ", ";
            }
            txtPlace.setVisibility(View.GONE);
            App.multiSelectPlaceId = android.text.TextUtils.join(",", statusIDArray);
            if (App.multiSelectPlaceId.equals("")) {
                txtPlace.setVisibility(View.VISIBLE);
            }
        } else if (filter_type == REGIONS) {
            App.indexListRegionSchedule = indices;
            ArrayList<String> statusIDArray = new ArrayList<>();
            for (Integer i : indices) {
                statusIDArray.add(App.regionList.get(i).getRegionID());
            }
            txtRegion.setVisibility(View.GONE);
            App.multiSelectRegionId = android.text.TextUtils.join(",", statusIDArray);
            if (App.multiSelectRegionId.equals("")) {
                txtRegion.setVisibility(View.VISIBLE);
            }
        } else if (filter_type == NOT_AVAILABLE) {
            txtNotAvailable.setVisibility(View.GONE);
            App.indexListNotAvailable = indices;
            ArrayList<String> statusIDArray = new ArrayList<>();
            for (Integer i : indices) {
                if (i == 0) {
                    statusIDArray.add("Approve");
                } else if (i == 1) {
                    statusIDArray.add("Pending");
                } else if (i == 2) {
                    statusIDArray.add("Deny");
                } else {
                    statusIDArray.add(String.valueOf(App.notAvailableList));
                }

            }
            //txtRegion.setVisibility(View.GONE);
            App.multiSelectedNotAvailable = android.text.TextUtils.join(",", statusIDArray);

                if (App.multiSelectedNotAvailable.equals("")) {
                     txtNotAvailable.setVisibility(View.VISIBLE);
                }
        }
    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {
        for (int j = 0; j < strings.size(); j++) {
            System.out.println("item:::" + strings.get(j));
        }
    }

    @Override
    public void onDeleteSchedule(final int pos) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteSchedule(emailId, token, userId, listSchedule.get(pos).getScheduleID());

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    @Override
    public void onCopyToTomorrowSchedule(int pos) {
            new CallRequest(instance).copyToSchedule(emailId,token,userId,listNotAvailableSchedule.get(pos).getScheduleID(),Constant.TOMORROW);
    }

    @Override
    public void onCopyToNextWeekSchedule(int pos) {
        new CallRequest(instance).copyToSchedule(emailId,token,userId,listNotAvailableSchedule.get(pos).getScheduleID(),Constant.NEXT);
    }

    @Override
    public void onCopyToEntireWeekSchedule(int pos) {
        new CallRequest(instance).copyToSchedule(emailId,token,userId,listNotAvailableSchedule.get(pos).getScheduleID(),Constant.ENTIRE);

    }

    @Override
    public void onDeleteNotAvailableSchedule(final int pos) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteSchedule(emailId, token, userId, listNotAvailableSchedule.get(pos).getScheduleID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }
    @Override
    public void onApproveNotAvailableSchedule(int pos) {
        new CallRequest(instance).approveDenyNoteAvailabeSchedule(emailId, token, userId, listNotAvailableSchedule.get(pos).getScheduleID(), "Approve");
    }
    @Override
    public void onDenyNotAvailableSchedule(int pos) {
        new CallRequest(instance).approveDenyNoteAvailabeSchedule(emailId, token, userId, listNotAvailableSchedule.get(pos).getScheduleID(), "Deny");
    }

    @Override
    public void onCopyToTomorrowNotAvailableSchedule(int pos) {
        new CallRequest(instance).copyToSchedule(emailId,token,userId,listNotAvailableSchedule.get(pos).getScheduleID(),Constant.TOMORROW);

    }
}
