package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.InvoiceContractorModel;
import com.ivaccessbeta.App;

import java.util.ArrayList;

/**
 * Created by User on 12-04-2018.
 */

public class InvoiceSelectContractorAdapter extends RecyclerView.Adapter<InvoiceSelectContractorAdapter.MyViewHolder> {

    public ArrayList<InvoiceContractorModel> list;
    public Context context;
    public String emailId, token, userId;
    public View itemView;
    public boolean isSelectAll = false;
    public CheckBox selectAll;

    @Override
    public InvoiceSelectContractorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_select_contractor_check, parent, false);
        return new InvoiceSelectContractorAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.cbContractor.setText(list.get(position).getName());
        final String id = list.get(position).getBillToID();
        if (isSelectAll) {
            holder.cbContractor.setChecked(true);
        } else {

            if (App.checkedArrayInvoice.contains(id)) {
                holder.cbContractor.setChecked(true);
            } else {
                holder.cbContractor.setChecked(false);
            }
        }

        if (App.indexInvoiceContractorList.size() == list.size()) {
            selectAll.setChecked(true);
        } else {
            selectAll.setChecked(false);
        }

        holder.cbContractor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    if (!App.indexInvoiceContractorList.contains(id)) {
                        App.indexInvoiceContractorList.add(id);
                    }
                    if (!App.checkedArrayInvoice.contains(id)) {
                        App.checkedArrayInvoice.add(id);
                    }
                } else {
                    App.indexInvoiceContractorList.remove(id);
                    try {
                        App.checkedArrayInvoice.remove(id);
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
                if (App.indexInvoiceContractorList.size() == list.size()) {
                    selectAll.setChecked(true);
                } else {
                    selectAll.setChecked(false);
                }

            }
        });

        if (App.indexInvoiceContractorList.size() == list.size()) {
            selectAll.setChecked(true);
        } else {
            selectAll.setChecked(false);
        }
    }


    public void selectAll(boolean isSelectAll) {
        this.isSelectAll = isSelectAll;
        if (!isSelectAll) {
            App.checkedArrayInvoice.clear();
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public InvoiceSelectContractorAdapter(Context context, ArrayList<InvoiceContractorModel> List, CheckBox selectAll) {
        this.context = context;
        this.list = List;
        this.selectAll = selectAll;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CheckBox cbContractor;

        public MyViewHolder(View view) {
            super(view);
            cbContractor = view.findViewById(R.id.checkBox);
        }
    }

}
