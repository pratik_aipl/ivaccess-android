package com.ivaccessbeta.chat;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.ChatDashboardAdapter;
import com.ivaccessbeta.model.ChatDashboardModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.fragment.flotingNotification.services.CustomFloatingViewService.iconViewChat;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatDashboardFragment extends Fragment implements AsynchTaskListner {
    public RecyclerView rvChat;
    public View view;
    public ChatDashboardModel chatDashboardModel;
    public ChatDashboardAdapter chatDashboardAdapter;
    public ArrayList<ChatDashboardModel> list = new ArrayList<>();
    public ChatDashboardFragment instance;
    public String emailId, token, userId, roleId, userName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat_dashboard, container, false);
        instance = this;
        setHasOptionsMenu(true);
        ((MainActivity)getActivity()).setBackButton();
        MainActivity.toolbar_title.setText("CHAT/MESSAGES");
        ((MainActivity) getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        rvChat = view.findViewById(R.id.rvChat);
        iconViewChat.setVisibility(View.GONE);
        App.isFromChatFragment = true;
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        userName = MySharedPref.getString(getActivity(), App.NAME, "");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvChat.setLayoutManager(mLayoutManager);
        getChatHistory();
        return view;
    }

    private void getChatHistory() {
        new CallRequest(instance).getChatHistory(emailId, token, userId);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_chat, menu);  // Use filter.xml from step 1

       /* if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            menu.findItem(R.id.addMessage).setVisible(false);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.addMessage) {
            ((MainActivity)getActivity()).changeFragment(new AddNewChatFragment(), true);
            return true;
        }
        if (id == R.id.drawer) {
            drawer.openDrawer(Gravity.RIGHT);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {
                    case getChatHistory:
                        list.clear();
                        Utils.hideProgressDialog();
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getString("status").equals("200")) {
                                JSONArray array = jObj.getJSONArray("data");
                                list.addAll(LoganSquare.parseList(array.toString(), ChatDashboardModel.class));
                                chatDashboardAdapter = new ChatDashboardAdapter(list, getContext());
                                rvChat.setItemAnimator(new DefaultItemAnimator());
                                rvChat.setAdapter(chatDashboardAdapter);
                                chatDashboardAdapter.notifyDataSetChanged();
                            } else {
                                Utils.showToast(jObj.getString("message"), getContext());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
