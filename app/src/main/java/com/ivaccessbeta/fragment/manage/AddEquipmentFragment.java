package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.model.EquipmentsModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddEquipmentFragment extends Fragment implements AsynchTaskListner {
    public EditText edtEquipmentName,edtEquipmentNumber, edtEquipmentDetail;
    public Button btnSave, btnCancel;
    public String equipmentName, equipmentNumber, equipmentDetail, equipmentId;
    public AddEquipmentFragment instance;
    public String emailId, token, userId;
    int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_equipment, container, false);
        instance = this;
        MainActivity.toolbar_title.setText("Add Equipment");
        ((MainActivity)getActivity()).showToolbarAction();
        setHasOptionsMenu(true);
        value = getArguments().getInt("value");
        linearJobs.setVisibility(View.GONE);
        edtEquipmentName = view.findViewById(R.id.edtEquipmentName);
        edtEquipmentNumber = view.findViewById(R.id.edtEquipmentNumber);
        edtEquipmentDetail = view.findViewById(R.id.edtEquipmentDetail);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        if (value == 1) {
            MainActivity.toolbar_title.setText("Edit Equipment");

            EquipmentsModel equipmentModel = (EquipmentsModel) getArguments().getSerializable("EquipmentModel");
            equipmentName = equipmentModel.getEquipmentName();
            equipmentId = equipmentModel.getEquimentID();
            equipmentNumber = equipmentModel.getEquipmentCode();
            equipmentDetail = equipmentModel.getEquipmentDetails();

            edtEquipmentName.setText(equipmentName);
            edtEquipmentNumber.setText(equipmentNumber);
            edtEquipmentDetail.setText(equipmentDetail);
        }
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                equipmentName = edtEquipmentName.getText().toString().trim();
                equipmentNumber = edtEquipmentNumber.getText().toString().trim();
                equipmentDetail = edtEquipmentDetail.getText().toString().trim();
                if (TextUtils.isEmpty(edtEquipmentName.getText().toString().trim())) {
                    Utils.showToast("Please Enter Equipment Name", getContext());
                } else if (TextUtils.isEmpty(edtEquipmentDetail.getText().toString().trim())) {
                    Utils.showToast("Please Enter Equipment Detail", getContext());
                } else {
                    if (value == 0) {
                        new CallRequest(instance).addEquipment(emailId, token, equipmentName,equipmentNumber, equipmentDetail, userId);
                    } else if (value == 1) {
                        new CallRequest(instance).updateEquipment(emailId, token, equipmentName, equipmentNumber,equipmentDetail, userId, equipmentId);
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addEquipment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Add Equipment Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateEquipment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Equipment Update Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
