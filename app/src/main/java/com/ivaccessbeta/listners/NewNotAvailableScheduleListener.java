package com.ivaccessbeta.listners;

/**
 * Created by User on 19-04-2018.
 */

public interface NewNotAvailableScheduleListener {

    void onDeleteNotAvailableSchedule(int pos, String scheduleID);
    void onApproveNotAvailableSchedule(int pos, String scheduleID);
    void onDenyNotAvailableSchedule(int pos, String scheduleID);
    void onCopyToTomorrowNotAvailableSchedule(int pos, String scheduleID);
}
