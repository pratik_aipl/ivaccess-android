package com.ivaccessbeta.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.ivaccessbeta.App;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by admin on 10/11/2016.
 */
public class CallRequest {

    private static final String TAG = "CallRequest";
    //    public static String BASEURL="http://192.168.0.104/iv_access/";
//    public static String COMMON_URL = BASEURL+"web_services/version_1/web_services/";
    public static String BASEURL = "https://ivaccess.blenzabi.com/";
    public static String COMMON_URL = BASEURL + "web_services/version_1/web_services/";
    public App app;
    public Context ct;
    public Fragment ft;

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public void login(String emailId, String password, String PlayerID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "login");
        map.put("login", emailId);
        map.put("password", password);
        map.put("PlayerID", PlayerID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.login, Constant.POST_TYPE.POST, map);
    }


    public void forgotPassword(String emailForgot) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "forgot_password");
        map.put("email", emailForgot);
        new AsynchHttpRequest(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }

    public void updateJobStatus(String emailId, String token, String userId, String job_id, String status_id, String note) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_job_status");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", job_id);
        map.put("status_id", status_id);
        map.put("note", note);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateJobStatus, Constant.POST_TYPE.POST, map);
    }

    public void getProfileJobSchedule(String emailId, String token, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_job_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getProfileJobSchedule, Constant.POST_TYPE.POST, map);
    }

    public void getRegion(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_region_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getRegion, Constant.POST_TYPE.POST, map);
    }

    public void getRegionActivity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_region_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ct, Constant.REQUESTS.getRegion, Constant.POST_TYPE.POST, map);
    }

    public void documentApprove(String emailId, String token, String user_id, String doc_id, String status) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_doc_approve_deny");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("doc_id", doc_id);
        map.put("status", status);
        new AsynchHttpRequest(ft, Constant.REQUESTS.documentApprove, Constant.POST_TYPE.POST, map);
    }

    public void documentDeny(String emailId, String token, String user_id, String doc_id, String status) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_doc_approve_deny");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("doc_id", doc_id);
        map.put("status", status);
        new AsynchHttpRequest(ft, Constant.REQUESTS.documentDeny, Constant.POST_TYPE.POST, map);
    }

    public void documentDelete(String emailId, String token, String user_id, String doc_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_doc_delete");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("doc_id", doc_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.documentDelete, Constant.POST_TYPE.POST, map);
    }

    public void getNotificationUserList(String emailId, String token, String roll_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_notification_user_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("roll_id", roll_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getNotificationUserList, Constant.POST_TYPE.POST, map);
    }

    public void sendNotify(String emailId, String token, String user_id, String notification, String user_list) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "send_notification_user");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("notification", notification);
        map.put("user_list", user_list);
        new AsynchHttpRequest(ft, Constant.REQUESTS.sendNotify, Constant.POST_TYPE.POST, map);
    }

    public void get_job_filter(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_filter");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.get_job_filter, Constant.POST_TYPE.POST, map);
    }

    public void getUserProfile(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_profile");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getUserProfile, Constant.POST_TYPE.POST, map);
    }

    public void getEmpDoc(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getEmpDoc, Constant.POST_TYPE.POST, map);
    }

    public void editUserProfile(String emailId, String token, String user_id, String first_name, String last_name, String description, String profile_img) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "edit_user_profile");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("description", description);
        map.put("profile_img", profile_img);
        new AsynchHttpRequest(ft, Constant.REQUESTS.editUserProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getJobState(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_state_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobState, Constant.POST_TYPE.POST, map);
    }

    public void getJobStateActivity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_state_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ct, Constant.REQUESTS.getJobState, Constant.POST_TYPE.POST, map);
    }


    public void getRole(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_role_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getRole, Constant.POST_TYPE.POST, map);
    }

    public void getPlace(String emailId, String token, String regionId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_places_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("region_id", regionId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getPlace, Constant.POST_TYPE.POST, map);
    }


    public void getPlaceActivity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_places_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getPlaceActivity, Constant.POST_TYPE.POST, map);
    }

    public void getEquipment(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_equipment_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getEquipment, Constant.POST_TYPE.POST, map);
    }

    public void addJobNote(String emailId, String token, String userId, String jobId, String note) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_job_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);
        map.put("note", note);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addJobNote, Constant.POST_TYPE.POST, map);
    }

    public void addVoucherToCheck(String emailId, String token, String check_date, String check_user_id, String note, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_voucher_no");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("check_date", check_date);
        map.put("check_user_id", check_user_id);
        map.put("voucher_no", note);
        map.put("user_id", userId);


        new AsynchHttpRequest(ft, Constant.REQUESTS.addVoucherToCheck, Constant.POST_TYPE.POST, map);
    }

//    public void addJobNote(String emailId, String token, String userId, String jobNo, String note) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("url", COMMON_URL + "add_job_note");
//        map.put("login_email", emailId);
//        map.put("login_token", token);
//        map.put("user_id", userId);
//        map.put("job_id", jobNo);
//        map.put("notificationMessage", note);
//
//        new AsynchHttpRequest(ft, Constant.REQUESTS.addJobNote, Constant.POST_TYPE.POST, map);
//    }

    public void markArrivalTime(String emailId, String token, String user_id, String jobId, String time) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_mark_arrival");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("job_id", jobId);
        map.put("time", time);
        new AsynchHttpRequest(ft, Constant.REQUESTS.markArrivalTime, Constant.POST_TYPE.POST, map);
    }

    public void getJobStatus(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_status_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobStatus, Constant.POST_TYPE.POST, map);
    }

    public void getJobStatusActivity(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_status_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getJobStatus, Constant.POST_TYPE.POST, map);
    }

    public void getJobHistory(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_past_job_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobHistory, Constant.POST_TYPE.POST, map);
    }

    public void getProfileSchedule(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getProfileSchedule, Constant.POST_TYPE.POST, map);
    }

    public void getShortBy(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_sort_by_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getShortBy, Constant.POST_TYPE.POST, map);
    }

    public void getShortByActivity(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_sort_by_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getShortBy, Constant.POST_TYPE.POST, map);
    }

    public void getSaveData(String emailId, String token, String user_id, String filterData, String isClear, String isFirst) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_save_data");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("IsClear", isClear);
        map.put("FilterData", filterData);
        map.put("IsFirst", isFirst);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSaveData, Constant.POST_TYPE.POST, map);
    }

    public void getJobList(String emailId, String token, String user_id, String status, String region_id, String person_id,
                           String facility_id, String sort_by, String state) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_jobs_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("status", status);
        map.put("region_id", region_id);
        map.put("person_id", person_id);
        map.put("facility_id", facility_id);
        map.put("sort_by", sort_by);
        map.put("state", state);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobList, Constant.POST_TYPE.POST, map);
    }

    public void getJobListNo(String emailId, String token, String user_id, String billto_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_jobs_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("billto_id", billto_id);


        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobList, Constant.POST_TYPE.POST, map);
    }

    public void getJobListNoCheck(String emailId, String token, String user_id, String person_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_jobs_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("person_id", person_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobListCheck, Constant.POST_TYPE.POST, map);
    }

    public void getSchedualDetail(String emailId, String token, String date, String region_id, String person_id, String places_id, String not_available) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_single_day_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("date", date);
        map.put("region_id", region_id);
        map.put("person_id", person_id);
        map.put("places_id", places_id);
        map.put("not_available", not_available);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSchedualDetail, Constant.POST_TYPE.POST, map);
    }

    public void getJobDetail(String emailId, String token, String job_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_job_details");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_id", job_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getJobDetail, Constant.POST_TYPE.POST, map);
    }

    public void getService(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_service_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getService, Constant.POST_TYPE.POST, map);
    }

    public void getPushDevider(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_push_divider");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getPushDevider, Constant.POST_TYPE.POST, map);
    }

    public void getServiceActivity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_service_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getServiceActivity, Constant.POST_TYPE.POST, map);
    }

    public void getConractor(String emailId, String token, String place_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_contractor_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("login_token", token);
        map.put("places_id", place_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getContractor, Constant.POST_TYPE.POST, map);
    }

    public void getConractorActivityHome(String emailId, String token, String place_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_contractor_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("login_token", token);
        map.put("places_id", place_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getContractor, Constant.POST_TYPE.POST, map);
    }

    public void getConractorActivity(String emailId, String token, String place_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_contractor_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("places_id", place_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getContractorActivity, Constant.POST_TYPE.POST, map);
    }

    public void getFacility(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_facility_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getFacility, Constant.POST_TYPE.POST, map);
    }

    public void getFacilityActivity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_facility_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ct, Constant.REQUESTS.getFacilityActivity, Constant.POST_TYPE.POST, map);
    }

    public void getBillTo(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_bill_to_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getBillTo, Constant.POST_TYPE.POST, map);
    }

    public void getBillToActivity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_bill_to_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getBillToActivity, Constant.POST_TYPE.POST, map);
    }

    public void getPeople(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_people_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getPeople, Constant.POST_TYPE.POST, map);
    }

    public void getDocuments(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_document_list");
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getDocument, Constant.POST_TYPE.POST, map);
    }

    public void getSchedual(String emailId, String token, String date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_schedule_date");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("date", date);
        // date
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSchedual, Constant.POST_TYPE.POST, map);
    }

    public void jobSearch(String emailId, String token, String status, String region_id, String person_id, String facility_id, String sort_by) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_jobs_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("status", status);
        map.put("region_id", region_id);
        map.put("person_id", person_id);
        map.put("facility_id", facility_id);
        map.put("sort_by", sort_by);

        new AsynchHttpRequest(ft, Constant.REQUESTS.jobSearch, Constant.POST_TYPE.POST, map);
    }


    public void addRegion(String emailId, String token, String region_name, String color, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_region");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("region_name", region_name);
        map.put("color", color);
        map.put("user_id", user_id);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addRegion, Constant.POST_TYPE.POST, map);
    }

    public void updateRegion(String emailId, String token, String region_name, String color, String user_id, String region_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_region");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("region_name", region_name);
        map.put("color", color);
        map.put("user_id", user_id);
        map.put("region_id", region_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateRegion, Constant.POST_TYPE.POST, map);
    }

    public void addSchedule(String emailId, String token, String places_id, String from_date, String to_date, String from_time, String to_time, String contractor_id, String user_id, String admin_notes,
                            String contractor_notes, String scheduleType, String notAvailableSchedule) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("places_id", places_id);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        map.put("from_time", from_time);
        map.put("to_time", to_time);
        map.put("contractor_id", contractor_id);
        map.put("user_id", user_id);
        map.put("admin_notes", admin_notes);
        map.put("contractor_notes", contractor_notes);
        map.put("schedule_type", scheduleType);
        map.put("not_available_status ", notAvailableSchedule);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSchedule, Constant.POST_TYPE.POST, map);
    }

    public void addScheduleFragment(String emailId, String token, String places_id, String from_date, String to_date, String from_time, String to_time, String contractor_id, String user_id, String admin_notes,
                                    String contractor_notes, String scheduleType, String notAvailableSchedule) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("places_id", places_id);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        map.put("from_time", from_time);
        map.put("to_time", to_time);
        map.put("contractor_id", contractor_id);
        map.put("user_id", user_id);
        map.put("admin_notes", admin_notes);
        map.put("contractor_notes", contractor_notes);
        map.put("schedule_type", scheduleType);
        map.put("not_available_status", notAvailableSchedule);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addScheduleFragment, Constant.POST_TYPE.POST, map);
    }

    public void editScheduleFragment(String emailId, String token, String places_id, String from_date, String to_date, String from_time, String to_time, String contractor_id, String user_id, String admin_notes,
                                     String contractor_notes, String scheduleType, String notAvailableSchedule, String scheduleId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("places_id", places_id);
        map.put("from_date", from_date);
        map.put("to_date", to_date);
        map.put("from_time", from_time);
        map.put("to_time", to_time);
        map.put("contractor_id", contractor_id);
        map.put("user_id", user_id);
        map.put("admin_notes", admin_notes);
        map.put("contractor_notes", contractor_notes);
        map.put("schedule_type", scheduleType);
        map.put("not_available_status", notAvailableSchedule);
        map.put("schedule_id", scheduleId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.editScheduleFragment, Constant.POST_TYPE.POST, map);
    }

    public void addService(String emailId, String token, String service_name, String user_id, String equipment_id, String services_rate) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_service");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("service_name", service_name);
        map.put("user_id", user_id);
        map.put("equipment_id", equipment_id);
        map.put("services_rate", services_rate);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addService, Constant.POST_TYPE.POST, map);
    }

    public void updateService(String emailId, String token, String service_name, String user_id, String equipment_id, String service_id, String services_rate) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_service");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("service_name", service_name);
        map.put("user_id", user_id);
        map.put("equipment_id", equipment_id);
        map.put("service_id", service_id);
        map.put("services_rate", services_rate);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateService, Constant.POST_TYPE.POST, map);
    }

    public void addEquipment(String emailId, String token, String equipment_name, String equipmentNumber, String equipment_details, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_equipment");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("equipment_name", equipment_name);
        map.put("equipment_code", equipmentNumber);
        map.put("equipment_details", equipment_details);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addEquipment, Constant.POST_TYPE.POST, map);
    }

    public void updateEquipment(String emailId, String token, String equipment_name, String equipmentNumber, String equipment_details, String user_id, String equiment_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_equipment");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("equipment_name", equipment_name);
        map.put("equipment_code", equipmentNumber);
        map.put("equipment_details", equipment_details);
        map.put("user_id", user_id);
        map.put("equiment_id", equiment_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateEquipment, Constant.POST_TYPE.POST, map);
    }

    public void addPeople(String emailId, String token, String first_name,
                          String last_name, String region_id, String qb_company, String mobile_number, String alternate_number,
                          String emailPeople, String picc_suppiles, String mid_suppiles, String role_id, String facility_id, String user_id, String directory_note,
                          String password, String confirm_password, String activated) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_people");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("region_id", region_id);
        map.put("qb_company", qb_company);
        map.put("mobile_number", mobile_number);
        map.put("alternate_number", alternate_number);
        map.put("email", emailPeople);
        map.put("picc_suppiles", picc_suppiles);
        map.put("mid_suppiles", mid_suppiles);
        map.put("role_id", role_id);
        map.put("facility_id", facility_id);
        map.put("user_id", user_id);
        map.put("directory_note", directory_note);
        map.put("password", password);
        map.put("confirm_password", confirm_password);
        map.put("activated", activated);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addPeople, Constant.POST_TYPE.POST, map);
    }

    public void updatePeople(String emailId, String token, String first_name,
                             String last_name, String region_id, String qb_company, String mobile_number, String alternate_number,
                             String emailPeople, String picc_suppiles, String mid_suppiles, String role_id, String facility_id, String user_id, String people_id, String directory_note, String activated) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_people");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("region_id", region_id);
        map.put("qb_company", qb_company);
        map.put("mobile_number", mobile_number);
        map.put("alternate_number", alternate_number);
        map.put("email", emailPeople);
        map.put("picc_suppiles", picc_suppiles);
        map.put("mid_suppiles", mid_suppiles);
        map.put("role_id", role_id);
        map.put("facility_id", facility_id);
        map.put("user_id", user_id);
        map.put("people_id", people_id);
        map.put("directory_note", directory_note);
        map.put("activated", activated);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updatePeople, Constant.POST_TYPE.POST, map);
    }

    public void addBillTo(String emailId, String token, String name,
                          String qb_comapny, String phone, String address, String address2, String city,
                          String state, String zip, String CustomInvoiceInfo, String serviceRateList, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_bill_to");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("name", name);
        map.put("qb_comapny", qb_comapny);
        map.put("phone", phone);
        map.put("address", address);
        map.put("address2", address2);
        map.put("city", city);
        map.put("state", state);
        map.put("zip", zip);
        map.put("CustomInvoiceInfo", CustomInvoiceInfo);
        map.put("service", serviceRateList);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addBillTo, Constant.POST_TYPE.POST, map);
    }

    public void updateBillTo(String emailId, String token, String name,
                             String qb_comapny, String phone, String address, String address2, String city,
                             String state, String zip, String CustomInvoiceInfo, String serviceRateList,
                             String user_id, String bill_to_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_bill_to");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("name", name);
        map.put("qb_comapny", qb_comapny);
        map.put("phone", phone);
        map.put("address", address);
        map.put("address2", address2);
        map.put("city", city);
        map.put("state", state);
        map.put("zip", zip);
        map.put("CustomInvoiceInfo", CustomInvoiceInfo);
        map.put("service", serviceRateList);
        map.put("user_id", user_id);
        map.put("bill_to_id", bill_to_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateBillTo, Constant.POST_TYPE.POST, map);
    }

    public void addFacility(String emailId, String token, String facility_name, String region_id,
                            String bill_to, String phone, String address, String address2, String city,
                            String state, String zip, String user_id, String note, String iv_access_supply) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_facility");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("facility_name", facility_name);
        map.put("region_id", region_id);
        map.put("bill_to", bill_to);
        map.put("phone", phone);
        map.put("address", address);
        map.put("address2", address2);
        map.put("city", city);
        map.put("state", state);
        map.put("zip", zip);
        map.put("user_id", user_id);
        map.put("note", note);
        map.put("iv_access_supply", iv_access_supply);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addFacility, Constant.POST_TYPE.POST, map);
    }

    public void updateFacility(String emailId, String token, String facility_name, String region_id,
                               String bill_to, String phone, String address, String address2, String city,
                               String state, String zip, String user_id, String facility_id, String note, String iv_access_supply) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_facility");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("facility_name", facility_name);
        map.put("region_id", region_id);
        map.put("bill_to", bill_to);
        map.put("phone", phone);
        map.put("address", address);
        map.put("address2", address2);
        map.put("city", city);
        map.put("state", state);
        map.put("zip", zip);
        map.put("user_id", user_id);
        map.put("facility_id", facility_id);
        map.put("note", note);
        map.put("iv_access_supply", iv_access_supply);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateFacility, Constant.POST_TYPE.POST, map);
    }

    public void addDocuments(String emailId, String token, String document_name, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("document_name", document_name);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addDocuments, Constant.POST_TYPE.POST, map);
    }

    public void addJobDoc(String emailId, String token, String job_id, String user_id, String job_doc) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_job_doc");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_id", job_id);
        map.put("user_id", user_id);
        map.put("job_doc", job_doc);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addJobDoc, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addUserDoc(String emailId, String token, String document_id, String user_id, ArrayList<Image> imagesList, String expire_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_user_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("document_id", document_id);
        map.put("user_id", user_id);
        if (imagesList.size() > 1) {
            int k = 0;
            for (int i = 1; i < imagesList.size(); i++) {

                map.put("document_file[" + k + "]", imagesList.get(i).imagePath);
                k++;

            }
        }

        //map.put("document_file", document_file);
        map.put("expire_date", expire_date);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addUserDoc, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addUserDocActivity(String emailId, String token, String document_id, String user_id, String imagesList, String expire_date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_user_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("document_id", document_id);
        map.put("user_id", user_id);
        map.put("document_file", imagesList);


        //map.put("document_file", document_file);
        map.put("expire_date", expire_date);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addUserDoc, Constant.POST_TYPE.POST_WITH_IMAGE_MULTIPAL, map);
    }

    public void updateDocuments(String emailId, String token, String document_name, String user_id, String document_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("document_name", document_name);
        map.put("user_id", user_id);
        map.put("document_id", document_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateDocument, Constant.POST_TYPE.POST, map);
    }

    public void addPlace(String emailId, String token, String user_id, String region_id, String places_name) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_places");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("region_id", region_id);
        map.put("region_id", region_id);
        map.put("places_name", places_name);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addPlace, Constant.POST_TYPE.POST, map);
    }

    public void updatePlace(String emailId, String token, String user_id, String region_id, String places_name, String places_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_places");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("region_id", region_id);
        map.put("region_id", region_id);
        map.put("places_name", places_name);
        map.put("places_id", places_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updatePlace, Constant.POST_TYPE.POST, map);
    }

    public void deletePlace(String emailId, String token, String user_id, String region_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_places");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("places_id", region_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deletePlace, Constant.POST_TYPE.POST, map);
    }

    public void deleteRegion(String emailId, String token, String user_id, String region_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_region");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("region_id", region_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteRegion, Constant.POST_TYPE.POST, map);
    }

    public void deleteUserDoc(String emailId, String token, String user_id, String user_document_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_user_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_document_id", user_document_id);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteUserDoc, Constant.POST_TYPE.POST, map);
    }

    public void deleteJob(String emailId, String token, String job_id, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_job");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_id", job_id);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteJob, Constant.POST_TYPE.POST, map);
    }

    public void deleteEquipment(String emailId, String token, String user_id, String equiment_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_equipment");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("equiment_id", equiment_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteEquipment, Constant.POST_TYPE.POST, map);
    }

    public void deleteFacility(String emailId, String token, String user_id, String facility_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_facility");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("facility_id", facility_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteFacility, Constant.POST_TYPE.POST, map);
    }

    public void deleteDocument(String emailId, String token, String user_id, String facility_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_document");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("document_id", facility_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteDocument, Constant.POST_TYPE.POST, map);
    }

    public void deletePeople(String emailId, String token, String user_id, String people_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_people");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("people_id", people_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deletePeople, Constant.POST_TYPE.POST, map);
    }

    public void deleteBillTo(String emailId, String token, String user_id, String bill_to_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_bill_to");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("bill_to_id", bill_to_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteBillTo, Constant.POST_TYPE.POST, map);
    }

    public void deleteService(String emailId, String token, String user_id, String service_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_service");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("service_id", service_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteService, Constant.POST_TYPE.POST, map);
    }

    public void updateJob(String jobID, String candidateID, String rankId, String shipId, String jobTitle, String experience, String joinDate,
                          String leavingDate, String description, String isFeatured, String isShore, String isFront, String update) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_update_job");
        map.put("job_id", jobID);
        map.put("company_id", candidateID);
        map.put("rank_id", rankId);
        map.put("ship_id", shipId);
        map.put("job_title", jobTitle);
        map.put("experience", experience);
        map.put("job_start_date", joinDate);
        map.put("job_end_date", leavingDate);
        map.put("description", description);
        map.put("is_featured", isFeatured);
        map.put("is_shore", isShore);
        map.put("is_front", isFront);
        map.put("update", update);

        new AsynchHttpRequest(ft, Constant.REQUESTS.updateJob, Constant.POST_TYPE.POST, map);
    }

    public void addJob(String emailId, String token, String facility_id, String service_id, String status_id,
                       String bill_to_id, String patient_name,
                       String room_number, String dob, String due_time_date, String notes,
                       String assign_to, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_job");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("facility_id", facility_id);
        map.put("service_id", service_id);
        map.put("status_id", status_id);
        map.put("bill_to_id", bill_to_id);
        map.put("patient_name", patient_name);
        map.put("room_number", room_number);
        map.put("dob", dob);
        map.put("due_time_date", due_time_date);
        map.put("notes", notes);
        map.put("assign_to", assign_to);
        map.put("user_id", user_id);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addJob, Constant.POST_TYPE.POST, map);
    }

    public void editJob(String emailId, String token, String facility_id, String service_id, String status_id, String bill_to_id, String patient_name,
                        String room_number, String dob, String due_time_date, String notes, String assign_to, String user_id, String job_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_job");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("facility_id", facility_id);
        map.put("service_id", service_id);
        map.put("bill_to_id", bill_to_id);
        map.put("status_id", status_id);
        map.put("patient_name", patient_name);
        map.put("room_number", room_number);
        map.put("dob", dob);
        map.put("due_time_date", due_time_date);
        map.put("notes", notes);
        map.put("assign_to", assign_to);
        map.put("user_id", user_id);
        map.put("job_id", job_id);

        new AsynchHttpRequest(ft, Constant.REQUESTS.editJob, Constant.POST_TYPE.POST, map);
    }

    public void getDirectortyByName(String emailId, String token, String sort_by) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_directory_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("sort_by", sort_by);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getDirectoryByName, Constant.POST_TYPE.POST, map);
    }

    public void deleteSchedule(String emailId, String token, String user_id, String schedule_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_schedule");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("schedule_id", schedule_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteSchedule, Constant.POST_TYPE.POST, map);
    }

    public void sendJobNotification(String emailId, String token, String user_id, String job_id, String notification) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_job_notification");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("job_id", job_id);
        map.put("notification", notification);

        new AsynchHttpRequest(ft, Constant.REQUESTS.sendJobNotification, Constant.POST_TYPE.POST, map);

    }

    public void approveDenyNoteAvailabeSchedule(String emailId, String token, String userId, String scheduleID, String status) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "schedule_not_available_approve_deny");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("schedule_id", scheduleID);
        map.put("status", status);
        new AsynchHttpRequest(ft, Constant.REQUESTS.approveDenyNotAvailableSchedule, Constant.POST_TYPE.POST, map);
    }

    public void getFacilityProfile(String emailId, String token, String user_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_facility");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getUserFacility, Constant.POST_TYPE.POST, map);
    }

    public void getNotification(String emailId, String token, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_notification");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getNotification, Constant.POST_TYPE.POST, map);

    }

    public void getMenuEquipment(String emailId, String token, String persons) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_equipment_order");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("persons", persons);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getMenuEquipment, Constant.POST_TYPE.POST, map);
    }

    public void getEquipmentQuantity(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_equipment_order_quantity");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getEquipmentQuentity, Constant.POST_TYPE.POST, map);
    }

    public void updatePushDevider(String emailId, String token, String newPushDeviderDate, String lastDateOfList) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_push_divider");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("PushDivider", newPushDeviderDate);
        map.put("LastDate", lastDateOfList);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updatePushDevider, Constant.POST_TYPE.POST, map);
    }

    public void addUpdateEquipmentOrder(String emailId, String token, String userId, String equipmentItem, String contractor, String quentity, String note) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_edit_equipment_order");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("EquimentID", equipmentItem);
        map.put("ContractorID", contractor);
        map.put("Quantity", quentity);
        map.put("Notes", note);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addUpdateEquipmentOrder, Constant.POST_TYPE.POST, map);
    }

    public void publishPushDivider(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "publish_push_divider");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.upblishPushDivider, Constant.POST_TYPE.POST, map);
    }

    public void chnageEquipmentStatus(String emailId, String token, String user_id, String EquimentOrderID, String status) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "equipment_order_status");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", user_id);
        map.put("EquimentOrderID", EquimentOrderID);
        map.put("Status", status);
        new AsynchHttpRequest(ft, Constant.REQUESTS.changeEquipmentStatus, Constant.POST_TYPE.POST, map);
    }

    public void updateEquipmentQTY(String emailId, String token, String status, String job_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "update_job_equipment_quantity");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("status", status);
        map.put("job_id", job_id);
        new AsynchHttpRequest(ft, Constant.REQUESTS.updateEquipmentQTY, Constant.POST_TYPE.POST, map);
    }

    public void getContractorEquipmentQTY(String emailId, String token, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_contractor_equipment_quantity");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("contractor_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getContractorEquipmentQTY, Constant.POST_TYPE.POST, map);
    }

    public void jobCheckApprove(String emailId, String token, String userId, String jobId, String checkStatus) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_check_approve");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);
        map.put("check", checkStatus);
        new AsynchHttpRequest(ft, Constant.REQUESTS.jobCheckApprove, Constant.POST_TYPE.POST, map);

    }

    public void getCheckList(String emailId, String token, String persons) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_checks_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("persons", persons);
        Log.d(TAG, "getCheckList: " + map.toString());
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCheckList, Constant.POST_TYPE.POST, map);

    }

    public void getInvoiceList(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_invoice_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getInvoiceList, Constant.POST_TYPE.POST, map);

    }

    public void getBillToServiceList(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_bill_to_service");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getBillToServiceList, Constant.POST_TYPE.POST, map);


    }

    public void deleteAllCheck(String emailId, String token, String userId, String checkDate) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_check_all");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("contractor_id", userId);
        map.put("date", checkDate);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteAllCheck, Constant.POST_TYPE.POST, map);
    }

    public void deletePerticularCheck(String emailId, String token, String checkID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_check");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("CheckID", checkID);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteCheck, Constant.POST_TYPE.POST, map);
    }

    public void jobBillToApprove(String emailId, String token, String userId, String jobId, String checkStatus) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_bill_approve");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);
        map.put("bill", checkStatus);
        new AsynchHttpRequest(ft, Constant.REQUESTS.billToApprove, Constant.POST_TYPE.POST, map);
    }

    public void deleteAllInvoice(String emailId, String token, String invoiceNo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_invoice_all");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("InvoiceNo", invoiceNo);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteAllInvoice, Constant.POST_TYPE.POST, map);
    }

    public void deletePerticularInvoice(String emailId, String token, String invoiceID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_invoice");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("InvoiceID", invoiceID);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteInvoice, Constant.POST_TYPE.POST, map);
    }

    public void invoice_check_mark(String emailId, String token, String invoiceID, String IsCheckMark) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "invoice_check_mark");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("InvoiceID", invoiceID);
        map.put("IsCheckMark", IsCheckMark);

        new AsynchHttpRequest(ft, Constant.REQUESTS.invoice_check_mark, Constant.POST_TYPE.POST, map);
    }

    public void getInvoiceContractor(String emailId, String token, String period) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_invoice_customer_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("period", period);

        new AsynchHttpRequest(ft, Constant.REQUESTS.getInvoiceContractorList, Constant.POST_TYPE.POST, map);

    }

    public void getCheckContractor(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_check_customer_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getCheckContractorList, Constant.POST_TYPE.POST, map);
    }

    public void addCheckAmount(String emailId, String token, String userId, String jobId, String amountCheck, String note) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_check_custom_amount");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);
        map.put("amount", amountCheck);
        map.put("note", note);


        new AsynchHttpRequest(ft, Constant.REQUESTS.addCheckCustomAmount, Constant.POST_TYPE.POST, map);

    }

    public void addInvoiceAmount(String emailId, String token, String userId, String jobId, String amountCheck, String note) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_invoice_custom_amount");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);
        map.put("amount", amountCheck);
        map.put("note", note);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addInvoiceCustomAmount, Constant.POST_TYPE.POST, map);
    }


    public void addCustomMemoCheck(String emailId, String token, String memoAmount, String contractorId, String date, String memoNote, String userId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_custom_memo_check");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("amount", memoAmount);
        map.put("contractor_id", contractorId);
        map.put("date", date);
        map.put("note", memoNote);
        map.put("user_id", userId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addCustomMemoCheck, Constant.POST_TYPE.POST, map);
    }

    public void addCustomMemoInvoice(String emailId, String token, String amount, String billToId, String invoiceNo, String date, String note, String userId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_custom_memo_invoice");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("amount", amount);
        map.put("BillToID", billToId);
        map.put("InvoiceNo", invoiceNo);
        map.put("date", date);
        map.put("note", note);
        map.put("user_id", userId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addCustomMemoInvioce, Constant.POST_TYPE.POST, map);
    }

    public void addJobToCheck(String emailId, String token, String jobNo, String checkDate, String userId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_job_check");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_no", jobNo);
        map.put("date", checkDate);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addJobToCheck, Constant.POST_TYPE.POST, map);

    }

    public void addJobToInvoice(String emailId, String token, String jobNo, String invoiceNo, String checkDate, String userId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_job_invoice");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_no", jobNo);
        map.put("invoice_no", invoiceNo);
        map.put("date", checkDate);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addJobToInvoice, Constant.POST_TYPE.POST, map);

    }

    public void addNoteToInvoice(String emailId, String token, String invoiceNo, String note, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_invoice_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("invoice_no", invoiceNo);
        map.put("note", note);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addNoteToInvoice, Constant.POST_TYPE.POST, map);
    }

    public void addPaymentNoteToInvoice(String emailId, String token, String invoiceNo, String note, String refNo, String amount, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_invoice_payment_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("invoice_no", invoiceNo);
        map.put("note", note);
        map.put("ref_no", refNo);
        map.put("amount", amount);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addPaymentNoteToInvoice, Constant.POST_TYPE.POST, map);
    }

    public void addPaymentNoteToCheck(String emailId, String token, String check_date, String check_user_id, String note, String refNo, String amount, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_check_payment_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("check_date", check_date);
        map.put("check_user_id", check_user_id);
        map.put("note", note);
        map.put("ref_no", refNo);
        map.put("amount", amount);
        map.put("user_id", userId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addPaymentNoteToCheck, Constant.POST_TYPE.POST, map);
    }

    public void deleteInvoiceNote(String emailId, String token, String invoiceNoteID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_invoice_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("InvoiceNoteID", invoiceNoteID);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteInvoiceNote, Constant.POST_TYPE.POST, map);
    }

    public void deleteInvoicePaymentNote(String emailId, String token, String invoicePaymentNoteID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_invoice_payment_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("InvoicePaymentNoteID", invoicePaymentNoteID);
        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteInvoicePaymentNote, Constant.POST_TYPE.POST, map);
    }

    public void chnagePasswor(String emailId, String token, String userId, String oldPassword, String newPassword, String confirmPassword) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "change_password");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("old_password", oldPassword);
        map.put("new_password", newPassword);
        map.put("confirm_new_password", confirmPassword);
        new AsynchHttpRequest(ct, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void chnagePassworFragment(String emailId, String token, String userId, String oldPassword, String newPassword, String confirmPassword) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "change_password");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("old_password", oldPassword);
        map.put("new_password", newPassword);
        map.put("confirm_new_password", confirmPassword);
        new AsynchHttpRequest(ft, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void changeUserDOCStatus(String emailId, String token, String userId, String deid, String status) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "user_document_status_change");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("user_document_id", deid);
        map.put("status", status);

        new AsynchHttpRequest(ft, Constant.REQUESTS.changeUserDocStatus, Constant.POST_TYPE.POST, map);
    }

    public void addNoteToCheck(String emailId, String token, String check_date, String check_user_id, String note, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_check_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("check_date", check_date);
        map.put("check_user_id", check_user_id);
        map.put("note", note);
        map.put("user_id", userId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.addNoteToCheck, Constant.POST_TYPE.POST, map);
    }

    public void deleteCheckPaymentNote(String emailId, String token, String CheckPaymentNoteID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_check_payment_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("CheckPaymentNoteID", CheckPaymentNoteID);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteCheckPaymentNote, Constant.POST_TYPE.POST, map);

    }

    public void deleteCheckNote(String emailId, String token, String checkNoteID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "delete_check_note");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("CheckNoteID", checkNoteID);

        new AsynchHttpRequest(ft, Constant.REQUESTS.deleteCheckNote, Constant.POST_TYPE.POST, map);
    }

    public void billingDate(String emailId, String token, String userId, String jobId, String date) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_blling_date_job");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);
        map.put("date", date);

        new AsynchHttpRequest(ft, Constant.REQUESTS.billingDate, Constant.POST_TYPE.POST, map);
    }

    public void approvDocStatus(String emailId, String token, String userId, String jobId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_doc_approve");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.approveDocumentStatus, Constant.POST_TYPE.POST, map);
    }

    public void denyDocStatus(String emailId, String token, String userId, String jobId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_doc_dispprove");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("job_id", jobId);

        new AsynchHttpRequest(ft, Constant.REQUESTS.denyDocumentStatus, Constant.POST_TYPE.POST, map);
    }

    public void printDocument(String emailId, String token, String jobDocId, String user_id, String s) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_doc_added_print");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_doc_id", jobDocId);
        map.put("user_id", user_id);
        map.put("status", s);

        new AsynchHttpRequest(ft, Constant.REQUESTS.printDocument, Constant.POST_TYPE.POST, map);
    }

    public void copyToSchedule(String emailId, String token, String user_id, String schedule_id, String copy) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "schedule_copy");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("schedule_id", schedule_id);
        map.put("user_id", user_id);
        map.put("copy", copy);

        new AsynchHttpRequest(ft, Constant.REQUESTS.copyToSchedule, Constant.POST_TYPE.POST, map);
    }

    public void addCheckToQuickBook(String emailId, String token, String checkContractorId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_check_quickbook");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("CustomerID", checkContractorId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addCheckQuickBook, Constant.POST_TYPE.POST, map);
    }

    public void addInvoiceToQuickBook(String emailId, String token, String invoiceContractorId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "add_invoice_quickbook");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("CustomerID", invoiceContractorId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addInvoiceToQuickBook, Constant.POST_TYPE.POST, map);
    }

    public void qbCompany(String emailId, String token) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "quickbook_company");
        map.put("login_email", emailId);
        map.put("login_token", token);
        new AsynchHttpRequest(ft, Constant.REQUESTS.qbCompanyList, Constant.POST_TYPE.POST, map);

    }

    public void viewNotification(String emailId, String token, String userId, String notificationID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "view_user_notification");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("NotificationID", notificationID);
        new AsynchHttpRequest(ft, Constant.REQUESTS.viewNotification, Constant.POST_TYPE.POST, map);
    }

    public void reCreateInvoice(String emailId, String token, String userId, String invoiceNo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "create_invoice/" + invoiceNo);
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.createInvoice, Constant.POST_TYPE.POST, map);
    }

    public void printJob(String emailId, String token, String jobId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "print_job/" + jobId);
        map.put("login_email", emailId);
        map.put("login_token", token);

        new AsynchHttpRequest(ft, Constant.REQUESTS.printJob, Constant.POST_TYPE.POST, map);
    }

    public void Archive(String emailId, String token, String jobId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_archive");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_id", jobId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.Archive, Constant.POST_TYPE.POST, map);
    }

    public void DeArchive(String emailId, String token, String jobId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "job_dearchive");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("job_id", jobId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.DeArchive, Constant.POST_TYPE.POST, map);
    }

    public void getNewSchedule(String emailId, String token, String date, String region_id, String person_id, String places_id, String not_available) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_schedule_date_new");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("date", date);
        map.put("region_id", region_id);
        map.put("person_id", person_id);
        map.put("places_id", places_id);
        map.put("not_available", not_available);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSchedual, Constant.POST_TYPE.POST, map);
    }

    public void addMessageGroup(String emailId, String token, String userId, String groupTitle, String notification, String type, String people) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "new_chat");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("GroupName", groupTitle);
        map.put("notification", notification);
        map.put("type", type);
        map.put("people", people);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addMessage, Constant.POST_TYPE.POST, map);
    }

    public void addMessage(String emailId, String token, String userId, String notification, String type, String people) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "new_chat");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("notification", notification);
        map.put("type", type);
        map.put("people", people);
        new AsynchHttpRequest(ft, Constant.REQUESTS.addMessage, Constant.POST_TYPE.POST, map);
    }

    public void getChat(String emailId, String token, String userId, String ThreadID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "view_chat");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("ThreadID", ThreadID);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getChatList, Constant.POST_TYPE.POST, map);
    }


    public void getChatHistory(String emailId, String token, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "chat_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.getChatHistory, Constant.POST_TYPE.POST, map);
    }

    public void getChatHistoryActivity(String emailId, String token, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "chat_list");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getChatHistory, Constant.POST_TYPE.POST, map);
    }

    public void replayMessage(String emailId, String token, String userId, String ThreadID, String message) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "send_chat");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        map.put("ThreadID", ThreadID);
        map.put("Message", message);
        new AsynchHttpRequest(ft, Constant.REQUESTS.replyMessage, Constant.POST_TYPE.POST, map);
    }

    public void get_user_pendding_count_notification(String emailId, String token, String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "get_user_pendding_count_notification");
        map.put("login_email", emailId);
        map.put("login_token", token);
        map.put("user_id", userId);
        new AsynchHttpRequest(ct, Constant.REQUESTS.get_user_pendding_count_notification, Constant.POST_TYPE.POST, map);
    }

}
