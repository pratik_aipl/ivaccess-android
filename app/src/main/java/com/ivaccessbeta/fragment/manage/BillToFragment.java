package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.BillToAdapter;
import com.ivaccessbeta.model.BillToModel;
import com.ivaccessbeta.model.BillToServiceModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class BillToFragment extends Fragment implements AsynchTaskListner {
    public RecyclerView rvBillTo;
    public BillToAdapter mAdapter;

    public ArrayList<BillToModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static BillToFragment instance;
    public EditText edtSearch;
    public View view;
    public BillToModel billToModel;
    public BillToServiceModel billToServiceModel;
    public String emailId, token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_bill_to, container, false);
        instance = this;
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        setHasOptionsMenu(true);
        rvBillTo = view.findViewById(R.id.rvBillTo);
        relativeAdd = view.findViewById(R.id.relative_add);

        relativeAdd.setOnClickListener(view1 -> {
            AddBillToFragment addBillToFragment = new AddBillToFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("value", 0);
            addBillToFragment.setArguments(bundle);
            ((MainActivity)getActivity()).changeFragment(addBillToFragment, true);
        });

        edtSearch = view.findViewById(R.id.edt_search);


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }
            }
        });

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getBillList();
    }

    private void getBillList() {
        list.clear();
        new CallRequest(instance).getBillTo(emailId, token);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getBillTo:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                billToModel = new BillToModel();
                                billToModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), BillToModel.class);
                                //  list.add(billToModel);

                                JSONArray jArrayLog = jObject.getJSONObject(i).getJSONArray("service");
                                for (int j = 0; jArrayLog.length() > j; j++) {
                                    BillToServiceModel billToServiceObj = new BillToServiceModel();
                                    billToServiceObj = LoganSquare.parse(jArrayLog.getJSONObject(j).toString(), BillToServiceModel.class);
                                    billToModel.billToServiceList.add(billToServiceObj);
                                }
                                list.add(billToModel);
                            }
                            mAdapter = new BillToAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvBillTo.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvBillTo.setLayoutManager(mLayoutManager);
                            rvBillTo.setItemAnimator(new DefaultItemAnimator());
                            rvBillTo.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();


                            Utils.longInfo("Bill to list", result);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteBillTo:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Bill Delete Successfully", getContext());
                            getBillList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);

    }
}