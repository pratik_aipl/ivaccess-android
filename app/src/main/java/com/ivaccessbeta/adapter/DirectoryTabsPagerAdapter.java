package com.ivaccessbeta.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ivaccessbeta.fragment.Directory.ByNameFragment;
import com.ivaccessbeta.fragment.Directory.ByRegionFragment;
import com.ivaccessbeta.fragment.schedule.AddNotAvailableFragment;
import com.ivaccessbeta.fragment.schedule.AddSchedualFragment;

/**
 * Created by User on 17-04-2018.
 */

public class DirectoryTabsPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public DirectoryTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                ByRegionFragment tab1 = new ByRegionFragment();
                return tab1;
            case 1:
                ByNameFragment tab2 = new ByNameFragment();
                return tab2;

        }
        return null;

    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 2;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 2; //No of Tabs
    }


}
