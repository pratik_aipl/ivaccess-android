package com.ivaccessbeta.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.schedule.EditNotAvailableScheduleFragment;
import com.ivaccessbeta.fragment.schedule.SchedualDetailFragment;
import com.ivaccessbeta.listners.NotAvailableScheduleListener;
import com.ivaccessbeta.model.ScheduleDetailModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import java.util.ArrayList;

/**
 * Created by User on 17-04-2018.
 */

public class NotAvailableScheduleAdapter extends RecyclerView.Adapter<NotAvailableScheduleAdapter.MyViewHolder> {

    public ArrayList<ScheduleDetailModel> list;
    //public List<ScheduleDetailModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId, roleId, scheduleStatus;
    public ScheduleDetailModel model;
    public SchedualDetailFragment schedualNADetailFragment;
    public NotAvailableScheduleListener scheduleNAListener;
    public TextView txtApprove, txtDeny, txtTrash, txtCopyToTomorrow;

    @Override
    public NotAvailableScheduleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_not_available_schedule, parent, false);
        return new NotAvailableScheduleAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(NotAvailableScheduleAdapter.MyViewHolder holder, final int position) {
        roleId = MySharedPref.getString(context, App.ROLEID, "");
        model = list.get(position);
        holder.txtFacliterName.setText(model.getFirst_name() + " " + model.getLast_name());
        holder.txtSubmitedBy.setText("by: " + model.getSubmittedBy());
        String cnote = model.getContractorNotes();
        String anote = model.getAdminNotes();
        Utils.dateConvertFormate(holder.txtSubmitedDate, model.getCreatedOn(), "submited: ");

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            holder.v.setVisibility(View.VISIBLE);
            holder.txtNote.setVisibility(View.VISIBLE);
            holder.txtAdminNote.setVisibility(View.GONE);
            holder.txtNote.setText("Note : " + cnote);
            if (!TextUtils.isEmpty(cnote)) {
                holder.txtNote.setText("Note : " + cnote);
                holder.txtNote.setVisibility(View.VISIBLE);
                holder.v.setVisibility(View.VISIBLE);
            } else {
                holder.txtNote.setVisibility(View.GONE);
                holder.v.setVisibility(View.GONE);
            }
        } else if (roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID)) {
            holder.v.setVisibility(View.VISIBLE);
            holder.txtNote.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(anote)) {
                holder.txtAdminNote.setText("Note : " + anote);
                holder.txtAdminNote.setVisibility(View.VISIBLE);
                holder.v.setVisibility(View.VISIBLE);
            } else {
                holder.txtAdminNote.setVisibility(View.GONE);
                holder.v.setVisibility(View.GONE);
            }
        } else {
            holder.txtAdminNote.setVisibility(View.GONE);
            holder.txtNote.setVisibility(View.GONE);
            holder.v.setVisibility(View.GONE);
        }
//        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clickeddddddddddd");
                EditNotAvailableScheduleFragment fragment = new EditNotAvailableScheduleFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("not_available_schedule", list.get(position));
                fragment.setArguments(bundle);
                ((MainActivity) context).changeFragment(fragment, true);
            }
        });
        scheduleStatus = model.getNotAvailableStatus();
        if (model.getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_APPROVE)) {
            holder.relativeSchedule.setBackgroundColor(Color.parseColor("#c6f7c6"));
        } else if (model.getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_DENY)) {
            holder.relativeSchedule.setBackgroundColor(Color.parseColor("#e41c1c"));
        } else {
            holder.relativeSchedule.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        holder.ivAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                showSortPopup(schedualNADetailFragment.getActivity(), x, y, position);
            }
        });

        holder.txtTimeFrom.setText(model.getFromTime() + " To ");
        holder.txtTimeTo.setText(model.getToTime());

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public NotAvailableScheduleAdapter(ArrayList<ScheduleDetailModel> List, SchedualDetailFragment schedualNADetailFragment) {
        this.list = List;
        //this.searchFilterList.addAll(listSchedule);
        this.schedualNADetailFragment = schedualNADetailFragment;
        this.context = schedualNADetailFragment.getActivity();
        this.scheduleNAListener = schedualNADetailFragment;
    }

    private void showSortPopup(final Activity context, int x, int y, final int position) {

        LinearLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_not_available, viewGroup);
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);

        txtApprove = layout.findViewById(R.id.txtApprove);
        txtDeny = layout.findViewById(R.id.txtDeny);
        txtTrash = layout.findViewById(R.id.txtTrash);
        txtCopyToTomorrow = layout.findViewById(R.id.txtCopyToTomorrow);
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtApprove.setVisibility(View.GONE);
            txtDeny.setVisibility(View.GONE);
        } else {
            if (list.get(position).getNotAvailableStatus().equals(Constant.NA_SCHEDULE_APPROVE)) {
                txtApprove.setVisibility(View.GONE);
                txtDeny.setVisibility(View.GONE);
            } else if (list.get(position).getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_DENY)) {
                txtDeny.setVisibility(View.GONE);
                txtApprove.setVisibility(View.GONE);
            } else if (list.get(position).getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_PENDING)) {
                txtApprove.setVisibility(View.VISIBLE);
                txtDeny.setVisibility(View.VISIBLE);
                txtTrash.setVisibility(View.VISIBLE);
            }
        }

        txtApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleNAListener.onApproveNotAvailableSchedule(position);

            }
        });
        txtDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleNAListener.onDenyNotAvailableSchedule(position);
            }
        });

        txtTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleNAListener.onDeleteNotAvailableSchedule(position);
            }
        });
        txtCopyToTomorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleNAListener.onCopyToTomorrowNotAvailableSchedule(position);
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtFacliterName, txtTimeFrom, txtTimeTo, txtSubmitedDate, txtSubmitedBy, txtNote, txtAdminNote;
        ImageView ivAction;
        RelativeLayout relativeSchedule;
        View v;

        public MyViewHolder(View view) {
            super(view);
            txtFacliterName = view.findViewById(R.id.txtContractorName);
            txtTimeFrom = view.findViewById(R.id.txtTimeFrom);
            txtTimeTo = view.findViewById(R.id.txtTimeTo);
            relativeSchedule = view.findViewById(R.id.relativeSchedule);
            ivAction = view.findViewById(R.id.ivAction);
            txtSubmitedDate = view.findViewById(R.id.txtSubmitedDate);
            txtSubmitedBy = view.findViewById(R.id.txtSubmitedBy);
            txtNote = view.findViewById(R.id.txtNote);
            txtAdminNote = view.findViewById(R.id.txtAdminNote);
            v = view.findViewById(R.id.view);
        }


    }


}
