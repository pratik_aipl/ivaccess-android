package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 01-03-2018.
 */
@JsonObject
public class BillToModel implements Serializable {
    @JsonField
    public String BillToID, Name, QBCompany, Phone, Address, Address2, City, State, Zip, CustomInvoiceInfo, PICCRate,
            MidlineRate, PIVRate, DCRate, CathfloRate, ConsultRate, PostAccessRate, EducationRate, PowerGlideRate,
            PIVWUltrasoundRate, PICC3LumRate, PICC2LumRate;

    public ArrayList<BillToServiceModel> billToServiceList = new ArrayList<BillToServiceModel>();

    public String getBillToID() {
        return BillToID;
    }

    public void setBillToID(String billToID) {
        BillToID = billToID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getQBCompany() {
        return QBCompany;
    }

    public void setQBCompany(String QBCompany) {
        this.QBCompany = QBCompany;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getPICC2LumRate() {
        return PICC2LumRate;
    }

    public void setPICC2LumRate(String PICC2LumRate) {
        this.PICC2LumRate = PICC2LumRate;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getCustomInvoiceInfo() {
        return CustomInvoiceInfo;
    }

    public void setCustomInvoiceInfo(String customInvoiceInfo) {
        CustomInvoiceInfo = customInvoiceInfo;
    }

    public String getPICCRate() {
        return PICCRate;
    }

    public void setPICCRate(String PICCRate) {
        this.PICCRate = PICCRate;
    }

    public String getMidlineRate() {
        return MidlineRate;
    }

    public void setMidlineRate(String midlineRate) {
        MidlineRate = midlineRate;
    }

    public String getPIVRate() {
        return PIVRate;
    }

    public void setPIVRate(String PIVRate) {
        this.PIVRate = PIVRate;
    }

    public String getDCRate() {
        return DCRate;
    }

    public void setDCRate(String DCRate) {
        this.DCRate = DCRate;
    }

    public String getCathfloRate() {
        return CathfloRate;
    }

    public void setCathfloRate(String cathfloRate) {
        CathfloRate = cathfloRate;
    }

    public String getConsultRate() {
        return ConsultRate;
    }

    public void setConsultRate(String consultRate) {
        ConsultRate = consultRate;
    }

    public String getPostAccessRate() {
        return PostAccessRate;
    }

    public void setPostAccessRate(String postAccessRate) {
        PostAccessRate = postAccessRate;
    }

    public String getEducationRate() {
        return EducationRate;
    }

    public void setEducationRate(String educationRate) {
        EducationRate = educationRate;
    }

    public String getPowerGlideRate() {
        return PowerGlideRate;
    }

    public void setPowerGlideRate(String powerGlideRate) {
        PowerGlideRate = powerGlideRate;
    }

    public String getPIVWUltrasoundRate() {
        return PIVWUltrasoundRate;
    }

    public void setPIVWUltrasoundRate(String PIVWUltrasoundRate) {
        this.PIVWUltrasoundRate = PIVWUltrasoundRate;
    }

    public String getPICC3LumRate() {
        return PICC3LumRate;
    }

    public void setPICC3LumRate(String PICC3LumRate) {
        this.PICC3LumRate = PICC3LumRate;
    }

    public ArrayList<BillToServiceModel> getBillToServiceList() {
        return billToServiceList;
    }

    public void setBillToServiceList(ArrayList<BillToServiceModel> billToServiceList) {
        this.billToServiceList = billToServiceList;
    }
}
