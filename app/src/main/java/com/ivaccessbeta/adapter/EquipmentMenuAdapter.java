package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.model.EquipmentMenuModel;
import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.equipment.EquipmentMenuFragment;
import com.ivaccessbeta.listners.EquipmentListener;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 14-04-2018.
 */

public class EquipmentMenuAdapter extends RecyclerView.Adapter<EquipmentMenuAdapter.MyViewHolder> {
    public EquipmentListener equipmentListener;
    public EquipmentMenuFragment equipmentMenuFragment;
    public ArrayList<EquipmentMenuModel> list;
    public Context context;

    @Override
    public EquipmentMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.layout_equipment_menu_list, parent, false);

        return new EquipmentMenuAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EquipmentMenuAdapter.MyViewHolder holder, final int position) {
        final EquipmentMenuModel model = list.get(position);
        holder.txtName.setText(model.getFullName());
        holder.txtQuntity.setText(model.getQuantity());
        holder.txtOrderStatus.setText(model.getEquipmentOrderStatusName());

        String roleId = MySharedPref.getString(context, App.ROLEID, "");
        String equipmentName = "";
        if (!roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID) && !roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            equipmentName = model.getEquipmentName() + " " + model.getEquipmentCode();
        } else {
            equipmentName = model.getEquipmentName();
        }

        holder.txtEquipmetName.setText(equipmentName);
        holder.txtNotes.setText(model.getNotes());
        if (!TextUtils.isEmpty(model.getEquipmentOrderStatusColor()))
            holder.rlMain.setBackgroundColor(Color.parseColor(model.getEquipmentOrderStatusColor()));
        if (!TextUtils.isEmpty(model.getTotal())) {
            int total = Integer.parseInt(model.getTotal())/* * 3*/;
            holder.txtTotalQty.setText("" + total);
        } else {
            holder.txtTotalQty.setText("");
        }


        if (TextUtils.isEmpty(model.getNotes())) {
            holder.txtNotes.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(model.getTotal())) {
            holder.txtTotalQty.setVisibility(View.GONE);
        }
        holder.ivAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                equipmentListener.onOpenActionDialog(position, v);
            }
        });
        if (!TextUtils.isEmpty(model.getStatus())) {
            if (model.getStatus().equalsIgnoreCase(Constant.EQUIPMENT_PENDING)) {
                holder.txtOrderStatus.setText("");
            }
            if (model.getStatus().equalsIgnoreCase("5")) {
                holder.ivAction.setVisibility(View.GONE);
                holder.txtOrderStatus.setVisibility(View.GONE);
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (!TextUtils.isEmpty(model.getCreatedOn())) {
            Date date = null;
            try {
                date = sdf.parse(model.getCreatedOn().trim());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat formatDate = new SimpleDateFormat("dd");
            DateFormat formatMonth = new SimpleDateFormat("MM");
            DateFormat formatAmPm = new SimpleDateFormat("hh a");
            String finalDate = formatDate.format(date);
            String finalMonth = formatMonth.format(date);
            String finalHour = formatAmPm.format(date);
            holder.txtTime.setText(finalHour + " " + finalMonth + "-" + finalDate);
        }else
            holder.txtTime.setText("");
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public EquipmentMenuAdapter(ArrayList<EquipmentMenuModel> List, EquipmentMenuFragment equipmentMenuFragment) {
        this.list = List;
        this.context = equipmentMenuFragment.getActivity();
        this.equipmentMenuFragment = equipmentMenuFragment;
        this.equipmentListener = equipmentMenuFragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTime, txtName, txtQuntity, txtNotes, txtOrderStatus, txtEquipmetName, txtTotalQty;
        ImageView ivAction;
        RelativeLayout rlMain;

        public MyViewHolder(View view) {
            super(view);
            txtTime = view.findViewById(R.id.txtTime);
            txtName = view.findViewById(R.id.txtName);
            txtQuntity = view.findViewById(R.id.txtQuntity);
            txtNotes = view.findViewById(R.id.txtEquNo);
            txtOrderStatus = view.findViewById(R.id.txt_orderStatus);
            txtEquipmetName = view.findViewById(R.id.txtEquipmetName);
            ivAction = view.findViewById(R.id.ivAction);
            rlMain = view.findViewById(R.id.rlMain);
            txtTotalQty = view.findViewById(R.id.txtTotalQty);
        }
    }

}
