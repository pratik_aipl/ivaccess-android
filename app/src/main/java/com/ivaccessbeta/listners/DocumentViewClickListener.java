package com.ivaccessbeta.listners;

import android.view.View;

/**
 * Created by User on 19-04-2018.
 */

public interface DocumentViewClickListener {
    void recyclerViewListClicked(View v, int position, int groupPos, int childPos);
}
