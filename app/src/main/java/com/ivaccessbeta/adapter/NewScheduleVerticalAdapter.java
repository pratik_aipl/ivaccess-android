package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.schedule.NewScheduleFragment;
import com.ivaccessbeta.listners.NewNotAvailableScheduleListener;
import com.ivaccessbeta.listners.NewScheduleListener;
import com.ivaccessbeta.model.NewScheduleModel;
import com.ivaccessbeta.utils.ItemOffsetDecoration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class NewScheduleVerticalAdapter extends RecyclerView.Adapter<NewScheduleVerticalAdapter.CellViewHolder> implements NewScheduleListener, NewNotAvailableScheduleListener {

    private ArrayList<NewScheduleModel> mList;
    private SparseIntArray listPosition = new SparseIntArray();
    private HorizontalSchedualAdapter.OnItemClickListener mItemClickListener;
    private NewScheduleFragment mContext;
    public NewScheduleListener scheduleListener;
    public NewNotAvailableScheduleListener notAvailableScheduleListener;

    public NewScheduleVerticalAdapter(ArrayList<NewScheduleModel> list, NewScheduleFragment newScheduleFragment) {
        this.mList = list;
        this.scheduleListener = newScheduleFragment;
        this.notAvailableScheduleListener = newScheduleFragment;
        this.mContext = newScheduleFragment;
    }

    @Override
    public void onDeleteSchedule(int pos, String scheduleID) {
        scheduleListener.onDeleteSchedule(pos, scheduleID);
    }

    @Override
    public void onCopyToTomorrowSchedule(int pos, String scheduleID) {
        scheduleListener.onCopyToTomorrowSchedule(pos, scheduleID);
    }

    @Override
    public void onCopyToNextWeekSchedule(int pos, String scheduleID) {
        scheduleListener.onCopyToNextWeekSchedule(pos, scheduleID);
    }

    @Override
    public void onCopyToEntireWeekSchedule(int pos, String scheduleID) {
        scheduleListener.onCopyToEntireWeekSchedule(pos, scheduleID);
    }

    @Override
    public void onDeleteNotAvailableSchedule(int pos, String scheduleID) {
        notAvailableScheduleListener.onDeleteNotAvailableSchedule(pos, scheduleID);
    }

    @Override
    public void onApproveNotAvailableSchedule(int pos, String scheduleID) {
        notAvailableScheduleListener.onApproveNotAvailableSchedule(pos, scheduleID);
    }

    @Override
    public void onDenyNotAvailableSchedule(int pos, String scheduleID) {
        notAvailableScheduleListener.onDenyNotAvailableSchedule(pos, scheduleID);
    }

    @Override
    public void onCopyToTomorrowNotAvailableSchedule(int pos, String scheduleID) {
        notAvailableScheduleListener.onCopyToTomorrowNotAvailableSchedule(pos, scheduleID);
    }

    public class CellViewHolder extends RecyclerView.ViewHolder {

        private RecyclerView mRecyclerView;
        private TextView txtDate, txtDay, txtMonth;
        public LinearLayout llDate;

        public CellViewHolder(View itemView) {
            super(itemView);

            mRecyclerView = itemView.findViewById(R.id.recyclerView);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtDay = itemView.findViewById(R.id.txtDay);
            txtMonth = itemView.findViewById(R.id.txtMonth);
            llDate = itemView.findViewById(R.id.llDate);
        }
    }

    @Override
    public CellViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
//        mContext = viewGroup.getContext();
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_schedule_vertical, viewGroup, false);

        return new CellViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CellViewHolder cellViewHolder, final int position) {
        //   CellViewHolder cellViewHolder = (CellViewHolder) viewHolder;

        boolean isNotAvailableStatus=false;
        for (int i = 0; i < mList.get(position).subArray.size(); i++) {
            NewScheduleModel newScheduleModel=mList.get(position).subArray.get(i);
            if (newScheduleModel.getAvailableSchedule().equalsIgnoreCase("0") && !isNotAvailableStatus){
                mList.get(position).subArray.get(i).setNotAvailable(true);
                isNotAvailableStatus=true;
            }else {
                mList.get(position).subArray.get(i).setNotAvailable(false);
            }
        }

        NewScheduleHorizontalAdapter adapter = new NewScheduleHorizontalAdapter(mList.get(position).subArray, mContext);
        cellViewHolder.mRecyclerView.setAdapter(adapter);
        cellViewHolder.mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext.getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        final int spacing = mContext.getResources().getDimensionPixelOffset(R.dimen.margin_10);
        cellViewHolder.mRecyclerView.setLayoutManager(layoutManager);
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String todayDateString = formatter.format(todayDate);

        if (mList.get(position).getSchedual_date().equals(todayDateString)) {
            cellViewHolder.llDate.setBackgroundColor(Color.parseColor("#F4F1A9"));
            cellViewHolder.mRecyclerView.scrollToPosition(position);
        } else {
            cellViewHolder.llDate.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        getDateMMYYFormate(position, cellViewHolder.txtDate, cellViewHolder.txtDay, cellViewHolder.txtMonth);
        adapter.SetOnItemClickListener(mItemClickListener);
        int lastSeenFirstPosition = listPosition.get(position, 0);
        if (lastSeenFirstPosition >= 0) {
            cellViewHolder.mRecyclerView.scrollToPosition(lastSeenFirstPosition);
        }

        layoutManager = ((LinearLayoutManager) cellViewHolder.mRecyclerView.getLayoutManager());
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
        listPosition.put(position, firstVisiblePosition);
    }

  /*  @Override
    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        final int position = viewHolder.getAdapterPosition();
        NewScheduleVerticalAdapter.CellViewHolder cellViewHolder = (NewScheduleVerticalAdapter.CellViewHolder) viewHolder;
        LinearLayoutManager layoutManager = ((LinearLayoutManager) cellViewHolder.mRecyclerView.getLayoutManager());
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
        listPosition.put(position, firstVisiblePosition);
        super.onViewRecycled(viewHolder);
    }*/

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    // for both short and long click
    public void SetOnItemClickListener(final HorizontalSchedualAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private void getDateMMYYFormate(int position, TextView txt, TextView day, TextView month) {
        String CurrentStringEnd = mList.get(position).getSchedual_date();
        String[] separated = CurrentStringEnd.split("-");
        final String s1End = separated[1];
        String sEndMonth = null;
        System.out.println("spilits" + s1End);

        if (s1End.equalsIgnoreCase("01")) {
            sEndMonth = "January ";
        } else if (s1End.equalsIgnoreCase("02")) {
            sEndMonth = "February ";
        } else if (s1End.equalsIgnoreCase("03")) {
            sEndMonth = "March ";
        } else if (s1End.equalsIgnoreCase("04")) {
            sEndMonth = "April ";
        } else if (s1End.equalsIgnoreCase("05")) {
            sEndMonth = "May ";
        } else if (s1End.equalsIgnoreCase("06")) {
            sEndMonth = "June ";
        } else if (s1End.equalsIgnoreCase("07")) {
            sEndMonth = "July ";
        } else if (s1End.equalsIgnoreCase("08")) {
            sEndMonth = "August ";
        } else if (s1End.equalsIgnoreCase("09")) {
            sEndMonth = "September ";
        } else if (s1End.equalsIgnoreCase("10")) {
            sEndMonth = "October ";
        } else if (s1End.equalsIgnoreCase("11")) {
            sEndMonth = "November ";
        } else if (s1End.equalsIgnoreCase("12")) {
            sEndMonth = "December ";
        }
        String asubstringEnd = CurrentStringEnd.substring(8, 10);

        String date = mList.get(position).getSchedual_date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dayOfTheWeek = (String) DateFormat.format("EEEE", myDate);
        txt.setText(asubstringEnd);
        day.setText(firstThree(dayOfTheWeek) + ", ");
        month.setText(firstThree(sEndMonth));


    }

    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }
}

