package com.ivaccessbeta.fragment.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ivaccessbeta.R;
import com.squareup.picasso.Picasso;


public class DocImageFragment extends Fragment {
    public String FileName = "", Path = "", imagePath = "";
    public ImageView ivDoc;
    public  View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_dociamge, container, false);
        setHasOptionsMenu(true);


        ivDoc = rootView.findViewById(R.id.ivDoc);
        imagePath = Path + FileName;
        if (!imagePath.equalsIgnoreCase("")) {
            Picasso.with(getContext()).load(imagePath).placeholder(R.drawable.progress).error(R.drawable.nopic).into(ivDoc);
        }
        final ProgressBar loader = rootView.findViewById(R.id.loader);
        loader.setVisibility(View.GONE);
        loader.setVisibility(View.VISIBLE);
        Picasso.with(getActivity()).load(imagePath)
                .into(ivDoc, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        if (loader != null) {
                            loader.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
        return rootView;
    }


    public void bindData(String obj, String Path) {
        this.FileName = obj;
        this.Path = Path;
    }
}