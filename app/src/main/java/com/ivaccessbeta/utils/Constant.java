package com.ivaccessbeta.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {
    public static final int REQUEST_STORAGE_PERMS = 1;
    public static final int REQUEST_CAMERA_PERMISSION = 2;
    public static final int REQUEST_MULTIPLE_PERMISSIONS = 3;
    public static final int REQUEST_CALL_PERMS = 4;

    public static final int TYPE_MULTI_PICKER = 3;
    public static final int TYPE_MULTI_CAPTURE = 4;

    public static final String KEY_BUNDLE_LIST = "BUNDLE_LIST";
    public static final String KEY_PARAMS = "PARAMS";
    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;
    public static final String IMAGE_PREFIX = "";
    public static final int ACCESS_FINE_COARSE_LOCATION = 820;
    public static final int ACCESS_FINE_LOCATION = 821;
    public static final int NUM_OF_COLUMNS = 3;

    // Gridview image padding
    public static final int GRID_PADDING = 8; // in dp

    // SD card image directory
    public static final String PHOTO_ALBUM = "DCIM/Camera";

    // supported file formats
    public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg",
            "png");
    public static final String InvoiceNo ="InvoiceNo" ;
    public static String chkDate="chkDate";
    public static String chkId="chkId";
    public static boolean isMap=false;
    public static String email="email";
    public static String app_token="app_token";
    public static String id="id";
    public static String RegionID="RegionID";
    public static String role_id="role_id";
    public static String first_name="first_name";
    public static String last_name="last_name";
    public static String password_change="password_change";

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE,POST_WITH_IMAGE_MULTIPAL, POST_WITH_JSON
    }

    public enum FILTER_TYPE {
        REGIONS, PERSONS, STATUS, FACILITY, PLACE, NOT_AVAILABLE,JOBSTATE
    }

    public enum REQUESTS {
        addJob, updateJob, login, getRegion, addRegion, deleteRegion, addEquipment, addService,
        addDocuments, getRole, getEquipment, deleteEquipment, getFacility, getFacilityActivity, deleteFacility,
        getPeople, getService, getServiceActivity, deleteService,
        addPeople, addFacility, forgotPassword,addMessage,getChatList,getChatHistory,replyMessage,
        getBillToActivity, getBillTo, deleteBillTo, deleteDocument, getDocument, addBillTo, deletePeople, getSchedual,
        getSchedualDetail, getContractor, addScheduleFragment, addSchedule, updateRegion, updateService, updateDocument,
        updateEquipment, updatePeople, updateFacility, updateBillTo, getJobList, getProfileJobSchedule,
        getJobDetail, deleteJob, addJobDoc, editJob, getJobStatus, addJobNote, markArrivalTime, getJobSearchStatus,
        getSaveData, getShortBy, jobSearch, getEquipmentQuentity, getUserProfile, addPlace, updatePlace, deletePlace, getPlace,
        updateJobStatus, getContractorActivity, getPlaceActivity, getJobState, getJobHistory, editUserProfile,
        getEmpDoc, getProfileSchedule, addUserDoc, deleteUserDoc, getPushDevider, documentApprove, documentDeny,
        documentDelete, getNotification, getMenuEquipment,
        getNotificationUserList, sendNotify, getDirectoryByName, deleteSchedule, notification, sendJobNotification,
        approveDenyNotAvailableSchedule, getUserFacility,
        updatePushDevider, addUpdateEquipmentOrder, upblishPushDivider, changeEquipmentStatus, updateEquipmentQTY,
        getInvoiceList, deleteInvoice, getCheckContractorList,printJob,
        getContractorEquipmentQTY, jobCheckApprove, getCheckList, getBillToServiceList, deleteCheck, deleteAllCheck,
        billToApprove, deleteAllInvoice, getInvoiceContractorList,editScheduleFragment,createInvoice,printCheck,
        addCheckCustomAmount, addInvoiceCustomAmount, addCustomMemoCheck, addCustomMemoInvioce, addJobToCheck,downlodeInvoice, printInvoice,
        addJobToInvoice,addNoteToInvoice,addPaymentNoteToInvoice,deleteInvoiceNote,deleteInvoicePaymentNote,viewNotification,
        getJobListCheck,changePassword,changeUserDocStatus,addNoteToCheck,addVoucherToCheck,addPaymentNoteToCheck,deleteCheckPaymentNote,qbCompanyList,
        deleteCheckNote,billingDate,approveDocumentStatus,denyDocumentStatus,printDocument,copyToSchedule,addCheckQuickBook, get_job_filter, get_user_pendding_count_notification, invoice_check_mark, DeArchive, Archive, addInvoiceToQuickBook
    }


    public static final String STATUS_CREATED = "1", STATUS_ACKNLOLOG = "2", STATUS_CANCELLED = "4", STATUS_COMPLETED = "3", STATUS_SERVICE_NOT_RENDER = "5",INREVIEW="in_review";
    //====check===
    public static final String CHECK_CREATED = "1", CHECK_NOT_CREATED = "0", CHECK_RIGHT = "2";

    //====BillTo===
    public static final String BILLTO_CREATED = "1", BILLTO_NOT_CREATED = "0", BILLTO_RIGHT = "2";
    //===approve,deny,pending===
    public static final String APPROVE = "Approve", DENY = "Deny",PENDING="Pending";

    //===Equipment Status====
    public static final String EQUIPMENT_APPROVE = "", EQUIPMENT_PENDING = "Pending";

    //====Not Available Schedule===

    public static final String NA_SCHEDULE_APPROVE = "Approve", NA_SCHEDULE_DENY = "Deny", NA_SCHEDULE_PENDING = "Pending";

    //======Schedule is Available or Not Available=======

    public static final String AVAILABLE_SCHEDULE = "1", NOT_AVAILABLE_SCHEDULE = "0";

    //=====Job Status===

    public static final String CURRENT_JOB_STATE = "current", IN_REVIEW_JOB_STATE = "in_review",AWAITING_PROCESSING="awaiting_processing",ARCHIVE="archive";

    //=====Document Status=====

    public static final String DOCUMENT_APPROVE = "Approve", DOCUMENT_DENY = "Deny", DOCUMENT_PENDING = "Pending",DOCUMENT_ISPRINT="1",DOCUMENT_ISNOTPRINT="0",IS_DOC_STATUS="1";

    //=====Check Rate and Invoice Rate=====

    public static final String CUSTOM_CHECK_RATE = "0.00", CUSTOM_BILL_RATE = "0.00";

    //======title parameter of notification=======
    public static String JOB_EDITED="Job Edited",JOB_NOTE="Job Note",JOB_MARKARRIVAL="Job MarkArrival",
            JOB_DOC_UPLOAD="job document upload",JOB_DOC_APPROVE="job document approve/deny",Message="New Message",
    JOB_ADD="Job Add",JOB_NOTIFICATION="Job Notification",JOB_EQUIPMENT_STATUS="Equipment Status",JOB_STATUS="Job Status",EQUIPMENT_ADD="Equipment Order Added"
           , SCHEDULE_ADD="Schedule Add",SCHEDULE_UPDATE="Schedule Update",DOCUMENT_EXPIRED="Document Expire Reminder",NOTIFY="Notify",JOB_DOSE_NOT_ACKNLOG="Job Does Not Acknowledged";

    public static  String TOMORROW="tomorrow",NEXT="next",ENTIRE="entire";
    public static final String IS_MARK_NOT_ARRIVAL ="0",IS_IVACCESS_SUPPLY="0";
    public static final String RED_BG="1",BLUE_BG="0";
    public static final String PASSWORD_NOT_CHANGE="0",PASSWORD_CHANGE="1";
    public static final  String Zero="0",One="1";
}
