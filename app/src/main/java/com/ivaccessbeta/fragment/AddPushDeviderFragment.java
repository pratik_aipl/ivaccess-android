package com.ivaccessbeta.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;


import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.PushDeviderAdapter;
import com.ivaccessbeta.listners.PublishOnClick;
import com.ivaccessbeta.listners.PushDividerListener;
import com.ivaccessbeta.model.LogHistoryModel;
import com.ivaccessbeta.model.PushDeviderModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.ivPublish;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

public class AddPushDeviderFragment extends Fragment implements AsynchTaskListner,PushDividerListener, PublishOnClick {

    public RecyclerView rvPushDevider;
    public AddPushDeviderFragment instance;
    public String emailId, token,userId,pushDeviderDate,PublishDate,lastDateOfList;
    public PushDeviderModel pushDeviderModel;
    public PushDeviderAdapter pushDeviderAdapter;
    public ArrayList<PushDeviderModel>pushDeviderList=new ArrayList<>();
    public int postion;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view= inflater.inflate(R.layout.fragment_add_push_devider, container, false);
        setHasOptionsMenu(true);
        MainActivity.toolbar_title.setText("SCHEDULES");
        ((MainActivity)getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        ivPublish.setVisibility(View.GONE);

        instance=this;
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        rvPushDevider =view.findViewById(R.id.recyclerPushdevider);
//        ivPublish.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                publishPushDivider();
//            }
//        });
        getPushDevider();
        return view;
    }

    private void getPushDevider() {
        new CallRequest(instance).getPushDevider(emailId,token);
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {

                case getPushDevider:
                    pushDeviderList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {

                            JSONObject firstObj = jObj.getJSONObject("data");
                            pushDeviderDate = firstObj.getString("PushDivider");
                            PublishDate = firstObj.getString("PublishDate");
                            JSONArray jObject = firstObj.getJSONArray("WeekList");
                            for (int i = 0; jObject.length() > i; i++) {
                                pushDeviderModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),PushDeviderModel.class);
                                pushDeviderModel.setPushDivider(pushDeviderDate);
                                pushDeviderList.add(pushDeviderModel);
                            }
                            lastDateOfList = pushDeviderModel.getEnd_date();
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvPushDevider.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvPushDevider.setLayoutManager(mLayoutManager);
                            pushDeviderAdapter = new PushDeviderAdapter(pushDeviderList, instance, postion,pushDeviderDate,PublishDate);
                            rvPushDevider.setItemAnimator(new DefaultItemAnimator());
                            rvPushDevider.setAdapter(pushDeviderAdapter);

                            rvPushDevider.smoothScrollToPosition(postion+1);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    }catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case updatePushDevider:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                             JSONObject firstObj=jObj.getJSONObject("data");
                             pushDeviderDate= firstObj.getString("PushDivider");

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getPushDevider();
                    break;

                case upblishPushDivider:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"),getActivity());
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getPushDevider();
                    break;


            }
        }
    }
    @Override
    public void onSetNextPushDivider(int pos,String newPushDeviderDate) {
        setNextPrePushdevider(pos,newPushDeviderDate);
        postion=pos;
    }

    private void setNextPrePushdevider(int pos,String newPushDeviderDate) {
        new CallRequest(instance).updatePushDevider(emailId,token,newPushDeviderDate,lastDateOfList);
    }

    @Override
    public void onSetPreviousPushDivider(int pos,String newPushDeviderDate) {
        setNextPrePushdevider(pos,newPushDeviderDate);
        postion=pos;

    }

    @Override
    public void callPublishClick(int pos) {
        new CallRequest(instance).publishPushDivider(emailId,token);
    }
}
