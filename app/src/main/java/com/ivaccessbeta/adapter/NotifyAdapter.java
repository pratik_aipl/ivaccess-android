package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.NotifyModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.ivaccessbeta.App.indexNotifyList;

public class NotifyAdapter extends RecyclerView.Adapter<NotifyAdapter.MyViewHolder> {
    public ArrayList<NotifyModel> list;
    public ArrayList<Integer> checkedArray = new ArrayList<>();
//    public Context context;
    public String emailId, token, userId;
    public boolean isSelectAll = false;
    public CheckBox selectAll;
    public List<NotifyModel> searchFilterList = new ArrayList<>();

    @Override
    public NotifyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_notify, parent, false);

        return new NotifyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NotifyAdapter.MyViewHolder holder, final int position) {
        final NotifyModel model = list.get(position);
        holder.name.setText(model.getFullName());
        if (isSelectAll) {
            holder.name.setChecked(true);

        } else {
            if (checkedArray.contains(position)) {
                holder.name.setChecked(true);
            } else {
                holder.name.setChecked(false);
            }
        }

        holder.name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String id = list.get(position).getId();
                if (isChecked) {
                    if (!indexNotifyList.contains(id)) {
                        indexNotifyList.add(id);
                    }
                    if (!checkedArray.contains(position)) {
                        checkedArray.add(position);
                    }
                } else {
                    indexNotifyList.remove(id);
                    try {
                        if (checkedArray.contains(position)) {
                            checkedArray.remove(position);
                        }
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }


                if (indexNotifyList.size() == list.size()) {
                    selectAll.setChecked(true);
                } else {
                    selectAll.setChecked(false);
                }
            }
        });
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (NotifyModel bean : searchFilterList) {
                if (bean.getFullName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void selectAll(boolean isSelectAll) {
        this.isSelectAll = isSelectAll;
        if (!isSelectAll) {
            checkedArray.clear();
        }
        this.notifyDataSetChanged();
    }

    public NotifyAdapter(ArrayList<NotifyModel> List, CheckBox selectAll) {
        this.list = List;
        this.selectAll = selectAll;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox name;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.checkBox);
        }
    }

}
