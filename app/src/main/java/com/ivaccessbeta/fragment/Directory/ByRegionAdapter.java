package com.ivaccessbeta.fragment.Directory;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by User on 17-04-2018.
 */

public class ByRegionAdapter  extends BaseExpandableListAdapter {
    Context context;
    ArrayList<ByRegionGroupModel> groupArray;
    String emailId, token, userId,roleId;
    public List<ByRegionGroupModel> searchFilterList=new ArrayList<>();
    public ByRegionAdapter(Context context, ArrayList<ByRegionGroupModel> groupArray) {
        this.context = context;
        this.groupArray = groupArray;
        this.searchFilterList.addAll(groupArray);
    }
    @Override
    public ByRegionChildModel getChild(int groupPos, int childPos) {
        return this.groupArray.get(groupPos).getChildList().get(childPos);
    }
    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }
    @Override
    public View getChildView(int groupPos, final int childPos,boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_byname, null);
        }
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(context, App.ROLEID, "");

        TextView txtName = convertView.findViewById(R.id.txtName);
        TextView txtEmail = convertView.findViewById(R.id.txtEmail);
        TextView txtNote= convertView.findViewById(R.id.txtNote);
        TextView txtMoNo = convertView.findViewById(R.id.txtMoNo);
        TextView txtMoNo2 = convertView.findViewById(R.id.txtMoNo2);
        ByRegionChildModel cModel = getChild(groupPos, childPos);
        txtName.setText(cModel.getFullName());
        txtMoNo.setText(cModel.getMobileNumber());
        txtEmail.setText(cModel.getEmail());
        txtNote.setText(cModel.getDirectoryNote());
        txtMoNo2.setText(cModel.getAlternateNumber());
        if(cModel.getRole_id().equals(App.ADMIN_ROLE_ID)){
            txtEmail.setVisibility(View.VISIBLE);
        }else {
            txtEmail.setVisibility(View.GONE);
        }
        return convertView;
    }
    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        groupArray.clear();
        if (charText.length() == 0) {
            groupArray.addAll(searchFilterList);
        } else {
            for (ByRegionGroupModel bean : searchFilterList) {
                if (bean.getRegion().toLowerCase(Locale.getDefault()  )
                        .contains(charText)) {
                    groupArray.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }
    @Override
    public int getChildrenCount(int listPosition) {
        return this.groupArray.get(listPosition).getChildList()
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.groupArray.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupArray.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        //  String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_by_region_header, null);
        }
        TextView txtRegionName = convertView.findViewById(R.id.txtHeader);


        txtRegionName.setTypeface(null, Typeface.BOLD);
        txtRegionName.setText(groupArray.get(groupPosition).getRegion());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
