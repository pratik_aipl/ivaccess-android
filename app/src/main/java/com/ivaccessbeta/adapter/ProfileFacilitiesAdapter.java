package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.ProfileFacilitiesModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 22-03-2018.
 */

public class ProfileFacilitiesAdapter extends RecyclerView.Adapter<ProfileFacilitiesAdapter.MyViewHolder> {

    public ArrayList<ProfileFacilitiesModel> list;
    public List<ProfileFacilitiesModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;

    @Override
    public ProfileFacilitiesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_profile_facilities_fragment, parent, false);
        return new ProfileFacilitiesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProfileFacilitiesAdapter.MyViewHolder holder, final int position) {
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        final ProfileFacilitiesModel model = list.get(position);
        holder.name.setText(model.getFacilityName());
        holder.txtAddress.setText(model.getAddress());
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public ProfileFacilitiesAdapter(Context context, ArrayList<ProfileFacilitiesModel> featuredJobList) {
        this.context = context;
        this.list = featuredJobList;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,txtAddress;
        ImageView ivAction, ivEdit;

        public MyViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.txtFacilityName);
            txtAddress = view.findViewById(R.id.txtFacilityAddress);

//            ivEdit=view.findViewById(R.id.ivEdit);

        }


    }

}
