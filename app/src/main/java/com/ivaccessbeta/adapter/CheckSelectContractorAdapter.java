package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.CheckContractorModel;
import com.ivaccessbeta.App;

import java.util.ArrayList;

/**
 * Created by User on 28-04-2018.
 */

public class CheckSelectContractorAdapter extends RecyclerView.Adapter<CheckSelectContractorAdapter.MyViewHolder> {

    public ArrayList<CheckContractorModel> list;
    public Context context;
    public String emailId, token, userId;
    public View itemView;
    public boolean isSelectAll = false;
    public CheckBox selectAll;

    @Override
    public CheckSelectContractorAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_select_contractor_check, parent, false);
        return new CheckSelectContractorAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CheckSelectContractorAdapter.MyViewHolder holder, final int position) {
        holder.cbContractor.setText(list.get(position).getFullname());
        final String id = list.get(position).getId();

        if (isSelectAll) {
            holder.cbContractor.setChecked(true);
        } else {

            if (App.checkedArray.contains(id)) {
                holder.cbContractor.setChecked(true);
            } else {
                holder.cbContractor.setChecked(false);
            }
        }

        holder.cbContractor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (!App.indexCheckContractorList.contains(id)) {
                        App.indexCheckContractorList.add(id);
                    }
                    if (!App.checkedArray.contains(id)) {
                        App.checkedArray.add(id);
                    }
                } else {
                    App.indexCheckContractorList.remove(id);
                    try {
                        App.checkedArray.remove(id);
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
                if (App.indexCheckContractorList.size() == list.size()) {
                    selectAll.setChecked(true);

                } else {
                    selectAll.setChecked(false);

                }
            }
        });

        if (App.indexCheckContractorList.size() == list.size()) {
            selectAll.setChecked(true);

        } else {
            selectAll.setChecked(false);

        }

    }

    public void selectAll(boolean isSelectAll) {
        this.isSelectAll = isSelectAll;
        if (!isSelectAll) {
            App.checkedArray.clear();
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public CheckSelectContractorAdapter(ArrayList<CheckContractorModel> List, CheckBox selectAll) {
        this.list = List;
        this.selectAll = selectAll;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CheckBox cbContractor;

        public MyViewHolder(View view) {
            super(view);
            cbContractor = view.findViewById(R.id.checkBox);
        }


    }

}
