package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by User on 02-04-2018.
 */
@JsonObject
public class ProfileScheduleModel {

    @JsonField
   public String schedual_date,FromTime,ToTime,PlacesName,first_name,last_name,ColorCode,AvailableSchedule,NotAvailableStatus,SubmittedBy,CreatedOn;

    public String getAvailableSchedule() {
        return AvailableSchedule;
    }

    public void setAvailableSchedule(String availableSchedule) {
        AvailableSchedule = availableSchedule;
    }

    public String getNotAvailableStatus() {
        return NotAvailableStatus;
    }

    public void setNotAvailableStatus(String notAvailableStatus) {
        NotAvailableStatus = notAvailableStatus;
    }

    public String getSchedual_date() {
        return schedual_date;
    }

    public void setSchedual_date(String schedual_date) {
        this.schedual_date = schedual_date;
    }

    public String getFromTime() {
        return FromTime;
    }

    public void setFromTime(String fromTime) {
        FromTime = fromTime;
    }

    public String getToTime() {
        return ToTime;
    }

    public void setToTime(String toTime) {
        ToTime = toTime;
    }

    public String getPlacesName() {
        return PlacesName;
    }

    public void setPlacesName(String placesName) {
        PlacesName = placesName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public ArrayList<ProfileScheduleModel> subArray = new ArrayList<>();

    public String getSubmittedBy() {
        return SubmittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        SubmittedBy = submittedBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }
}
