package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.ProfileEquipmentAdapter;
import com.ivaccessbeta.model.ProfileEquipmentModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class EquipmentsProfileFragment extends Fragment implements AsynchTaskListner {
    public ArrayList<ProfileEquipmentModel> list = new ArrayList<>();
    public ProfileEquipmentAdapter profileEquipmentAdapter;
    public RecyclerView rvEquipment;
    public EquipmentsProfileFragment instance;
    public String emailId, token, userId;
    public TextView txtQTY;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_equipments_profile, container, false);
        setHasOptionsMenu(true);
        instance = this;
        rvEquipment = view.findViewById(R.id.rvEquipments);
        txtQTY = view.findViewById(R.id.txtQTY);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        try {
            if (App.contractorId.equalsIgnoreCase("")) {
                userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
            } else {
                userId = App.contractorId;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getEquipmentList();
    }

    private void getEquipmentList() {
        new CallRequest(instance).getContractorEquipmentQTY(emailId, token, userId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContractorEquipmentQTY:
                    list.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            list.addAll(LoganSquare.parseList(jObject.toString(), ProfileEquipmentModel.class));
                            profileEquipmentAdapter = new ProfileEquipmentAdapter(getActivity(), list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvEquipment.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvEquipment.setLayoutManager(mLayoutManager);
                            rvEquipment.setItemAnimator(new DefaultItemAnimator());
                            rvEquipment.setAdapter(profileEquipmentAdapter);
                            profileEquipmentAdapter.notifyDataSetChanged();

                            int total = 0;
                            for (int i = 0; i < list.size(); i++) {
                                total += list.get(i).getStock();
                            }
                            txtQTY.setText("" + total);
                            if (list.size() == 0) {
                                txtQTY.setVisibility(View.GONE);
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

            }
        }
    }
}
