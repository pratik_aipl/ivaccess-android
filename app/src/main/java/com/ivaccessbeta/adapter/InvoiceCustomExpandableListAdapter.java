package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.invoice.InvoiceFragment;
import com.ivaccessbeta.listners.InvoiceListener;
import com.ivaccessbeta.model.InvoiceChildModel;
import com.ivaccessbeta.model.InvoiceGroupModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 14-04-2018.
 */

public class InvoiceCustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<InvoiceGroupModel> groupArray;
    public InvoiceListener invoiceListener;
    public InvoiceFragment invoiceFragment;
    public String IsCheckMark;
    public List<InvoiceGroupModel> searchFilterList = new ArrayList<>();

    public InvoiceCustomExpandableListAdapter(Context context, ArrayList<InvoiceGroupModel> groupArray, InvoiceFragment invoiceFragment) {
        this.context = context;
        this.groupArray = groupArray;
        this.invoiceFragment = invoiceFragment;
        this.invoiceListener = invoiceFragment;
        this.searchFilterList.addAll(groupArray);
    }

    @Override
    public InvoiceChildModel getChild(int groupPos, int childPos) {
        return this.groupArray.get(groupPos).getChildList().get(childPos);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int groupPos, final int childPos,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.check_list_child, null);
        }
        TextView expandedListTxtJobNo = convertView
                .findViewById(R.id.txtCheckNo);
        TextView expandedLisTxtInvoiceDate = convertView
                .findViewById(R.id.txtCheckDate);
        TextView expandedLisTxt1 = convertView
                .findViewById(R.id.txt1);
        TextView expandedLisTxt2 = convertView
                .findViewById(R.id.txt2);
        TextView expandedLisTxt3 = convertView
                .findViewById(R.id.txt3);


        TextView expandedLisTxtPrice = convertView
                .findViewById(R.id.txt_price);
        //  ImageView ivEdit=convertView.findViewById(R.id.iv_edit);
        ImageView ivCancel = convertView.findViewById(R.id.iv_cancel);
        ImageView iv_rightmark = convertView.findViewById(R.id.iv_rightmark);

        final EditText edtItemMemo;
        final EditText edtAmount;
        edtItemMemo = convertView
                .findViewById(R.id.edtItemMemo);
        edtAmount = convertView
                .findViewById(R.id.edtAmount);
        ImageView ivPlush = convertView
                .findViewById(R.id.ivPlush);
        InvoiceChildModel cModel = getChild(groupPos, childPos);

        Utils.converMMddyyyyFormate(cModel.getCompletedDate(), expandedLisTxtInvoiceDate);

//        expandedLisTxtInvoiceDate.setText(cModel.getCompletedDate());
        expandedLisTxt1.setText(cModel.getFacilityName());
        expandedLisTxtPrice.setText(cModel.getInvoiceAmount());
        if (cModel.getInvoiceStatus().equalsIgnoreCase(Constant.BLUE_BG)) {
            expandedListTxtJobNo.setBackgroundResource(R.drawable.dynamic_round);
            GradientDrawable gd = (GradientDrawable) expandedListTxtJobNo.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#4C85BA"));
            gd.setCornerRadii(new float[]{20, 20, 20, 20, 20, 20, 20, 20});
            gd.setStroke(2, Color.parseColor("#4C85BA"), 5, 0);
        } else if (cModel.getInvoiceStatus().equalsIgnoreCase(Constant.RED_BG)) {
            //  expandedListTxtJobNo.setBackgroundColor(Color.parseColor("#e41c1c"));
            expandedListTxtJobNo.setBackgroundResource(R.drawable.dynamic_round);
            GradientDrawable gd = (GradientDrawable) expandedListTxtJobNo.getBackground().getCurrent();
            gd.setColor(Color.parseColor("#e41c1c"));
            gd.setCornerRadii(new float[]{20, 20, 20, 20, 20, 20, 20, 20});
            gd.setStroke(2, Color.parseColor("#e41c1c"), 5, 0);
        }
        if (isLastChild) {
            edtItemMemo.setVisibility(View.VISIBLE);
            edtAmount.setVisibility(View.VISIBLE);
            ivPlush.setVisibility(View.VISIBLE);
        } else {
            edtItemMemo.setVisibility(View.GONE);
            edtAmount.setVisibility(View.GONE);
            ivPlush.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(cModel.getFacilityName())) {
            expandedLisTxt1.setText(cModel.getNote());
        } else {
            expandedLisTxt1.setText(cModel.getFacilityName());
            expandedListTxtJobNo.setText(cModel.getJobNo());
            expandedLisTxt2.setText("JN:#" + cModel.getJobNo() + " " + cModel.getPatientName() + " RM-" + cModel.getRoomNumber());
            expandedLisTxt3.setText(" DOB:" + cModel.getDOB());
        }
//        ivEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                invoiceListener.onEditInvoice(groupPos,childPos,v);
//            }
//        });
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invoiceListener.onDeletePerticularInvoice(groupPos, childPos, v);
            }
        });
        if (cModel.getIsCheckMark().equals("0")) {
            IsCheckMark = "1";
            iv_rightmark.setImageResource(R.drawable.ic_right_gray);
        } else {
            IsCheckMark = "0";
        }
        iv_rightmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                invoiceListener.onCheckInvoice(groupPos, childPos, view, IsCheckMark);
            }
        });
        ivPlush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String memoNote = edtItemMemo.getText().toString();
                String memoAmount = edtAmount.getText().toString();
                if (TextUtils.isEmpty(memoAmount)) {
                    Utils.showToast("Please Enter Amount", context);
                } else {
                    invoiceListener.onAddInvoiceMemo(groupPos, childPos, v, memoNote, memoAmount);
                }
            }
        });
        if (!cModel.getJobNo().equals(""))
            expandedListTxtJobNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invoiceListener.onJobNoClick(groupPos, childPos, v);
                }
            });

        return convertView;
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        groupArray.clear();
        if (charText.length() == 0) {
            groupArray.addAll(searchFilterList);
        } else {
            for (InvoiceGroupModel bean : searchFilterList) {
                if (bean.getBillName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    groupArray.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.groupArray.get(listPosition).getChildList()
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.groupArray.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.groupArray.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        //  String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.invoice_list_group, null);

        }
        TextView listInvoiceNo = convertView
                .findViewById(R.id.txtInvoiceNo);
        TextView listTitleTxtName = convertView
                .findViewById(R.id.txtName);
        TextView listTitleTxtPrice = convertView
                .findViewById(R.id.txtPrice);
        ImageView listAction = convertView.findViewById(R.id.ivAction);
        LinearLayout llMain = convertView.findViewById(R.id.llHeader);
        listInvoiceNo.setTypeface(null, Typeface.BOLD);
//        listInvoiceNo.setText(groupArray.get(groupPosition).getInvoiceNo());
//        listInvoiceNo.setText(groupArray.get(groupPosition).getInvoiceNo());
        Utils.converMMddyyyyFormate(groupArray.get(groupPosition).getInvoiceDate(), listInvoiceNo);
        listTitleTxtName.setText(groupArray.get(groupPosition).getBillName());
        listTitleTxtPrice.setText(groupArray.get(groupPosition).getAmount());

        if (groupArray.get(groupPosition).getInvoiceStatus().equalsIgnoreCase(Constant.BLUE_BG)) {
            llMain.setBackgroundColor(Color.parseColor("#4C85BA"));
        } else if (groupArray.get(groupPosition).getInvoiceStatus().equalsIgnoreCase(Constant.RED_BG)) {
            llMain.setBackgroundColor(Color.parseColor("#e41c1c"));
        }
//        listTitleDate.setText(groupArray.get(groupPosition).getInvoiceDate());
        listAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invoiceListener.onOpenActionDialog(groupPosition, v);
            }
        });

        LinearLayout linearNote = convertView.findViewById(R.id.linear_note);
        LinearLayout linearPaymentNote = convertView.findViewById(R.id.linear_payment_note);

        if (!isExpanded && (linearNote.getChildCount() + linearPaymentNote.getChildCount()) == 0) {
            System.out.println("Not Expanded called");

            for (int j = 0; j < groupArray.get(groupPosition).getNoteList().size(); j++) {

                LayoutInflater layoutInflater2 = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater2.inflate(R.layout.invoice_note_list, null);
                linearNote.addView(view);
                TextView txtDate = view.findViewById(R.id.txtDate);
                TextView txtNote = view.findViewById(R.id.txtNote);
                ImageView ivClose = view.findViewById(R.id.ivClose);

                Utils.converMMddyyyyFormate(groupArray.get(groupPosition).getNoteList().get(j).getCreatedOn(), txtDate);
                final int finalJ = j;
                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        invoiceListener.onDeleteNote(groupPosition, finalJ, v);
                    }
                });
                //  txtDate.setText(groupArray.get(groupPosition).getNoteList().get(j).getCreatedOn());
                txtNote.setText(groupArray.get(groupPosition).getNoteList().get(j).getNote());
            }

            for (int k = 0; k < groupArray.get(groupPosition).getNotePaymentList().size(); k++) {

                LayoutInflater layoutInflater2 = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View view = layoutInflater2.inflate(R.layout.invoice_payment_note_list, null);
                linearPaymentNote.addView(view);


                TextView txtDate = view.findViewById(R.id.txtDate);
                TextView txtNote = view.findViewById(R.id.txtNote);
                TextView txtRFNo = view.findViewById(R.id.txtRfNo);
                TextView txtPayment = view.findViewById(R.id.txtPayment);
                ImageView ivClose = view.findViewById(R.id.ivClose);
                final int finalK = k;
                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        invoiceListener.onDeletePaymentNote(groupPosition, finalK, v);
                    }
                });
                txtDate.setText(groupArray.get(groupPosition).getNotePaymentList().get(k).getCreatedOn());
                txtNote.setText(groupArray.get(groupPosition).getNotePaymentList().get(k).getNote());
                txtRFNo.setText("Ref No: " + groupArray.get(groupPosition).getNotePaymentList().get(k).getRefNo());
                txtPayment.setText("Payment: $" + groupArray.get(groupPosition).getNotePaymentList().get(k).getAmount());
            }

        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}