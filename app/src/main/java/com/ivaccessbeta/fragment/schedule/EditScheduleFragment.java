package com.ivaccessbeta.fragment.schedule;


import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.FacilitesAdapter;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.adapter.PlaceAdapter;
import com.ivaccessbeta.model.BillToModel;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.model.NewScheduleModel;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.model.ScheduleDetailModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditScheduleFragment extends Fragment implements AsynchTaskListner {


    public Spinner spFacility, spContactor, spPlaces;
    public Button btnSave, btnCancel;
    public EditScheduleFragment instance;
    public EditText edtFromTime, edtToTime, edtAdminNotes, edtContractorNotes;
    public String fromDate, toDate, fromTime, fromTime24, toTime, toTime24, facility, emailId, token, userId, place, contractor, adminNotes, contractorNotes;
    public ArrayList<ContractorModel> contractorList = new ArrayList<>();
    public ArrayList<String> contractorStringList = new ArrayList<>();
    public ArrayList<FacilitesModel> facilityList = new ArrayList<>();
    public ArrayList<String> facillityStringList = new ArrayList<>();
    public ArrayList<PlaceModel> placeList = new ArrayList<>();
    public ArrayList<String> placeStringList = new ArrayList<>();
    public FacilitesModel facilityModel;
    public PlaceModel placeModel;
    public ContractorModel contractorModel;
    public FacilitesAdapter mAdapter;
    public PlaceAdapter placeAdapter;
    public String curentDate;
    public String time, formattedTime;
    public NewScheduleModel scheduleDetailModel;
    public View view;
    public int contractorPos, placePos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_edit_schedule, container, false);
        instance = this;
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        MainActivity.toolbar_title.setText("Edit Schedule");
        ((MainActivity)getActivity()).showToolbarAction();
        scheduleDetailModel = (NewScheduleModel) getArguments().getSerializable("schedule");
        curentDate = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date());
//        edtFromDate = view.findViewById(R.id.edtFromDate);
//        edtToDate = view.findViewById(R.id.edtToDate);
        edtFromTime = view.findViewById(R.id.edtFromTime);
        edtToTime = view.findViewById(R.id.edtToTime);
        //   spFacility = view.findViewById(R.id.spinnerFacilities);
        spContactor = view.findViewById(R.id.spinnerContractor);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        edtAdminNotes = view.findViewById(R.id.edtAdminNotes);
        edtContractorNotes = view.findViewById(R.id.edtContractorNotes);
        spPlaces = view.findViewById(R.id.spinnerPlaces);

        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        contractor = scheduleDetailModel.getFirst_name() + " " + scheduleDetailModel.getLast_name();
        place = scheduleDetailModel.getPlacesName();
        fromTime = scheduleDetailModel.getFromTime();
        toTime = scheduleDetailModel.getToTime();
        adminNotes = scheduleDetailModel.getAdminNotes();
        contractorNotes = scheduleDetailModel.getContractorNotes();

        System.out.println("contractor==" + contractor);
        System.out.println("place==" + place);
        System.out.println("fromTime==" + fromTime);
        System.out.println("adminNotes==" + adminNotes);
        System.out.println("contractorNotes==" + contractorNotes);

        edtFromTime.setText(fromTime);
        edtToTime.setText(toTime);
        edtAdminNotes.setText(adminNotes);
        edtContractorNotes.setText(contractorNotes);

        getContractor();

//        edtFromDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                Utils.generateDatePicker(getContext(), edtFromDate);
//            }
//        });
//        edtToDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                Utils.generateDatePicker(getContext(), edtToDate);
//            }
//        });

        edtFromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getTime(edtFromTime);
            }
        });
        edtToTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime(edtToTime);
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                curentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                fromTime = edtFromTime.getText().toString();
                toTime = edtToTime.getText().toString();
                adminNotes = edtAdminNotes.getText().toString();
                contractorNotes = edtContractorNotes.getText().toString();
//                try {
//                    StringBuilder dateFormBuilder = new StringBuilder();
//                    StringBuilder dateToBuilder = new StringBuilder();
//                    dateFormBuilder = dateFormBuilder.append(fromDate.substring(6)).append("-").append(fromDate.substring(0, 2)).append("-").append(fromDate.substring(3, 5));
//                    dateToBuilder = dateToBuilder.append(toDate.substring(6)).append("-").append(toDate.substring(0, 2)).append("-").append(toDate.substring(3, 5));
//                    fromDate = String.valueOf(dateFormBuilder);
//                    toDate = String.valueOf(dateToBuilder);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                convertTo24FormateFrom();
//                convertTo24FormateTo();
               if (toTime.equals("")) {
                    Utils.showAlert("Enter To Time", getContext());
                } else if (fromTime.equals("")) {
                    Utils.showAlert("Enter From Time", getContext());
                } else {
                    new CallRequest(instance).editScheduleFragment(emailId, token, place, "", "", fromTime, toTime, contractor, userId, adminNotes, contractorNotes, "1", "",scheduleDetailModel.getScheduleID());
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        spPlaces.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getContext(), view);
                try {
                    place = placeList.get(i - 1).getPlacesID();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spContactor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getContext(), view);
                try {
                    contractor = contractorList.get(i - 1).getId();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    private void getTime(final EditText edt) {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time = selectedHour + ":" + selectedMinute;
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time);
                } catch (ParseException e) {

                    e.printStackTrace();
                }

                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                formattedTime = fmtOut.format(date);
                edt.setText(formattedTime);


            }


        }, hour, minute, false);//Yes 24 hour time


        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void convertTo24FormateFrom() {
        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");

        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

        try {
            fromTime24 = date24Format.format(date12Format.parse(fromTime));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void convertTo24FormateTo() {
        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");

        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

        try {
            toTime24 = date24Format.format(date12Format.parse(toTime));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    private void getContractor() {
        contractorList.clear();
        contractorStringList.clear();
        new CallRequest(instance).getConractor(emailId, token, "");
    }

    private void getPlace() {
        placeList.clear();
        placeStringList.clear();
        new CallRequest(instance).getPlace(emailId, token, "");
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContractor:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                contractorModel = new ContractorModel();
                                contractorModel = LoganSquare.parse(ob.toString(),ContractorModel.class);
                                contractorList.add(contractorModel);
                                contractorStringList.add(contractorModel.getFullName());

                            }
                            ArrayAdapter aa = null;
                            try {
                                aa = new ArrayAdapter(getContext(), R.layout.spinner_text, contractorStringList);
                                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spContactor.setAdapter(new NothingSelectedSpinnerAdapter(
                                        aa, R.layout.contractor_spinner_row_nothing_selected,
                                        getActivity()));


                                if (contractor != null) {
                                    int spinnerPosition = aa.getPosition(contractor);
                                    spContactor.setSelection(spinnerPosition + 1);
                                }
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    getPlace();
                    break;
                case getPlace:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                placeModel = new PlaceModel();
                                placeModel = LoganSquare.parse(ob1.getJSONObject(i).toString(),PlaceModel.class);
                                placeList.add(placeModel);
                                placeStringList.add(placeModel.getPlacesName());
                                }
                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, placeStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaces.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.nothingselected_place,
                                    getActivity()));
                            if (place != null) {
                                int spinnerPosition = aa.getPosition(place);
                                spPlaces.setSelection(spinnerPosition + 1);
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case editScheduleFragment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
