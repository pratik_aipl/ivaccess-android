package com.ivaccessbeta.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.DocumentImagePagerAdapter;
import com.ivaccessbeta.adapter.DocumentListAdapter;
import com.ivaccessbeta.adapter.JobStatusAdapter;
import com.ivaccessbeta.adapter.LogHistoryAdapter;
import com.ivaccessbeta.fragment.checks.CheckFragment;
import com.ivaccessbeta.fragment.invoice.DownloadInvoiceFragment;
import com.ivaccessbeta.fragment.invoice.InvoiceFragment;
import com.ivaccessbeta.listners.DocumentViewClickListener;
import com.ivaccessbeta.listners.LogOnClick;
import com.ivaccessbeta.listners.OnActivityResult;
import com.ivaccessbeta.model.JobDetailsModel;
import com.ivaccessbeta.model.JobStatusModel;
import com.ivaccessbeta.model.LogHistoryModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.ImageProcess;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.RecyclerTouchListener;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;
import static com.ivaccessbeta.fragment.flotingNotification.services.ChatHeadService.iconView;
import static com.ivaccessbeta.utils.Utils.hasPermissions;

public class JobDetailFragment extends Fragment implements AsynchTaskListner, DocumentViewClickListener, LogOnClick {
    private static final String TAG = "JobDetailFragment";
    public RecyclerView rvDocument, rvLogHistory, rvJobStatus;
    public LinearLayout llChildDetail, llHospital;
    public RecyclerView.LayoutManager layoutManager;
    public TextView txtJobAcknologe, txtCreatedDate, txtDueDate, txtFacilityName, txtPatientName,
            txtDOB, txtRoome, txtService, txtContractorName, txtJobNo, txtPhoneNo, txtAddress,
            txtBillTo, txtStatusName, txtEquipmentQTy, txtCheck, txtBill, txtNoteName;
    public RelativeLayout rlPlush, rlMinush, rlJobDetail, rlJobStatus, rlBillTo, relativeImage, rlTop, rlTopBill, relativeCheck;
    public ViewPager mViewPager;
    public ImageView ivAction, ivDocStatus, ivDoc, ivStatusInfo, ivApprove, ivDeny, ivDelete, ivPdf,
            ivClose, ivLeft, ivRight, ivStatusInfoBill, ivStatusInfoCheck, ivBillTo, ivCheck, ivDown;
    public LinearLayout llQuantity, llNote;
    public View checkView, billView;
    public CardView rlJobNo;
    public int click = 1;

    public DocumentListAdapter documentListAdapter;
    public JobStatusAdapter jobStatusAdapter;
    public LogHistoryAdapter LogListAdapter;
    public static ArrayList<JobDetailsModel> documentList = new ArrayList<>();
    public ArrayList<LogHistoryModel> logList = new ArrayList<>();
    public ArrayList<JobDetailsModel> jobDetailList = new ArrayList<>();
    public ArrayList<JobStatusModel> jobStatusList = new ArrayList<>();
    public View view;


    public JobDetailFragment instance;

    public String jobId, time, formattedTime, createdBy;

    public String emailId, token, userId, dob, equipmentQTY, statusQTY, billToStatus = "0", checkStatus = "0", noteAmount, amountCheck,
            address, colorCode, statusId = "", jobNo, roleId, notificationMessage;
    public JobDetailsModel jobDetailsModel;
    //    public String imgPath = "";
    public final int PICK_IMAGE_CAMERA = 1;

    //    public Uri selectedUri;
    private String mCameraPhotoPath;
    public int imagePosition;

    public int qtyCount = 1;

    public String timeStamp;
    public DocumentImagePagerAdapter photoViewAdapter;
    public OnActivityResult onActivityResultListner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_job_detail, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        instance = this;

        jobId = getArguments().getString("jobNo");
        iconView.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        llChildDetail = view.findViewById(R.id.llChildDetail);
        llHospital = view.findViewById(R.id.llHospital);
        rvDocument = view.findViewById(R.id.rvDocList);
        rvLogHistory = view.findViewById(R.id.rvLogHistory);
        ivAction = view.findViewById(R.id.iv_action);
        txtJobAcknologe = view.findViewById(R.id.txtJobAcknologe);
        txtContractorName = view.findViewById(R.id.txtContractorName);
        txtDOB = view.findViewById(R.id.txtDOB);
        txtCreatedDate = view.findViewById(R.id.txtCreatedDate);
        txtFacilityName = view.findViewById(R.id.txtFacilityName);
        txtDueDate = view.findViewById(R.id.txtDueDate);
        txtRoome = view.findViewById(R.id.txtRoome);
        txtPatientName = view.findViewById(R.id.txtPatientName);
        txtService = view.findViewById(R.id.txtService);
        txtJobNo = view.findViewById(R.id.txtJobNo);
        txtPhoneNo = view.findViewById(R.id.txtPhoneNo);
        txtAddress = view.findViewById(R.id.txtAddress);
        rlJobDetail = view.findViewById(R.id.rlJobDetail);
        rlJobNo = view.findViewById(R.id.rv_jobNo);
        rlJobStatus = view.findViewById(R.id.rlJobStatus);
        rlBillTo = view.findViewById(R.id.relativeBill);
        mViewPager = view.findViewById(R.id.pager);
        ivApprove = view.findViewById(R.id.ivApprove);
        ivClose = view.findViewById(R.id.ivClose);
        ivDeny = view.findViewById(R.id.ivDeny);
        ivPdf = view.findViewById(R.id.ivPdf);
        ivDelete = view.findViewById(R.id.ivDelete);
        ivLeft = view.findViewById(R.id.ivLeft);
        ivRight = view.findViewById(R.id.ivRight);
        relativeImage = view.findViewById(R.id.relativeImage);
        txtBillTo = view.findViewById(R.id.txtBillTo);
        ivStatusInfo = view.findViewById(R.id.ivStatusInfo);
        txtStatusName = view.findViewById(R.id.txtStatusName);
        txtEquipmentQTy = view.findViewById(R.id.txtEquipmentQTy);
        rlPlush = view.findViewById(R.id.rl_plush);
        rlMinush = view.findViewById(R.id.rl_minush);
        ivStatusInfoBill = view.findViewById(R.id.ivStatusInfoBill);
        ivStatusInfoCheck = view.findViewById(R.id.ivStatusInfoCheck);
        ivCheck = view.findViewById(R.id.ivCheck);
        ivBillTo = view.findViewById(R.id.ivBillTo);
        llQuantity = view.findViewById(R.id.relativeQty);
        txtCheck = view.findViewById(R.id.txtCheck);
        txtBill = view.findViewById(R.id.txtBill);
        checkView = view.findViewById(R.id.checkView);
        billView = view.findViewById(R.id.billView);
        ivDown = view.findViewById(R.id.ic_down);
        rlTop = view.findViewById(R.id.rlTop);
        rlTopBill = view.findViewById(R.id.rlTopBill);
        relativeCheck = view.findViewById(R.id.relativeCheck);
        llNote = view.findViewById(R.id.llNote);
        txtNoteName = view.findViewById(R.id.txtNoteName);
        ivDocStatus = view.findViewById(R.id.ivdoc);
        //changeJobCheckStatus();

        // changeJobBillToStatus();

        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            ivDown.setVisibility(View.GONE);
        }


        txtEquipmentQTy.setVisibility(View.VISIBLE);
        rlMinush.setVisibility(View.VISIBLE);
        rlPlush.setVisibility(View.VISIBLE);
        rlPlush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qtyCount = qtyCount + 1;
                equipmentQTY = String.valueOf(qtyCount);
                txtEquipmentQTy.setText(equipmentQTY);
                statusQTY = "plus";
                updateEquipmentQTY();
            }
        });
        rlMinush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qtyCount > 0) {
                    qtyCount = qtyCount - 1;
                    equipmentQTY = String.valueOf(qtyCount);
                    txtEquipmentQTy.setText(equipmentQTY);
                    statusQTY = "minus";
                    updateEquipmentQTY();
                }
            }
        });
        txtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + address));
                startActivity(searchAddress);
            }
        });
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            rlBillTo.setVisibility(View.GONE);
        }

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeImage.setVisibility(View.GONE);
                toolbar.setVisibility(View.VISIBLE);
            }
        });
        ivApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.VISIBLE);
                approveDocument();
            }
        });
        ivDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.VISIBLE);
                denyDocument();
            }
        });
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toolbar.setVisibility(View.VISIBLE);
                if (documentList.size() == 0) {
                    relativeImage.setVisibility(View.GONE);
                } else {
                    deleteDocument();
                }

            }
        });
        ivPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.VISIBLE);
                printDocument();
            }
        });
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(getItem(1), true);
            }
        });
        llHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click = click + 1;
                if ((click % 2) == 0) {
                    llChildDetail.setVisibility(View.VISIBLE);
                    if (jobDetailList.get(0).getFacilityNote().equalsIgnoreCase("")) {
                        llNote.setVisibility(View.GONE);
                    } else {
                        llNote.setVisibility(View.VISIBLE);
                    }
                } else {
                    llChildDetail.setVisibility(View.GONE);
                    llNote.setVisibility(View.GONE);
                }
            }
        });
        rlJobStatus.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                //===Manager Can not Change Job Status====//
                if (!roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
                    showJobStatusPopup(getActivity(), x, y);
                } else {
                    ivDown.setVisibility(View.GONE);
                }

//                PopupMenu popup = new PopupMenu(getActivity(), v);
//                popup.setOnMenuItemClickListener(instance);
//                popup.inflate(R.menu.job_status);
//                @SuppressLint("RestrictedApi") MenuPopupHelper menuHelper2 = new MenuPopupHelper(getActivity(), (MenuBuilder) popup.getMenu(), v);
//                menuHelper2.setForceShowIcon(true);
//                popup.show();
            }
        });


        ivAction.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                showJobActionPopup(getActivity(), x, y);
            }

        });
        //getJobStatus();
        return view;
    }

    private void printDocument() {
        if (documentList.get(imagePosition).getIsPrint().equals(Constant.DOCUMENT_ISPRINT)) {
            new CallRequest(instance).printDocument(emailId, token, documentList.get(imagePosition).getJDID(), userId, Constant.DOCUMENT_ISNOTPRINT);
        } else {
            new CallRequest(instance).printDocument(emailId, token, documentList.get(imagePosition).getJDID(), userId, Constant.DOCUMENT_ISPRINT);
        }
    }

    private void changeJobBillToStatus() {

        //  if (!statusId.equalsIgnoreCase(Constant.STATUS_CREATED) && !statusId.equalsIgnoreCase(Constant.STATUS_ACKNLOLOG)) {
        if (billToStatus.equalsIgnoreCase(Constant.BILLTO_CREATED)) {
            ivStatusInfoBill.setImageResource(R.drawable.ic_dot_green);

            if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {

                ivBillTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        billToStatus = Constant.BILLTO_NOT_CREATED;
                        ivStatusInfoBill.setVisibility(View.GONE);

                        jobBillApproved();
                    }
                });
            } else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {

                ivBillTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ivStatusInfoBill.setVisibility(View.VISIBLE);
                        ivStatusInfoBill.setImageResource(R.drawable.ic_right_green);
                        billToStatus = Constant.BILLTO_RIGHT;
                        jobBillApproved();
                    }
                });

            }
        } else if (billToStatus.equalsIgnoreCase(Constant.BILLTO_NOT_CREATED)) {
            ivStatusInfoBill.setVisibility(View.GONE);
            if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {
                ivBillTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        billToStatus = Constant.BILLTO_CREATED;
                        ivStatusInfoBill.setVisibility(View.VISIBLE);
                        ivStatusInfoBill.setImageResource(R.drawable.ic_dot_green);
                        jobBillApproved();
                    }
                });
            } else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {
                ivBillTo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ivStatusInfoBill.setVisibility(View.VISIBLE);
                        ivStatusInfoBill.setImageResource(R.drawable.ic_right_green);
                        billToStatus = Constant.BILLTO_RIGHT;
                        jobBillApproved();
                    }
                });
            }
        } else {
            ivStatusInfoBill.setVisibility(View.VISIBLE);
            ivStatusInfoBill.setImageResource(R.drawable.ic_right_green);
            ivBillTo.setClickable(false);
        }
    }


    private void changeJobCheckStatus() {

        System.out.println("status id==" + statusId);
        //  if (!statusId.equalsIgnoreCase(Constant.STATUS_CREATED) && !statusId.equalsIgnoreCase(Constant.STATUS_ACKNLOLOG)) {
        if (checkStatus.equalsIgnoreCase(Constant.CHECK_CREATED)) {
            ivStatusInfoCheck.setImageResource(R.drawable.ic_dot_green);
            if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {
                ivCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkStatus = Constant.CHECK_NOT_CREATED;
                        ivStatusInfoCheck.setVisibility(View.GONE);
                        jobCheckApproved();
                    }
                });
            } else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {
                ivCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ivStatusInfoCheck.setVisibility(View.VISIBLE);
                        ivStatusInfoCheck.setImageResource(R.drawable.ic_right_green);
                        checkStatus = Constant.CHECK_RIGHT;
                        jobCheckApproved();
                    }
                });
            }
        } else if (checkStatus.equalsIgnoreCase(Constant.CHECK_NOT_CREATED)) {
            ivStatusInfoCheck.setVisibility(View.GONE);
            if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {

                ivCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkStatus = Constant.CHECK_CREATED;
                        ivStatusInfoCheck.setVisibility(View.VISIBLE);
                        ivStatusInfoCheck.setImageResource(R.drawable.ic_dot_green);
                        jobCheckApproved();
                    }
                });
            } else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {
                ivCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ivStatusInfoCheck.setImageResource(R.drawable.ic_right_green);
                        checkStatus = Constant.CHECK_RIGHT;
                        jobCheckApproved();
                    }
                });
            }
        } else {
            ivStatusInfoCheck.setVisibility(View.VISIBLE);
            ivStatusInfoCheck.setImageResource(R.drawable.ic_right_green);
            ivCheck.setClickable(false);
        }
        //  }
    }

    private void jobCheckApproved() {
        new CallRequest(instance).jobCheckApprove(emailId, token, userId, jobId, checkStatus);
    }

    private void jobBillApproved() {
        new CallRequest(instance).jobBillToApprove(emailId, token, userId, jobId, billToStatus);
    }

    private void updateEquipmentQTY() {
        new CallRequest(instance).updateEquipmentQTY(emailId, token, statusQTY, jobId);
    }

    private void approveDocument() {
        new CallRequest(instance).documentApprove(emailId, token, userId, documentList.get(imagePosition).getJDID(), "Approve");
    }

    private void denyDocument() {
        new CallRequest(instance).documentDeny(emailId, token, userId, documentList.get(imagePosition).getJDID(), "Deny");
    }

    private void deleteDocument() {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).documentDelete(emailId, token, userId, documentList.get(imagePosition).getJDID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();

    }

    private void showJobActionPopup(final Activity context, int x, int y) {
        final PopupWindow popup = new PopupWindow(context);
        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_job_action, viewGroup);
        LinearLayout markArrivalTime = layout.findViewById(R.id.MarkArrivalTime);
        LinearLayout AddNote = layout.findViewById(R.id.AddNote);
        final LinearLayout AddDoc = layout.findViewById(R.id.AddDoc);
        LinearLayout SendNotification = layout.findViewById(R.id.SendNotification);
        LinearLayout EditJob = layout.findViewById(R.id.EditJob);
        LinearLayout Print = layout.findViewById(R.id.Print);
        LinearLayout CheckAmount = layout.findViewById(R.id.CheckAmount);
        LinearLayout InvoiceAmount = layout.findViewById(R.id.InvoiceAmount);
        LinearLayout BillingDate = layout.findViewById(R.id.BillingDate);
        LinearLayout Archive = layout.findViewById(R.id.Archive);
        LinearLayout Delete = layout.findViewById(R.id.Delete);


        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) && !createdBy.equalsIgnoreCase(userId)) {
            //EditJob.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
        }
        //======for contractor login=====
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            Print.setVisibility(View.GONE);
            CheckAmount.setVisibility(View.GONE);
            SendNotification.setVisibility(View.GONE);
            //   SendNotification.setVisibility(View.GONE);
            InvoiceAmount.setVisibility(View.GONE);
            BillingDate.setVisibility(View.GONE);
            Archive.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
        }
        // ======for Manager Login======

        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            markArrivalTime.setVisibility(View.GONE);
            AddDoc.setVisibility(View.GONE);
            AddNote.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
            CheckAmount.setVisibility(View.GONE);
            InvoiceAmount.setVisibility(View.GONE);
            EditJob.setVisibility(View.GONE);
            SendNotification.setVisibility(View.GONE);

        }
        if (billToStatus.equalsIgnoreCase(Constant.BILLTO_RIGHT) || checkStatus.equalsIgnoreCase(Constant.CHECK_RIGHT)) {
            EditJob.setVisibility(View.GONE);
        }
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);

        markArrivalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                selectDateEndTimePicker();
            }
        });
        AddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                dialogAddNote();
            }
        });
        AddDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraPhotoPath = "";
                popup.dismiss();
                dialogAddDoc();
            }
        });
        SendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                dialogSendNotification();
            }
        });
        EditJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                editJob();
            }
        });
        CheckAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                checkAmount();
            }
        });

        InvoiceAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                invoiceAmount();
            }
        });
        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) && createdBy.equalsIgnoreCase(userId)) {
                    popup.dismiss();
                    deleteJob();
                } else if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID)) {
                    popup.dismiss();
                    deleteJob();
                } else {
                    Utils.showToast("Not Editable", getActivity());
                }
            }
        });
        BillingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                selectBillToDate();
            }
        });
        Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                new CallRequest(instance).printJob(emailId, token, jobId);
            }
        });
    }

    private void selectBillToDate() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_select_billing_date);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        final EditText edtDate = dialog.findViewById(R.id.edtDate);
        // final EditText edtTime = dialog.findViewById(R.id.edtTime);
        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getActivity(), edtDate);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Date = edtDate.getText().toString().trim();

                if (Date.equalsIgnoreCase("")) {
                    Utils.showToast("Select Date ", getActivity());
                } else {
                    String sendingDate = null;
                    try {
                        StringBuilder dateFormBuilder = new StringBuilder();
                        dateFormBuilder = dateFormBuilder.append(Date.substring(6)).append("-").append(Date, 0, 2).append("-").append(Date, 3, 5);
                        sendingDate = String.valueOf(dateFormBuilder);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    new CallRequest(instance).billingDate(emailId, token, userId, jobId, sendingDate);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void deleteJob() {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteJob(emailId, token, jobId, userId);
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    private void checkAmount() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_check_amount);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        final EditText edtNotes = dialog.findViewById(R.id.edtNote);
        final EditText edtAmount = dialog.findViewById(R.id.edtAmount);
        TextView txtHeader = dialog.findViewById(R.id.txtHeader);

        txtHeader.setText("Custom Check Amount");
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteAmount = edtNotes.getText().toString();
                amountCheck = edtAmount.getText().toString();
                if (amountCheck.equals("")) {
                    Utils.showToast("Please Add Note", getContext());
                } else if (noteAmount.equals("")) {
                    Utils.showToast("Please Add Amount", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addCheckAmount(emailId, token, userId, jobId, amountCheck, noteAmount);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void invoiceAmount() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_check_amount);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        TextView txtHeader = dialog.findViewById(R.id.txtHeader);
        txtHeader.setText("Custom Invoice Amount");
        final EditText edtNotes = dialog.findViewById(R.id.edtNote);
        final EditText edtAmount = dialog.findViewById(R.id.edtAmount);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteAmount = edtNotes.getText().toString();
                amountCheck = edtAmount.getText().toString();
                if (amountCheck.equals("")) {
                    Utils.showToast("Please Add Note", getContext());
                } else if (noteAmount.equals("")) {
                    Utils.showToast("Please Add Amount", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addInvoiceAmount(emailId, token, userId, jobId, amountCheck, noteAmount);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showJobStatusPopup(final Activity context, int x, int y) {
        final PopupWindow popup = new PopupWindow(context);
        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_custom_job_status, viewGroup);

        rvJobStatus = layout.findViewById(R.id.jobStatus);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvJobStatus.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvJobStatus.setItemAnimator(new DefaultItemAnimator());
        rvJobStatus.setLayoutManager(mLayoutManager);
        rvJobStatus.setAdapter(jobStatusAdapter);
        //jobStatusAdapter.notifyDataSetChanged();
        rvJobStatus.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvJobStatus, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                int oldStausId = 0, newStatusId = 0;
                oldStausId = Integer.parseInt(statusId);
                statusId = jobStatusList.get(position).getJSID();
                newStatusId = Integer.parseInt(statusId);
                if (statusId.equals(Constant.STATUS_CANCELLED)) {
                    dialogAddJobStatusNote();
                } else if (statusId.equals(Constant.STATUS_SERVICE_NOT_RENDER)) {
                    dialogAddJobStatusNote();
                } else {
                    // if(oldStausId<newStatusId) {
                    new CallRequest(instance).updateJobStatus(emailId, token, userId, jobId, statusId, "");
                    // }
                }
                popup.dismiss();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
    }

    private void dialogAddJobStatusNote() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString().trim();
                if (note.equals("")) {
                    Utils.showToast("Please add Note", getActivity());
                } else {
                    new CallRequest(instance).updateJobStatus(emailId, token, userId, jobId, statusId, note);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void getJobDetail() {
        new CallRequest(instance).getJobDetail(emailId, token, jobId);
    }

    private void getJobStatus() {
        try {
            new CallRequest(instance).getJobStatus(emailId, token, userId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }


    public void selectDateEndTimePicker() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_select_date_and_time);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        final EditText edtDate = dialog.findViewById(R.id.edtDate);
        final EditText edtTime = dialog.findViewById(R.id.edtTime);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = df.format(c);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        String str = sdf.format(new Date());

        edtDate.setText(formattedDate);
        edtTime.setText(str);
        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Utils.generateDatePicker(getActivity(), edtDate);
            }
        });
        edtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTime(edtTime);
                // Utils.generateDatePicker(getActivity(),edtDate);
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Date = edtDate.getText().toString().trim();
                time = edtTime.getText().toString().trim();

                if (Date.equalsIgnoreCase("")) {
                    Utils.showToast("Select Date ", getActivity());
                } else if (time.equalsIgnoreCase("")) {
                    Utils.showToast("Select Time", getActivity());
                } else {
                    String sendingDate = null;
                    try {
                        StringBuilder dateFormBuilder = new StringBuilder();
                        dateFormBuilder = dateFormBuilder.append(Date.substring(6)).append("-").append(Date, 0, 2).append("-").append(Date, 3, 5);
                        sendingDate = String.valueOf(dateFormBuilder);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String FianlDatAndTime = sendingDate + " " + time;
                    new CallRequest(instance).markArrivalTime(emailId, token, userId, jobId, FianlDatAndTime);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    public void getTime(final EditText edt) {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time = selectedHour + ":" + selectedMinute;
                Log.d("time==", time);
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time);
                } catch (ParseException e) {

                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                formattedTime = fmtOut.format(date);
                Log.d("formattedTime==", formattedTime);
                edt.setText(formattedTime);
            }

        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void editJob() {
        AddJobActivity fragment = new AddJobActivity();
        Bundle args = new Bundle();
        args.putInt("value", 1);
        args.putSerializable("JobDetailModel", jobDetailsModel);
        fragment.setArguments(args);
        ((MainActivity)getActivity()).changeFragment(fragment, true);
    }

    public void dialogSendNotification() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_send_notification);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnSend);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        TextView txtHeader = dialog.findViewById(R.id.txtHeader);
        txtHeader.setText("Send Notification Job# -" + jobNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationMessage = edtNotes.getText().toString();
                if (notificationMessage.equals("")) {
                    Utils.showToast("Please Add Note", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).sendJobNotification(emailId, token, userId, jobId, notificationMessage);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dialogAddDoc() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_doc);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        LinearLayout linearAddDoc = dialog.findViewById(R.id.linearAddDoc);
        //txtFileName = dialog.findViewById(R.id.txtFileName);
        ivDoc = dialog.findViewById(R.id.ivDoc);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCameraPhotoPath.equalsIgnoreCase("")) {
                    Utils.showToast("Please Select Image", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addJobDoc(emailId, token, jobId, userId, mCameraPhotoPath);

                }
            }
        });

        linearAddDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {android.Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!hasPermissions(getActivity(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PICK_IMAGE_CAMERA);
                    } else {
                        selectImage();
                    }
                } else {
                    selectImage();
                }


            }
        });
        dialog.show();
    }

    public void selectImage() {
        try {
            //  final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            final CharSequence[] options = {"Take Photo"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = null;
                        try {
                            file = ImageProcess.createImageFile(getActivity());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (file != null) {
                            mCameraPhotoPath = "file:" + file.getAbsolutePath();
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                        } else {
                            intent = null;
                        }
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != 0 && requestCode == PICK_IMAGE_CAMERA) {
            Log.d(TAG, "onActivityResult: 12 " + mCameraPhotoPath);
            if (mCameraPhotoPath != null) {
                Log.d(TAG, "onActivityResult: 13 " + ImageProcess.getUriRealPath(getActivity(), Uri.fromFile(new File(mCameraPhotoPath))).substring(1));
                try {
                    Uri uri = ImageProcess.handleSamplingAndRotationBitmap(getActivity(), Uri.parse(mCameraPhotoPath), new File(mCameraPhotoPath).getAbsolutePath());
                    ivDoc.setImageURI(uri);
                    mCameraPhotoPath = new File(uri.getPath()).getAbsolutePath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//        if (requestCode == PICK_IMAGE_CAMERA && resultCode != 0) {
//            ByteArrayOutputStream bytes = null;
//            Bitmap photo = null;
//            try {
//                imgPath = ImageProcess.getUriRealPath(getActivity(), selectedUri);
//                selectedUri = Uri.fromFile(new File(imgPath));//ImageProcess.handleSamplingAndRotationBitmap(getActivity(), selectedUri, new File(imgPath).getAbsolutePath());
//
//                Log.d(" dsds dsad sad ", "onActivityResult: " + imgPath);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            if (photo != null)
//                ivDoc.setImageBitmap(photo);
//        }
    }

    public void dialogJobDelete() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_delete_job);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        dialogButton.setOnClickListener(v -> {
            getActivity().onBackPressed();
            dialog.dismiss();
        });
        dialog.show();
    }

    public void dialogAddNote() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        dialogButton.setOnClickListener(v -> {
            String note = edtNotes.getText().toString().trim();
            if (note.equals("")) {
                Utils.showToast("Please add Note", getActivity());
            } else {
                new CallRequest(instance).addJobNote(emailId, token, userId, jobId, note);
            }
            dialog.dismiss();
        });
        dialog.show();
    }

    public void dialogMakeArrival() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_mark_arrival_time);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public final static long MILLIS_PER_DAY = 24 * 60 * 60 * 1000L;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getJobDetail:
                    logList.clear();
                    documentList.clear();
                    jobDetailList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            if (!TextUtils.isEmpty(mCameraPhotoPath)) {
                                ImageProcess.scanFile(getActivity(), mCameraPhotoPath);
                            }
                            clearData();
                            JSONObject jObject = jObj.getJSONObject("data");

                            jobDetailsModel = new JobDetailsModel();
                            jobDetailsModel = LoganSquare.parse(jObject.toString(), JobDetailsModel.class);
                            jobDetailList.add(jobDetailsModel);

                            String isDocPaarove = jObject.getString("job_docs_status");
                            System.out.println("status doc===" + isDocPaarove);
                            jobNo = jobDetailList.get(0).getJobNo();
                            MainActivity.toolbar_title.setText(jobNo + " DETAILS");
                            ((MainActivity) getActivity()).showToolbarAction();
                            txtJobNo.setText(jobNo);
                            txtContractorName.setText(jobDetailList.get(0).getFirst_name() + " " + jobDetailList.get(0).getLast_name());
                            txtFacilityName.setText(jobDetailList.get(0).getFacilityName());
                            //txtDueDate.setText("Due: " + ChildJobDetailList.get(0).getDueTimeDate());
                            txtRoome.setText(jobDetailList.get(0).getRoomNumber());
                            txtPatientName.setText(jobDetailList.get(0).getPatientName());
                            txtService.setText(jobDetailList.get(0).getServiceName());
                            colorCode = jobDetailList.get(0).getJobStatusColor();
                            txtPhoneNo.setText(jobDetailList.get(0).getPhone());
                            txtPhoneNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                    callIntent.setData(Uri.parse("tel:" + jobDetailList.get(0).getPhone()));
                                    startActivity(callIntent);
                                }
                            });
                            address = jobDetailList.get(0).getAddress() + " , " + jobDetailList.get(0).getState() + ", " + jobDetailList.get(0).getCity() + ", " + jobDetailList.get(0).getZip();
                            txtAddress.setText(address);
                            colorCode = jobDetailList.get(0).getJobStatusColor();
                            statusId = jobDetailList.get(0).getStatusID();
                            rlJobDetail.setBackgroundColor(Color.parseColor(colorCode));
                            txtJobAcknologe.setText(jobDetailList.get(0).getJobStatusName());
                            txtEquipmentQTy.setText(jobDetailList.get(0).getEquipmentQuantity());
                            qtyCount = Integer.parseInt((jobDetailList.get(0).getEquipmentQuantity()));
                            String createdDate = jobDetailList.get(0).getCreatedOn();
                            String dueDate = jobDetailList.get(0).getDueTimeDate();
                            createdBy = jobDetailList.get(0).getCreatedBy();
                            txtDOB.setText(jobDetailList.get(0).getDOB());
                            billToStatus = jobDetailList.get(0).getIsBill();
                            checkStatus = jobDetailList.get(0).getIsCheck();

                            String checkRate = jobDetailList.get(0).getCheckRate();
                            String customCheckRate = jobDetailList.get(0).getCustomCheckAmount();
                            String billRate = jobDetailList.get(0).getBillRate();
                            String customBillRate = jobDetailList.get(0).getCustomBillAmount();
                            String updateCCA = jobDetailList.get(0).getUpdateCCA();
                            String updateCBA = jobDetailList.get(0).getUpdateCBA();

                            if (updateCCA.equalsIgnoreCase("1")) {
                                checkView.setVisibility(View.VISIBLE);
                                txtCheck.setText(" $ " + checkRate + " $ " + customCheckRate);
                            } else {
                                txtCheck.setText(" $ " + checkRate);
                            }
                            if (!checkStatus.equalsIgnoreCase("2") || !billToStatus.equalsIgnoreCase("2")) {
                                ivAction.setVisibility(View.VISIBLE);
                            } else {
                                ivAction.setVisibility(View.GONE);
                            }


                            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) || roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
                                llQuantity.setVisibility(View.VISIBLE);
                                rlPlush.setVisibility(View.VISIBLE);
                                rlMinush.setVisibility(View.VISIBLE);
                            }
                            boolean isAc;
                            if ((checkStatus.equalsIgnoreCase("2") && billToStatus.equalsIgnoreCase("2"))|| jobDetailList.get(0).getIsArchive().equalsIgnoreCase("1")) {
                                isAc=false;
                            } else {
                                isAc=true;
                            }
                            if (isAc){
                                rlPlush.setVisibility(View.VISIBLE);
                            }else {
                                rlPlush.setVisibility(View.GONE);
                            }
                            int qt = Integer.parseInt(jobDetailList.get(0).getEquipmentQuantity());
                            if (qt > 0 && isAc) {
                                rlMinush.setVisibility(View.VISIBLE);
                            } else
                                rlMinush.setVisibility(View.GONE);



//                            if (jobDetailList.size() > 0 && jobDetailList.get(0).getIvAccessSupply() != null && !TextUtils.isEmpty(jobDetailList.get(0).getIvAccessSupply())) {
//                                rlPlush.setVisibility(View.VISIBLE);
//                                rlMinush.setVisibility(View.VISIBLE);
//                            } else {
//                                rlPlush.setVisibility(View.GONE);
//                                rlMinush.setVisibility(View.GONE);
//                            }


//                            if (jobDetailList.size() > 0 && jobDetailList.get(0).getIsArchive() != null && !TextUtils.isEmpty(jobDetailList.get(0).getIsArchive())) {
//                                if (!jobDetailList.get(0).getIsArchive().equalsIgnoreCase("0")) {
//                                    rlPlush.setVisibility(View.VISIBLE);
//                                } else
//                                    rlPlush.setVisibility(View.GONE);
//                            } else {
//                                rlPlush.setVisibility(View.GONE);
//                            }




                            if (!customBillRate.equalsIgnoreCase(Constant.CUSTOM_BILL_RATE)) {
                                billView.setVisibility(View.VISIBLE);
                                txtBill.setText(" $ " + billRate + " $ " + customBillRate);
                            } else if (updateCBA.equalsIgnoreCase("1")) {
                                checkView.setVisibility(View.VISIBLE);
                                txtCheck.setText(" $ " + checkRate + " $ " + customCheckRate);
                            } else {
                                txtBill.setText(" $ " + billRate);
                            }
                            //=====plush minush quantity========//
                            changeJobCheckStatus();
                            changeJobBillToStatus();

                            if (checkStatus.equalsIgnoreCase(Constant.CHECK_CREATED)) {
                                ivStatusInfoCheck.setVisibility(View.VISIBLE);
                                ivStatusInfoCheck.setImageResource(R.drawable.ic_dot_green);
                            }
                            if (billToStatus.equalsIgnoreCase(Constant.BILLTO_CREATED)) {
                                ivStatusInfoBill.setVisibility(View.VISIBLE);
                                ivStatusInfoBill.setImageResource(R.drawable.ic_dot_green);
                            }
                            Boolean isWithinPrior24Hours = null;
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date date = dateFormat.parse(createdDate);
                            System.out.println(date);
                            try {
                                String strDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
                                Date strDate = dateFormat.parse(strDateString);
                                System.out.println("current Date==" + strDate + "Created Date==" + date);
                                isWithinPrior24Hours = Math.abs(strDate.getTime() - date.getTime()) > MILLIS_PER_DAY;
                                System.out.println("is24Hours==" + isWithinPrior24Hours);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (statusId.equalsIgnoreCase(Constant.STATUS_CREATED)
                                    || statusId.equalsIgnoreCase(Constant.STATUS_ACKNLOLOG)
                                    || roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)
                                    || roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) ||
                                    jobDetailList.get(0).getDocsApprove().equals("0") ||
                                    jobDetailList.get(0).getDocsApprove().equalsIgnoreCase("0")/*|| !isWithinPrior24Hours*/) {
                                ivCheck.setClickable(false);
                                ivBillTo.setClickable(false);
                            } else {
                                ivCheck.setClickable(true);
                                ivBillTo.setClickable(true);
                            }

                            if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
                                relativeCheck.setVisibility(View.GONE);
                                rlTopBill.setVisibility(View.GONE);
                                rlTop.setVisibility(View.GONE);
                            }
                            txtBillTo.setText("Bill To " + jobDetailList.get(0).getBillName());
                            String note = jobDetailList.get(0).getFacilityNote();
                            if (note.equalsIgnoreCase("")) {
                                llNote.setVisibility(View.GONE);
                            } else {
                                txtNoteName.setText(jobDetailList.get(0).getFacilityNote());
                            }
                            if (dueDate.equalsIgnoreCase("0000-00-00 00:00:00") || dueDate.equalsIgnoreCase("1970-01-01 00:00:00")) {
                                txtDueDate.setText("");
                            } else {
                                dateConvertFormate(txtDueDate, dueDate, "Due:");
                            }
                            if (TextUtils.isEmpty(createdDate)) {
                                txtCreatedDate.setText("");
                            } else {
                                dateConvertFormate(txtCreatedDate, createdDate, "Created:");
                            }
                            rlJobNo.setBackgroundResource(R.drawable.gray_sqare_round);
                            GradientDrawable gd = (GradientDrawable) rlJobNo.getBackground().getCurrent();
                            gd.setColor(Color.parseColor(colorCode));
                            gd.setCornerRadii(new float[]{10, 10, 10, 10, 10, 10, 10, 10});
                            gd.setStroke(2, Color.parseColor("#000000"), 5, 0);
                            JSONArray jArray = jObject.getJSONArray("job_docs");
                            JobDetailsModel model = null;
                            documentList.addAll(LoganSquare.parseList(jArray.toString(), JobDetailsModel.class));

                            for (int i = 0; documentList.size() > i; i++) {
                                model = documentList.get(i);

                                if (App.title.equalsIgnoreCase(Constant.JOB_DOC_APPROVE) || App.title.equalsIgnoreCase(Constant.JOB_DOC_UPLOAD) || App.isFromNotificationFragment) {
                                    if (App.docId.equalsIgnoreCase(documentList.get(i).getJDID())) {
                                        System.out.println("View pager position==" + i);
                                        imagePosition = i;
                                        relativeImage.setVisibility(View.VISIBLE);
                                        photoViewAdapter = new DocumentImagePagerAdapter(getContext(), documentList);
                                        mViewPager.setAdapter(photoViewAdapter);
                                        toolbar.setVisibility(View.GONE);
                                    }
                                    mViewPager.setCurrentItem(imagePosition);
//                                    photoViewAdapter.notifyDataSetChanged();
                                }

                            }
                            documentListAdapter = new DocumentListAdapter(documentList, this);
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            rvDocument.setItemAnimator(new DefaultItemAnimator());
                            rvDocument.setLayoutManager(horizontalLayoutManagaer);
                            rvDocument.setAdapter(documentListAdapter);

                            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                                ivApprove.setVisibility(View.GONE);
                                ivDeny.setVisibility(View.GONE);
                                if (documentList.size() != 0) {
                                    if (model.getDocumentStatus().equals(Constant.DOCUMENT_DENY) || model.getDocumentStatus().equals(Constant.DOCUMENT_APPROVE)) {
                                        ivDelete.setVisibility(View.GONE);
                                    } else {
                                        ivDelete.setVisibility(View.VISIBLE);
                                    }
                                }
                            } else {
                                ivApprove.setVisibility(View.VISIBLE);
                                ivDeny.setVisibility(View.VISIBLE);
                            }
                            //==== Manager Only Views Document====

                            if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
                                ivApprove.setVisibility(View.GONE);
                                ivDelete.setVisibility(View.GONE);
                                ivDeny.setVisibility(View.GONE);
                                ivPdf.setVisibility(View.GONE);
                            }
                            //=====contractor can not able to click on blue pdf icon=====
                            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                                ivPdf.setVisibility(View.GONE);
                            }


                            if (jobDetailList.get(0).getDocsApprove().equalsIgnoreCase(Constant.One)) {
                                txtStatusName.setText("DOCS approved");
                                ivStatusInfo.setImageResource(R.drawable.ic_right_green);
                            } else {
                                ivStatusInfo.setImageResource(R.drawable.ic_exclamantion);
                                txtStatusName.setText("DOCS Not approved");
                            }
                            if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {

                                ivDocStatus.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (jobDetailList.get(0).getDocsApprove().equalsIgnoreCase(Constant.Zero)) {
                                            new CallRequest(instance).approvDocStatus(emailId, token, userId, jobId);
                                        } else if (jobDetailList.get(0).getDocsApprove().equalsIgnoreCase(Constant.One)) {
                                            new CallRequest(instance).denyDocStatus(emailId, token, userId, jobId);
                                        }
                                    }
                                });

                            }

                            if (isDocPaarove.equalsIgnoreCase(Constant.IS_DOC_STATUS)) {
                                ivDocStatus.setClickable(true);
                            } else if (documentList.size() == 0) {
                                ivDocStatus.setClickable(true);

                            } else {
                                ivDocStatus.setClickable(false);
                            }
                            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                                @Override
                                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                    imagePosition = position;
                                }

                                @Override
                                public void onPageSelected(int position) {
                                    imagePosition = position;
                                }

                                @Override
                                public void onPageScrollStateChanged(int state) {

                                }
                            });
                            JSONArray jArrayLog = jObject.getJSONArray("job_logs");
                            logList.clear();
//                            for (int i = 0; jArrayLog.length() > i; i++) {
//                                LogHistoryModel hmodel = new LogHistoryModel();
//                                hmodel = (LogHistoryModel) jParsar.parseJson(jArrayLog.getJSONObject(i), new LogHistoryModel());
//
//                                if (jArrayLog.getJSONObject(i).has("CheckData")) {
//                                    JSONObject jsonObject = jArrayLog.getJSONObject(i).getJSONObject("CheckData");
//                                    if (jsonObject.has("CheckDate"))
//                                        hmodel.setCheckDate(jsonObject.getString("CheckDate"));
//                                    if (jsonObject.has("UserID"))
//                                        hmodel.setUserID(jsonObject.getString("UserID"));
//                                }
//                                logList.add(hmodel);
//                            }
                            logList.addAll(LoganSquare.parseList(jArrayLog.toString(), LogHistoryModel.class));
                            LogListAdapter = new LogHistoryAdapter(logList, JobDetailFragment.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvLogHistory.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvLogHistory.setItemAnimator(new DefaultItemAnimator());
                            rvLogHistory.setLayoutManager(mLayoutManager);
                            rvLogHistory.setAdapter(LogListAdapter);
                            LogListAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //getJobStatus();

                    Utils.hideProgressDialog();
                    break;
                case deleteJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            dialogJobDelete();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateEquipmentQTY:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case documentApprove:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            relativeImage.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case documentDeny:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            relativeImage.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case documentDelete:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            relativeImage.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addJobDoc:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            ImageProcess.scanFile(getActivity(), mCameraPhotoPath);
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addJobNote:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Note Add Successfully", getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case markArrivalTime:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            dialogMakeArrival();
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case getJobStatus:
                    jobStatusList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            jobStatusList.addAll(LoganSquare.parseList(jObject.toString(), JobStatusModel.class));
                            jobStatusAdapter = new JobStatusAdapter(getActivity(), jobStatusList);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case updateJobStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getJobDetail();
                    break;
                case sendJobNotification:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // getJobDetail();
                    break;
                case jobCheckApprove:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case billToApprove:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addCheckCustomAmount:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addInvoiceCustomAmount:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case billingDate:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case approveDocumentStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case denyDocumentStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case printDocument:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case printJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            String pdf = jObj.getString("data");
                            if (!TextUtils.isEmpty(pdf)) {
                                Bundle bundle = new Bundle();
                                DownloadInvoiceFragment downlodInvoiceFragment = new DownloadInvoiceFragment();
                                bundle.putString("url", pdf);
                                downlodInvoiceFragment.setArguments(bundle);
                                ((MainActivity)getActivity()).changeFragment(downlodInvoiceFragment, true);
                            }
                            //  getJobDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else {
            Utils.hideProgressDialog();
        }
    }

    public int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    public void clearData() {
        txtContractorName.setText("");
        txtFacilityName.setText("");
        txtDueDate.setText("");
        txtRoome.setText("");
        txtPatientName.setText("");
        txtService.setText("");
        txtPhoneNo.setText("");
        txtAddress.setText("");
        txtJobAcknologe.setText("");
    }

    public void dateConvertFormate(TextView txt, String Date, String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat formatDate = new SimpleDateFormat("dd");
        DateFormat formatMonth = new SimpleDateFormat("MM");
        DateFormat formatYear = new SimpleDateFormat("yyyy");
        DateFormat formatAmPm = new SimpleDateFormat("hh:mm a");
        String finalDate = formatDate.format(date);
        String finalMonth = formatMonth.format(date);
        String finalHour = formatAmPm.format(date);
        String finalYear = formatYear.format(date);
        txt.setText(msg + " " + finalHour + " " + finalMonth + "-" + finalDate + "-" + finalYear);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getJobDetail();
        getJobStatus();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (App.checkStateEdit == true) {
            getJobDetail();
        }
    }


    @Override
    public void recyclerViewListClicked(View v, int position, int groupPos, int childPos) {
        imagePosition = position;
        relativeImage.setVisibility(View.VISIBLE);
        photoViewAdapter = new DocumentImagePagerAdapter(getContext(), documentList);
        mViewPager.setAdapter(photoViewAdapter);
        mViewPager.setCurrentItem(position);
        toolbar.setVisibility(View.GONE);
    }


    @Override
    public void callLogClick(int pos, LogHistoryModel model) {
        Bundle bundle = new Bundle();
        Fragment fragment = null;

        Log.d(TAG, "callLogClick: " + model.getTitle());
        if (model.getTitle().equalsIgnoreCase("Added Check")) {
            fragment = new CheckFragment();
            if (!TextUtils.isEmpty(model.getUserID())) {
                bundle.putString(Constant.chkId, model.getUserID());
                bundle.putString(Constant.chkDate, model.getCheckDate());
            }
        } else if (model.getTitle().equalsIgnoreCase("Added Invoice")) {
            fragment = new InvoiceFragment();
            bundle.putString(Constant.InvoiceNo, model.getNote());
        }
        if (fragment != null) {
            fragment.setArguments(bundle);
            ((MainActivity)getActivity()).changeFragment(fragment, true);
        }
    }
}
