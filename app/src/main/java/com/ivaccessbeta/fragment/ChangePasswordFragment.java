package com.ivaccessbeta.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * Created by User on 18-05-2018.
 */

public class ChangePasswordFragment extends Fragment implements AsynchTaskListner {

    EditText edtOldPassword, edtNewPassword, edtCPassword;
    Button btnSend;
    String oldPassword, newPassword, confirmPassword, emailId, token, userId;
    public ChangePasswordFragment instance;
    public TextView txtTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ((MainActivity)getActivity()).setBackButton();
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        MainActivity.toolbar_title.setText("CHANGE PASSWORD");
        ((MainActivity)getActivity()).showToolbarAction();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        edtOldPassword = view.findViewById(R.id.edt_oldPassword);
        edtNewPassword = view.findViewById(R.id.edt_newPassword);
        edtCPassword = view.findViewById(R.id.edt_cPassword);
        btnSend = view.findViewById(R.id.btn_send);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        instance = this;
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPassword = edtOldPassword.getText().toString();
                newPassword = edtNewPassword.getText().toString();
                confirmPassword = edtCPassword.getText().toString();
                if (TextUtils.isEmpty(oldPassword)) {
                    Utils.showToast("Please Enter Old Password", getActivity());
                } else if (TextUtils.isEmpty(newPassword)) {
                    Utils.showToast("Please Enter New Password", getActivity());
                } else if (TextUtils.isEmpty(confirmPassword)) {
                    Utils.showToast("Please Enter Confirm  Password", getActivity());
                } else if (confirmPassword == newPassword) {
                    Utils.showToast("Please Enter Currect Confirm Password", getActivity());
                }else if (edtNewPassword.getText().toString().length() < 6) {
                    Utils.showAlert("Please enter the password of atlest 6 characters", getActivity());
                }  else {
                    new CallRequest(instance).chnagePassworFragment(emailId, token, userId, oldPassword, newPassword, confirmPassword);
                }
            }
        });
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case changePassword:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            MySharedPref.setString(getActivity(), App.IS_PASSWORD_CHANGE, Constant.PASSWORD_CHANGE);
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
