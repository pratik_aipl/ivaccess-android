package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.AddJobActivity;
import com.ivaccessbeta.fragment.ContractorListFragment;
import com.ivaccessbeta.listners.ContractorSpinnerListener;
import com.ivaccessbeta.model.ContractorModel;

import java.util.List;

/**
 * Created by User on 19-03-2018.
 */

public class SelectContractorAdapter extends ArrayAdapter<String>{

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<ContractorModel> items;
    private final int mResource;
    private static ContractorSpinnerListener itemListener;
    public SelectContractorAdapter(@NonNull Context context, @LayoutRes int resource,
                                   @NonNull List objects, AddJobActivity activity) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;

        itemListener = activity;
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(final int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView txtContractorName = view.findViewById(R.id.txtContractorName);
        TextView txtViewSchedule= view.findViewById(R.id.txtViewSchedule);
        ContractorModel offerData = items.get(position);
        txtContractorName.setText(offerData.getFullName());
        txtViewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               itemListener.viewSchedule(v,position);
            }
        });
        return view;
    }
}
