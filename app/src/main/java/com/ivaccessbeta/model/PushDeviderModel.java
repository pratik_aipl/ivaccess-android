package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 23-04-2018.
 */
@JsonObject
public class PushDeviderModel {
    @JsonField
    public String start_date, end_date, PushDivider;
    @JsonField
    public int week;
    @JsonField
    public boolean isVisible = false;

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getPushDivider() {
        return PushDivider;
    }

    public void setPushDivider(String pushDivider) {
        PushDivider = pushDivider;
    }
}
