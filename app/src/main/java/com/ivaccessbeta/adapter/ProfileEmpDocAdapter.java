package com.ivaccessbeta.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.ImageSelectionActivity;
import com.ivaccessbeta.fragment.profile.EmployeeDocProfileFragment;
import com.ivaccessbeta.fragment.profile.ExpiredDocFragment;
import com.ivaccessbeta.fragment.profile.NotExpiredDocFragment;
import com.ivaccessbeta.listners.ProfileDocumentListner;
import com.ivaccessbeta.model.ProfileEmpDocModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by User on 22-03-2018.
 */

public class ProfileEmpDocAdapter extends RecyclerView.Adapter<ProfileEmpDocAdapter.MyViewHolder> {
    private static final String TAG = "ProfileEmpDocAdapter";
    public ArrayList<ProfileEmpDocModel> list;
    public List<ProfileEmpDocModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId,  roleId;
    public View itemView;
    Fragment fragment;
    public ProfileDocumentListner docUploadListner;
    boolean isExpired;

    public ProfileEmpDocAdapter(ArrayList<ProfileEmpDocModel> list, ExpiredDocFragment expiredDocFragment) {
        this.context = expiredDocFragment.getActivity();
        this.list = list;
        this.searchFilterList.addAll(list);
        this.docUploadListner = expiredDocFragment;
        this.fragment = expiredDocFragment;
        this.isExpired = true;
    }

    public ProfileEmpDocAdapter(ArrayList<ProfileEmpDocModel> list, NotExpiredDocFragment notExpiredDocFragment) {
        this.context = notExpiredDocFragment.getActivity();
        this.list = list;
        this.searchFilterList.addAll(list);
        this.docUploadListner = notExpiredDocFragment;
        this.fragment = notExpiredDocFragment;
        this.isExpired = false;
    }

    @Override
    public ProfileEmpDocAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_profile_emp_doc, parent, false);

        return new ProfileEmpDocAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(final ProfileEmpDocAdapter.MyViewHolder holder, final int position) {
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(context, App.ROLEID, "");
        final ProfileEmpDocModel model = list.get(position);
        holder.name.setText(model.getDocumentName());
        //   holder.txtDocStatus.setText(model.getDocumentStatus());
//        holder.txtExpDate.setText(model.getDocumentExpire());
        Log.d(TAG, "onBindViewHolder: --> " + model.getDocumentExpire());
        if (isExpired) {
            holder.rel_main.setBackgroundResource(R.color.redBg);
        } else {
            holder.rel_main.setBackgroundResource(R.drawable.square_gray_border);
        }
        /*if (CheckExpierd(model.getDocumentExpire())) {
            if (!TextUtils.isEmpty(model.getDocumentExpire())) {
                Utils.converMMddyyyyFormate(model.getDocumentExpire(), holder.txtExpDate);
            }
        } else {
            holder.rel_main.setBackgroundResource(R.drawable.square_gray_border);
        }*/


        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            holder.ivAction.setVisibility(View.INVISIBLE);
        }
        holder.ivAction.setOnClickListener(v -> {
            int[] location = new int[2];
            v.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            showSortPopup(fragment.getActivity(), x, y, position, isExpired);
        });

        if (list.get(position).getDocumentStatus().equalsIgnoreCase(Constant.APPROVE)) {
            holder.ivAction.setVisibility(View.INVISIBLE);
            holder.ivStatusSymbol.setImageResource(R.drawable.ic_right_green);
        } else if (list.get(position).getDocumentStatus().equalsIgnoreCase(Constant.DENY)) {
            holder.ivStatusSymbol.setImageResource(R.drawable.ic_red_exlametry);
        } else {
            holder.ivStatusSymbol.setImageResource(R.drawable.ic_exclamantion);
        }

        if (list.get(position).getExpired().equalsIgnoreCase(Constant.One)) {
            holder.ivAction.setVisibility(View.VISIBLE);
        }

    }

    public boolean CheckExpierd(String DocDate) {
        if (!DocDate.equals("") && !DocDate.equals(null)) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                String str1 = DocDate;
                Date date1 = formatter.parse(str1);
                Date c = Calendar.getInstance().getTime();
                Date date2 = formatter.parse(formatter.format(Calendar.getInstance().getTime()));
                if (date1.compareTo(date2) < 0) {
                    System.out.println("date2 is Greater than my date1");
                    return true;
                } else {
                    System.out.println("date1 is Greater than my date2");
                    return false;
                }

            } catch (ParseException e1) {
                e1.printStackTrace();
                return false;

            }
        }
        return false;
    }

    private void showSortPopup(final Activity context, int x, int y, final int position, boolean isExpired) {
        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_profile_emp_doc_action, viewGroup);
        PopupWindow changeSortPopUp = new PopupWindow(context);
        changeSortPopUp.setContentView(layout);
        changeSortPopUp.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        changeSortPopUp.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        changeSortPopUp.setFocusable(true);
        changeSortPopUp.setBackgroundDrawable(new BitmapDrawable());
        changeSortPopUp.showAtLocation(layout, Gravity.TOP, x, y);

        TextView txtUpload = layout.findViewById(R.id.txtUpload);
        TextView txtTrash = layout.findViewById(R.id.txtTrash);
        TextView txtApprove = layout.findViewById(R.id.txtApprove);
        TextView txtDeny = layout.findViewById(R.id.txtDeny);
        TextView txtChek_view = layout.findViewById(R.id.txtChek_view);
        TextView txtDowload_pdf = layout.findViewById(R.id.txtDowload_pdf);

        if (list.get(position).getFileName().size() > 0) {
            txtChek_view.setVisibility(View.VISIBLE);
            txtTrash.setVisibility(View.VISIBLE);
            txtChek_view.setOnClickListener(v -> docUploadListner.onViewDocumentClick(position));
        } else {
            txtChek_view.setVisibility(View.GONE);
            txtTrash.setVisibility(View.GONE);
        }

        if (isExpired) {
            if (list.get(position).getNewUploaded().equalsIgnoreCase("0")) {
                txtUpload.setVisibility(View.VISIBLE);
            } else {
                txtUpload.setVisibility(View.GONE);
            }
            txtDowload_pdf.setVisibility(View.GONE);
            txtTrash.setVisibility(View.VISIBLE);
        } else {
            if (list.get(position).getNewUploaded().equalsIgnoreCase("0") && list.get(position).getIsUploaded().equalsIgnoreCase("0")) {
                txtUpload.setVisibility(View.VISIBLE);
            } else {
                txtUpload.setVisibility(View.GONE);
            }
            if (list.get(position).getDocumentStatus().equalsIgnoreCase(Constant.DENY)){
                txtUpload.setVisibility(View.VISIBLE);
            }
            if (!list.get(position).getPdf().equals("")) {
                txtDowload_pdf.setVisibility(View.VISIBLE);
            } else {
                txtDowload_pdf.setVisibility(View.GONE);
            }

        }

        txtDowload_pdf.setOnClickListener(view -> {
            if (!list.get(position).getPdf().equals("")) {

                Uri URL = Uri.parse("http://ivaccess.blenzabi.com/" + list.get(position).getPdf());
                String extantion = list.get(position).getPdf().substring(list.get(position).getPdf().lastIndexOf("."));

                String fileName = list.get(position).getPdf().substring(list.get(position).getPdf().lastIndexOf("/"), list.get(position).getPdf().lastIndexOf("."));
                File pdf = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName + extantion);
                Utils.showToast("Downloading", context);
                DownloadManager.Request request = new DownloadManager.Request(URL);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName + extantion);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); // to notify when download is complete
                request.allowScanningByMediaScanner();// if you want to be available from media players
                DownloadManager manager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
                manager.enqueue(request);
            }
        });

        txtTrash.setOnClickListener(v -> {
            changeSortPopUp.dismiss();
            docUploadListner.onDeleteDocumentClick(position);
        });

        txtUpload.setOnClickListener(v -> {
            changeSortPopUp.dismiss();
            Intent i = new Intent(context, ImageSelectionActivity.class)
                    .putExtra("ProfileEmpDocModel", list.get(position));
            context.startActivity(i);
        });




        if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            if (!TextUtils.isEmpty(list.get(position).getDEID())) {
                txtApprove.setVisibility(View.VISIBLE);
                txtDeny.setVisibility(View.VISIBLE);
            }
            if (list.get(position).getDocumentStatus().equalsIgnoreCase(Constant.APPROVE) || list.get(position).getDocumentStatus().equalsIgnoreCase(Constant.DENY)) {
                txtApprove.setVisibility(View.GONE);
                txtDeny.setVisibility(View.GONE);
            }
            if (list.get(position).getDocumentStatus().equalsIgnoreCase(Constant.APPROVE)) {
                txtTrash.setVisibility(View.GONE);
            }
        }
        txtApprove.setOnClickListener(v -> {
            changeSortPopUp.dismiss();
            docUploadListner.onApproveDocumentClick(position);
        });
        txtDeny.setOnClickListener(v -> {
            changeSortPopUp.dismiss();
            docUploadListner.onDenyDocumentClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, txtExpDate;
        RelativeLayout rel_main;
        ImageView ivAction, ivEdit, ivStatusSymbol;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.txtDocName);
            rel_main = view.findViewById(R.id.rel_main);
            txtExpDate = view.findViewById(R.id.txtExpDate);
            ivAction = view.findViewById(R.id.ivAction);
            rel_main = view.findViewById(R.id.rel_main);
            ivStatusSymbol = view.findViewById(R.id.ivStatusSymbol);
//            ivEdit=view.findViewById(R.id.ivEdit);
        }
    }
}