package com.ivaccessbeta.fragment.profile;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.model.UserProfileModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

public class EditProfileFragment extends Fragment implements AsynchTaskListner {

    public String emailId, token, userId;
    public EditProfileFragment instance;
    public ImageView ivCamera;
    public CircleImageView ivProfile;
    public EditText edtFName, edtLName, edtShortBio;
    public String fName, lName, shortBio;
    public Button btnUpdate;
    public Uri selectedUri;
    public String profImagePath,selectedType;

    public CropImage.ActivityResult result;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        MainActivity.toolbar_title.setText("Edit Profile");
        ((MainActivity)getActivity()).showToolbarAction();
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        instance = this;
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        if(App.contractorId.equalsIgnoreCase("")){
            userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        }else {
            userId=App.contractorId;
        }

        ivProfile = view.findViewById(R.id.iv_profile);
        edtFName = view.findViewById(R.id.edtFName);
        edtLName = view.findViewById(R.id.edtLName);
        edtShortBio = view.findViewById(R.id.edtShortBio);
        btnUpdate=view.findViewById(R.id.btnUpdate);
        ivCamera=view.findViewById(R.id.iv_camera);
        getUserProfile();
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CropImage.isExplicitCameraPermissionRequired(getActivity())) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(getActivity());
                }
            }
        });
        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CropImage.isExplicitCameraPermissionRequired(getActivity())) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(getActivity());
                }
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fName=edtFName.getText().toString().trim();
               lName= edtLName.getText().toString().trim();
                shortBio=edtShortBio.getText().toString().trim();
                if(fName.equalsIgnoreCase("")){
                    Utils.showToast("Plese Enter First Name",getActivity());
                }
                else if(lName.equalsIgnoreCase("")){
                    Utils.showToast("Please Enter Last Name",getActivity());
                }
                else {
                    new CallRequest(instance).editUserProfile(emailId,token,userId,fName,lName,shortBio,profImagePath);
                }

            }
        });
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    private void getUserProfile() {

        new CallRequest(instance).getUserProfile(emailId, token, userId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getUserProfile:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONObject jObjData = jObj. getJSONObject("data");
                            UserProfileModel userProfileModel = new UserProfileModel();
                            userProfileModel = LoganSquare.parse(jObjData.toString(),UserProfileModel.class);
                            ArrayList<UserProfileModel> list = new ArrayList<>();
                            list.add(userProfileModel);
                            edtFName.setText(list.get(0).getFirst_name());
                            edtLName.setText(list.get(0).getLast_name());
                            edtShortBio.setText(list.get(0).getDescription());
                            String imagePath="http://ivaccess.blenzabi.com/"+list.get(0).getProfile_image_path()+list.get(0).getProfile_image_name();
                            if (!imagePath.equalsIgnoreCase("")) {
                                Picasso.with(getContext()).load(imagePath).placeholder(R.drawable.default_user).error(R.drawable.default_user).into(ivProfile);
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case editUserProfile:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Edit Profile Successfully",getActivity());
                            App.description=shortBio;
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(getActivity());
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getContext(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(
                    getContext(), imageUri)) {
                selectedUri = imageUri;
                profImagePath = selectedUri.getPath();
                ivProfile.setImageURI(result.getUri());
              //  ivProfile.setImageBitmap(BitmapFactory.decodeFile(profImagePath));
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
              App.checkEditProfileState=true;
                ivProfile.setImageURI(result.getUri());
                selectedUri = result.getUri();
                profImagePath = selectedUri.getPath();
                Log.d("selected image----", profImagePath);
                ivProfile.setImageURI(result.getUri());
              //  ivProfile.setImageBitmap(BitmapFactory.decodeFile(profImagePath));
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 222) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (selectedType.equalsIgnoreCase("Take Photo")) {
                    Log.i("TAG", " REQUET Take Photo");
                    File f = new File(Environment.getExternalStorageDirectory() + "/IvAccess/images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "IvAccess/images/img_" + System.currentTimeMillis() + ".jpg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, 222);
                }

            } else {
                Utils.showToast("Permission not granted",
                        getContext());
            }
        }
    }
}
