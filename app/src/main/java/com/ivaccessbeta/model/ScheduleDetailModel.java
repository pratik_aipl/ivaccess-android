package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 08-03-2018.
 */
@JsonObject
public class ScheduleDetailModel implements Serializable {

    @JsonField
    public String ScheduleID = "", FromTime = "", ToTime = "", PlacesName = "", first_name = "", last_name = "", ColorCode = "",
            AvailableSchedule = "", NotAvailableStatus = "", CreatedOn = "", SubmittedBy = "", ContractorNotes = "", AdminNotes = "";

    public String getAdminNotes() {
        return AdminNotes;
    }

    public void setAdminNotes(String adminNotes) {
        AdminNotes = adminNotes;
    }

    public String getScheduleID() {
        return ScheduleID;
    }

    public void setScheduleID(String scheduleID) {
        ScheduleID = scheduleID;
    }

    public String getContractorNotes() {
        return ContractorNotes;
    }

    public void setContractorNotes(String contractorNotes) {
        ContractorNotes = contractorNotes;
    }

    public String getSubmittedBy() {
        return SubmittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        SubmittedBy = submittedBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getAvailableSchedule() {
        return AvailableSchedule;
    }

    public void setAvailableSchedule(String availableSchedule) {
        AvailableSchedule = availableSchedule;
    }

    public String getNotAvailableStatus() {
        return NotAvailableStatus;
    }

    public void setNotAvailableStatus(String notAvailableStatus) {
        NotAvailableStatus = notAvailableStatus;
    }

    public String getFromTime() {
        return FromTime;
    }

    public void setFromTime(String fromTime) {
        FromTime = fromTime;
    }

    public String getToTime() {
        return ToTime;
    }

    public void setToTime(String toTime) {
        ToTime = toTime;
    }

    public String getPlacesName() {
        return PlacesName;
    }

    public void setPlacesName(String placesName) {
        PlacesName = placesName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }
}
