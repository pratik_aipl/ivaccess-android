package com.ivaccessbeta.listners;

import android.view.View;

/**
 * Created by User on 14-04-2018.
 */

public interface InvoiceListener {

    void  onDeletePerticularInvoice(int groupPos, int pos, View v);
    void  onCheckInvoice(int groupPos, int pos, View v, String IsCheckMark);
    void onEditInvoice(int groupPos, int pos, View v);
    void onOpenActionDialog(int pos, View v);
    void onAddInvoiceMemo(int groupPos, int pos, View v, String note, String amount);
    void onDeleteNote(int groupPos, int pos, View v);
    void onDeletePaymentNote(int groupPos, int pos, View v);
    void onJobNoClick(int groupPos, int pos, View v);

}
