package com.ivaccessbeta.fragment.invoice;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class DownloadInvoiceFragment extends Fragment {


    public DownloadInvoiceFragment() {
        // Required empty public constructor
    }

    public WebView myWebView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view= inflater.inflate(R.layout.fragment_downlod_invoice, container, false);
        setHasOptionsMenu(true);
        int intent=getArguments().getInt("intent");
        String pdf= getArguments().getString("url");
        myWebView = view.findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
         pdf = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf;

        System.out.println("pdf=="+pdf);
        myWebView.loadUrl(pdf);
        return view;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        Utils.allMenuHide(menu);
        super.onPrepareOptionsMenu(menu);
    }
}
