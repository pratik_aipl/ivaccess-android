package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.BillToModel;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddFacilityFragment extends Fragment implements AsynchTaskListner {

    public EditText edtFacilityName, edtPhone, edtAddress, edtAddress2, edtCity, edtState, edtZip, edtNote;
    public Spinner spRegion, spBillTo;
    public Button btnSave, btnCancel;
    public String facilityName, primaryRegion, billTo, phone, address, address2, city, state, zipCode, facilityId, note, ivAccessSupply;
    public AddFacilityFragment instance;
    public String emailId, token, userId;
    public ArrayList<RegionModel> regionList = new ArrayList<>();
    public ArrayList<String> regionStringList = new ArrayList<>();
    public RegionModel regionModel;
    public ArrayList<BillToModel> listBillTo = new ArrayList<>();
    public ArrayList<String> billToStringList = new ArrayList<>();
    public BillToModel billToModel;
    public int value;
    public int facilityPos = 0, billtoPos = 0, regionPos = 0;
    public RadioButton rbHospital, rbIvAccess;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_facility, container, false);
        MainActivity.toolbar_title.setText("Add Facility");
        ((MainActivity) getActivity()).showToolbarAction();
        instance = this;
        linearJobs.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        value = getArguments().getInt("value");
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        edtFacilityName = view.findViewById(R.id.edtFacilityName);
        spRegion = view.findViewById(R.id.spinnerPrimariRegion);
        spBillTo = view.findViewById(R.id.spinnerBillTo);
        edtPhone = view.findViewById(R.id.edtPhone);
        edtAddress = view.findViewById(R.id.edtAddress);
        edtAddress2 = view.findViewById(R.id.edtAddress2);
        edtCity = view.findViewById(R.id.edtCity);
        edtState = view.findViewById(R.id.edtState);
        edtZip = view.findViewById(R.id.edtZip);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        edtNote = view.findViewById(R.id.edtNote);
        rbHospital = view.findViewById(R.id.rbHospital);
        rbIvAccess = view.findViewById(R.id.rbIvAccess);

        rbIvAccess.setChecked(true);
        rbHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbHospital.isChecked())
                    ivAccessSupply = "0";
            }
        });
        rbIvAccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbIvAccess.isChecked()) {
                    ivAccessSupply = "1";
                }
            }
        });
        getRegion();
        if (value == 1) {
            MainActivity.toolbar_title.setText("Update Facility");
            FacilitesModel facilityModel = (FacilitesModel) getArguments().getSerializable("FacilityModel");
            facilityId = facilityModel.getFacilityID();
            facilityName = facilityModel.getFacilityName();
            primaryRegion = facilityModel.getPrimaryRegion();
            billTo = facilityModel.getDefaultBillTo();
            phone = facilityModel.getPhone();
            address = facilityModel.getAddress();
            address2 = facilityModel.getAddress2();
            city = facilityModel.getCity();
            state = facilityModel.getState();
            zipCode = facilityModel.getZip();
            note = facilityModel.getNote();
            ivAccessSupply = facilityModel.getIvAccessSupply();


            edtFacilityName.setText(facilityName);
            edtPhone.setText(phone);
            edtAddress.setText(address);
            edtAddress2.setText(address2);
            edtCity.setText(city);
            edtState.setText(state);
            edtZip.setText(zipCode);
            edtNote.setText(note);
            if (ivAccessSupply.equalsIgnoreCase("0")) {
                rbHospital.setChecked(true);
            } else {
                rbIvAccess.setChecked(true);
            }

        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                facilityName = edtFacilityName.getText().toString().trim();
                phone = edtPhone.getText().toString().trim();
                address = edtAddress.getText().toString().trim();
                address2 = edtAddress2.getText().toString().trim();
                city = edtCity.getText().toString().trim();
                state = edtState.getText().toString().trim();
                zipCode = edtZip.getText().toString().trim();
                note = edtNote.getText().toString();
                if (facilityName.equals("")) {
                    Utils.showToast("Enter Facility Name", getContext());
                } else if (phone.equals("")) {
                    Utils.showToast("Enter Phone Number", getContext());
                } else if (address.equals("")) {
                    Utils.showToast("Enter Address", getContext());
                } else if (state.equals("")) {
                    Utils.showToast("Enter State", getContext());
                } else if (zipCode.equals("")) {
                    Utils.showToast("Enter ZipCode", getContext());
                }else if (TextUtils.isEmpty(primaryRegion)) {
                    Utils.showToast("Please Select Region", getContext());
                } else if (TextUtils.isEmpty(billTo)) {
                    Utils.showToast("Please Select Bill To", getContext());
                } else {
                    if (value == 0) {
                        new CallRequest(instance).addFacility(emailId, token, facilityName, primaryRegion, billTo, phone, address, address2, city, state, zipCode, userId, note, ivAccessSupply);
                    } else if (value == 1) {
                        new CallRequest(instance).updateFacility(emailId, token, facilityName, primaryRegion, billTo, phone, address, address2, city, state, zipCode, userId, facilityId, note, ivAccessSupply);
                    }
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        spRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Utils.disableKeyboard(getContext(), view);
                    primaryRegion = regionList.get(i-1).getRegionID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spBillTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Utils.disableKeyboard(getContext(), view);
                    billTo = listBillTo.get(i-1).getBillToID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
             //   billTo = listBillTo.get(i).getBillToID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return view;
    }

    private void getRegion() {
        new CallRequest(instance).getRegion(emailId, token);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    private void getBillList() {
        listBillTo.clear();
        new CallRequest(instance).getBillTo(emailId, token);

    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getRegion:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jsonArray = jObj.getJSONArray("data");
                            for (int i = 0; jsonArray.length() > i; i++) {
                                RegionModel roleModel = LoganSquare.parse(jsonArray.getJSONObject(i).toString(),RegionModel.class);
                                regionList.add(roleModel);
                                regionStringList.add(regionList.get(i).getRegionName());
                                if (primaryRegion != null) {
                                    if (primaryRegion.equalsIgnoreCase(roleModel.getRegionID())) {
                                        regionPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, regionStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spRegion.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.resign_spinner_row_nothing_selected,
                                    getActivity()));
                            spRegion.setSelection(regionPos);


                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getBillList();

                    break;
                case getBillTo:
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                BillToModel billToModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), BillToModel.class);
                                listBillTo.add(billToModel);
                                billToStringList.add(listBillTo.get(i).getName());
                                if (billTo != null) {
                                    if (billTo.equalsIgnoreCase(billToModel.getBillToID())) {
                                        billtoPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, billToStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spBillTo.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.default_built_to_spinner_row_nothing_selected,
                                    getActivity()));
                            spBillTo.setSelection(regionPos);

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addFacility:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Facility Add Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateFacility:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Facility Update Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
