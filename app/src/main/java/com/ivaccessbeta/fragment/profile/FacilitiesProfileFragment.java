package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.ProfileFacilitiesAdapter;
import com.ivaccessbeta.model.ProfileFacilitiesModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class FacilitiesProfileFragment extends Fragment implements AsynchTaskListner {

    public RecyclerView rvEmpDoc;
    public ArrayList<ProfileFacilitiesModel> list = new ArrayList<>();
    public ProfileFacilitiesAdapter profileFacilityAdapter;
    public FacilitiesProfileFragment instance;
    public String emailId, token, userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_facilities_profile, container, false);
        setHasOptionsMenu(true);
        instance = this;
        rvEmpDoc = view.findViewById(R.id.rvFacilities);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");

//        if (App.contractorId.equalsIgnoreCase("")) {
//            userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
//        } else {
//            userId = App.contractorId;
//        }

        try {
            if (App.contractorId.equalsIgnoreCase("")) {
                userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
            } else {
                userId = App.contractorId;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        profileFacilityAdapter = new ProfileFacilitiesAdapter(getActivity(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvEmpDoc.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvEmpDoc.setLayoutManager(mLayoutManager);
        rvEmpDoc.setItemAnimator(new DefaultItemAnimator());
        rvEmpDoc.setAdapter(profileFacilityAdapter);
        profileFacilityAdapter.notifyDataSetChanged();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getFacility();
    }

    private void getFacility() {

        new CallRequest(instance).getFacilityProfile(emailId, token, userId);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getUserFacility:
                    list.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; i < jObject.length(); i++) {
                                ProfileFacilitiesModel profileFacilitiesModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),ProfileFacilitiesModel.class);
                                list.add(profileFacilitiesModel);
                            }
                            profileFacilityAdapter = new ProfileFacilitiesAdapter(getActivity(), list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvEmpDoc.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                            rvEmpDoc.setLayoutManager(mLayoutManager);
                            rvEmpDoc.setItemAnimator(new DefaultItemAnimator());
                            rvEmpDoc.setAdapter(profileFacilityAdapter);
                            profileFacilityAdapter.notifyDataSetChanged();
                            //jobStatusAdapter = new JobStatusAdapter(jobStatusList);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

            }
        }
    }


}
