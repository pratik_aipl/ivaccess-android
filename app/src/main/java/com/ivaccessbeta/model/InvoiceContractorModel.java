package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 12-04-2018.
 */
@JsonObject
public class InvoiceContractorModel {

    @JsonField
    public String BillToID,Name;

    public String getBillToID() {
        return BillToID;
    }

    public void setBillToID(String billToID) {
        BillToID = billToID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
