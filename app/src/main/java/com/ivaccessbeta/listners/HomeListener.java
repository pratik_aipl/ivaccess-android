package com.ivaccessbeta.listners;

/**
 * Created by Karan - Empiere on 9/7/2018.
 */

public interface HomeListener {



    void documentDeny(String emailId, String token, String userId, String JDID, String Deny, int pos);

    void documentDelete(String emailId, String token, String userId, String JDID, int pos);


    void markArrivalTime(String emailId, String token, String userId, String jobId, String FianlDatAndTime, int pos);

    void addJobNote(String emailId, String token, String userId, String jobId, String note, int pos);

    void addJobDoc(String emailId, String token, String jobId, String userId, String imgPath, int pos);

    void sendJobNotification(String emailId, String token, String userId, String jobId, String notificationMessage, int pos);


    void addCheckAmount(String emailId, String token, String userId, String jobId, String amountCheck, String noteAmount, int pos);

    void addInvoiceAmount(String emailId, String token, String userId, String jobId, String amountCheck, String noteAmount, int pos);

    void deleteJob(String emailId, String token, String jobId, String userId, int pos);

    void billingDate(String emailId, String token, String userId, String jobId, String sendingDate, int pos);

    void printJob(String emailId, String token, String jobId, int pos);

    void Archive(String emailId, String token, String jobId, int pos);
    void DeArchive(String emailId, String token, String jobId, int pos);
    void updateEquipmentQTY(String emailId, String token, String statusQTY, String jobId, int pos);

    void documentApprove(String emailId, String token, String userId, String JDID, String Approve, int pos);

    void approvDocStatus(String emailId, String token, String userId, String jobId, int pos);

    void denyDocStatus(String emailId, String token, String userId, String jobId, int pos);

    void jobCheckApprove(String emailId, String token, String userId, String jobId, String checkStatus, int pos);


    void  jobBillToApprove(String emailId, String token, String userId, String jobId, String billToStatus, int pos);

    void  printDocument(String emailId, String token, String JDID, String userId, String DOCUMENT_ISPRINT, int pos);

    void  updateJobStatus(String emailId, String token, String userId, String jobId, String statusId, String note, int pos);
    }
