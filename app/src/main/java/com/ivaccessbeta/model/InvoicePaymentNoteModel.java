package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 10-05-2018.
 */
@JsonObject
public class InvoicePaymentNoteModel {

    @JsonField
    public String InvoicePaymentNoteID,InvoiceNo,Note,RefNo,Amount,CreatedOn,CreatedBy;

    public String getInvoicePaymentNoteID() {
        return InvoicePaymentNoteID;
    }

    public void setInvoicePaymentNoteID(String invoicePaymentNoteID) {
        InvoicePaymentNoteID = invoicePaymentNoteID;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getRefNo() {
        return RefNo;
    }

    public void setRefNo(String refNo) {
        RefNo = refNo;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }
}
