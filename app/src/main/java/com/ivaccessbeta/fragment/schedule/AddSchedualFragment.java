package com.ivaccessbeta.fragment.schedule;


import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.FacilitesAdapter;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddSchedualFragment extends Fragment implements AsynchTaskListner {
    private static final String TAG = "AddSchedualFragment";
    public Spinner spContactor, spPlaces;
    public Button btnSave, btnCancel;
    public AddSchedualFragment instance;
    public EditText edtFromDate, edtToDate, edtAdminNotes, edtContractorNotes;
   EditText edtFromTime, edtToTime;
    public String fromDate, toDate, fromTime, fromTime24, toTime, toTime24, facility, emailId, token, userId, place, contractor, adminNotes, contractorNotes;
    public ArrayList<ContractorModel> contractorList = new ArrayList<>();
    public ArrayList<String> contractorStringList = new ArrayList<>();
    public ArrayList<PlaceModel> placeList = new ArrayList<>();
    public ArrayList<String> placeStringList = new ArrayList<>();
    public ContractorModel contractorModel;
    public FacilitesAdapter mAdapter;
    public String curentDate;
    public String time, formattedTime;
    int contractorPos = 0, placePos = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_schedual, container, false);
        instance = this;
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        // toolbar_title.setText("Add Schedual");
        curentDate = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date());
        edtFromDate = view.findViewById(R.id.edtFromDate);
        edtToDate = view.findViewById(R.id.edtToDate);
        edtFromTime = view.findViewById(R.id.edtFromTime);
        edtToTime = view.findViewById(R.id.edtToTime);
        //   spFacility = view.findViewById(R.id.spinnerFacilities);
        spContactor = view.findViewById(R.id.spinnerContractor);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        edtAdminNotes = view.findViewById(R.id.edtAdminNotes);
        edtContractorNotes = view.findViewById(R.id.edtContractorNotes);
        spPlaces = view.findViewById(R.id.spinnerPlaces);

        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        getContractor();

        edtFromDate.setOnClickListener(view12 -> {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view12.getWindowToken(), 0);
            Utils.generateDatePicker(getContext(), edtFromDate);
        });
        edtToDate.setOnClickListener(view1 -> {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
            Utils.generateDatePicker(getContext(), edtToDate);
        });


        btnSave.setOnClickListener(view13 -> {
            curentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            fromDate = edtFromDate.getText().toString().trim();
            toDate = edtToDate.getText().toString().trim();
            fromTime = edtFromTime.getText().toString();
            toTime = edtToTime.getText().toString();
            adminNotes = edtAdminNotes.getText().toString();
            contractorNotes = edtContractorNotes.getText().toString();

            if (fromDate.equals("")) {
                Utils.showAlert("Enter From Date", getContext());
            } else if (toDate.equals("")) {
                Utils.showAlert("Enter To Date", getContext());
            } else if (fromTime.equals("")) {
                Utils.showAlert("Enter From Time", getContext());
            }else if (toTime.equals("")) {
                Utils.showAlert("Enter To Time", getContext());
            }  else if (TextUtils.isEmpty(contractor)) {
                Utils.showAlert("Please Select Contractor", getContext());
            } else if (TextUtils.isEmpty(place)) {
                Utils.showAlert("Please Select Place", getContext());
            } else {
                StringBuilder dateFormBuilder = new StringBuilder();
                StringBuilder dateToBuilder = new StringBuilder();
                dateFormBuilder = dateFormBuilder.append(fromDate.substring(6)).append("-").append(fromDate, 0, 2).append("-").append(fromDate, 3, 5);
                dateToBuilder = dateToBuilder.append(toDate.substring(6)).append("-").append(toDate, 0, 2).append("-").append(toDate, 3, 5);
                fromDate = String.valueOf(dateFormBuilder);
                toDate = String.valueOf(dateToBuilder);
                if (fromDate.compareTo(curentDate) < 0) {
                    Utils.showAlert("Enter Select Valid From Date", getContext());
                } else if (toDate.compareTo(fromDate) <= 0) {
                    Utils.showAlert("Enter Select Valid To Date", getContext());
                } else {
                    new CallRequest(instance).addScheduleFragment(emailId, token, place, fromDate, toDate, fromTime, toTime, contractor, userId, adminNotes, contractorNotes, "1", "");
                }
            }
        });
        btnCancel.setOnClickListener(view14 -> getActivity().onBackPressed());

        spPlaces.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getContext(), view);
                if (placeList.size() > 0 && i != 0) {
                    place = placeList.get(i - 1).getPlacesID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spContactor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getContext(), view);
                if (contractorList.size() > 0 && i != 0)
                    contractor = contractorList.get(i - 1).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    private void getContractor() {
        contractorList.clear();
        contractorStringList.clear();
        new CallRequest(instance).getConractor(emailId, token, "");
    }

    private void getPlace() {
        placeList.clear();
        placeStringList.clear();
        new CallRequest(instance).getPlace(emailId, token, "");
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContractor:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jsonArray = jObj.getJSONArray("data");

                            for (int i = 0; jsonArray.length() > i; i++) {
                                ContractorModel contractorModel = LoganSquare.parse(jsonArray.getJSONObject(i).toString(), ContractorModel.class);
                                contractorList.add(contractorModel);
                                contractorStringList.add(contractorList.get(i).getFullName());
                                if (contractor != null) {
                                    if (contractor.equalsIgnoreCase(contractorModel.getId())) {
                                        contractorPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, contractorStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spContactor.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.contractor_spinner_row_nothing_selected,
                                    getActivity()));
                            spContactor.setSelection(contractorPos);

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    getPlace();
                    break;


                case getPlace:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Log.d(TAG, "onTaskCompleted: " + jObj.toString());
                            JSONArray jsonArray = jObj.getJSONArray("data");
                            for (int i = 0; jsonArray.length() > i; i++) {
                                PlaceModel placeModel = LoganSquare.parse(jsonArray.getJSONObject(i).toString(), PlaceModel.class);
                                placeList.add(placeModel);
                                placeStringList.add(placeList.get(i).getPlacesName());
                                if (place != null) {
                                    if (place.equalsIgnoreCase(placeModel.getPlacesID())) {
                                        placePos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, placeStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPlaces.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.nothingselected_place,
                                    getActivity()));
                            spPlaces.setSelection(placePos);

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case addScheduleFragment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
