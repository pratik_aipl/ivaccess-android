package com.ivaccessbeta.fragment.manage;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.model.ColorPickerModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import com.jaredrummler.android.colorpicker.ColorPickerDialog;
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class AddRegionFragment extends Fragment implements View.OnClickListener, ColorPickerDialogListener, AsynchTaskListner {

    public EditText edtRegionName;
    public ArrayList<ColorPickerModel> list = new ArrayList<>();
    public AddRegionFragment instance;
    public ImageView ivColorCode;
    public TextView txtColorCode;
    public Button btnSave, btnCancel;
    public String regionName, colorCode,regionId;
    String emailId, token, userId;
    public RelativeLayout relativeColor;
    public int value;
    public String hexColor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_region, container, false);
        MainActivity.toolbar_title.setText("Add Region");
        ((MainActivity)getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        instance = this;
        setHasOptionsMenu(true);
        value=getArguments().getInt("value");

        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        edtRegionName = view.findViewById(R.id.edtRegionName);
        ivColorCode = view.findViewById(R.id.colorCode);
        txtColorCode = view.findViewById(R.id.txtColorCode);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        relativeColor = view.findViewById(R.id.relative_color);
        if(value==1){
            MainActivity.toolbar_title.setText("Update Region");
            RegionModel regionModel = (RegionModel) getArguments().getSerializable("RegionModel");
            regionName=regionModel.getRegionName();
            regionId=regionModel.getRegionID();
            hexColor=regionModel.getColorCode();
            edtRegionName.setText(regionName);
            ivColorCode.setBackgroundColor(Color.parseColor(hexColor));
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regionName = edtRegionName.getText().toString().trim();
                if (regionName.equals("")) {
                    Utils.showAlert("Please Enter Region Name", getContext());
                } else {

                    if (value == 0) {
                        addRegion();
                    }
                    if (value == 1) {
                        updateRegion();
                    }

                }
            }
        });

        relativeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ColorPickerDialog pickerDialog = ColorPickerDialog.newBuilder().create();
                pickerDialog.setColorPickerDialogListener(instance);
                pickerDialog.show(getActivity().getFragmentManager(), "Foo");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        txtColorCode.setOnClickListener(this);

        return view;
    }

    private void updateRegion() {
        new CallRequest(instance).updateRegion(emailId, token, regionName, hexColor, userId,regionId);
    }

    private void addRegion() {
        new CallRequest(instance).addRegion(emailId, token, regionName, hexColor, userId);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onColorSelected(int dialogId, int color) {
        hexColor = String.format("#%06X", (0xFFFFFF & color));
       // Log.d("code----", String.valueOf(hexColor));
        ivColorCode.setBackgroundColor(color);
    }

    @Override
    public void onDialogDismissed(int dialogId) {

    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        ColorPickerDialog pickerDialog = ColorPickerDialog.newBuilder().create();
        pickerDialog.setColorPickerDialogListener(this);
        pickerDialog.show(getActivity().getFragmentManager(), "Foo");
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addRegion:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Add Region Succesfully", getContext());
                            getActivity().onBackPressed();
                        }  else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case updateRegion:

                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);

                            if (jObj.getString("status").equals("200")) {
                                Utils.showToast("Update Region Succesfully", getContext());
                                getActivity().onBackPressed();
                            }  else {
                                Utils.handleJSonAPIError(jObj, getActivity());
                            }


                        } catch (JSONException e) {
                        e.printStackTrace();

                        Log.d("new exception:", String.valueOf(e));
                    }
                    break;

            }
        }
    }
}



