package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.VerticalScheduleAdapter;
import com.ivaccessbeta.model.PeofileJobScheduleModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.ItemOffsetDecoration;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class JobScheduleProfileFragment extends Fragment implements AsynchTaskListner  {
    public ArrayList<PeofileJobScheduleModel> subList = new ArrayList<>();
    public ArrayList<PeofileJobScheduleModel> listFinal = new ArrayList<>();
    public RecyclerView rvJobSchDetail;
    public String emailId, token, userId;
    public JobScheduleProfileFragment instance;
    public PeofileJobScheduleModel jobStatusModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_job_schedule, container, false);
        setHasOptionsMenu(true);
        rvJobSchDetail = view.findViewById(R.id.rvJobSchDetail);
        instance=this;
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        final int spacing = getResources().getDimensionPixelOffset(R.dimen.margin_10);
        rvJobSchDetail.addItemDecoration(new ItemOffsetDecoration(spacing));
        try {
            if (App.contractorId.equalsIgnoreCase("")) {
                userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
            } else {
                userId = App.contractorId;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return view;
    }

    private void getProfileJobSchedule() {

        new CallRequest(instance).getProfileJobSchedule(emailId, token, userId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getProfileJobSchedule();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getProfileJobSchedule:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            subList.clear();

                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject objDate=jObject.getJSONObject(i);
                                 jobStatusModel = new PeofileJobScheduleModel();
                                jobStatusModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),PeofileJobScheduleModel.class);
                                JSONArray jArray = objDate.getJSONArray("job_list");

                                for (int j=0;jArray.length()>j;j++) {
                                    PeofileJobScheduleModel jobStatusModelSchedule = new PeofileJobScheduleModel();
                                    jobStatusModelSchedule = LoganSquare.parse(jArray.getJSONObject(j).toString(),PeofileJobScheduleModel.class);
                                    jobStatusModel.subArray.add(jobStatusModelSchedule);
                                }
                                subList.add(jobStatusModel);
                            }
                            rvJobSchDetail.setHasFixedSize(false);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            rvJobSchDetail.setLayoutManager(layoutManager);
                            VerticalScheduleAdapter adapter = new VerticalScheduleAdapter(getActivity(),subList);
                            rvJobSchDetail.setAdapter(adapter);

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

            }
        }
    }
}
