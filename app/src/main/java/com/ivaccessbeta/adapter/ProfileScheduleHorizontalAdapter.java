package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.ProfileScheduleModel;
import com.ivaccessbeta.utils.Constant;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ProfileScheduleHorizontalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ProfileScheduleModel> mList;
    private HorizontalSchedualAdapter.OnItemClickListener mItemClickListener;
    Context context;

    public ProfileScheduleHorizontalAdapter(Context mContext, ArrayList<ProfileScheduleModel> list) {
        this.mList = list;
        this.context = mContext;
    }

    public class CellViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView txtHospitalName, txtFacliterName, txtTimeTo, txtTimeFrom, txtJobStatus, txtSubmitedTime;
        public RelativeLayout relativeSchedule;
        public ImageView ivClock, ivProfile;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtHospitalName = itemView.findViewById(R.id.txtHospitalName);
            txtFacliterName = itemView.findViewById(R.id.txtFacliterName);
            txtTimeTo = itemView.findViewById(R.id.txtTimeTo);
            txtTimeFrom = itemView.findViewById(R.id.txtTimeFrom);
            txtJobStatus = itemView.findViewById(R.id.txtJobStatus);
            relativeSchedule = itemView.findViewById(R.id.relativeSchedule);
            txtSubmitedTime = itemView.findViewById(R.id.txtSubmitedTime);
            ivClock = itemView.findViewById(R.id.ivClock);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getLayoutPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemLongClick(view, getLayoutPosition());
                return true;
            }
            return false;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_horizontal_profile_history, viewGroup, false);
                return new ProfileScheduleHorizontalAdapter.CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            default: {
                ProfileScheduleHorizontalAdapter.CellViewHolder cellViewHolder = (ProfileScheduleHorizontalAdapter.CellViewHolder) viewHolder;
                cellViewHolder.txtHospitalName.setText("" + mList.get(position).getPlacesName());
                cellViewHolder.txtFacliterName.setText("" + mList.get(position).getFirst_name() + " " + mList.get(position).getLast_name());
                String availableSchedule = mList.get(position).getAvailableSchedule();
                String scheduleStatus = mList.get(position).getNotAvailableStatus();
                if (availableSchedule.equals(Constant.NOT_AVAILABLE_SCHEDULE)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = null;
                    try {
                        date = sdf.parse(mList.get(position).getCreatedOn());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    DateFormat formatDate = new SimpleDateFormat("dd");
                    DateFormat formatMonth = new SimpleDateFormat("MM");
                    DateFormat formatAmPm = new SimpleDateFormat("hh a");
                    //  DateFormat formatAmPm=new SimpleDateFormat("a");
                    String finalDate = formatDate.format(date);
                    String finalMonth = formatMonth.format(date);
                    String finalHour = formatAmPm.format(date);
                    cellViewHolder.txtHospitalName.setText("by:" + mList.get(position).getSubmittedBy());
                    cellViewHolder.txtSubmitedTime.setText("  Submited: " + finalHour + " " + finalMonth + "-" + finalDate);
                    mList.get(position).getNotAvailableStatus();

                    cellViewHolder.txtHospitalName.setTextColor(Color.parseColor("#000000"));
                    cellViewHolder.txtFacliterName.setTextColor(Color.parseColor("#000000"));
                    cellViewHolder.txtSubmitedTime.setTextColor(Color.parseColor("#000000"));
                    cellViewHolder.txtTimeFrom.setTextColor(Color.parseColor("#000000"));
                    cellViewHolder.txtTimeTo.setTextColor(Color.parseColor("#000000"));
                    cellViewHolder.ivClock.setColorFilter(ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);
                    cellViewHolder.ivProfile.setColorFilter(ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.MULTIPLY);

                    if (scheduleStatus.equalsIgnoreCase(Constant.NA_SCHEDULE_APPROVE)) {
                        cellViewHolder.relativeSchedule.setBackgroundColor(Color.parseColor("#c6f7c6"));
                    } else if (scheduleStatus.equalsIgnoreCase(Constant.NA_SCHEDULE_DENY)) {
                        cellViewHolder.relativeSchedule.setBackgroundColor(Color.parseColor("#dddddd"));
                    } else {
                        cellViewHolder.relativeSchedule.setBackgroundColor(Color.parseColor("#ffffff"));
                    }
                } else {
                    cellViewHolder.relativeSchedule.setBackgroundColor(Color.parseColor(mList.get(position).getColorCode()));
                }
                cellViewHolder.txtTimeFrom.setText(mList.get(position).getFromTime());
                cellViewHolder.txtTimeTo.setText(mList.get(position).getToTime());
//                convertTo24FormateFrom(cellViewHolder.txtTimeFrom, mList.get(position).getFromTime());
//                convertTo24FormateFrom(cellViewHolder.txtTimeTo, mList.get(position).getToTime());
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    // for both short and long click
    public void SetOnItemClickListener(final HorizontalSchedualAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void convertTo24FormateFrom(TextView txt, String timeStr) {
        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
        String fromTime24 = null;
        try {
            fromTime24 = date12Format.format((date24Format.parse(timeStr)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txt.setText(fromTime24);

    }
}
