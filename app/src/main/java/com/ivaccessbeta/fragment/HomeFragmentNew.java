package com.ivaccessbeta.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.checks.CheckFragment;
import com.ivaccessbeta.fragment.invoice.InvoiceFragment;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.model.JobStateModel;
import com.ivaccessbeta.model.JobStatusModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.model.SearchData;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.ImageProcess;
import com.ivaccessbeta.utils.MultiSelectionSpinner;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.ivaccessbeta.activity.MainActivity.hideToolbarAction;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;
import static com.ivaccessbeta.utils.CallRequest.BASEURL;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.FACILITY;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.JOBSTATE;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.PERSONS;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.REGIONS;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.STATUS;

public class HomeFragmentNew extends Fragment implements AsynchTaskListner, MultiSelectionSpinner.OnMultipleItemsSelectedListener, AdapterView.OnItemSelectedListener {
    public View view;
    public HomeFragmentNew instance;
    public SwipeRefreshLayout mySwipeRefreshLayout;
    public WebView webview;
    public static Dialog dialog;
    public static final int REQUEST_SELECT_FILE = 100;
    public ValueCallback<Uri[]> uploadMessage;
    private Uri mCapturedImageURI = null;
    private String mCameraPhotoPath;
    private static final int INPUT_FILE_REQUEST_CODE = 1;
    private ValueCallback<Uri[]> mFilePathCallback;
    private static final int FILECHOOSER_RESULTCODE = 1;
    private ValueCallback<Uri> mUploadMessage;
    public MultiSelectionSpinner spRegion, spPerson, spFacility, spStatuses, spJobState;

    TextView mRegionCount, mPersonCount, mFacilityCount, mStatusesCount, mJobStatusCount;
    public Spinner spShoryBy;
    public TextView txtJobState, txtShortBy, txtPersons, txtFacility, txtStatuses, txtRegion;
    public String emailId, token, userId, roleId,
            multiSelecteStatusId = "", multiSelectPersonId = "", multiSelectRegionId = "",
            multiSelectFacility = "", multiSelectedJobState = "", shortBy = "j.CreatedOn-desc",
            multiSelecteStatusName = "", multiSelectPersonName = "", multiSelectRegionName = "",
            multiSelectFacilityName = "", multiSelectedJobStateName = "";

    public View viewPerson;
    public static String type;
    String url;
    boolean callUsingFilter = false;

    public ArrayAdapter aaShortBy;

    private static final String TAG = "HomeFragmentNew";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_new, container, false);
        instance = this;
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.VISIBLE);
        hideToolbarAction();
        toolbar.setVisibility(View.VISIBLE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");

        mySwipeRefreshLayout = view.findViewById(R.id.swipeContainer);

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            multiSelectPersonId = userId;
        }
        webview = view.findViewById(R.id.webView);

        if (Utils.isNetworkAvailable(getActivity()))
            getSaveDate("0", "1");
        else
            Utils.showAlert("No Internet, Please try again later", getActivity());
        Utils.setWebViewSettings(webview);


        webview.addJavascriptInterface(new WebInterface(getActivity()), "Android");
        webview.setWebViewClient(new myWebClient());
        webview.setWebChromeClient(new ChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });

        if (Build.VERSION.SDK_INT >= 19) {
            webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        Log.i("TAG", " url :-> " + url);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        webview.reload();
                    }
                }
        );

        linearJobs.setOnClickListener(v -> showJobSearchDialog());

        webview.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == MotionEvent.ACTION_UP && webview.canGoBack()) {
                webViewGoBack();
                return true;
            }
            return false;
        });


        return view;
    }

    private void getSaveDate(String isClear, String isFirst) {
        String toJsonData = new Gson().toJson(new SearchData(multiSelectedJobState, shortBy, multiSelectRegionId, multiSelectPersonId, multiSelectFacility, multiSelecteStatusId));
        new CallRequest(instance).getSaveData(emailId, token, userId, toJsonData, isClear, isFirst);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    public class WebInterface {
        Context mContext;

        public WebInterface(Context c) {
            this.mContext = c;
        }

        @JavascriptInterface
        public void showToast(String type, String id, String date) {
            Log.d(TAG, "showToast() called with: type = [" + type + "], id = [" + id + "], date = [" + date + "]");
            Bundle bundle = new Bundle();
            Fragment fragment = null;
            if (type.equalsIgnoreCase("check")) {
                fragment = new CheckFragment();
                bundle.putString(Constant.chkId, id);
                bundle.putString(Constant.chkDate, date);
            } else if (type.equalsIgnoreCase("invoice")) {
                fragment = new InvoiceFragment();
                bundle.putString(Constant.InvoiceNo, id);
            }
            fragment.setArguments(bundle);
            if (fragment != null)
                changeFragment(fragment, getFragmentManager(), true);
        }
    }

    public void changeFragment(Fragment fragment, FragmentManager fragmentManager, boolean doAddToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame, fragment);

        if (doAddToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
            MainActivity.ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getActivity().onBackPressed();
                }
            });
        } else {
        }
        transaction.commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
    }


    private void webViewGoBack() {
        webview.goBack();
        Constant.isMap = false;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.getItem(1).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refrash) {
            webview.reload();
            mySwipeRefreshLayout.setRefreshing(true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showJobSearchDialog() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.job_search_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        mRegionCount = dialog.findViewById(R.id.mRegionCount);
        mPersonCount = dialog.findViewById(R.id.mPersonCount);
        mFacilityCount = dialog.findViewById(R.id.mFacilityCount);
        mStatusesCount = dialog.findViewById(R.id.mStatusesCount);
        mJobStatusCount = dialog.findViewById(R.id.mJobStatusCount);

        Button dialogBtnOk = dialog.findViewById(R.id.btnOk);
        Button dialogClear = dialog.findViewById(R.id.btnClear);


        if (App.isFilterApply) {
            dialogClear.setBackgroundResource(R.drawable.dialog_button_blue);
            dialogClear.setClickable(true);
        }
        dialogClear.setOnClickListener(v -> {
            dialogClear.setBackgroundResource(R.drawable.dialog_btn_gray);
            App.isFilterApply = false;
            shortBy = "j.CreatedOn-desc";
            spShoryBy.setSelection(1);

            if (!((MainActivity) getActivity()).statusStringList.isEmpty()) {
                spStatuses.clearSelection();
                txtStatuses.setVisibility(View.VISIBLE);
                App.indexListStatus.clear();
                multiSelecteStatusId = "";
            }
            if (!((MainActivity) getActivity()).regionStringList.isEmpty()) {
                spRegion.clearSelection();
                txtRegion.setVisibility(View.VISIBLE);
                App.indexListRegion.clear();
                multiSelectRegionId = "";
            }
            if (!((MainActivity) getActivity()).facillityStringList.isEmpty()) {
                spFacility.clearSelection();
                txtFacility.setVisibility(View.VISIBLE);
                App.indexListFacility.clear();
                multiSelectFacility = "";
            }

            if (!((MainActivity) getActivity()).contactorArrayList.isEmpty()) {
                spPerson.clearSelection();
                txtPersons.setVisibility(View.VISIBLE);
                App.indexListPerson.clear();
                multiSelectPersonId = "";

            }
            if (!((MainActivity) getActivity()).jobstateStringList.isEmpty()) {
                spJobState.clearSelection();
                txtJobState.setVisibility(View.VISIBLE);
                App.indexListJobState.clear();
                multiSelectedJobState = "";

            }
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                txtPersons.setVisibility(View.GONE);
                spPerson.setVisibility(View.GONE);
                viewPerson.setVisibility(View.GONE);
                multiSelectPersonId = userId;
            }

            mRegionCount.setText(String.format(getString(R.string.count), 0, ((MainActivity) getActivity()).regionStringList.size()));
            mPersonCount.setText(String.format(getString(R.string.count), 0, ((MainActivity) getActivity()).contactorArrayList.size()));
            mFacilityCount.setText(String.format(getString(R.string.count), 0, ((MainActivity) getActivity()).facillityStringList.size()));
            mStatusesCount.setText(String.format(getString(R.string.count), 0, ((MainActivity) getActivity()).statusStringList.size()));
            mJobStatusCount.setText(String.format(getString(R.string.count), 0, ((MainActivity) getActivity()).jobstateStringList.size()));

            if (Utils.isNetworkAvailable(getActivity()))
                getSaveDate("1", "0");
            else
                Utils.showAlert("No Internet, Please try again later", getActivity());

        });
        dialogBtnOk.setOnClickListener(v -> {
            dialogClear.setBackgroundColor(R.drawable.dialog_button_blue);
            App.isFilterApply = true;
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                multiSelectPersonId = userId;
            }
            url = BASEURL + "app?q=&user_id=" + userId +
                    "&login_email=" + emailId +
                    "&login_token=" + token +
                    "&state=" + multiSelectedJobState +
                    "&sort_by=" + shortBy +
                    "&regions=" + multiSelectRegionId +
                    "&persons=" + multiSelectPersonId +
                    "&facilities=" + multiSelectFacility +
                    "&statuses=" + multiSelecteStatusId + "&time=" + System.currentTimeMillis() + "&device_type=android";

            webview.loadUrl(url);
            dialog.dismiss();
            callUsingFilter = true;
            if (Utils.isNetworkAvailable(getActivity()))
                getSaveDate("0", "0");
            else
                Utils.showAlert("No Internet, Please try again later", getActivity());


        });
        dialog.show();

        App.isDialogOpen = true;
        spJobState = dialog.findViewById(R.id.spJobStatus);
        spShoryBy = dialog.findViewById(R.id.spShoryBy);
        spFacility = dialog.findViewById(R.id.spFacility);
        spPerson = dialog.findViewById(R.id.spPerson);
        spRegion = dialog.findViewById(R.id.spRegion);
        spStatuses = dialog.findViewById(R.id.spStatuses);
        txtPersons = dialog.findViewById(R.id.txtPersons);
        txtFacility = dialog.findViewById(R.id.txtFacility);
        txtJobState = dialog.findViewById(R.id.txtJobState);
        txtShortBy = dialog.findViewById(R.id.txtShortBy);
        txtRegion = dialog.findViewById(R.id.txtRegion);
        txtStatuses = dialog.findViewById(R.id.txtStatuses);
        viewPerson = dialog.findViewById(R.id.viewPerson);

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtPersons.setVisibility(View.GONE);
            spPerson.setVisibility(View.GONE);
            viewPerson.setVisibility(View.GONE);
            multiSelectPersonId = userId;
        }
        spJobState.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "jobStatus";
                return false;
            }
        });
        spRegion.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "region";
                return false;
            }
        });
        spPerson.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "person";
                return false;
            }
        });

        spStatuses.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "status";
                return false;
            }
        });
        spShoryBy.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "shortBy";
                return false;
            }
        });
        spFacility.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                type = "facility";
                return false;
            }
        });

        spStatuses.setItems(((MainActivity) getActivity()).statusStringList, Constant.FILTER_TYPE.STATUS);
        spStatuses.setSelection(new int[]{0});
        spStatuses.clearSelection();
        spStatuses.setListener(this);

        spPerson.setItems(((MainActivity) getActivity()).contactorArrayList, Constant.FILTER_TYPE.PERSONS);
        spPerson.setSelection(new int[]{0});
        spPerson.clearSelection();
        spPerson.setListener(this);

        spFacility.setItems(((MainActivity) getActivity()).facillityStringList, Constant.FILTER_TYPE.FACILITY);
        spFacility.setSelection(new int[]{0});
        spFacility.clearSelection();
        spFacility.setListener(this);

        spRegion.setItems(((MainActivity) getActivity()).regionStringList, Constant.FILTER_TYPE.REGIONS);
        spRegion.setSelection(new int[]{0});
        spRegion.clearSelection();
        spRegion.setListener(this);

        aaShortBy = new ArrayAdapter(getActivity(), R.layout.spinner_text, ((MainActivity) getActivity()).shortByStringList);
        aaShortBy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spShoryBy.setAdapter(aaShortBy);

        spJobState.setItems(((MainActivity) getActivity()).jobstateStringList, JOBSTATE);
        spJobState.setSelection(new int[]{0});
        spJobState.clearSelection();
        spJobState.setListener(this);

        Log.d(TAG, "showJobSearchDialog: " + shortBy);
        if (shortBy.equalsIgnoreCase("j.CreatedOn-asc")) {
            spShoryBy.setSelection(0);
        } else if (shortBy.equalsIgnoreCase("j.CreatedOn-desc")) {
            spShoryBy.setSelection(1);
        } else if (shortBy.equalsIgnoreCase("j.StatusID-asc")) {
            spShoryBy.setSelection(2);
        }

        spShoryBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    shortBy = "j.CreatedOn-asc";
                } else if (position == 1) {
                    shortBy = "j.CreatedOn-desc";
                } else
                    shortBy = "j.StatusID-asc";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setDefaultData();

        mRegionCount.setText(String.format(getString(R.string.count), spRegion.getSelectedIndices().size(), ((MainActivity) getActivity()).regionStringList.size()));
        mPersonCount.setText(String.format(String.format(getString(R.string.count), spPerson.getSelectedIndices().size(), ((MainActivity) getActivity()).contactorArrayList.size())));
        mFacilityCount.setText(String.format(String.format(getString(R.string.count), spFacility.getSelectedIndices().size(), ((MainActivity) getActivity()).facillityStringList.size())));
        mStatusesCount.setText(String.format(String.format(getString(R.string.count), spStatuses.getSelectedIndices().size(), ((MainActivity) getActivity()).statusStringList.size())));
        mJobStatusCount.setText(String.format(getString(R.string.count), spJobState.getSelectedIndices().size(), ((MainActivity) getActivity()).jobstateStringList.size()));
    }


    private void setDefaultData() {


        if (!TextUtils.isEmpty(multiSelecteStatusId)) {
            String[] values = multiSelecteStatusId.split(",");
            App.indexListStatus.clear();
            for (int j = 0; j < values.length; j++) {
                for (int i = 0; i < ((MainActivity) getActivity()).statusList.size(); i++) {
                    JobStatusModel jobStateModel = ((MainActivity) getActivity()).statusList.get(i);
                    if (jobStateModel.getJSID().equalsIgnoreCase(values[j])) {
                        App.indexListStatus.add(i);
                    }
                }
            }
        }
        if (!TextUtils.isEmpty(multiSelectPersonId)) {
            String[] values = multiSelectPersonId.split(",");
            App.indexListPerson.clear();
            for (int j = 0; j < values.length; j++) {
                for (int i = 0; i < ((MainActivity) getActivity()).listPerson.size(); i++) {
                    ContractorModel jobStateModel = ((MainActivity) getActivity()).listPerson.get(i);
                    if (jobStateModel.getId().equalsIgnoreCase(values[j])) {
                        App.indexListPerson.add(i);
                    }
                }
            }

        }
        if (!TextUtils.isEmpty(multiSelectFacility)) {
            String[] values = multiSelectFacility.split(",");
            App.indexListFacility.clear();
            for (int j = 0; j < values.length; j++) {
                for (int i = 0; i < ((MainActivity) getActivity()).facilityList.size(); i++) {
                    FacilitesModel jobStateModel = ((MainActivity) getActivity()).facilityList.get(i);
                    if (jobStateModel.getFacilityID().equalsIgnoreCase(values[j])) {
                        App.indexListFacility.add(i);
                    }
                }
            }
        }
        if (!TextUtils.isEmpty(multiSelectRegionId)) {
            String[] values = multiSelectRegionId.split(",");
            App.indexListRegion.clear();
            for (int j = 0; j < values.length; j++) {
                for (int i = 0; i < ((MainActivity) getActivity()).regionList.size(); i++) {
                    RegionModel jobStateModel = ((MainActivity) getActivity()).regionList.get(i);
                    if (jobStateModel.getRegionID().equalsIgnoreCase(values[j])) {
                        App.indexListRegion.add(i);
                    }
                }
            }
        }

        if (!TextUtils.isEmpty(multiSelectedJobState)) {
            String[] values = multiSelectedJobState.split(",");
            App.indexListJobState.clear();
            for (int j = 0; j < values.length; j++) {
                for (int i = 0; i < ((MainActivity) getActivity()).jobStateList.size(); i++) {
                    JobStateModel jobStateModel = ((MainActivity) getActivity()).jobStateList.get(i);
                    if (jobStateModel.getId().equalsIgnoreCase(values[j])) {
                        App.indexListJobState.add(i);
                    }
                }
            }

            mJobStatusCount.setText(String.format(getString(R.string.count), spJobState.getSelectedIndices().size(), ((MainActivity) getActivity()).jobstateStringList.size()));
        }

        if (!TextUtils.isEmpty(multiSelectRegionId)) {
            if (App.indexListRegion.size() > 0) {
                int[] array = new int[App.indexListRegion.size()];
                int counter = 0;
                for (Integer myInt : App.indexListRegion) {
                    array[counter++] = myInt;
                }
                spRegion.setSelection(array);
            }
            txtRegion.setVisibility(View.GONE);
            mRegionCount.setText(String.format(String.format(getString(R.string.count), spRegion.getSelectedIndices().size(), +((MainActivity) getActivity()).regionStringList.size())));
        }
        if (!TextUtils.isEmpty(multiSelecteStatusId)) {

            if (App.indexListStatus.size() > 0) {
                int[] array = new int[App.indexListStatus.size()];
                int counter = 0;
                for (Integer myInt : App.indexListStatus) {
                    array[counter++] = myInt;
                }
                spStatuses.setSelection(array);
            }
            txtStatuses.setVisibility(View.GONE);
            mStatusesCount.setText(String.format(String.format(getString(R.string.count), spStatuses.getSelectedIndices().size(), ((MainActivity) getActivity()).statusStringList.size())));
        }
        if (!TextUtils.isEmpty(multiSelectFacility)) {
            if (App.indexListFacility.size() > 0) {
                int[] array = new int[App.indexListFacility.size()];
                int counter = 0;
                for (Integer myInt : App.indexListFacility) {
                    array[counter++] = myInt;
                }
                spFacility.setSelection(array);
            }
            txtFacility.setVisibility(View.GONE);
            mFacilityCount.setText(String.format(String.format(getString(R.string.count), spFacility.getSelectedIndices().size(), ((MainActivity) getActivity()).facillityStringList.size())));
        }
        if (!TextUtils.isEmpty(multiSelectPersonId)) {
            if (App.indexListPerson.size() > 0) {
                int[] array = new int[App.indexListPerson.size()];
                int counter = 0;
                for (Integer myInt : App.indexListPerson) {
                    array[counter++] = myInt;
                }
                spPerson.setSelection(array);
            }
            txtPersons.setVisibility(View.GONE);
            mPersonCount.setText(String.format(String.format(getString(R.string.count), spPerson.getSelectedIndices().size(), ((MainActivity) getActivity()).contactorArrayList.size())));
        }
        if (!TextUtils.isEmpty(multiSelectedJobState)) {
            if (App.indexListJobState.size() > 0) {
                int[] array = new int[App.indexListJobState.size()];
                int counter = 0;
                for (Integer myInt : App.indexListJobState) {
                    array[counter++] = myInt;
                }
                spJobState.setSelection(array);
            }
            txtJobState.setVisibility(View.GONE);
            mJobStatusCount.setText(String.format(getString(R.string.count), spPerson.getSelectedIndices().size(), ((MainActivity) getActivity()).jobstateStringList.size()));
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : Save_data " + result);
            switch (request) {
                case getSaveData:
                    Utils.hideProgressDialog();
                    if (!callUsingFilter) {
                        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                            multiSelectPersonId = userId;
                        }
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                                SearchData searchData = LoganSquare.parse(jsonObject.getString("data"), SearchData.class);
                                multiSelectedJobState = !TextUtils.isEmpty(searchData.getJobState()) ? searchData.getJobState() : "";

                                if (!TextUtils.isEmpty(searchData.getShortBy())) {
                                    shortBy = searchData.getShortBy();
                                }
                                multiSelectRegionId = !TextUtils.isEmpty(searchData.getRegions()) ? searchData.getRegions() : "";
                                multiSelectPersonId = !TextUtils.isEmpty(searchData.getPersons()) ? searchData.getPersons() : "";
                                multiSelectFacility = !TextUtils.isEmpty(searchData.getFacilities()) ? searchData.getFacilities() : "";
                                multiSelecteStatusId = !TextUtils.isEmpty(searchData.getStatuses()) ? searchData.getStatuses() : "";
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        url = BASEURL + "app?q=&user_id=" + userId +
                                "&login_email=" + emailId +
                                "&login_token=" + token +
                                "&state=" + multiSelectedJobState +
                                "&sort_by=" + shortBy +
                                "&regions=" + multiSelectRegionId +
                                "&persons=" + multiSelectPersonId +
                                "&facilities=" + multiSelectFacility +
                                "&statuses=" + multiSelecteStatusId + "&time=" + System.currentTimeMillis() + "&device_type=android";

                        webview.loadUrl(url);
                    }
                    callUsingFilter = false;
                    break;
            }
        }
    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {

        Log.d(TAG, "selectedIndices: " + indices);
        if (filter_type == STATUS) {
            App.indexListStatus = indices;
            multiSelecteStatusId = "";
            multiSelecteStatusName = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            ArrayList<String> statusNameArray = new ArrayList<>();
            for (Integer i : indices) {
                statusIDArray.add(((MainActivity) getActivity()).statusList.get(i).getJSID());
                statusNameArray.add(((MainActivity) getActivity()).statusList.get(i).getJobStatusName());
            }
            txtStatuses.setVisibility(View.GONE);
            multiSelecteStatusId = android.text.TextUtils.join(",", statusIDArray);
            multiSelecteStatusName = android.text.TextUtils.join(",", statusNameArray);
            if (multiSelecteStatusId.equals("")) {
                txtStatuses.setVisibility(View.VISIBLE);
            }
            mStatusesCount.setText(String.format(getString(R.string.count), spStatuses.getSelectedIndices().size(), ((MainActivity) getActivity()).statusStringList.size()));
        } else if (filter_type == PERSONS) {
            App.indexListPerson = indices;
            multiSelectPersonId = "";
            multiSelecteStatusName = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            ArrayList<String> statusNameArray = new ArrayList<>();
            String selectedNames = "";
            for (Integer i : indices) {
                statusIDArray.add(((MainActivity) getActivity()).listPerson.get(i).getId());
                statusNameArray.add(((MainActivity) getActivity()).listPerson.get(i).getFullName());
                selectedNames += ((MainActivity) getActivity()).listPerson.get(i).getFullName() + ", ";
            }
            txtPersons.setVisibility(View.GONE);
            multiSelectPersonId = android.text.TextUtils.join(",", statusIDArray);
            multiSelectPersonName = android.text.TextUtils.join(",", statusNameArray);
            if (multiSelectPersonId.equals("")) {
                txtPersons.setVisibility(View.VISIBLE);
            }
            mPersonCount.setText(String.format(getString(R.string.count), spPerson.getSelectedIndices().size(), ((MainActivity) getActivity()).contactorArrayList.size()));

        } else if (filter_type == FACILITY) {
            App.indexListFacility = indices;
            multiSelectFacility = "";
            multiSelectFacilityName = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            ArrayList<String> statusNameArray = new ArrayList<>();
            for (Integer i : indices) {
                statusIDArray.add(((MainActivity) getActivity()).facilityList.get(i).getFacilityID());
                statusNameArray.add(((MainActivity) getActivity()).facilityList.get(i).getFacilityName());
            }
            txtFacility.setVisibility(View.GONE);
            multiSelectFacility = android.text.TextUtils.join(",", statusIDArray);
            multiSelectFacilityName = android.text.TextUtils.join(",", statusNameArray);
            if (multiSelectFacility.equals("")) {
                txtFacility.setVisibility(View.VISIBLE);
            }
            mFacilityCount.setText(String.format(getString(R.string.count), spFacility.getSelectedIndices().size(), ((MainActivity) getActivity()).facillityStringList.size()));

        } else if (filter_type == REGIONS) {

            App.indexListRegion = indices;
            multiSelectRegionId = "";
            multiSelectRegionName = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            ArrayList<String> statusNameArray = new ArrayList<>();
            for (Integer i : indices) {
                statusIDArray.add(((MainActivity) getActivity()).regionList.get(i).getRegionID());
                statusNameArray.add(((MainActivity) getActivity()).regionList.get(i).getRegionName());
            }
            txtRegion.setVisibility(View.GONE);

            multiSelectRegionId = android.text.TextUtils.join(",", statusIDArray);
            multiSelectRegionName = android.text.TextUtils.join(",", statusNameArray);
            if (multiSelectRegionId.equals("")) {
                txtRegion.setVisibility(View.VISIBLE);
            }
            mRegionCount.setText(String.format(getString(R.string.count), spRegion.getSelectedIndices().size(), ((MainActivity) getActivity()).regionStringList.size()));

        } else if (filter_type == JOBSTATE) {

            App.indexListJobState = indices;
            multiSelectedJobState = "";
            multiSelectedJobStateName = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            ArrayList<String> statusNameArray = new ArrayList<>();
            for (Integer i : indices) {
                statusIDArray.add(((MainActivity) getActivity()).jobStateList.get(i).getId());
                statusNameArray.add(((MainActivity) getActivity()).jobStateList.get(i).getName());
            }
            txtJobState.setVisibility(View.GONE);

            multiSelectedJobState = android.text.TextUtils.join(",", statusIDArray);
            multiSelectedJobStateName = android.text.TextUtils.join(",", statusNameArray);
            if (multiSelectedJobState.equals("")) {
                txtJobState.setVisibility(View.VISIBLE);
            }
            mJobStatusCount.setText(String.format(getString(R.string.count), spJobState.getSelectedIndices().size(), ((MainActivity) getActivity()).jobstateStringList.size()));

        }

    }


    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MainActivity.toolbar_title.setText("");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }


    public class ChromeClient extends WebChromeClient {
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePath;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = ImageProcess.createImageFile(getActivity());
                } catch (IOException ex) {
                    Log.e("TAG", "Unable to create Image File", ex);
                }
                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                } else {
                    takePictureIntent = null;
                }
            }
            startActivityForResult(takePictureIntent, INPUT_FILE_REQUEST_CODE);
            return true;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_FILE) {
            if (uploadMessage == null)
                return;
            uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
            uploadMessage = null;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            Uri[] results = null;
            if (resultCode == RESULT_OK) {
                if (data == null) {
                    Log.d(TAG, "onActivityResult: 12 " + mCameraPhotoPath);
                    if (mCameraPhotoPath != null) {
                        Log.d(TAG, "onActivityResult: 13 " + ImageProcess.getUriRealPath(getActivity(), Uri.fromFile(new File(mCameraPhotoPath))).substring(1));
                        try {
                            Uri uri = ImageProcess.handleSamplingAndRotationBitmap(getActivity(), Uri.parse(mCameraPhotoPath), new File(mCameraPhotoPath).getAbsolutePath());
                            results = new Uri[]{uri};
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }
            if (results != null)
                mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == this.mUploadMessage) {
                    return;
                }
                Uri result = null;
                try {
                    if (resultCode != RESULT_OK) {
                        result = null;
                    } else {
                        Log.d(TAG, "onActivityResult: result = data == null");
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
               /*     Toast.makeText(getActivity(), "activity :" + e,
                            Toast.LENGTH_LONG).show();
             */
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
        return;
    }

    private class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:") || url.startsWith("mms:") || url.startsWith("mmsto:")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
            if (url.contains("geo:")) {
                Constant.isMap = true;
                Uri gmmIntentUri = Uri.parse(url);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                return true;
            }
            Log.d(TAG, "shouldOverrideUrlLoading: " + url);
            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Utils.hideProgressDialog();
            mySwipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
