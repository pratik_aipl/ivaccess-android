package com.ivaccessbeta.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ivaccessbeta.fragment.schedule.SchedualDetailFragment;

/**
 * Created by User on 05-03-2018.
 */

public class SchedualTabsPagerAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;

    public SchedualTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
                return SchedualDetailFragment.newInstance(position);

    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return POSITION_NONE;
    }


    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
