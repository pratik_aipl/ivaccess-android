package com.ivaccessbeta.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.LoginActivity;
import com.ivaccessbeta.activity.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by admin on 10/12/2016.
 */
public class Utils {


    private static final String TAG = "Utils";
    public static Dialog dialog;

    public static SimpleDateFormat InputDate = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat output = new SimpleDateFormat("MMMM dd, yyyy");
    public static int mYear;
    public static int mMonth;
    public static int mDay;


    public static void longInfo(String str, String tag) {
        if (str.length() > 4000) {
            Log.i("TAG " + tag + " -->", str.substring(0, 4000));
            longInfo(str.substring(4000), tag);
        } else
            Log.i("TAG " + tag + " -->", str);
    }

    public static void handleJSonAPIError(JSONObject jObj, final Context ct) {
        try {
            if (jObj.getString("status").equals("400")) {
                Utils.showAlert(jObj.getString("message"), ct);
            }
            if (jObj.getString("status").equals("401")) {

                String message = jObj.getString("message");
                new AlertDialog.Builder(ct)
                        .setMessage(message)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Utils.hideProgressDialog();
                                MySharedPref.setisLogin(false);
                                MySharedPref.SharedPrefClear(ct);
                                App.multiSelectPlaceId = "";
                                App.multiSelectRegionId = "";
                                App.multiSelectPersonId = "";
                                App.contractorId = "";
                                Intent i = new Intent(ct, LoginActivity.class);
                                ct.startActivity(i);
                                // ct.finish();
                                ((MainActivity) ct).finish();
                            }
                        })
                        .show();
                Utils.showAlert(jObj.getString("message"), ct);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void showProgressDialog(Context context, String msg) {
        try {

            if (dialog == null) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.custom_progress_dialog);

            }

            TextView txtView = dialog.findViewById(R.id.textView);
            txtView.setText(msg);

            if (!dialog.isShowing())
                dialog.show();

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static String getDateFormat(String input, String output, String mydate) {
        SimpleDateFormat srcDf = new SimpleDateFormat(input);

        try {
            Date date = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat(output);
            mydate = destDf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mydate;
    }

    public static void allMenuHide(Menu menu) {
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        menu.getItem(2).setVisible(false);
    }

    public static void setWebViewSettings(WebView webview) {
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setAllowContentAccess(true);
        webview.getSettings().setAllowFileAccessFromFileURLs(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setDomStorageEnabled(true);

        webview.getSettings().setDatabaseEnabled(true);
        webview.getSettings().setAppCacheEnabled(true);
        webview.getSettings().setAppCacheMaxSize(1);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webview.clearCache(true);
        webview.clearHistory();
    }

    public static void converMMddyyyyFormate(String date, TextView txt) {

        if (!TextUtils.isEmpty(date)) {
            Date initDate = null;
            try {
                initDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
            String parsedDate = formatter.format(initDate);
            txt.setText(parsedDate);
        } else {
            txt.setText(date);
        }
    }

    public static String converMMddyyyyFormate(String date) {
        if (!TextUtils.isEmpty(date)) {
            Date initDate = null;
            try {
                initDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
            String parsedDate = formatter.format(initDate);
            return parsedDate;
        } else {
            return "";
        }
    }

    public static void generateDatePicker(final Context context, final TextView edtDate) {

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        edtDate.setText(Utils.getDateFormat("M", "MM", String.valueOf((monthOfYear + 1))) + "-" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth)) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        dpd.show();
    }

    public static void disableKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static void showAlert(String msg, final Context ctx) {

        new AlertDialog.Builder(ctx)
                .setMessage(msg)
                .setPositiveButton("Ok", (dialog, which) -> {
                    dialog.dismiss();
                    Utils.hideProgressDialog();
                })
                .show();

    }

    public static void dateConvertFormate(TextView txt, String Date, String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = null;
        try {
            date = sdf.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat formatDate = new SimpleDateFormat("dd");
        DateFormat formatMonth = new SimpleDateFormat("MM");
        DateFormat formatYear = new SimpleDateFormat("yyyy");
        DateFormat formatAmPm = new SimpleDateFormat("hh: mm a");
        //  DateFormat formatAmPm=new SimpleDateFormat("a");
        String finalDate = formatDate.format(date);
        String finalMonth = formatMonth.format(date);
        String finalHour = formatAmPm.format(date);
        String finalYear = formatYear.format(date);
        System.out.println("fianl day===" + finalDate + " finalMonth==== " + finalMonth + " finalHorsh=== " + finalHour + "final Year===" + finalYear);

        txt.setText(msg + finalMonth + "-" + finalDate + "-" + finalYear + " " + finalHour);
    }

    public static void showProgressDialog(Context context) {
        if (!((Activity) context).isFinishing()) {
            showProgressDialog(context, "Please Wait..");
        }
    }

    public static void showToast(String msg, final Context ctx) {
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
        toast.show();
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void showShortSnack(View parent, String message) {
        Snackbar snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
        textView.setMaxLines(10);
        snackbar.show();
    }

    public static void showLongSnack(View parent, String message) {
        Snackbar snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_LONG);
        TextView textView = snackbar.getView().findViewById(R.id.snackbar_text);
        textView.setMaxLines(10);
        snackbar.show();
    }

    public static void initToolBar(AppCompatActivity activity, Toolbar toolbar, boolean homeUpIndicator) {
        activity.setSupportActionBar(toolbar);
        final ActionBar ab = activity.getSupportActionBar();
        ab.setDisplayShowHomeEnabled(false);
        ab.setDisplayHomeAsUpEnabled(homeUpIndicator);
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
    }

    public static boolean hasCameraHardware(Context context) {
        return context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA);
    }

    public static boolean hasCameraFlashHardware(Context context) {
        return context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH);
    }

    public static void setViewBackgroundColor(Activity activity, View view, int color) {
        if (color == 0)
            return;
        if (view instanceof ImageButton) {
            AppCompatImageButton imageButton = (AppCompatImageButton) view;
            GradientDrawable drawable = (GradientDrawable) imageButton.getBackground();
            drawable.setColor(color);
            if (Build.VERSION.SDK_INT >= 16)
                imageButton.setBackground(drawable);
            else
                imageButton.setBackgroundDrawable(drawable);
        } else if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            GradientDrawable drawable = (GradientDrawable) imageView.getBackground();
            drawable.setColor(color);
            if (Build.VERSION.SDK_INT >= 16)
                imageView.setBackground(drawable);
            else
                imageView.setBackgroundDrawable(drawable);
        } else if (view instanceof Toolbar) {
            Toolbar toolbar = (Toolbar) view;
            toolbar.setBackgroundColor(color);
            if (Build.VERSION.SDK_INT >= 21) {
                Window window = activity.getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getDarkColor(color));
            }
        }
    }

    public static void setButtonTextColor(View view, int color) {
        if (color == 0)
            return;
        if (view instanceof Button) {
            ((Button) view).setTextColor(color);
        }
    }

    public static int getDarkColor(int color) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.rgb((int) (red * 0.8), (int) (green * 0.8), (int) (blue * 0.8));
    }

    public static int getLightColor(int color) {
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb((int) (255 * 0.5), red, green, blue);
    }
}





