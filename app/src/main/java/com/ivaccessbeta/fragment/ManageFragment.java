package com.ivaccessbeta.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.ManageTabsPagerAdapter;
import com.ivaccessbeta.utils.Utils;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

public class ManageFragment extends Fragment {
    public View view;
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public ManageTabsPagerAdapter adapter;
    public RelativeLayout relativeLeft,relativeRight;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_manage, container, false);
        viewPager = view. findViewById(R.id.viewpager);
        MainActivity.toolbar_title.setText("MANAGE");
        ((MainActivity)getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        tabLayout = view. findViewById(R.id.tabs);
        relativeLeft=view.findViewById(R.id.relative_left);
        relativeRight=view.findViewById(R.id.relative_right);
        tabLayout.addTab(tabLayout.newTab().setText("REGIONS "));
        tabLayout.addTab(tabLayout.newTab().setText("PLACE "));
        tabLayout.addTab(tabLayout.newTab().setText("PEOPLES "));
        tabLayout.addTab(tabLayout.newTab().setText("FACILITES "));
        tabLayout.addTab(tabLayout.newTab().setText("BILL TO "));
        tabLayout.addTab(tabLayout.newTab().setText("SERVICE TYPE "));
        tabLayout.addTab(tabLayout.newTab().setText("EQUIPMENTS "));
        tabLayout.addTab(tabLayout.newTab().setText("DOCUMENTS "));

        relativeLeft.setOnClickListener(view -> viewPager.setCurrentItem(getItem(-1), true));
        relativeRight.setOnClickListener(view -> viewPager.setCurrentItem(getItem(1), true));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new ManageTabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        setHasOptionsMenu(true);
        return view;
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }
}
