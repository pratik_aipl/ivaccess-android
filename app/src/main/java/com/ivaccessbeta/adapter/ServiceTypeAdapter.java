package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.manage.AddServiceFragment;
import com.ivaccessbeta.model.ServieTypeModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.ivaccessbeta.fragment.manage.ServiceTypeFragment.instance;

/**
 * Created by User on 01-03-2018.
 */

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.MyViewHolder> {

    public ArrayList<ServieTypeModel> list;
    public List<ServieTypeModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;

    @Override
    public ServiceTypeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_region, parent, false);

        return new ServiceTypeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ServiceTypeAdapter.MyViewHolder holder, final int position) {
        ServieTypeModel model = list.get(position);
        holder.name.setText(model.getServiceName());
        holder.ivDelete.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        emailId = MySharedPref.getString(context, App.EMAIL, "");
                        token = MySharedPref.getString(context, App.APP_TOKEN, "");
                        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");

                        doDelete(position);
                    }
                });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddServiceFragment addServiceFragment=new AddServiceFragment();
                Bundle b=new Bundle();
                b.putInt("value",1);
                b.putSerializable("ServiceModel",list.get(position));
//                b.putString("serviceId",list.get(position).getServiceID());
//                b.putString("serviceName",list.get(position).getServiceName());
//                b.putString("equipmentId",list.get(position).getEquipmentID());
//                b.putString("serviceRate",list.get(position).getServiceRate());
                addServiceFragment.setArguments(b);
                ((MainActivity)context).changeFragment(addServiceFragment,true);
            }
        });

    }

    private void doDelete(final int position) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteService(emailId, token, userId, list.get(position).getServiceID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (ServieTypeModel bean : searchFilterList) {
                if (bean.getServiceName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ServiceTypeAdapter(Context context, ArrayList<ServieTypeModel> List) {
        this.context = context;
        this.list = List;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView ivDelete,ivEdit;

        public MyViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.txtName);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivEdit=view.findViewById(R.id.ivEdit);

        }


    }
}
