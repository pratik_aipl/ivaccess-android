package com.ivaccessbeta.chat;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 28-06-2018.
 */

@JsonObject
public class ChatModel {
    @JsonField
    public String SenderImage, ProfileImage, ChatID, ThreadID, GroupID, Message, SenderID, ReceiverID, SendingDate, ChatStatus, ReceiverStatus, SenderName, ProfileName;

    public String getSenderImage() {
        return SenderImage;
    }

    public void setSenderImage(String senderImage) {
        SenderImage = senderImage;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }

    public String getChatID() {
        return ChatID;
    }

    public void setChatID(String chatID) {
        ChatID = chatID;
    }

    public String getThreadID() {
        return ThreadID;
    }

    public void setThreadID(String threadID) {
        ThreadID = threadID;
    }

    public String getGroupID() {
        return GroupID;
    }

    public void setGroupID(String groupID) {
        GroupID = groupID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    public String getReceiverID() {
        return ReceiverID;
    }

    public void setReceiverID(String receiverID) {
        ReceiverID = receiverID;
    }

    public String getSendingDate() {
        return SendingDate;
    }

    public void setSendingDate(String sendingDate) {
        SendingDate = sendingDate;
    }

    public String getChatStatus() {
        return ChatStatus;
    }

    public void setChatStatus(String chatStatus) {
        ChatStatus = chatStatus;
    }

    public String getReceiverStatus() {
        return ReceiverStatus;
    }

    public void setReceiverStatus(String receiverStatus) {
        ReceiverStatus = receiverStatus;
    }

    public String getSenderName() {
        return SenderName;
    }

    public void setSenderName(String senderName) {
        SenderName = senderName;
    }

    public String getProfileName() {
        return ProfileName;
    }

    public void setProfileName(String profileName) {
        ProfileName = profileName;
    }
}
