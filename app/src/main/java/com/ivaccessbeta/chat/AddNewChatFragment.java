package com.ivaccessbeta.chat;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NotifyAdapter;
import com.ivaccessbeta.model.NotifyModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.App.indexNotifyList;
import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewChatFragment extends Fragment implements AsynchTaskListner {
    public ArrayList<NotifyModel> notifyArrayList = new ArrayList<>();
    public RecyclerView rvNotify;
    public NotifyAdapter notifyAdapter;
    public Button btnNext;
    public AddNewChatFragment instance;
    public String emailId, token, userId, chatType = "", message, roleId = "";
    public CheckBox selectAll;
    public boolean isSelectAll;
    public EditText edtSearch;
    public Dialog dialog;


    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_new_chat, container, false);
        rvNotify = view.findViewById(R.id.rvNotify);
        btnNext = view.findViewById(R.id.btnNext);
        selectAll = view.findViewById(R.id.cbSelectAll);
        instance = this;
        setHasOptionsMenu(true);
        MainActivity.toolbar_title.setText("NOTIFY");
        ((MainActivity) getActivity()).showToolbarAction();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvNotify.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvNotify.setLayoutManager(mLayoutManager);
        getNotifyUserList();
        edtSearch = view.findViewById(R.id.edtSearch);
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                    notifyAdapter.filters(text);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        selectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelectAll = ((CheckBox) v).isChecked();
                if (isSelectAll) {
                    indexNotifyList.clear();
                    for (NotifyModel n : notifyArrayList) {
                        indexNotifyList.add(n.getId());
                    }
                } else {
                    indexNotifyList.clear();
                }
                notifyAdapter.selectAll(isSelectAll);
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.notifyPersonId = android.text.TextUtils.join(",", indexNotifyList);
                if (App.notifyPersonId.equals("")) {
                    Utils.showToast("Please Select Person", getContext());
                } else {
                    SendMessageDialog(view);
                }
            }
        });
        return view;
    }

    private void SendMessageDialog(View view) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.message_send_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final RadioGroup mGp = dialog.findViewById(R.id.mGp);
        final RadioButton rbIndividual = dialog.findViewById(R.id.rbIndividual);
        final RadioButton rbGroup = dialog.findViewById(R.id.rbGroup);
        final EditText edtGroupTitle = dialog.findViewById(R.id.edtGroupTitle);
        final EditText edtMessage = dialog.findViewById(R.id.edtMessage);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            rbGroup.setVisibility(View.GONE);
            rbIndividual.setChecked(true);
        }

        mGp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rbIndividual:
                        chatType = "0";
                        edtGroupTitle.setVisibility(View.GONE);
                        edtMessage.requestFocus();
                        break;
                    case R.id.rbGroup:
                        chatType = "1";
                        edtGroupTitle.setVisibility(View.VISIBLE);
                        edtGroupTitle.requestFocus();
                        break;
                }
            }
        });
//        rbGroup.setChecked(true);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rbGroup.isChecked()) {
                    chatType = "1";
                } else if (rbIndividual.isChecked()) {
                    chatType = "0";
                }
                message = edtMessage.getText().toString();
                if (TextUtils.isEmpty(chatType)) {
                    Utils.showToast("Please Select Chat Type", getActivity());
                } else if (TextUtils.isEmpty(message)) {
                    Utils.showToast("Please Enter Message.", getActivity());
                } else {
                    if (rbGroup.isChecked()) {
                        if (TextUtils.isEmpty(edtGroupTitle.getText().toString().trim())) {
                            Utils.showToast("Please Enter Group Title.", getActivity());
                        } else {
                            addComment(edtGroupTitle.getText().toString(), message, chatType);
                        }
                    } else if (rbIndividual.isChecked()) {
                        addComment("", message, chatType);
                    }
                }
            }
        });
        dialog.show();
    }

    private void addComment(String s, String notification, String type) {
        if (TextUtils.isEmpty(s))
            new CallRequest(instance).addMessage(emailId, token, userId, notification, type, App.notifyPersonId);
        else
            new CallRequest(instance).addMessageGroup(emailId, token, userId, s, notification, type, App.notifyPersonId);
    }

//    public void onRadioButtonClicked(View view) {
//        boolean checked = ((RadioButton) view).isChecked();
//        switch (view.getId()) {
//            case R.id.rbIndividual:
//                if (checked)
//                    chatType = "0";
//                break;
//            case R.id.rbGroup:
//                if (checked)
//                    chatType = "1";
//                break;
//        }
//    }

    public void getNotifyUserList() {
        new CallRequest(instance).getNotificationUserList(emailId, token, roleId);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(false);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getNotificationUserList:
                    notifyArrayList.clear();
                    App.indexNotifyList.clear();
                    selectAll.setChecked(false);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            notifyArrayList.addAll(LoganSquare.parseList(ob1.toString(), NotifyModel.class));
                            notifyAdapter = new NotifyAdapter(notifyArrayList, selectAll);
                            rvNotify.setItemAnimator(new DefaultItemAnimator());
                            rvNotify.setAdapter(notifyAdapter);
                            notifyAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();

                    break;
                case addMessage:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            dialog.dismiss();
                            getActivity().onBackPressed();
                        } else {
                            Utils.showToast(jObj.getString("message"), getContext());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

            }
        }

    }


}
