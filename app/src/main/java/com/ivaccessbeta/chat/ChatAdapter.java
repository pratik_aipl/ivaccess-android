package com.ivaccessbeta.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by abc on 11/28/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ChatFragment chatFragment;
    public ArrayList<ChatModel> commentList = new ArrayList<>();
    public String userId;
    public ChatAdapter(ArrayList<ChatModel> commentList, Context mContext) {
        this.mContext = mContext;
        this.commentList = commentList;
        this.chatFragment = chatFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.list_item_comment_admin, parent, false);
                viewHolder = new ViewHolderAdmin(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.list_item_comment_user, parent, false);
                viewHolder = new ViewHolderUser(v2);
                break;
        }
        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != commentList ? commentList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        try {
            //TODO
            if (commentList.get(position).getSenderID().equalsIgnoreCase(MySharedPref.getString(mContext, App.LOGGED_USER_ID, ""))) {
                    return 0;
            } else {
            return 1;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderAdmin viewHolderAdmin = (ViewHolderAdmin) holder;
                bindAdminHolder(viewHolderAdmin, position);
                break;
            case 1:
                ViewHolderUser holderImages = (ViewHolderUser) holder;
                bindUserHolder(holderImages, position);
                break;

        }
    }

    private void bindUserHolder(ViewHolderUser holder, final int position) {

        holder.tvComment.setText(commentList.get(position).getMessage());
        holder.tvTime.setText(getFormattedTime(commentList.get(position).getSendingDate()));
        holder.tvUserName.setText(commentList.get(position).getSenderName());
        Picasso.with(mContext).load(commentList.get(position).getSenderImage()).into(holder.ivSenderProfile);
    }
    private void bindAdminHolder(ViewHolderAdmin holder, int position) {
        holder.tvComment.setText(commentList.get(position).getMessage());
        holder.tvTime.setText(getFormattedTime(commentList.get(position).getSendingDate()));

        Picasso.with(mContext).load(commentList.get(position).getProfileImage()).into(holder.ivProfile);
    }
    public String getFormattedTime(String date) {
        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat newFormat = new SimpleDateFormat("hh:mm a | MMMM dd, yyyy");
            Date currentDate = new Date();

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public class ViewHolderAdmin extends RecyclerView.ViewHolder {

        public TextView tvComment, tvTime,tvDate;
        public ImageView ivProfile;
        ViewHolderAdmin(View v) {
            super(v);
            tvComment = v.findViewById(R.id.tvComment);
         //  tvUserName = v.findViewById(R.id.tvUserName);
            tvTime = v.findViewById(R.id.tvTime);
            tvDate= v.findViewById(R.id.tvDate);
            ivProfile=v.findViewById(R.id.ivProfile);
        }
    }


    public class ViewHolderUser extends RecyclerView.ViewHolder {
        public ImageView ivSenderProfile;
        public TextView tvComment, tvTime,tvUserName;

        ViewHolderUser(View v) {
            super(v);

            tvComment = v.findViewById(R.id.tvComment);
             tvUserName = v.findViewById(R.id.tvUserName);
            tvTime = v.findViewById(R.id.tvTime);
            ivSenderProfile=v.findViewById(R.id.ivSenderProfile);

        }
    }


}
