package com.ivaccessbeta.fragment.profile;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.ProfileEmpDocAdapter;
import com.ivaccessbeta.listners.ProfileDocumentListner;
import com.ivaccessbeta.model.ProfileEmpDocModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.DocChangeEvent;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.ivaccessbeta.utils.Utils.hasPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpiredDocFragment extends Fragment implements AsynchTaskListner, ProfileDocumentListner {
    public RecyclerView rvEmpDoc;
    public ArrayList<ProfileEmpDocModel> list = new ArrayList<>();
    public ProfileEmpDocAdapter profileEmpDOCAdapter;
    public String emailId, token, userId, expiredDate;
    public RecyclerView rvDocument;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public static Uri selectedUri;
    public static String imgPath;
    public static Dialog dialog;
    public static View layout;
    private static int RESULT_CROP_DP = 3;

    public static Fragment newInstance(ArrayList<ProfileEmpDocModel> list) {
        ExpiredDocFragment fragment = new ExpiredDocFragment();
        Bundle args = new Bundle();
        args.putSerializable("list", list);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_employee_doc, container, false);
        setHasOptionsMenu(true);
        ArrayList<ProfileEmpDocModel> tempList = new ArrayList<>();

        tempList.addAll((Collection<? extends ProfileEmpDocModel>) getArguments().getSerializable("list"));

        for (int i = 0; i < tempList.size(); i++) {
            ProfileEmpDocModel empDocModel = tempList.get(i);
            if (empDocModel.getExpired().equalsIgnoreCase("1")) {
                list.add(empDocModel);
            }
        }

        rvEmpDoc = view.findViewById(R.id.rvEmpDoc);
        rvEmpDoc.setVisibility(View.VISIBLE);

        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        try {
            if (App.contractorId.equalsIgnoreCase("")) {
                userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
            } else {
                userId = App.contractorId;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvEmpDoc.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvEmpDoc.setLayoutManager(mLayoutManager);
        rvEmpDoc.setItemAnimator(new DefaultItemAnimator());

        profileEmpDOCAdapter = new ProfileEmpDocAdapter(list, ExpiredDocFragment.this);
        rvEmpDoc.setAdapter(profileEmpDOCAdapter);
        profileEmpDOCAdapter.notifyDataSetChanged();

        return view;
    }

    public void dialogViewDoc(final int pos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_view_document);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        rvDocument = dialog.findViewById(R.id.rvDocument);

        DocumentListAdapter documentListAdapter = new DocumentListAdapter(list.get(pos).getFileName(), list.get(pos).getFilePath());

        rvDocument.setHasFixedSize(true);
        rvDocument.setNestedScrollingEnabled(false);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvDocument.setLayoutManager(horizontalLayoutManagaer);
        rvDocument.setAdapter(documentListAdapter);
        dialog.show();
    }

    private void getEmpDoc() {
        EventBus.getDefault().post(new DocChangeEvent(true));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            //final CharSequence[] options = {"Take Photo"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        File f = new File(Environment.getExternalStorageDirectory() + "/IVACESS/images");
                        if (!f.exists()) {
                            f.mkdirs();
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "IVACESS/images/img_" + System.currentTimeMillis() + ".jpg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);

                    }
                    if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        Intent i = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, PICK_IMAGE_GALLERY);

                        App.checkEmpDocState = true;
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PICK_IMAGE_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
            case PICK_IMAGE_GALLERY: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    selectImage();
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }


    private void dialogAddDoc(final int position) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_emp_doc);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        LinearLayout linearAddDoc = dialog.findViewById(R.id.linearAddDoc);
        //txtFileName = dialog.findViewById(R.id.txtFileName);
        final EditText edtExpiredDate = dialog.findViewById(R.id.edtExpiredDate);
        Button dialogButton = dialog.findViewById(R.id.btnOk);

        edtExpiredDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.generateDatePicker(getContext(), edtExpiredDate);
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expiredDate = edtExpiredDate.getText().toString();
                try {
                    StringBuilder dateFormBuilder = new StringBuilder();
                    StringBuilder dateToBuilder = new StringBuilder();
                    dateFormBuilder = dateFormBuilder.append(expiredDate.substring(6)).append("-").append(expiredDate, 0, 2).append("-").append(expiredDate, 3, 5);

                    expiredDate = String.valueOf(dateFormBuilder);
                    System.out.println("expired Date===" + dateToBuilder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (imgPath.equals("")) {
                        Utils.showToast("Please Select Image", getContext());
                    } else if (expiredDate.equals("")) {
                        Utils.showToast("Please Select Expired Date", getContext());
                    } else {
                        dialog.dismiss();
                        // new CallRequest(instance).addUserDoc(emailId, token, list.get(position).getDOCID(), userId, imgPath, expiredDate);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        linearAddDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!hasPermissions(getActivity(), PERMISSIONS)) {
                        //ActivityCompat.requestPermissions(activity, PERMISSIONS, PICK_IMAGE_CAMERA);
                        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PICK_IMAGE_CAMERA);
                    } else {

                        selectImage();
                        //ivDoc.setImageURI(selectedUri);
                    }
                } else {

                    selectImage();
                    //ivDoc.setImageURI(selectedUri);
                }


            }
        });
        dialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA && resultCode != 0) {
            imgPath = getPath(selectedUri);
        } else if (PICK_IMAGE_GALLERY == 2 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgPath = cursor.getString(columnIndex);

            cursor.close();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {

                case addUserDoc:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Document Add Successfully", getContext());
                            File image = new File(imgPath);
                            image.delete();
                            getEmpDoc();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case deleteUserDoc:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Document Delete Successfully", getContext());
                            getEmpDoc();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case changeUserDocStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getContext());
                            getEmpDoc();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onUploadDocumentClick(int pos) {
        dialogAddDoc(pos);

    }

    @Override
    public void onDeleteDocumentClick(int pos) {
        doDelete(pos);
    }

    @Override
    public void onViewDocumentClick(int pos) {
        dialogViewDoc(pos);
    }

    @Override
    public void onViewExpDocumentClick(int pos) {
        dialogViewExpDoc(pos);
    }

    @Override
    public void onApproveDocumentClick(int pos) {
        new CallRequest(ExpiredDocFragment.this).changeUserDOCStatus(emailId, token, userId, list.get(pos).getDEID(), Constant.APPROVE);
    }

    @Override
    public void onDenyDocumentClick(int pos) {
        new CallRequest(ExpiredDocFragment.this).changeUserDOCStatus(emailId, token, userId, list.get(pos).getDEID(), Constant.DENY);
    }

    public void dialogViewExpDoc(final int pos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_view_document);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        rvDocument = dialog.findViewById(R.id.rvDocument);

        DocumentListAdapter documentListAdapter = new DocumentListAdapter(list.get(pos).getExpiredDocArray(), "");

        rvDocument.setHasFixedSize(true);
        rvDocument.setNestedScrollingEnabled(false);

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        //  holder.rvDocument.setItemAnimator(new DefaultItemAnimator());
        rvDocument.setLayoutManager(horizontalLayoutManagaer);
        rvDocument.setAdapter(documentListAdapter);


        // list.get(pos).getFileName();

        String imagePath = "http://ivaccess.blenzabi.com/" + list.get(pos).getFilePath() /* +list.get(pos).getFileName()*/;


        dialog.show();
    }

    private void doDelete(final int pos) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(ExpiredDocFragment.this).deleteUserDoc(emailId, token, userId, list.get(pos).getDEID());

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();

    }

    public class DocumentListAdapter extends RecyclerView.Adapter<DocumentListAdapter.MyViewHolder> {

        public ArrayList<String> list;
        public Context context;
        public View itemView;
        public String path;

        public DocumentListAdapter(ArrayList<String> List, String path) {
            this.list = List;
            this.path = path;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_dociamge, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            String imageURL = "http://ivaccess.blenzabi.com/" + path + list.get(position);
            Log.i("URL", ":-> " + imageURL);
            if (imageURL != null) {
                Picasso.with(getActivity()).load(imageURL).placeholder(R.drawable.nopic).error(R.drawable.nopic).into(holder.imageDoc);
            }


        }

        @Override
        public int getItemCount() {
            return list.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            public ImageView imageDoc, ivApprove, ivDeny, ivPrint;

            public MyViewHolder(View view) {
                super(view);
                imageDoc = view.findViewById(R.id.ivDoc);
            }

            @Override
            public void onClick(View v) {
                //itemListener.recyclerViewListClicked(v, this.getPosition(), groupPos, childPos);
            }
        }

    }


}



