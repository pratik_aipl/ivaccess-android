package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.model.DocumentsModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddDocumentFragment extends Fragment implements AsynchTaskListner {

    public EditText edtDocumentName, edtExpiryIn;
    public Button btnSave, btnCancel;
    public String documentName, documentExpiryIn, emailId, token, userId, docId;
    public AddDocumentFragment instance;
    public int value;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_document, container, false);
        MainActivity.toolbar_title.setText("Add Documents");
        ((MainActivity)getActivity()).showToolbarAction();
        instance = this;
        linearJobs.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        value = getArguments().getInt("value");
        edtDocumentName = view.findViewById(R.id.edtDocumentName);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnSave = view.findViewById(R.id.btnSave);
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        if (value == 1) {
            MainActivity.toolbar_title.setText("Update Documents");
            DocumentsModel documentsModel = (DocumentsModel) getArguments().getSerializable("DocumentsModel");
            documentName = documentsModel.getDocumentName();
            docId = documentsModel.getDOCID();
            edtDocumentName.setText(documentName);

        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                documentName = edtDocumentName.getText().toString().trim();

                if (TextUtils.isEmpty(documentName)) {
                    Utils.showToast("Please Enter Document Name", getContext());
                }  else {
                    if (value == 0) {
                        new CallRequest(instance).addDocuments(emailId, token, documentName, userId);
                    } else if (value == 1) {
                        new CallRequest(instance).updateDocuments(emailId, token, documentName, userId, docId);
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addDocuments:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Document Add Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateDocument:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Document Update Succesfully", getContext());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
