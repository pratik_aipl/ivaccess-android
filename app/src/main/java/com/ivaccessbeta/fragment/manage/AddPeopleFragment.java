package com.ivaccessbeta.fragment.manage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.model.PeopleModel;
import com.ivaccessbeta.model.QBModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.model.RoleModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MultiSelectionSpinnerFacility;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * Created by User on 26-02-2018.
 */

public class AddPeopleFragment extends Fragment implements AsynchTaskListner, MultiSelectionSpinnerFacility.OnMultipleItemsSelectedListener {
    public EditText edtFirstName, edtLastName, edtPhone, edtPhone2, edtEmail, edtEstimatedPiccSupplies,
            edtEstimatedMidSupplies, edtDirectoryNote, edtPassword, edtCPassword;
    public Spinner spPrimaryRegion, spinnerQbCompany, spRole;
    public Button btnSave, btnCancel;
    public View view;
    public RoleModel roleModel;
    public FacilitesModel facilityModel;
    public RegionModel regionModel;
    public ArrayList<RoleModel> roleList = new ArrayList<>();
    public ArrayList<FacilitesModel> facilityList = new ArrayList<>();
    public ArrayList<RegionModel> regionList = new ArrayList<>();
    public ArrayList<String> roleStringList = new ArrayList<>();
    public ArrayList<String> facillityStringList = new ArrayList<>();
    public ArrayList<String> regionStringList = new ArrayList<>();
    public AddPeopleFragment instance;
    public CheckBox chk_active;
    public List<String> rankMultiArray = new ArrayList<String>();
    public List<Integer> indexList = new ArrayList<>();
    public ArrayList<String> selectedFacility = new ArrayList<>();
    public MultiSelectionSpinnerFacility spinnerFacilites;
    public String firstName, lastName, pone, phone2, emailId, token, emailPeople, userId,
            estimatedPiccSupplies, edtimtedMidSupplies, primaryRegion, qbCompany, role = "", facilites, peopleId, directoryNote, password, cPassword,
            multiSelectFacilityId = "", qb = "", activated = "";
    public int value;
    public int facilityPos = 0, rolePos = 0, regionPos = 0;
    public boolean isSelectedRank = false;
    public TextView txtPassword, txtCPassword;
    public ArrayList<FacilitesModel> selectedFacilityList = new ArrayList<>();
    public ArrayList<String> qbStringList = new ArrayList<>();
    public int qbPos;
    public ArrayList<QBModel> qbList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_people, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        MainActivity.toolbar_title.setText("Add People");
        ((MainActivity) getActivity()).showToolbarAction();
        instance = this;
        value = getArguments().getInt("value");
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        edtFirstName = view.findViewById(R.id.edtFirstName);
        edtLastName = view.findViewById(R.id.edtLastName);
        edtPhone = view.findViewById(R.id.edtPhone);
        edtPhone2 = view.findViewById(R.id.edtPhone2);
        edtEmail = view.findViewById(R.id.edtEmail);
        chk_active = view.findViewById(R.id.chk_active);
        edtDirectoryNote = view.findViewById(R.id.edtDirectoryNote);
        edtEstimatedPiccSupplies = view.findViewById(R.id.edtEstimatedPiccSupplies);
        edtEstimatedMidSupplies = view.findViewById(R.id.edtEstimatedMidSupplies);
        spPrimaryRegion = view.findViewById(R.id.spinnerPrimaryRegion);
        spinnerQbCompany = view.findViewById(R.id.spinnerQbCompany);
        spRole = view.findViewById(R.id.spinnerRole);
        spinnerFacilites = view.findViewById(R.id.spinnerFacilites);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        edtPassword = view.findViewById(R.id.edtPassword);
        edtCPassword = view.findViewById(R.id.edtCPassword);
        txtPassword = view.findViewById(R.id.txtPassword);
        txtCPassword = view.findViewById(R.id.txtCPassword);
        new CallRequest(instance).qbCompany(emailId, token);
        if (value == 1) {
            txtPassword.setVisibility(View.GONE);
            txtCPassword.setVisibility(View.GONE);
            edtPassword.setVisibility(View.GONE);
            edtCPassword.setVisibility(View.GONE);

            MainActivity.toolbar_title.setText("Update People");
            PeopleModel peopleModel = (PeopleModel) getArguments().getSerializable("PeopleModel");
            firstName = peopleModel.getFirst_name();
            lastName = peopleModel.getLast_name();
            pone = peopleModel.getMobileNumber();
            phone2 = peopleModel.getAlternateNumber();
            emailPeople = peopleModel.getEmail();
            estimatedPiccSupplies = peopleModel.getEstimatedPICCSupplies();
            edtimtedMidSupplies = peopleModel.getEstimatedMidSupplies();
            role = peopleModel.getRole_id();
            facilites = peopleModel.getFacilities();
            qbCompany = peopleModel.getQB_Company();
            primaryRegion = peopleModel.getRegionID();
            directoryNote = peopleModel.getDirectoryNote();
            peopleId = peopleModel.getId();
            peopleModel.getFacilityList();
            qb = peopleModel.getQB_Company();
            for (int i = 0; i < peopleModel.getFacilityList().size(); i++) {
                selectedFacilityList.add(new FacilitesModel(peopleModel.getFacilityList().get(i).getFacility_id()));
            }
            multiSelectFacilityId = TextUtils.join(", ", selectedFacilityList);
            edtEmail.setText(emailPeople);
            edtFirstName.setText(firstName);
            edtLastName.setText(lastName);
            edtPhone2.setText(phone2);
            edtPhone.setText(pone);
            edtEstimatedPiccSupplies.setText(estimatedPiccSupplies);
            edtEstimatedMidSupplies.setText(edtimtedMidSupplies);
            edtDirectoryNote.setText(directoryNote);
            edtEstimatedMidSupplies.setClickable(false);
            edtEstimatedPiccSupplies.setClickable(false);
            if (peopleModel.getActivated().equals("1"))
                chk_active.setChecked(true);
            else
                chk_active.setChecked(false);
        }



        getRole();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstName = edtFirstName.getText().toString().trim();
                lastName = edtLastName.getText().toString().trim();
                pone = edtPhone.getText().toString().trim();
                phone2 = edtPhone2.getText().toString().trim();
                emailPeople = edtEmail.getText().toString().trim();
                estimatedPiccSupplies = edtEstimatedPiccSupplies.getText().toString().trim();
                edtimtedMidSupplies = edtEstimatedMidSupplies.getText().toString().trim();
                directoryNote = edtDirectoryNote.getText().toString();
                password = edtPassword.getText().toString();
                cPassword = edtCPassword.getText().toString();
                if (firstName.equals("")) {
                    Utils.showToast("Enter First Name", getActivity());
                } else if (lastName.equals("")) {
                    Utils.showToast("Enter Last Name", getActivity());
                } else if (pone.equals("")) {
                    Utils.showToast("Enter Phone Number", getActivity());
                } else if (emailPeople.equals("")) {
                    Utils.showToast("Enter Email", getActivity());
                } else if (qb.equals("")) {
                    Utils.showToast("Please Select QB Company", getActivity());
                } else if (TextUtils.isEmpty(primaryRegion)) {
                    Utils.showToast("Please Select Region", getContext());
                } else if (TextUtils.isEmpty(role)) {
                    Utils.showToast("Please Select Role", getContext());
                } else if (TextUtils.isEmpty(multiSelectFacilityId)) {
                    Utils.showToast("Please Select Facilities", getContext());
                } else {
                    if (chk_active.isChecked()) {
                        activated = "1";
                    } else {
                        activated = "0";
                    }
                    if (value == 0) {
                        if (edtPassword.getText().toString().length() < 6) {
                            Utils.showAlert("Please enter the password of atlest 6 characters", getActivity());
                        } else if (!cPassword.equals(password)) {
                            Utils.showAlert(" Password dose not match", getActivity());
                        } else {

                            new CallRequest(instance).addPeople(emailId, token, firstName, lastName, primaryRegion, qb, pone, phone2, emailPeople, estimatedPiccSupplies, edtimtedMidSupplies, role, multiSelectFacilityId, userId, directoryNote, password, cPassword, activated);
                        }
                    } else if (value == 1) {

                        new CallRequest(instance).updatePeople(emailId, token, firstName, lastName, primaryRegion, qb, pone, phone2, emailPeople,
                                estimatedPiccSupplies, edtimtedMidSupplies, role, multiSelectFacilityId, userId, peopleId, directoryNote, activated);
                    }
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        spRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getActivity(), view);
                if (roleList.size() > 0 && i != 0)
                    role = roleList.get(i - 1).getRoleID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerQbCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getActivity(), view);
                if (qbList.size() > 0 && i != 0)
                    qb = qbList.get(i - 1).getID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
//        spinnerFacilites.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Utils.disableKeyboard(getActivity(), view);
//                if (facilityList.size() > 0 && i != 0)
//                    facilites = facilityList.get(i - 1).getFacilityID();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        spPrimaryRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Utils.disableKeyboard(getContext(), view);
                if (regionList.size() > 0 && i != 0)
                    primaryRegion = regionList.get(i - 1).getRegionID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    private void getRole() {
        new CallRequest(instance).getRole(emailId, token);
    }

    private void getFacility() {
        new CallRequest(instance).getFacility(emailId, token);
    }

    private void getRegion() {
        new CallRequest(instance).getRegion(emailId, token);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getFacility:
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                facilityModel = LoganSquare.parse(ob.toString(),FacilitesModel.class);
                                facilityList.add(facilityModel);
                                facillityStringList.add(facilityModel.getFacilityName());
                            }

                            spinnerFacilites.setItems(facillityStringList);
                            spinnerFacilites.setSelection(new int[]{0});
                            spinnerFacilites.setListener(this);

                            if (selectedFacilityList != null) {
                                int[] selectionArray = new int[selectedFacilityList.size()];
                                String[] rankSelectedIDsArray = new String[selectedFacilityList.size()];
                                int counter = 0;
                                for (int j = 0; j < facilityList.size(); j++) {
                                    for (FacilitesModel r : selectedFacilityList) {
                                        try {
                                            System.out.println("Selected R.id => " + r.getFacilityID());
                                            System.out.println("Selected App J.id => " + facilityList.get(j).getFacilityID());
                                            if (r.getFacilityID().equalsIgnoreCase(facilityList.get(j).getFacilityID())) {
                                                selectionArray[counter] = j;
                                                rankSelectedIDsArray[counter] = r.getFacilityID();
                                                counter++;
                                            }
                                            try {
                                                multiSelectFacilityId = convertStringArrayToString(rankSelectedIDsArray, ",");
                                                System.out.println(multiSelectFacilityId);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                spinnerFacilites.setSelection(selectionArray);
                                //multiSelectionSpinner.setListener(instance);
                            } else {
                                spinnerFacilites.clearSelection();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("new exception:", String.valueOf(e));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getRegion();

                    break;
                case getRole:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
//
                            for (int i = 0; ob1.length() > i; i++) {
                                RoleModel roleModel =LoganSquare.parse(ob1.getJSONObject(i).toString(),RoleModel.class);
                                roleList.add(roleModel);
                                roleStringList.add(roleList.get(i).getRoleName());
                                if (role != null) {
                                    if (role.equalsIgnoreCase(roleModel.getRoleID())) {
                                        rolePos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, roleStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spRole.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.role_spinner_row_nothing_selected,
                                    getActivity()));
                            spRole.setSelection(rolePos);


                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getFacility();

                    break;
                case getRegion:
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jsonArray = jObj.getJSONArray("data");

//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                regionModel = new RegionModel();
//                                regionModel = (RegionModel) jParser.parseJson(jsonArray.getJSONObject(i), new RegionModel());
//                                regionList.add(regionModel);
//                                regionStringList.add(regionModel.getRegionName());
//                                if (primaryRegion != null) {
//                                    if (primaryRegion.equalsIgnoreCase(regionModel.getRegionID())) {
//                                        regionPos = i;
//                                    }
//                                }
//                            }
//
//                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, regionStringList);
//                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spPrimaryRegion.setAdapter(aa);
//                            spPrimaryRegion.setSelection(regionPos);

                            for (int i = 0; jsonArray.length() > i; i++) {
                                RegionModel roleModel =LoganSquare.parse(jsonArray.getJSONObject(i).toString(),RegionModel.class);
                                regionList.add(roleModel);
                                regionStringList.add(regionList.get(i).getRegionName());
                                if (primaryRegion != null) {
                                    if (primaryRegion.equalsIgnoreCase(roleModel.getRegionID())) {
                                        regionPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, regionStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spPrimaryRegion.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.resign_spinner_row_nothing_selected,
                                    getActivity()));
                            spPrimaryRegion.setSelection(regionPos);


                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

                case addPeople:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("People Add Succesfully", getActivity());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case updatePeople:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("People Update Succesfully", getActivity());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case qbCompanyList:
                    qbStringList.clear();
                    qbList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                QBModel qbModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),QBModel.class);
                                qbList.add(qbModel);
                                qbStringList.add(qbList.get(i).getName());
                                if (qb != null) {
                                    if (qb.equalsIgnoreCase(qbModel.getID())) {
                                        qbPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, qbStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerQbCompany.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.qb_spinner_row_nothing_selected,
                                    getActivity()));
                            spinnerQbCompany.setSelection(qbPos);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


    public static String convertStringArrayToString(String[] strArr, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr)
            sb.append(str).append(delimiter);
        return sb.substring(0, sb.length() - 1);
    }

    @Override
    public void selectedIndices(List<Integer> indices) {

        multiSelectFacilityId = "";
        System.out.println("size" + indexList.size());

        ArrayList<String> rankIDArray = new ArrayList<>();
        for (Integer i : indices) {
            rankIDArray.add(facilityList.get(i).getFacilityID());
        }
        multiSelectFacilityId = android.text.TextUtils.join(",", rankIDArray);

        System.out.println("multiSelectFacilityId  : " + multiSelectFacilityId);

    }

    @Override
    public void selectedStrings(List<String> strings) {
        for (int j = 0; j < strings.size(); j++) {
            System.out.println("item:::" + strings.get(j));
        }
    }
}
