package com.ivaccessbeta.chat;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.model.ChatDashboardModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment implements AsynchTaskListner {

    public RecyclerView rvComments;
    public ChatAdapter commentsAdapter;
    public EditText etComment;
    public ImageView ivSend;
    public LinearLayoutManager linearLayoutManager;
    public ImageView ivBack;
    public TextView txtName;
    public static ChatFragment instance;
    public ArrayList<ChatModel> chatList = new ArrayList<>();
    public String fromId, toId;
    public View view;
    public String emailId, token, userId, roleId, userName;
    public ChatDashboardModel chatDashboardModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        instance = this;
        setHasOptionsMenu(true);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        userName = MySharedPref.getString(getActivity(), App.NAME, "");
        rvComments = view.findViewById(R.id.rvComments);
        etComment = view.findViewById(R.id.etComment);
        ivSend = view.findViewById(R.id.ivSend);
        txtName = view.findViewById(R.id.txtName);
        if (TextUtils.isEmpty(App.threadID)) {
            chatDashboardModel = (ChatDashboardModel) getArguments().getSerializable("model");
            MainActivity.toolbar_title.setText(chatDashboardModel.getChatName());
            ((MainActivity) getActivity()).showToolbarAction();
            App.threadID = chatDashboardModel.getThreadID();
        }
        getCommentsList();
        final Handler refreshHandler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refreshHandler.postDelayed(this, 5 * 1000);
            }
        };
        refreshHandler.postDelayed(runnable, 5 * 1000);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvComments.setLayoutManager(linearLayoutManager);

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = etComment.getText().toString().trim();
                if (TextUtils.isEmpty(comment)) {
                    return;
                }
                addComment(comment);
            }
        });
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);
                switch (request) {

                    case getChatList:
                        chatList.clear();
                        Utils.hideProgressDialog();
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getString("status").equals("200")) {
                                MainActivity.toolbar_title.setText(jObj.getString("ChatTitle"));
                                JSONArray array = jObj.getJSONArray("data");
                                chatList.addAll(LoganSquare.parseList(array.toString(), ChatModel.class));
                                commentsAdapter = new ChatAdapter(chatList, getContext());
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                rvComments.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                                rvComments.setLayoutManager(mLayoutManager);
                                rvComments.setItemAnimator(new DefaultItemAnimator());
                                rvComments.setAdapter(commentsAdapter);
                                commentsAdapter.notifyItemRangeChanged(0, commentsAdapter.getItemCount());
                                rvComments.smoothScrollToPosition(chatList.size() + 1);
                            } else {
                                Utils.showToast(jObj.getString("message"), getContext());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        break;
                    case replyMessage:
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getString("status").equals("200")) {

                                etComment.setText("");
                                getCommentsList();

                            } else {
                                Utils.showToast(jObj.getString("message"), getContext());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        Utils.hideProgressDialog();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addComment(String comment) {
        new CallRequest(instance).replayMessage(emailId, token, userId, App.threadID, comment);
    }

    private void getCommentsList() {
        new CallRequest(instance).getChat(emailId, token, userId, App.threadID);
    }
}
