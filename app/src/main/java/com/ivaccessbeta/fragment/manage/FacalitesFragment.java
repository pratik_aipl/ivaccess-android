package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.FacilitesAdapter;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class FacalitesFragment extends Fragment implements AsynchTaskListner {
    public RecyclerView rvPeople;
    public FacilitesAdapter mAdapter;
    public ArrayList<FacilitesModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static FacalitesFragment instance;
    public EditText edtSearch;
    public String emailId, token;
    public FacilitesModel facilitesModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_facalites, container, false);
        rvPeople = view.findViewById(R.id.rvFacilites);
        instance = this;
        linearJobs.setVisibility(View.GONE);

        relativeAdd = view.findViewById(R.id.relative_add);
        edtSearch = view.findViewById(R.id.edt_search);

        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");

//        emailId = App.uEmail;
//        token = App.uToken;
        setHasOptionsMenu(true);

        relativeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddFacilityFragment addFacilityFragment=new AddFacilityFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("value",0);
                addFacilityFragment.setArguments(bundle);
                ((MainActivity)getActivity()).changeFragment(addFacilityFragment, true);
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }
            }
        });
        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getFacilityDetail();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

     //   menu.getItem(1).setVisible(false);
    }

    private void getFacilityDetail() {
        list.clear();
        new CallRequest(instance).getFacility(emailId, token);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getFacility:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                facilitesModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),FacilitesModel.class);
                                list.add(facilitesModel);
                            }
                            mAdapter = new FacilitesAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvPeople.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvPeople.setLayoutManager(mLayoutManager);
                            rvPeople.setItemAnimator(new DefaultItemAnimator());
                            rvPeople.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteFacility:
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Facility Delete Successfully", getContext());
                            getFacilityDetail();

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}