package com.ivaccessbeta.listners;

/**
 * Created by User on 24-04-2018.
 */

public interface PushDividerListener {
    void onSetNextPushDivider(int pos, String date);
    void onSetPreviousPushDivider(int pos, String date);
}
