package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.DocumentsAdapter;
import com.ivaccessbeta.model.DocumentsModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;


public class DocumentsFragment extends Fragment implements AsynchTaskListner {

    public View view;
    public RecyclerView rvDocuments;
    public DocumentsAdapter mAdapter;
    public ArrayList<DocumentsModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static DocumentsFragment instance;
    public EditText edtSearch;
    public String emailId, token;
    public DocumentsModel documentsModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_documents, container, false);
        setHasOptionsMenu(true);
        instance = this;
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");

//        emailId = App.uEmail;
//        token = App.uToken;
        rvDocuments = view.findViewById(R.id.rvDocuments);
        relativeAdd = view.findViewById(R.id.relative_add);

        relativeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddDocumentFragment addDocumentFragment = new AddDocumentFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                addDocumentFragment.setArguments(bundle);
                ((MainActivity)getActivity()).changeFragment(addDocumentFragment, true);

            }
        });

        edtSearch = view.findViewById(R.id.edt_search);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }

            }
        });
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getDocumentDetail();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

    }

    private void getDocumentDetail() {

        list.clear();
        new CallRequest(instance).getDocuments(emailId, token);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getDocument:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            list.addAll(LoganSquare.parseList(jObject.toString(), DocumentsModel.class));
                            mAdapter = new DocumentsAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvDocuments.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvDocuments.setLayoutManager(mLayoutManager);
                            rvDocuments.setItemAnimator(new DefaultItemAnimator());
                            rvDocuments.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteDocument:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Document Delete Successfully", getContext());
                            getDocumentDetail();

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
