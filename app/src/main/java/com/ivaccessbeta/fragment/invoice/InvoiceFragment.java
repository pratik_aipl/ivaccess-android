package com.ivaccessbeta.fragment.invoice;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.InvoiceCustomExpandableListAdapter;
import com.ivaccessbeta.adapter.InvoiceSelectContractorAdapter;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.listners.InvoiceListener;
import com.ivaccessbeta.model.HomeModel;
import com.ivaccessbeta.model.InvoiceChildModel;
import com.ivaccessbeta.model.InvoiceContractorModel;
import com.ivaccessbeta.model.InvoiceGroupModel;
import com.ivaccessbeta.model.InvoiceNoteModel;
import com.ivaccessbeta.model.InvoicePaymentNoteModel;
import com.ivaccessbeta.model.JobDetailsModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvoiceFragment extends Fragment implements InvoiceListener, AsynchTaskListner {

    String InvoiceNo;
    public ExpandableListView expandableListView;
    public ArrayList<InvoiceNoteModel> noteList = new ArrayList<>();
    public ArrayList<InvoicePaymentNoteModel> paymentNoteList = new ArrayList<>();
    public ArrayList<InvoiceGroupModel> expandableListGroup = new ArrayList<>();
    public ArrayList<InvoiceChildModel> expandableListChild = new ArrayList<>();
    public ArrayList<InvoiceChildModel> expandableListChild2 = new ArrayList<>();
    public InvoiceFragment instance;
    public TextView txtPeriod, txtCustomer, txtInvoice;
    public InvoiceSelectContractorAdapter contractorAdapter;
    public ArrayList<String> periodList = new ArrayList<>();
    public String emailId, token, userId, roleId, jobNo, jobID, period = "all";
    public ArrayList<HomeModel> list = new ArrayList<>();
    public ArrayList<String> jobNoStringList = new ArrayList<>();
    public InvoiceChildModel groupModel2;
    public InvoiceContractorModel contractorModel;
    public ArrayList<InvoiceContractorModel> contractorFilterList = new ArrayList<>();
    public ArrayList<String> contractorStringList = new ArrayList<>();
    public TextView txtService, txtFacility, txtAmount, txtNote;
    public JobDetailsModel jobDetailsModel;
    public ArrayAdapter aaJob;
    public EditText edtSearch;
    public InvoiceCustomExpandableListAdapter customListAdapter;
    public Spinner spJob;
    public ArrayAdapter<String> adptr;
    public ArrayList<String> invoiceTIme = new ArrayList<>();

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view;
        view = inflater.inflate(R.layout.fragment_invoice, container, false);
        instance = this;

        InvoiceNo= getArguments().getString(Constant.InvoiceNo);

        MainActivity.toolbar_title.setText("INVOICE");
        ((MainActivity) getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        expandableListView = view.findViewById(R.id.expandableListView);
        txtPeriod = view.findViewById(R.id.txtPeriod);
        txtCustomer = view.findViewById(R.id.txtCustomer);
        txtInvoice = view.findViewById(R.id.txtInvoice);
        edtSearch = view.findViewById(R.id.edt_search);
        txtCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selcectContractor();
            }
        });
        txtPeriod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPeriod();
            }
        });
        txtInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //addJobToInvoice();
                if (!TextUtils.isEmpty(App.invoiceContractorId)) {
                    new CallRequest(instance).addInvoiceToQuickBook(emailId, token, App.invoiceContractorId);
                } else {
                    Toast.makeText(getActivity(), "Please select period or customer.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                    customListAdapter.filters(text);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //   Utils.disableKeyboard(getContext(), view);
            }
        });
        return view;
    }

    private void getJobList(int pos) {
        new CallRequest(instance).getJobListNo(emailId, token, userId, expandableListGroup.get(pos).getBillToID());
    }

    private void getInvoiceContractor() {
        new CallRequest(instance).getInvoiceContractor(emailId, token, period);
    }

    private void addJobToInvoice(final int pos) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_job_invoice);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getJobList(pos);

        spJob = dialog.findViewById(R.id.spJob);
        txtService = dialog.findViewById(R.id.txtServiceName);
        txtFacility = dialog.findViewById(R.id.txtFacilityName);
        txtAmount = dialog.findViewById(R.id.txtAmount);
        txtNote = dialog.findViewById(R.id.txtNote);
        Button btnOk = dialog.findViewById(R.id.btnOk);


        spJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jobNo = list.get(position).getJobNo();
                jobID = list.get(position).getJobID();
                getSelectedJobDetail();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnOk.setOnClickListener(v -> {
            dialog.dismiss();
            new CallRequest(instance).addJobToInvoice(emailId, token, jobNo, expandableListGroup.get(pos).getInvoiceNo(), expandableListGroup.get(pos).getInvoiceDate(), userId);
        });
        dialog.show();
    }

    private void getSelectedJobDetail() {

        new CallRequest(instance).getJobDetail(emailId, token, jobID);
    }

    private void selectPeriod() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_contractor_filter_qeuipmet);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final ListView rvSelectContractor = dialog.findViewById(R.id.rv_contractor);
        TextView txtDialogTitle = dialog.findViewById(R.id.txtDialogTitle);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnClear = dialog.findViewById(R.id.btnClear);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInvoiceContractor();
                dialog.dismiss();
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                period = "all";
                getInvoiceContractor();
                dialog.dismiss();
            }
        });
        txtDialogTitle.setText("INVOICE THROUGH");
        invoiceTIme.clear();
        invoiceTIme.add("All Time");
        invoiceTIme.add("Last Week");
        invoiceTIme.add("Last Month");
        adptr = new ArrayAdapter<String>(getContext(), R.layout.layout_checkbox, invoiceTIme);
        rvSelectContractor.setAdapter(adptr);

        rvSelectContractor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    period = "all";
                }
                if (position == 1) {
                    period = "last_week";
                }
                if (position == 2) {
                    period = "last_month";
                }
            }
        });
        if (period.equalsIgnoreCase("all")) {
            rvSelectContractor.setItemChecked(0, true);
        } else if (period.equalsIgnoreCase("last_week")) {
            rvSelectContractor.setItemChecked(1, true);
        } else if (period.equalsIgnoreCase("last_month")) {
            rvSelectContractor.setItemChecked(2, true);
        }

        dialog.show();
    }

    private void selcectContractor() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_contractor_filter_invoice);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        RecyclerView rvSelectContractor = dialog.findViewById(R.id.rv_contractor);
        TextView txtDialogTitle = dialog.findViewById(R.id.txtDialogTitle);
        final CheckBox cbSelectAll = dialog.findViewById(R.id.cbSelectAll);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnClear = dialog.findViewById(R.id.btnClear);
        txtDialogTitle.setText("CUSTOMERS WITH JOBS TO INVOICE");

        contractorAdapter = new InvoiceSelectContractorAdapter(getActivity(),contractorFilterList, cbSelectAll);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvSelectContractor.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvSelectContractor.setItemAnimator(new DefaultItemAnimator());
        rvSelectContractor.setLayoutManager(mLayoutManager);
        rvSelectContractor.setAdapter(contractorAdapter);
        contractorAdapter.notifyDataSetChanged();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                App.invoiceContractorId = android.text.TextUtils.join(",", App.indexInvoiceContractorList);
                dialog.dismiss();
                txtInvoice.setText("Select Invoice (" + App.indexInvoiceContractorList.size() + ")");
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.invoiceContractorId = "";
                getInvoiceList();
                dialog.dismiss();
                txtInvoice.setText("Select Invoice");
                App.checkedArrayInvoice.clear();
                cbSelectAll.setChecked(false);
                App.indexInvoiceContractorList.clear();
            }
        });
        cbSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.indexInvoiceContractorList.clear();
                boolean isSelectAll = ((CheckBox) v).isChecked();
                if (isSelectAll) {
                    for (InvoiceContractorModel n : contractorFilterList)
                        App.indexInvoiceContractorList.add(n.getBillToID());

                } else {
                    App.indexInvoiceContractorList.clear();
                }
                contractorAdapter.selectAll(isSelectAll);
            }

        });


        dialog.show();
        // contractorAdapter.notifyDataSetChanged();
//
//
//        rvJobStatus.addOnItemTouchListener(new RecyclerTouchListener(getContext(), rvJobStatus, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                String statusId = jobStatusList.get(position).getJSID();
//
//                new CallRequest(instance).updateJobStatus(emailId, token, userId, jobNo, statusId);
//                popup.dismiss();
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }

    @Override
    public void onDeletePerticularInvoice(final int groupPos, final int pos, View v) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deletePerticularInvoice(emailId, token, expandableListGroup.get(groupPos).getChildList().get(pos).getInvoiceID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    @Override
    public void onCheckInvoice(int groupPos, int pos, View v, String IsCheckMark) {
        new CallRequest(instance).invoice_check_mark(emailId, token, expandableListGroup.get(groupPos).getChildList().get(pos).getInvoiceID(), IsCheckMark);

    }

    @Override
    public void onEditInvoice(int groupPos, int pos, View v) {
        showEditInvoicePopup(groupPos, pos);
    }

    @Override
    public void onOpenActionDialog(int groupPos, View v) {
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        showActionDialog(groupPos, x, y);
    }

    @Override
    public void onAddInvoiceMemo(int groupPos, int pos, View v, String note, String amount) {
        String billToId = null, invoiceNo = null, date = null;
        billToId = expandableListGroup.get(groupPos).getBillToID();
        invoiceNo = expandableListGroup.get(groupPos).getInvoiceNo();
        date = expandableListGroup.get(groupPos).getInvoiceDate();
        new CallRequest(instance).addCustomMemoInvoice(emailId, token, amount, billToId, invoiceNo, date, note, userId);
    }

    @Override
    public void onDeleteNote(final int groupPos, final int pos, View v) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteInvoiceNote(emailId, token, expandableListGroup.get(groupPos).getNoteList().get(pos).getInvoiceNoteID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    @Override
    public void onDeletePaymentNote(final int groupPos, final int pos, View v) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteInvoicePaymentNote(emailId, token, expandableListGroup.get(groupPos).getNotePaymentList().get(pos).getInvoicePaymentNoteID());

                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();

    }

    @Override
    public void onJobNoClick(int groupPos, int pos, View v) {
        String jobId = expandableListGroup.get(groupPos).getChildList().get(pos).getJobID();
        System.out.println("is going in job detail =====");
        JobDetailFragment fragment = new JobDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jobNo", jobId);
        fragment.setArguments(bundle);
        ((MainActivity)getActivity()).changeFragment(fragment, true);

    }

    private void showActionDialog(final int groupPos, int x, int y) {

        RelativeLayout viewGroup = getActivity().findViewById(R.id.rlativeTop);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.dialog_invoice_action, viewGroup);
        final PopupWindow popupWindow = new PopupWindow(getActivity());
        popupWindow.setContentView(layout);
        popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
        TextView txtAddJob = layout.findViewById(R.id.txtAddJob);
        TextView txtAddNote = layout.findViewById(R.id.txtAddNote);
        TextView txtRecreateInvoice = layout.findViewById(R.id.txtRecreateInvoice);
        TextView txtDownloadInvoice = layout.findViewById(R.id.txtDownloadInvoice);
        TextView txtPrint = layout.findViewById(R.id.txtPrint);
        TextView txtDeleteInvoice = layout.findViewById(R.id.txtDeleteInvoice);
        TextView txtAddPaymentNote = layout.findViewById(R.id.txtAddPaymentNote);
        if (expandableListGroup.get(groupPos).getPdf_url().equalsIgnoreCase("")) {
            txtRecreateInvoice.setClickable(false);
            txtPrint.setClickable(false);
            txtDownloadInvoice.setClickable(false);
        }
        txtRecreateInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                new CallRequest(instance).reCreateInvoice(emailId, token, userId, expandableListGroup.get(groupPos).getInvoiceNo());
            }
        });
        txtDownloadInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (!TextUtils.isEmpty(expandableListGroup.get(groupPos).getPdf_url())) {
                    Bundle bundle = new Bundle();
                    DownloadInvoiceFragment downlodInvoiceFragment = new DownloadInvoiceFragment();
                    bundle.putString("url", expandableListGroup.get(groupPos).getPdf_url());
                    downlodInvoiceFragment.setArguments(bundle);
                    ((MainActivity)getActivity()).changeFragment(downlodInvoiceFragment, true);
                }
            }
        });
        txtPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (!TextUtils.isEmpty(expandableListGroup.get(groupPos).getPdf_url())) {
                    Bundle bundle = new Bundle();
                    DownloadInvoiceFragment downloadInvoiceFragment = new DownloadInvoiceFragment();
                    bundle.putString("url", expandableListGroup.get(groupPos).getPdf_url());
                    downloadInvoiceFragment.setArguments(bundle);
                    ((MainActivity)getActivity()).changeFragment(downloadInvoiceFragment, true);
                }
            }
        });
        txtAddPaymentNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                addPaymentNote(groupPos);
            }
        });
        txtAddJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                addJobToInvoice(groupPos);
            }
        });
        txtAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNoteToInvoice(groupPos);
                popupWindow.dismiss();
            }
        });
        txtDeleteInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

                final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .content("Are you sure you want to Delete?")
                        .positiveText("Ok")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                new CallRequest(instance).deleteAllInvoice(emailId, token, expandableListGroup.get(groupPos).getInvoiceNo());

                            }
                        })
                        .negativeText("Cancel")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .build();
                dialog.show();
            }
        });

    }

    private void addPaymentNote(final int groupPos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_payment_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txtAddTitle = dialog.findViewById(R.id.txtAddTitle);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        final EditText edtRef = dialog.findViewById(R.id.edtRef);
        final EditText edtAmount = dialog.findViewById(R.id.edtAmount);

        Button btnOk = dialog.findViewById(R.id.btnOk);
        txtAddTitle.setText(" Add Note  " + expandableListGroup.get(groupPos).getInvoiceNo());
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString();
                String amount = edtAmount.getText().toString();
                String refNo = edtRef.getText().toString();

                if (TextUtils.isEmpty(note)) {
                    Utils.showToast("Please Enter Note", getContext());
                } else if (TextUtils.isEmpty(amount)) {
                    Utils.showToast("Please Enter Amount", getContext());
                } else if (TextUtils.isEmpty(refNo)) {
                    Utils.showToast("Please Enter Ref No", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addPaymentNoteToInvoice(emailId, token, expandableListGroup.get(groupPos).InvoiceNo, note, refNo, amount, userId);
                }
            }
        });
        dialog.show();
    }

    private void addNoteToInvoice(final int groupPos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txtAddTitle = dialog.findViewById(R.id.txtAddTitle);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        txtAddTitle.setText(" Add Note  " + expandableListGroup.get(groupPos).getInvoiceNo());
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString();
                if (TextUtils.isEmpty(note)) {
                    Utils.showToast("Please Enter Note", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addNoteToInvoice(emailId, token, expandableListGroup.get(groupPos).InvoiceNo, note, userId);
                }
            }
        });
        dialog.show();

    }


    private void showEditInvoicePopup(int groupPos, int pos) {
        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_job_invoice);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txtDialogTitle = dialog.findViewById(R.id.txtDialogTitle);
        txtDialogTitle.setText("Edit Invoice" + expandableListGroup.get(groupPos).getInvoiceNo());
        dialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getInvoiceContractor();

    }

    public void getInvoiceList() {

        try {
            new CallRequest(instance).getInvoiceList(emailId, token);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getInvoiceList:
                    expandableListGroup.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            InvoiceGroupModel groupModel = null;
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject ob = jObject.getJSONObject(i);
                                groupModel = new InvoiceGroupModel();
//                                groupModel = (InvoiceGroupModel) jParsar.parseJson(ob, new InvoiceGroupModel());
                                groupModel = LoganSquare.parse(ob.toString(),InvoiceGroupModel.class); //jParsar.parseJson(ob, new InvoiceGroupModel());

                                JSONArray noteArray = ob.getJSONArray("Notes");
                                groupModel.NoteList.addAll(LoganSquare.parseList(noteArray.toString(),InvoiceNoteModel.class));
//                                for (int j = 0; j < noteArray.length(); j++) {
//                                    InvoiceNoteModel invoiceNoteModel = new InvoiceNoteModel();
//                                    invoiceNoteModel = (InvoiceNoteModel) jParsar.parseJson(noteArray.getJSONObject(j), new InvoiceNoteModel());
//                                    groupModel.NoteList.add(invoiceNoteModel);
//                                }
                                JSONArray notePaymentArray = ob.getJSONArray("PaymentNotes");
                                groupModel.NotePaymentList.addAll(LoganSquare.parseList(notePaymentArray.toString(),InvoicePaymentNoteModel.class));
//                                for (int j = 0; j < notePaymentArray.length(); j++) {
//                                    InvoicePaymentNoteModel invoicePaymentNoteModel = new InvoicePaymentNoteModel();
//                                    invoicePaymentNoteModel = (InvoicePaymentNoteModel) jParsar.parseJson(notePaymentArray.getJSONObject(j), new InvoicePaymentNoteModel());
//                                    groupModel.NotePaymentList.add(invoicePaymentNoteModel);
//                                }

                                JSONArray childArray = ob.getJSONArray("invoice");
                                for (int j = 0; childArray.length() > j; j++) {
                                    groupModel2 = new InvoiceChildModel();
//                                    groupModel2 = (InvoiceChildModel) jParsar.parseJson(childArray.getJSONObject(j), new InvoiceChildModel());
                                    groupModel2 = LoganSquare.parse(childArray.getJSONObject(j).toString(),InvoiceChildModel.class);
                                    groupModel.childList.add(groupModel2);
                                    expandableListChild.add(groupModel2);
                                }
                                expandableListGroup.add(groupModel);
                            }
                            customListAdapter = new InvoiceCustomExpandableListAdapter(getActivity(), expandableListGroup, instance);
                            expandableListView.setAdapter(customListAdapter);

                            for (int i = 0; i < expandableListGroup.size(); i++) {
                                if (!TextUtils.isEmpty(InvoiceNo)){
                                    if (InvoiceNo.equalsIgnoreCase(expandableListGroup.get(i).getInvoiceNo())){
                                        expandableListView.expandGroup(i);
                                        expandableListView.smoothScrollToPosition(i);
                                        break;
                                    }
                                }else
                                    break;
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case deleteAllInvoice:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case deleteInvoice:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case invoice_check_mark:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case getInvoiceContractorList:
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            contractorFilterList.clear();
                            contractorStringList.clear();
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                contractorModel = new InvoiceContractorModel();
//                                contractorModel = (InvoiceContractorModel) jParsar.parseJson(ob, new InvoiceContractorModel());
                                contractorModel = LoganSquare.parse(ob.toString(),InvoiceContractorModel.class);// jParsar.parseJson(ob, new InvoiceContractorModel());
                                contractorFilterList.add(contractorModel);
                                contractorStringList.add(contractorModel.getName());
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getInvoiceList();
                    //  Utils.hideProgressDialog();
                    break;
                case addCustomMemoInvioce:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getJobList:
                    list.clear();
                    jobNoStringList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                HomeModel homeModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),HomeModel.class);
//                                homeModel = (HomeModel) jParsar.parseJson(jObject.getJSONObject(i), new HomeModel());
                                list.add(homeModel);
                                jobNoStringList.add(homeModel.getJobNo());
                            }
                            aaJob = new ArrayAdapter(getContext(), R.layout.spinner_text, jobNoStringList);
                            aaJob.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spJob.setAdapter(aaJob);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // getInvoiceList();
                    Utils.hideProgressDialog();
                    break;
                case getJobDetail:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {

                            JSONObject jObject = jObj.getJSONObject("data");
                            jobDetailsModel = new JobDetailsModel();
//                            jobDetailsModel = (JobDetailsModel) jParsar.parseJson(jObject, new JobDetailsModel());
                            jobDetailsModel = LoganSquare.parse(jObject.toString(),JobDetailsModel.class);// jParsar.parseJson(jObject, new JobDetailsModel());
                            txtService.setText(jobDetailsModel.getServiceName());
                            txtFacility.setText(jobDetailsModel.getFacilityName());
                            txtNote.setText(jobDetailsModel.getNotes());

                            if (jobDetailsModel.getCustomCheckAmount().equalsIgnoreCase("0.00")) {
                                txtAmount.setText(jobDetailsModel.getCheckRate());
                            } else {
                                txtAmount.setText(jobDetailsModel.getCustomCheckAmount());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addJobToInvoice:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addNoteToInvoice:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addPaymentNoteToInvoice:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteInvoiceNote:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteInvoicePaymentNote:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addInvoiceToQuickBook:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case createInvoice:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getInvoiceList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;


                case downlodeInvoice:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
//                            Utils.showToast(jObj.getString("message"), getActivity());
//                            getInvoiceList();
//                            UserDetailModel userDetailModel;
//                            userDetailModel = new UserDetailModel();
//                            userDetailModel = (UserDetailModel) jParser.parseJson(jObj, new UserDetailModel());
//                            String resumeUrl=userDetailModel.getResume_url();
                            //  Log.d("resume::",resumeUrl);

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

            }
        }
    }
}
