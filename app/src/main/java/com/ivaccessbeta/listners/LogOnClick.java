package com.ivaccessbeta.listners;

import com.ivaccessbeta.model.LogHistoryModel;

public interface LogOnClick {
    void callLogClick(int pos, LogHistoryModel historyModel);
}
