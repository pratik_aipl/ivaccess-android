package com.ivaccessbeta.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.BuildConfig;
import com.ivaccessbeta.R;
import com.ivaccessbeta.model.AdminDetailModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity implements AsynchTaskListner {
    private static final String TAG = "LoginActivity";
    public Button btnLogin;
    public LoginActivity instance;
    public TextView txtFPassword, txtVersion;
    public EditText edtEmail, edtPassword;
    public String emailId, password;
    public AdminDetailModel detailModel;
    public String playerID = "";
    public int intent;
    public SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        instance = this;
        btnLogin = findViewById(R.id.btn_login);
        txtFPassword = findViewById(R.id.txt_fpassword);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        txtVersion = findViewById(R.id.txtVersion);
        shared = getSharedPreferences(getApplication().getPackageName(), 0);
        Bundle bundle = getIntent().getExtras();
        intent = bundle.getInt("intent");

        OneSignal.idsAvailable((userId, registrationId) -> {
            if (userId != null)
                playerID = userId;
        });

        txtFPassword.setOnClickListener(view -> {
            Intent intent = new Intent(instance, ForgotPasswordActivity.class);
            startActivity(intent);
        });

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            txtVersion.setText("Version " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (intent == 0) {
            password = MySharedPref.getString(getApplicationContext(), App.PASSWORD, "");
            edtEmail.setText(MySharedPref.getString(getApplicationContext(), App.EMAIL, ""));
            edtPassword.setText(password);
            new CallRequest(instance).login(edtEmail.getText().toString(), password, playerID);
        } else {
            edtEmail.setText("");
            edtPassword.setText("");
        }
        /*
            Super Admin
            superadmin@iv_access.com
            super_1243

            Admin
            devid@blenzabi.com
            123456

            Manager
            scott@blenzabi.com
            123456

            Contractor
            jony@blenzabi.com
            123456
         */
        if (BuildConfig.DEBUG) {
            edtEmail.setText("superadmin@iv_access.com");
            edtPassword.setText("super_1243");
//            edtEmail.setText("jony@blenzabi.com");
//            edtPassword.setText("123456");
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailId = edtEmail.getText().toString();
                password = edtPassword.getText().toString();
                if (emailId.equals("")) {
                    edtEmail.requestFocus();
                    Utils.showAlert("Please Enter Email", instance);
                } else if (password.isEmpty()) {
                    Utils.hideProgressDialog();
                    edtPassword.requestFocus();
                    Utils.showAlert("Please Enter Password", instance);
                } else {
                    new CallRequest(instance).login(emailId, password, playerID);
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case login:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONObject jObject = jObj.getJSONObject("data");
                            detailModel = LoganSquare.parse(jObject.toString(), AdminDetailModel.class);

                            String uEmail = detailModel.getEmail();
                            String uToken = detailModel.getApp_token();
                            String uID = detailModel.getId();
                            String uRoleId = detailModel.getRole_id();
                            String uRegionId = detailModel.getRegionID();
                            String uName = detailModel.getFirst_name() + " " + detailModel.getLast_name();
                            String uPasswordStatus = detailModel.getPassword_change();
                            MySharedPref.setString(instance, App.EMAIL, uEmail);
                            MySharedPref.setString(instance, App.APP_TOKEN, uToken);
                            MySharedPref.setString(instance, App.LOGGED_USER_ID, uID);
                            MySharedPref.setString(instance, App.PASSWORD, password);
                            MySharedPref.setString(instance, App.ROLEID, uRoleId);
                            MySharedPref.setString(instance, App.NAME, uName);
                            MySharedPref.setString(instance, App.REGION_ID, uRegionId);
                            MySharedPref.setString(instance, App.IS_PASSWORD_CHANGE, uPasswordStatus);
                            MySharedPref.setisLogin(true);
                            App.password = password;

                            Log.d(TAG, "onTaskCompleted: " + uPasswordStatus);
                            if (uPasswordStatus.equalsIgnoreCase(Constant.PASSWORD_NOT_CHANGE)) {
                                Intent intent = new Intent(instance, ChangePasswordActivity.class);
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(instance, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, instance);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
