package com.ivaccessbeta.utils;

public class DocChangeEvent {
    boolean isCall;

    public DocChangeEvent(boolean isCall) {
        this.isCall = isCall;
    }

    public boolean isCall() {
        return isCall;
    }
}
