package com.ivaccessbeta.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ivaccessbeta.fragment.manage.BillToFragment;
import com.ivaccessbeta.fragment.manage.DocumentsFragment;
import com.ivaccessbeta.fragment.manage.EquipmentFragment;
import com.ivaccessbeta.fragment.manage.FacalitesFragment;
import com.ivaccessbeta.fragment.manage.PeoplesFragment;
import com.ivaccessbeta.fragment.manage.PlaceFragment;
import com.ivaccessbeta.fragment.manage.RegionFragment;
import com.ivaccessbeta.fragment.manage.ServiceTypeFragment;

public class ManageTabsPagerAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;

    public ManageTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RegionFragment tab1 = new RegionFragment();
                return tab1;
            case 1:
                PlaceFragment tab2 = new PlaceFragment();
                return tab2;
            case 2:
                PeoplesFragment tab3 = new PeoplesFragment();
                return tab3;
            case 3:
                FacalitesFragment tab4 = new FacalitesFragment();
                return tab4;
            case 4:
                BillToFragment tab5 = new BillToFragment();
                return tab5;
            case 5:
                ServiceTypeFragment tab6 = new ServiceTypeFragment();
                return tab6;
            case 6:
                EquipmentFragment tab7 = new EquipmentFragment();
                return tab7;
            case 7:
                DocumentsFragment tab8 = new DocumentsFragment();
                return tab8;
            default:
                return null;
        }
    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 8;
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
