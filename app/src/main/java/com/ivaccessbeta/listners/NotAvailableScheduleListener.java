package com.ivaccessbeta.listners;

/**
 * Created by User on 19-04-2018.
 */

public interface NotAvailableScheduleListener  {

    void onDeleteNotAvailableSchedule(int pos);
    void onApproveNotAvailableSchedule(int pos);
    void onDenyNotAvailableSchedule(int pos);
    void onCopyToTomorrowNotAvailableSchedule(int pos);
}
