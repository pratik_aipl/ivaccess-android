package com.ivaccessbeta.listners;

import android.view.View;

import com.ivaccessbeta.model.CheckGroupModel;

/**
 * Created by User on 13-04-2018.
 */

public interface CheckListener {

    void onOpenPopupListClick(int pos, View v, CheckGroupModel checkGroupModel);
    void onEditJobChildListClick(int groupPos, int pos, View v);
    void  onDeletePerticularChildClick(int groupPos, int pos, View v);
    void  onAddMemoChildClick(int groupPos, int pos, View v, String note, String amount);
    void onDeleteNote(int groupPos, int pos, View v);
    void  onDeletePerticularInvoice(int groupPos, int pos, View v);
    void onDeletePaymentNote(int groupPos, int pos, View v);

    void onJobNoClick(int groupPos, int childPos, View v);
}
