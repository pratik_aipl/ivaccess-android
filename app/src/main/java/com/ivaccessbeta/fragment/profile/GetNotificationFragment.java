package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.GetNotificationAdapter;
import com.ivaccessbeta.listners.NotificationInterface;
import com.ivaccessbeta.model.GetNotificationModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.fragment.flotingNotification.services.ChatHeadService.iconView;

public class GetNotificationFragment extends Fragment implements AsynchTaskListner, NotificationInterface {

    public RecyclerView rvNotification;
    public ArrayList<GetNotificationModel> notificationList = new ArrayList();
    public GetNotificationFragment instance;
    public String emailId, token, userId;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view;
        view = inflater.inflate(R.layout.fragment_get_notification, container, false);
        ((MainActivity)getActivity()).setBackButton();
        MainActivity.toolbar_title.setText("Notification");
        ((MainActivity)getActivity()).showToolbarAction();
        setHasOptionsMenu(true);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        instance = this;
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        rvNotification = view.findViewById(R.id.rvNotification);
        App.isFromNotificationFragment = true;
        if (iconView != null)
            iconView.setVisibility(View.GONE);
        getNotificationList();

        return view;
    }

    private void getNotificationList() {
        new CallRequest(instance).getNotification(emailId, token, userId);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getNotification:
                    notificationList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObjData = jObj.getJSONArray("data");
                            for (int i = 0; jObjData.length() > i; i++) {
//                                Log.i("TAG", "Count :-> " + i);
                                String Noti = jObjData.getJSONObject(i).getString("NotiJson");
                                JSONObject objNotiJson = new JSONObject(Noti);
                                GetNotificationModel model = new GetNotificationModel();
                                model = LoganSquare.parse(jObjData.getJSONObject(i).toString(), GetNotificationModel.class);
                                if (model.getTitle().equalsIgnoreCase(Constant.JOB_DOC_APPROVE) || model.getTitle().equalsIgnoreCase(Constant.JOB_DOC_UPLOAD)) {
                                    model.setDoc_id(objNotiJson.getString("doc_id"));
                                }
                                if (model.getTitle().equalsIgnoreCase(Constant.SCHEDULE_ADD) || model.getTitle().equalsIgnoreCase(Constant.SCHEDULE_UPDATE)) {
                                    model.setSchedule_date(objNotiJson.getString("schedule_date"));
                                }
                                notificationList.add(model);
                            }
                            GetNotificationAdapter LogListAdapter = new GetNotificationAdapter(notificationList, instance,getActivity());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvNotification.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvNotification.setItemAnimator(new DefaultItemAnimator());
                            rvNotification.setLayoutManager(mLayoutManager);
                            rvNotification.setAdapter(LogListAdapter);
                            LogListAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
                case viewNotification:
                    notificationList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getNotificationList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

    @Override
    public void onClick(int pos) {
        new CallRequest(instance).viewNotification(emailId, token, userId, notificationList.get(pos).getNotificationID());
    }
}
