package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ivaccessbeta.R;
import com.ivaccessbeta.model.JobStatusModel;

import java.util.ArrayList;

/**
 * Created by User on 23-03-2018.
 */

public class JobStatusAdapter extends RecyclerView.Adapter<JobStatusAdapter.MyViewHolder> {

    public ArrayList<JobStatusModel> list;
    public Context context;
    public   String emailId,token,userId;
    @Override
    public JobStatusAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_textview, parent, false);

        return new JobStatusAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JobStatusAdapter.MyViewHolder holder, final int position) {
        final JobStatusModel model = list.get(position);
        holder.name.setText(model.getJobStatusName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public JobStatusAdapter(Context context, ArrayList<JobStatusModel> List) {
        this.context = context;
        this.list = List;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.txtTitle);
        }
    }

}
