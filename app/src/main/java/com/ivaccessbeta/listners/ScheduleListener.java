package com.ivaccessbeta.listners;

/**
 * Created by User on 18-04-2018.
 */

public interface ScheduleListener {

    void onDeleteSchedule(int pos);
    void onCopyToTomorrowSchedule(int pos);
    void onCopyToNextWeekSchedule(int pos);
    void onCopyToEntireWeekSchedule(int pos);
}
