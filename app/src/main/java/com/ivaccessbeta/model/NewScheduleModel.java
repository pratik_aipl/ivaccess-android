package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

@JsonObject
public class NewScheduleModel implements Serializable {
// "ScheduleID": "122",
//         "ScheduleDate": "2018-08-20",
//         "ToTime": "09:15:00",
//         "FromTime": "09:15:00",
//         "AvailableSchedule": "1",
//         "NotAvailableStatus": "Pending",
//         "CreatedBy": "1",
//         "SubmittedBy": "Super Admin",
//         "CreatedOn": "2018-08-20 09:18:37",
//         "ContractorNotes": "",
//         "AdminNotes": "",
//         "PlacesName": "Miami North",
//         "first_name": "christine",
//         "last_name": "doe",
//         "ColorCode": "#5f087a"

    @JsonField
    public String ScheduleID, ScheduleDate, schedual_date, FromTime, ToTime, PlacesName, AdminNotes, ContractorNotes, first_name, last_name, ColorCode, AvailableSchedule, NotAvailableStatus, SubmittedBy, CreatedOn;

    boolean isNotAvailable = false;

    public boolean isNotAvailable() {
        return isNotAvailable;
    }

    public void setNotAvailable(boolean notAvailable) {
        isNotAvailable = notAvailable;
    }

    public String getAdminNotes() {
        return AdminNotes;
    }

    public void setAdminNotes(String adminNotes) {
        AdminNotes = adminNotes;
    }

    public String getContractorNotes() {
        return ContractorNotes;
    }

    public void setContractorNotes(String contractorNotes) {
        ContractorNotes = contractorNotes;
    }

    public String getAvailableSchedule() {
        return AvailableSchedule;
    }

    public void setAvailableSchedule(String availableSchedule) {
        AvailableSchedule = availableSchedule;
    }

    public String getScheduleID() {
        return ScheduleID;
    }

    public void setScheduleID(String scheduleID) {
        ScheduleID = scheduleID;
    }

    public String getScheduleDate() {
        return ScheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        ScheduleDate = scheduleDate;
    }

    public ArrayList<NewScheduleModel> getSubArray() {
        return subArray;
    }

    public void setSubArray(ArrayList<NewScheduleModel> subArray) {
        this.subArray = subArray;
    }

    public String getNotAvailableStatus() {
        return NotAvailableStatus;
    }

    public void setNotAvailableStatus(String notAvailableStatus) {
        NotAvailableStatus = notAvailableStatus;
    }

    public String getSchedual_date() {
        return schedual_date;
    }

    public void setSchedual_date(String schedual_date) {
        this.schedual_date = schedual_date;
    }

    public String getFromTime() {
        return FromTime;
    }

    public void setFromTime(String fromTime) {
        FromTime = fromTime;
    }

    public String getToTime() {
        return ToTime;
    }

    public void setToTime(String toTime) {
        ToTime = toTime;
    }

    public String getPlacesName() {
        return PlacesName;
    }

    public void setPlacesName(String placesName) {
        PlacesName = placesName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public ArrayList<NewScheduleModel> subArray = new ArrayList<>();

    public String getSubmittedBy() {
        return SubmittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        SubmittedBy = submittedBy;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }
}


