package com.ivaccessbeta.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.DateInputMask;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.adapter.PeopleAdapter;
import com.ivaccessbeta.adapter.SelectContractorAdapter;
import com.ivaccessbeta.adapter.ServiceTypeAdapter;
import com.ivaccessbeta.fragment.profile.MyProfileFragment;
import com.ivaccessbeta.listners.ContractorSpinnerListener;
import com.ivaccessbeta.model.BillToModel;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.FacilitesModel;
import com.ivaccessbeta.model.JobDetailsModel;
import com.ivaccessbeta.model.JobStatusModel;
import com.ivaccessbeta.model.ServieTypeModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.utils.Utils.mDay;
import static com.ivaccessbeta.utils.Utils.mMonth;
import static com.ivaccessbeta.utils.Utils.mYear;

public class AddJobActivity extends Fragment implements AsynchTaskListner, ContractorSpinnerListener {
    private static final String TAG = "AddJobActivity";
    public Spinner/* spFacility,*/ spServices, spBillTo, spContractorName;
    public EditText edtpatientName, edtRoomNo, edtDueDate, edtNotes, edtContractorName;
    public Button btnSave, btnCancel;
    public String patientName, userName, roomNo, dueDate, dob, notes, facility, contractor, billTo, date, service, jobId, jobStatus;
    public AddJobActivity instance;
    public int mHour, mMinute;
    public PeopleAdapter mAdapter;
    public ArrayList<ContractorModel> list = new ArrayList<>();
    public ArrayList<JobStatusModel> jobStatusList = new ArrayList<>();
    public ArrayList<String> jobStatusStringList = new ArrayList<>();
    public ContractorModel contractorModel;
    public String emailId, token, userId, roleId;
    public FacilitesModel facilityModel;
    public ArrayList<String> contactorStringList = new ArrayList<>();
    public ArrayList<String> facillityStringList = new ArrayList<>();
    public ArrayList<FacilitesModel> facilityList = new ArrayList<>();
    public int facilityPos = 0, billtoPos = 0, contractorPos = 0, servicePos = 0;
    public ArrayList<BillToModel> listBillTo = new ArrayList<>();
    public ArrayList<String> billToStringList = new ArrayList<>();
    public ServiceTypeAdapter serviceAdapter;
    public ArrayList<ServieTypeModel> serviceList = new ArrayList<>();
    public ArrayList<String> serviceStringList = new ArrayList<>();
    public BillToModel billToModel;
    public JobStatusModel jobStatusModel;
    public ServieTypeModel servieTypeModel;
    public TextView selectedContractor, txtTitle,spFacility;
    public String dueDateTime24;
    public SimpleDateFormat _24HourSDF;
    public String time, formattedTime;
    public JobDetailsModel jobDetailsModel;
    public int intent;
    public ImageView ivBack;
    public String dateYYMMDD;
    SpinnerDialog spinnerDialog;

    public SelectContractorAdapter adapter;
    public TextView txtBillto;
    public EditText edtDOB;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.activity_add_job, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        instance = this;
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        intent = getArguments().getInt("value");
        MainActivity.toolbar_title.setText("Add Job");
        ((MainActivity)getActivity()).showToolbarAction();
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        userName = MySharedPref.getString(getActivity(), App.NAME, "");
        spFacility = view.findViewById(R.id.spinnerFacility);
        spServices = view.findViewById(R.id.spinnerService);
        spBillTo = view.findViewById(R.id.spinnerBillTo);
        spContractorName = view.findViewById(R.id.spinnerContractorName);
        edtpatientName = view.findViewById(R.id.edtpatientName);
        edtRoomNo = view.findViewById(R.id.edtRoomNo);
        edtDueDate = view.findViewById(R.id.edtDueDate);
        edtDOB = view.findViewById(R.id.edtDOB);
        edtNotes = view.findViewById(R.id.edtNotes);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        ivBack = view.findViewById(R.id.ivBack);
        edtContractorName = view.findViewById(R.id.edtContractorName);
        edtContractorName.setText(userName);
        txtBillto = view.findViewById(R.id.txtBillTo);
        RelativeLayout relativeBillTo = view.findViewById(R.id.relativBillTo);
        RelativeLayout relativeContractor = view.findViewById(R.id.relativeContractor);

        spBillTo.setEnabled(false);

        new DateInputMask(edtDOB, getActivity());
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            edtContractorName.setVisibility(View.VISIBLE);
            spContractorName.setVisibility(View.GONE);
            spBillTo.setVisibility(View.GONE);
            relativeBillTo.setVisibility(View.GONE);
            txtBillto.setVisibility(View.GONE);
            relativeContractor.setVisibility(View.GONE);
        } else {
            edtContractorName.setVisibility(View.GONE);
            spContractorName.setVisibility(View.VISIBLE);
            relativeContractor.setVisibility(View.VISIBLE);
        }


        if (intent == 1) {
            MainActivity.toolbar_title.setText("Edit Job");
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                spBillTo.setVisibility(View.GONE);
               // edtRoomNo.setClickable(false);
//                edtNotes.setClickable(false);
//                edtDueDate.setEnabled(true);
//                edtDueDate.setClickable(true);
             //   edtRoomNo.setFocusable(false);
//                edtNotes.setFocusable(false);
//                edtDueDate.setFocusable(true);

            } else {
                spBillTo.setVisibility(View.VISIBLE);
                edtDueDate.setClickable(true);
                edtDueDate.setFocusable(true);
            }

            jobDetailsModel = (JobDetailsModel) getArguments().getSerializable("JobDetailModel");
            edtpatientName.setText(jobDetailsModel.getPatientName());
            edtRoomNo.setText(jobDetailsModel.getRoomNumber());
            service = jobDetailsModel.getServiceName();
            // contractor = jobDetailsModel.getFirst_name();
            facility = jobDetailsModel.getFacilityID();
            service = jobDetailsModel.getServiceID();
            billTo = jobDetailsModel.getBillToID();
            jobStatus = jobDetailsModel.getStatusID();
            contractor = jobDetailsModel.getAssignTo();
            edtNotes.setText(jobDetailsModel.getNotes());
            jobId = jobDetailsModel.JobID;
            dueDateTime24 = jobDetailsModel.getDueTimeDate();
            dob = jobDetailsModel.getDOB();
            edtDOB.setText(dob);
            dateConvertFormate(edtDueDate, jobDetailsModel.getDueTimeDate(), "");

            if (dueDateTime24.equalsIgnoreCase("0000-00-00 00:00:00")) {
                edtDueDate.setText("");
            }
        }
        if (intent == 2) {
            contractor = getArguments().getString("FaciliterName");
            dateYYMMDD = getArguments().getString("date");
            time="00:00";
            dueDateTime24=dateYYMMDD+" "+time;
            System.out.println("first time=="+dueDateTime24);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        edtDueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) && intent == 1) {
//                } else {
                    if (intent == 2) {
                        tiemPicker();
                    } else {
                        getDatePicker();
                    }
//                }
            }
        });
        spContractorName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getActivity(), view);
                try {
                    contractor = list.get(i - 1).getId();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                }
                try {
                    view.findViewById(R.id.txtViewSchedule).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.ivRight).setVisibility(View.INVISIBLE);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinnerDialog = new SpinnerDialog(getActivity(), facillityStringList,"Select or Search Facility");

        spinnerDialog.setCancellable(true);
        spinnerDialog.setShowKeyboard(false);

        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                spFacility.setText(item);
                    facility = facilityList.get(position).getFacilityID();
                    billTo = facilityList.get(position).getDefaultBillTo();
                    getBillList();
                    spBillTo.setEnabled(false);
            }
        });

        spFacility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerDialog.showSpinerDialog();
            }
        });

        spServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Utils.disableKeyboard(getActivity(), view);
                try {
                    service = serviceList.get(i - 1).getServiceID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSave.setOnClickListener(view1 -> {
            patientName = edtpatientName.getText().toString();
            roomNo = edtRoomNo.getText().toString();
            dob = edtDOB.getText().toString();
            notes = edtNotes.getText().toString();
            if (TextUtils.isEmpty(facility)) {
                Utils.showToast("Please Select Facility", getActivity());
            }else {
                if (intent == 0) {
                    if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                        new CallRequest(instance).addJob(emailId, token, facility, service, jobStatus, billTo, patientName, roomNo, dob, dueDateTime24, notes, userId, userId);
                    } else {
                        if (TextUtils.isEmpty(contractor)) {
                            Utils.showToast("Please Select Contractor", getActivity());
                        } else {
                            new CallRequest(instance).addJob(emailId, token, facility, service, jobStatus, billTo, patientName, roomNo, dob, dueDateTime24, notes, contractor, userId);
                        }
                    }
                } else if (intent == 1) {
                    if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                        new CallRequest(instance).editJob(emailId, token, facility, service, jobStatus, billTo, patientName, roomNo, dob, dueDateTime24, notes, userId, userId, jobId);
                    } else {

                        if (TextUtils.isEmpty(contractor)) {
                            Utils.showToast("Please Select Contractor", getActivity());
                        } else {
                            new CallRequest(instance).editJob(emailId, token, facility, service, jobStatus, billTo, patientName, roomNo, dob, dueDateTime24, notes, contractor, userId, jobId);
                        }
                    }

                } else if (intent == 2) {
                    if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                        new CallRequest(instance).addJob(emailId, token, facility, service, jobStatus, billTo, patientName, roomNo, dob, dueDateTime24, notes, userId, userId);
                    } else {
                        if (TextUtils.isEmpty(contractor)) {
                            Utils.showToast("Please Select Contractor", getActivity());
                        } else {
                            System.out.println("first time2=="+dueDateTime24);
                            new CallRequest(instance).addJob(emailId, token, facility, service, jobStatus, billTo, patientName, roomNo, dob, dueDateTime24, notes, contractor, userId);
                        }
                    }
                }
            }
        });
        return view;
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        Utils.allMenuHide(menu);

        super.onPrepareOptionsMenu(menu);
    }

    public void dateConvertFormate(EditText txt, String Date, String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = null;
        try {
            date = sdf.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat formatDate = new SimpleDateFormat("dd");
        DateFormat formatMonth = new SimpleDateFormat("MM");
        DateFormat formatYear = new SimpleDateFormat("yyyy");
        DateFormat formatAmPm = new SimpleDateFormat("hh: mm a");
        String finalDate = formatDate.format(date);
        String finalMonth = formatMonth.format(date);
        String finalHour = formatAmPm.format(date);
        String finalYear = formatYear.format(date);
        txt.setText(finalHour + " " + finalDate + "-" + finalMonth + "-" + finalYear);
    }

    private void getDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (view, year, monthOfYear, dayOfMonth) -> {
                    date = (monthOfYear + 1) + "-" + dayOfMonth + "-" + year;
                    dateYYMMDD = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                    tiemPicker();
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getFacility();
    }
    public static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }
    private void tiemPicker() {
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                (view, hourOfDay, minute) -> {
                    mHour = hourOfDay;
                    mMinute = minute;
                    String time1 = hourOfDay + ":" + minute;
                    _24HourSDF = new SimpleDateFormat("HH:mm");
                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                    Date _24HourDt = null;
                    try {
                        _24HourDt = _24HourSDF.parse(time1);
                        formattedTime = _12HourSDF.format(_24HourDt);
                        time = _24HourSDF.format(_12HourSDF.parse(formattedTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (intent == 2) {
                        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
                        Date dateDDMMYY = null;
                        try {
                            dateDDMMYY = inputFormat.parse(dateYYMMDD);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        date = outputFormat.format(dateDDMMYY);
                        System.out.println(_12HourSDF.format(_24HourDt) + " " + date);
                        edtDueDate.setText(_12HourSDF.format(_24HourDt) + " " + date);
                    } else {
                        edtDueDate.setText(_12HourSDF.format(_24HourDt) + " " + date);
                    }
                    dueDateTime24 = dateYYMMDD + " " + time;
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void getPeople() {
        try {
            list.clear();
            new CallRequest(instance).getConractorActivity(emailId, token, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getBillList() {
        try {
            spBillTo.setClickable(false);
            listBillTo.clear();
            new CallRequest(instance).getBillToActivity(emailId, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getFacility() {

        try {
            new CallRequest(instance).getFacility(emailId, token);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getService() {
        try {
            serviceList.clear();
            new CallRequest(instance).getServiceActivity(emailId, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContractorActivity:
                    Utils.hideProgressDialog();
                    contactorStringList.clear();
                    list.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                contractorModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),ContractorModel.class);
                                list.add(contractorModel);
                                if (contractor != null) {
                                    if (contractor.equalsIgnoreCase(contractorModel.getId())) {
                                        contractorPos = i + 1;
                                    }
                                }
                                contactorStringList.add(contractorModel.getFullName());
                            }
                            adapter = new SelectContractorAdapter(getActivity(),
                                    R.layout.layout_spinner_contactor_item, list, instance);
                            SelectContractorAdapter adapter1 = new SelectContractorAdapter(getActivity(),
                                    R.layout.layout_spinner_contactor_item, contactorStringList, instance);
                            spContractorName.setAdapter(new NothingSelectedSpinnerAdapter(
                                    adapter, R.layout.contractor_spinner_row_nothing_selected,
                                    getActivity()));
                            spContractorName.setSelection(contractorPos);

                            if (intent == 2) {
                                int spinnerPosition = adapter1.getPosition(contractor);
                                spContractorName.setSelection(spinnerPosition + 1);
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getService();
                    break;
                case getFacility:
                    facillityStringList.clear();
                    facilityList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                facilityModel = LoganSquare.parse(ob.toString(), FacilitesModel.class);
                                facilityList.add(facilityModel);
                                if (facility != null) {
                                    if (facility.equalsIgnoreCase(facilityModel.getFacilityID())) {
                                        facilityPos = i + 1;
                                    }
                                }
                                facillityStringList.add(facilityModel.getFacilityName());
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getBillList();
                    break;
                case getBillToActivity:
                    billToStringList.clear();
                    listBillTo.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                billToModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),BillToModel.class);
                                listBillTo.add(billToModel);
                                billToStringList.add(billToModel.getName());
                                if (billTo != null) {
                                    if (billTo.equalsIgnoreCase(billToModel.getBillToID())) {
                                        billtoPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, billToStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spBillTo.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.billto_spinner_row_nothing_selected,
                                    getActivity()));
                            spBillTo.setSelection(billtoPos);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    getPeople();
                    break;

                case getServiceActivity:
                    serviceStringList.clear();
                    serviceList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                servieTypeModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),ServieTypeModel.class);
                                serviceList.add(servieTypeModel);
                                serviceStringList.add(servieTypeModel.getServiceName());
                                if (service != null) {
                                    if (service.equalsIgnoreCase(servieTypeModel.getServiceID())) {
                                        servicePos = i + 1;
                                    }
                                }
                            }
                            try {
                                ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, serviceStringList);
                                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spServices.setAdapter(new NothingSelectedSpinnerAdapter(
                                        aa, R.layout.service_nothing_selected,
                                        getActivity()));
                                spServices.setSelection(servicePos);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case addJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Job Add Succesfully", getActivity());
                            App.checkState = true;
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case editJob:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Job Update Succesfully", getActivity());
                            App.checkStateEdit = true;
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void viewSchedule(View v, int position) {
        MyProfileFragment myProfileFragment = new MyProfileFragment();
        Bundle b = new Bundle();
        b.putInt("value", 1);
        b.putString("contractorId", list.get(position).getId());
        myProfileFragment.setArguments(b);
        ((MainActivity)getActivity()).changeFragment(myProfileFragment, true);
    }
}

