package com.ivaccessbeta.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ivaccessbeta.R;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends AppCompatActivity implements AsynchTaskListner {

    EditText edtOldPassword,edtNewPassword,edtCPassword;
    Button btnSend;
    String oldPassword,newPassword,confirmPassword,emailId,token,userId;
    public ChangePasswordActivity instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        edtOldPassword=findViewById(R.id.edt_oldPassword);
        edtNewPassword=findViewById(R.id.edt_newPassword);
        edtCPassword=findViewById(R.id.edt_cPassword);
        btnSend=findViewById(R.id.btn_send);
        emailId = MySharedPref.getString(getApplicationContext(), App.EMAIL, "");
        token = MySharedPref.getString(getApplicationContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getApplicationContext(), App.LOGGED_USER_ID, "");

        instance=this;
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPassword=edtOldPassword.getText().toString();
                newPassword=edtNewPassword.getText().toString();
                confirmPassword=edtCPassword.getText().toString();

                if(TextUtils.isEmpty(oldPassword)){
                    Utils.showToast("Please Enter Old Password",getApplicationContext());
                } else if(TextUtils.isEmpty(newPassword)){
                    Utils.showToast("Please Enter New Password",getApplicationContext());
                } else if(TextUtils.isEmpty(confirmPassword)){
                    Utils.showToast("Please Enter Confirm  Password",getApplicationContext());
                } else if(confirmPassword==newPassword){
                    Utils.showToast("Please Enter Currect Confirm Password",getApplicationContext());
                }else if (edtNewPassword.getText().toString().length() < 6) {
                    Utils.showAlert("Please enter the password of atlest 6 characters", getApplicationContext());
                } else {
                   new CallRequest(instance).chnagePasswor(emailId,token,userId,oldPassword,newPassword,confirmPassword);
                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case changePassword:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            MySharedPref.setString(instance, App.IS_PASSWORD_CHANGE, Constant.PASSWORD_CHANGE);
                            Intent i=new Intent(instance,MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                        else {
                            Utils.handleJSonAPIError(jObj, instance);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
