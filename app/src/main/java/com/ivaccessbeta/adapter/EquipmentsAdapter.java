package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.manage.AddEquipmentFragment;
import com.ivaccessbeta.model.EquipmentsModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.ivaccessbeta.fragment.manage.EquipmentFragment.instance;

/**
 * Created by User on 01-03-2018.
 */

public class EquipmentsAdapter extends RecyclerView.Adapter<EquipmentsAdapter.MyViewHolder> {

    public ArrayList<EquipmentsModel> list;
    public List<EquipmentsModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;

    @Override
    public EquipmentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_region, parent, false);

        return new EquipmentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EquipmentsAdapter.MyViewHolder holder, final int position) {
        final EquipmentsModel model = list.get(position);

        String roleId = MySharedPref.getString(context, App.ROLEID, "");
        String equipmentName = "";
        if (!roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID) && !roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            equipmentName = model.getEquipmentName() + " " + model.getEquipmentCode();
        } else {
            equipmentName = model.getEquipmentName();
        }

        holder.name.setText(equipmentName);
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailId = MySharedPref.getString(context, App.EMAIL, "");
                token = MySharedPref.getString(context, App.APP_TOKEN, "");
                userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
                doDelete(position);

            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddEquipmentFragment addEquipmentFragment = new AddEquipmentFragment();
                Bundle b = new Bundle();
                b.putInt("value", 1);
                b.putSerializable("EquipmentModel", list.get(position));
                addEquipmentFragment.setArguments(b);
                ((MainActivity)context).changeFragment(addEquipmentFragment, true);
            }
        });

    }

    private void doDelete(final int position) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteEquipment(emailId, token, userId, list.get(position).getEquimentID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (EquipmentsModel bean : searchFilterList) {
                if (bean.getEquipmentName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public EquipmentsAdapter(Context context, ArrayList<EquipmentsModel> List) {
        this.context = context;
        this.list = List;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView ivDelete, ivEdit;

        public MyViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.txtName);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivEdit = view.findViewById(R.id.ivEdit);

        }


    }

}
