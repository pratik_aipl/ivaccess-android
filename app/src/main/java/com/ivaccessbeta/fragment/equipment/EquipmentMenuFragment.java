package com.ivaccessbeta.fragment.equipment;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.adapter.SearchableAdapter;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.EquipmentMenuModel;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.EquipmentFilterAdapter;
import com.ivaccessbeta.adapter.EquipmentMenuAdapter;
import com.ivaccessbeta.listners.EquipmentListener;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class EquipmentMenuFragment extends Fragment implements EquipmentListener, AsynchTaskListner {

    public ArrayList<EquipmentMenuModel> equipmentList = new ArrayList<>();
    public ArrayList<ContractorModel> equipmentFilterList = new ArrayList<>();
    public ContractorModel contractorModel;
    public EquipmentMenuAdapter equipmentMenuAdapter;
    public EquipmentMenuModel equipmentMenuModel;
    public RecyclerView rvEquipmentList;
    public TextView txtOrderNew, txtFiter;
    public EquipmentListener equipmentListener;
    public EquipmentMenuFragment instance;
    public String emailId, token, userId, roleId, persons, status, equipmentId,personsName;
    public ArrayList<String> contractorStringList = new ArrayList<>();
    public TextView txtMarkShiped, txtCancel, txtApprove, txtUndoCancel, txtNotMarkShiped, txtNoData;
//    public ArrayAdapter<String> adptr;
   // public SearchableAdapter adptr;
    public int pos;
  public   LinearLayout llFilter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_equipment_menu, container, false);
        MainActivity.toolbar_title.setText("EQUIPMENTS");
        ((MainActivity)getActivity()).showToolbarAction();
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        instance = this;
        rvEquipmentList = view.findViewById(R.id.rvEquipmentList);
        txtOrderNew = view.findViewById(R.id.txtOrderNew);
        txtFiter = view.findViewById(R.id.txtFiter);
        txtNoData = view.findViewById(R.id.txtNoData);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        llFilter=view.findViewById(R.id.llFilter);
        txtOrderNew.setOnClickListener(v -> ((MainActivity)getActivity()).changeFragment(new AddEquipmentOrderFragment(), true));
        llFilter.setOnClickListener(v -> showFilterDialog());
        if(roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)){
            llFilter.setVisibility(View.GONE);
        }
        return view;
    }

    private void getOrderEquipment() {
        try {
            if(roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)){
                persons=userId;
            }
            new CallRequest(instance).getMenuEquipment(emailId, token, persons);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void showFilterDialog() {

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_contractor_filter_qeuipmet);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ListView rvSelectContractor = dialog.findViewById(R.id.rv_contractor);
        TextView txtDialogTitle = dialog.findViewById(R.id.txtDialogTitle);
        EditText inputSearch = dialog.findViewById(R.id.inputSearch);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnClear = dialog.findViewById(R.id.btnClear);
        txtDialogTitle.setText("Select Contractors");
        inputSearch.setVisibility(View.VISIBLE);

        SearchableAdapter adptr = new SearchableAdapter(getActivity(),contractorStringList);
        rvSelectContractor.setAdapter(adptr);

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence cs, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence cs, int i, int i1, int i2) {
                adptr.getFilter().filter(cs);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnOk.setOnClickListener(view -> {
            dialog.dismiss();
            getOrderEquipment();
        });

        btnClear.setOnClickListener(v -> {
            if (persons.equalsIgnoreCase(equipmentFilterList.get(pos).getId())) {
                rvSelectContractor.setItemChecked(pos, false);
                persons = "";
            }
            personsName="";
            getOrderEquipment();
            dialog.dismiss();

        });

        rvSelectContractor.setOnItemClickListener((parent, view, position, id) -> {
            persons = equipmentFilterList.get(position).getId();
            personsName=equipmentFilterList.get(position).getFullName();
            pos = position;
        });

        if (persons.equalsIgnoreCase(equipmentFilterList.get(pos).getId())) {
            rvSelectContractor.setItemChecked(pos, true);
        }
        dialog.show();
    }

    public void getContractor() {
        new CallRequest(instance).getConractor(emailId, token, "");
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }

    @Override
    public void onOpenActionDialog(int pos, View v) {
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        showActionDialog(pos, x, y);
    }

    private void showActionDialog(int pos, int x, int y) {
        RelativeLayout viewGroup = getActivity().findViewById(R.id.rlativeTop);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.dialog_equipment_action, viewGroup);
        final PopupWindow popupWindow = new PopupWindow(getActivity());
        popupWindow.setContentView(layout);
        popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
        txtMarkShiped = layout.findViewById(R.id.txtMarkShiped);
        txtCancel = layout.findViewById(R.id.txtCancel);
        txtApprove = layout.findViewById(R.id.txtApprove);
        txtNotMarkShiped = layout.findViewById(R.id.txtMarkNotShiped);
        txtUndoCancel = layout.findViewById(R.id.txtUndoCancel);
        status = equipmentList.get(pos).getStatus();
        equipmentId = equipmentList.get(pos).getEquimentOrderID();


        txtMarkShiped.setOnClickListener(v -> {
            popupWindow.dismiss();
            status = "4";
            changeEquipmentStatus();
        });
        txtApprove.setOnClickListener(v -> {
            popupWindow.dismiss();
            status = "2";
            changeEquipmentStatus();
        });
        txtNotMarkShiped.setOnClickListener(v -> {
            popupWindow.dismiss();
            status = "2";
           changeEquipmentStatus();
        });
        txtUndoCancel.setOnClickListener(v -> {
            popupWindow.dismiss();
            status = "1";
            changeEquipmentStatus();
        });
        txtCancel.setOnClickListener(v -> {
            popupWindow.dismiss();
            status = "3";
            changeEquipmentStatus();
        });
        if(roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)){
            txtApprove.setVisibility(View.GONE);

            if (status.equalsIgnoreCase("1")) {
               // txtApprove.setVisibility(View.VISIBLE);
                txtMarkShiped.setVisibility(View.VISIBLE);
                txtCancel.setVisibility(View.VISIBLE);
                txtNotMarkShiped.setVisibility(View.GONE);
                txtUndoCancel.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("2")) {
                txtCancel.setVisibility(View.VISIBLE);
                txtApprove.setVisibility(View.GONE);
                txtMarkShiped.setVisibility(View.VISIBLE);
                txtNotMarkShiped.setVisibility(View.GONE);
                txtUndoCancel.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("3")) {
                txtApprove.setVisibility(View.GONE);
                txtMarkShiped.setVisibility(View.VISIBLE);
                txtUndoCancel.setVisibility(View.VISIBLE);
                txtCancel.setVisibility(View.GONE);
                txtNotMarkShiped.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("4")) {
                txtApprove.setVisibility(View.GONE);
                txtMarkShiped.setVisibility(View.GONE);
                txtCancel.setVisibility(View.VISIBLE);
                txtNotMarkShiped.setVisibility(View.VISIBLE);
                txtUndoCancel.setVisibility(View.GONE);
            }
        }
        else {
            if (status.equalsIgnoreCase("1")) {
                txtApprove.setVisibility(View.VISIBLE);
                txtMarkShiped.setVisibility(View.VISIBLE);
                txtCancel.setVisibility(View.VISIBLE);
                txtNotMarkShiped.setVisibility(View.GONE);
                txtUndoCancel.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("2")) {
                txtCancel.setVisibility(View.VISIBLE);
                txtApprove.setVisibility(View.GONE);
                txtMarkShiped.setVisibility(View.VISIBLE);
                txtNotMarkShiped.setVisibility(View.GONE);
                txtUndoCancel.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("3")) {
                txtApprove.setVisibility(View.GONE);
                txtMarkShiped.setVisibility(View.VISIBLE);
                txtUndoCancel.setVisibility(View.VISIBLE);
                txtCancel.setVisibility(View.GONE);
                txtNotMarkShiped.setVisibility(View.GONE);
            } else if (status.equalsIgnoreCase("4")) {
                txtApprove.setVisibility(View.GONE);
                txtMarkShiped.setVisibility(View.GONE);
                txtCancel.setVisibility(View.VISIBLE);
                txtNotMarkShiped.setVisibility(View.VISIBLE);
                txtUndoCancel.setVisibility(View.GONE);
            }
        }
    }

    private void changeEquipmentStatus() {
        new CallRequest(instance).chnageEquipmentStatus(emailId, token, userId, equipmentId, status);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        persons="";
        personsName="";
        getContractor();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMenuEquipment:
                    equipmentList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            equipmentList.addAll(LoganSquare.parseList(ob1.toString(),EquipmentMenuModel.class));
                            if (equipmentList.size() == 0) {
                                txtNoData.setVisibility(View.VISIBLE);
                            } else {
                                txtNoData.setVisibility(View.GONE);
                            }
                            equipmentMenuAdapter = new EquipmentMenuAdapter(equipmentList, instance);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvEquipmentList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvEquipmentList.setLayoutManager(mLayoutManager);
                            rvEquipmentList.setItemAnimator(new DefaultItemAnimator());
                            rvEquipmentList.setAdapter(equipmentMenuAdapter);
                            equipmentMenuAdapter.notifyDataSetChanged();

                            if(!personsName.equalsIgnoreCase("")) {
                                txtFiter.setText("FILTER BY : "+personsName);
                            }
                            else {
                                txtFiter.setText("FILTER BY : ALL ");

                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getContractor:
                    equipmentFilterList.clear();
                    contractorStringList.clear();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                contractorModel = new ContractorModel();
                                contractorModel = LoganSquare.parse(ob.toString(),ContractorModel.class);
                                equipmentFilterList.add(contractorModel);
                                contractorStringList.add(contractorModel.getFullName());
                            }
//                            adptr = new ArrayAdapter<String>(getContext(), R.layout.layout_checkbox, contractorStringList);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    getOrderEquipment();
                    //  Utils.hideProgressDialog();
                    break;

                case changeEquipmentStatus:
                    try {
                        JSONObject obj = new JSONObject(result);
                        if (obj.getString("status").equals("200")) {
                           Utils.showToast(obj.getString("message"), getActivity());
                            getOrderEquipment();
                        } else {
                            Utils.handleJSonAPIError(obj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }

    }

}
