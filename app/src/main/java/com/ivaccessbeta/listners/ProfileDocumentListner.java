package com.ivaccessbeta.listners;

/**
 * Created by User on 02-04-2018.
 */

public interface ProfileDocumentListner {

    void onUploadDocumentClick(int pos);
    void onDeleteDocumentClick(int pos);
    void onViewDocumentClick(int pos);
    void onViewExpDocumentClick(int pos);
    void onApproveDocumentClick(int pos);
    void onDenyDocumentClick(int pos);
}
