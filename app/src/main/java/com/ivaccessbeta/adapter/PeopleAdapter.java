package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.manage.AddPeopleFragment;
import com.ivaccessbeta.model.PeopleModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.ivaccessbeta.fragment.manage.PeoplesFragment.instance;

/**
 * Created by User on 01-03-2018.
 */

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.MyViewHolder> {

    public ArrayList<PeopleModel> list;
    public List<PeopleModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;

    @Override
    public PeopleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_region, parent, false);

        return new PeopleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PeopleAdapter.MyViewHolder holder, final int position) {
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        final PeopleModel model = list.get(position);
        holder.name.setText(model.getFirst_name() + " " + model.getLast_name());


        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doDelete(position);
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddPeopleFragment addPeopleFragment = new AddPeopleFragment();
                Bundle b = new Bundle();
                b.putInt("value", 1);
                b.putSerializable("PeopleModel", list.get(position));
//                b.putString("peopleId",model.getId());
//                b.putString("firstName",model.getFirst_name());
//                b.putString("lastName",model.getLast_name());
//                b.putString("phone",model.getMobileNumber());
//                b.putString("phone2",model.getAlternateNumber());
//                b.putString("emailPeople",model.getEmail());
//                b.putString("estimatedPiccSupplies",model.getEstimatedPICCSupplies());
//                b.putString("edtimtedMidSupplies",model.getEstimatedMidSupplies());
//                b.putString("roleId",model.getRole_id());
//                b.putString("facilityId",model.getFacilities());
//                b.putString("qbCompanyId",model.getQB_Company());
//                b.putString("regionId",model.getRegionID());
//                b.putString("directoryNote",model.getDirectoryNote());
//                Log.d("roleId",model.getRole_id());
//                Log.d("regionId",model.getRegionID());

                addPeopleFragment.setArguments(b);
                ((MainActivity)context).changeFragment(addPeopleFragment, true);
            }
        });

    }

    private void doDelete(final int position) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deletePeople(emailId, token, userId, list.get(position).getId());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (PeopleModel bean : searchFilterList) {
                if (bean.getFirst_name().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public PeopleAdapter(Context activity, ArrayList<PeopleModel> featuredJobList) {
        this.context = activity;
        this.list = featuredJobList;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView ivDelete, ivEdit;

        public MyViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.txtName);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivEdit = view.findViewById(R.id.ivEdit);

        }


    }
}