package com.ivaccessbeta;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.ivaccessbeta.utils.Utils;

import java.util.Calendar;
import java.util.Locale;

public class DateInputMask implements TextWatcher {

    private String current = "";
    private String ddmmyyyy = "DDMMYYYY";
    private Calendar cal = Calendar.getInstance();
    private EditText input;
    public Context ct;
    public boolean isEditing = true;

    public DateInputMask(EditText input, Context mContext) {
        this.input = input;
        this.input.addTextChangedListener(this);
        this.ct = mContext;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        try {
            if (s.toString().length() == 0) {
                isEditing = true;
                return;
            }

            if (s.toString().length() == 10) {
                isEditing = true;
                return;
            }
            if (isEditing) {

                if (s.toString().length() > 10) {
                    isEditing = false;
                    return;
                }

                int day = 1, month = 1, year = 2018;

                String clean = s.toString();
                if (s.toString().length() == 2) {
                    month = Integer.parseInt(clean.substring(0, 2));

                    if (month < 1 || month > 12) {
                        System.out.println("Fired from month");
                        clean = "";
                        input.setText("");
                        Utils.showToast("Please enter valid month", ct);
                    } else {
                        clean = month + "/";
                        input.setText(clean);


                    }
                    input.setSelection(input.getText().toString().length());
                }

                if (s.length() == 5) {
                    month = Integer.parseInt(clean.substring(0, 2));
                    day = Integer.parseInt(clean.substring(3, 5));

                    if (day > 31 || day < 1) {
                        System.out.println("Fired from day");
                        clean = month + "/";
                        input.setText(clean);
                        Utils.showToast("Please enter valid day of month", ct);


                    } else {
                        clean = month + "/" + day + "/";
                        input.setText(clean);

                    }
                    input.setSelection(input.getText().toString().length());
                }


                if (s.length() == 10) {
                    month = Integer.parseInt(clean.substring(0, 2));
                    day = Integer.parseInt(clean.substring(3, 5));
                    year = Integer.parseInt(clean.substring(6, 10));

                    if (year > cal.get(Calendar.YEAR)) {
                        System.out.println("Fired from year");
                        clean = month + "/" + day + "/";
                        input.setText(clean);
                        Utils.showToast("Please enter valid Year", ct);
                        input.setSelection(input.getText().toString().length());
                    } else {
                        clean = month + "/" + day + "/" + year;
                        isEditing = false;
                        input.setText(clean);


                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void afterTextChanged(Editable s) {

      /*  try {
            if (s.toString().equals(current)) {
                return;
            }

            if (s.toString().length() > 10) {
                return;
            }

            int day = 1, month = 1, year = 2018;

            String clean = s.toString();
            if (s.toString().length() == 2) {


                month = Integer.parseInt(clean.substring(0, 2));

                if (month < 0 || month > 12) {
                    System.out.println("Fired from month");
                    clean = day + "/";
                    input.setText(clean);
                    Utils.showToast("Please enter valid month", ct);
                } else {
                    clean = day + "/" + month;
                    input.setText(clean);
                }
            }

            if (s.length() == 5) {
                day = Integer.parseInt(clean.substring(3, 5));

                if (day > 31 || day < 0) {
                    System.out.println("Fired from day");
                    clean = "";
                    input.setText("");
                    Utils.showToast("Please enter valid day of month", ct);

                } else {
                    clean = day + "/";
                    input.setText(clean);
                }
            }

            if (s.length() == 10) {
                year = Integer.parseInt(clean.substring(5, 9));

                if (year > cal.get(Calendar.YEAR)) {
                    System.out.println("Fired from year");
                    clean = day + "/" + month;
                    input.setText(clean);
                    Utils.showToast("Please enter valid month", ct);
                } else {
                    clean = day + "/" + month + "/" + year;
                    input.setText(clean);
                }
            }

            input.setSelection(input.getText().toString().length());
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


}