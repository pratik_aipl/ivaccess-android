package com.ivaccessbeta.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.equipment.EquipmentMenuFragment;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.fragment.schedule.NewScheduleFragment;
import com.ivaccessbeta.fragment.profile.GetNotificationFragment;
import com.ivaccessbeta.fragment.profile.MyProfileFragment;
import com.ivaccessbeta.listners.NotificationInterface;
import com.ivaccessbeta.model.GetNotificationModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;

import static com.ivaccessbeta.fragment.flotingNotification.services.ChatHeadService.iconView;

/**
 * Created by User on 21-04-2018.
 */

public class GetNotificationAdapter extends RecyclerView.Adapter<GetNotificationAdapter.MyViewHolder> {
    private static final String TAG = "GetNotificationAdapter";

    public ArrayList<GetNotificationModel> list;
    public Context context;
    public String roleId;
    public NotificationInterface notificationInterface;

    @Override
    public GetNotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_get_notification, parent, false);
        roleId = MySharedPref.getString(context, App.ROLEID, "");
        return new GetNotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final GetNotificationAdapter.MyViewHolder holder, final int position) {
        final GetNotificationModel model = list.get(position);
        holder.txtNotification.setText(model.getNotification());
        holder.txtTime.setText(model.getDateDiff());

        if (model.getTitle().equalsIgnoreCase(Constant.NOTIFY)) {
            if (model.getIsView().equalsIgnoreCase(Constant.Zero)) {
                holder.btnView.setVisibility(View.VISIBLE);
            } else {
                holder.btnView.setVisibility(View.GONE);
            }
        }
//        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/" + "Lato-Black.ttf");
//        Typeface tfbold = Typeface.createFromAsset(context.getAssets(), "fonts/" + "Lato-Semibold.ttf");
        Log.d(TAG, "onBindViewHolder: "+model.getIsView());
        if (!model.getIsView().equalsIgnoreCase(Constant.Zero)) {
            holder.txtNotification.setTypeface(null, Typeface.BOLD);
        } else {
            holder.txtNotification.setTypeface(null, Typeface.NORMAL);
        }

        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationInterface.onClick(position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iconView.setVisibility(View.VISIBLE);
                if (model.getTitle().equalsIgnoreCase(Constant.JOB_EDITED) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_NOTE) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_MARKARRIVAL) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_DOC_APPROVE) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_DOC_UPLOAD) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_ADD) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_NOTIFICATION) ||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_STATUS)||
                        model.getTitle().equalsIgnoreCase(Constant.JOB_DOSE_NOT_ACKNLOG)) {
                    if (model.getTitle().equalsIgnoreCase(Constant.JOB_DOC_APPROVE) ||
                            model.getTitle().equalsIgnoreCase(Constant.JOB_DOC_UPLOAD)) {
                        App.docId = model.getDoc_id();
                        // TODO: 21/12/2018 hide status
//                        App.isFromNotificationFragment = true;
                    }
                    JobDetailFragment fragment = new JobDetailFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("jobNo", model.getJobID());
                    fragment.setArguments(bundle);
                    ((MainActivity)context).changeFragment(fragment, true);

                } else if (model.getTitle().equalsIgnoreCase(Constant.JOB_EQUIPMENT_STATUS) || model.getTitle().equalsIgnoreCase(Constant.EQUIPMENT_ADD)) {
                    ((MainActivity)context).changeFragment(new EquipmentMenuFragment(), true);
                } else if (model.getTitle().equalsIgnoreCase(Constant.SCHEDULE_ADD) || model.getTitle().equalsIgnoreCase(Constant.SCHEDULE_UPDATE)) {
                    App.scheduleDate = model.getSchedule_date();
                    ((MainActivity)context).changeFragment(new NewScheduleFragment(), true);
                } else if (model.getTitle().equalsIgnoreCase(Constant.NOTIFY)) {


                } else if (model.getTitle().equalsIgnoreCase(Constant.DOCUMENT_EXPIRED)) {
                    App.contractorId = model.getUser_id();
                    //  contractorDocId=additionalData.getString("doc_id");
                    App.isContactorDoc = true;
                    if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                        MyProfileFragment myProfileFragment = new MyProfileFragment();
                        Bundle b = new Bundle();
                        b.putInt("value", 0);
                        myProfileFragment.setArguments(b);
                        ((MainActivity)context).changeFragment(myProfileFragment, true);
                    } else {
                        MyProfileFragment myProfileFragment = new MyProfileFragment();
                        Bundle b = new Bundle();
                        b.putInt("value", 1);
                        b.putString("contractorId", App.contractorId);
                        // b.putSerializable("ContractorModel", contractorModel);
                        myProfileFragment.setArguments(b);
                        ((MainActivity)context).changeFragment(myProfileFragment, true);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public GetNotificationAdapter(ArrayList<GetNotificationModel> List, GetNotificationFragment getNotificationFragment, Context context) {
        this.list = List;
        this.context = context;
        this.notificationInterface = getNotificationFragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtNotification, txtTime;
        Button btnView;
        CardView card_view;

        public MyViewHolder(View view) {
            super(view);
            txtNotification = view.findViewById(R.id.txtNotification);
            txtTime = view.findViewById(R.id.txtTime);
            btnView = view.findViewById(R.id.btnView);
            card_view = view.findViewById(R.id.card_view);
        }
    }
}
