package com.ivaccessbeta.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.JobDetailsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;


/**
 * Created by User on 17-04-2018.
 */

public class DocumentImagePagerAdapter extends PagerAdapter {
    private static final String TAG = "DocumentImagePagerAdapt";
    public Context context;
    public ArrayList<JobDetailsModel> list;
    public LayoutInflater layoutInflater;

    public DocumentImagePagerAdapter(Context context, ArrayList<JobDetailsModel> list) {
        this.context = context;
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Log.d(TAG, "instantiateItem: ");
        View itemView = layoutInflater.inflate(R.layout.layout_image, container, false);
        PhotoView imageView = itemView.findViewById(R.id.imageView);
        final ProgressBar loader = itemView.findViewById(R.id.loader);
        String imageURL = "http://ivaccess.blenzabi.com/" + list.get(position).getDocumentPath() + list.get(position).getDocumentName();
        if (!TextUtils.isEmpty(imageURL)) {
            Picasso.with(context).load(imageURL)
                    .into(imageView, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (loader != null) {
                                loader.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}