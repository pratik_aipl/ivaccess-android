package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 20-03-2018.
 */

@JsonObject
public class JobDetailsModel implements Serializable {

    @JsonField
    public String JobID, DueTimeDate, FacilityName, first_name, last_name, JobStatusColor, JobStatusName, RoomNumber, DOB, Phone, Address, Notes, JobNo, BillName,
            PatientName, ServiceName, job_logs, job_docs, DocumentPath = "", DocumentName = "", FacilityNote = "", IsCheck, IsBill,
            FacilityID, ServiceID, BillToID, StatusID, AssignTo, CreatedOn, CreatedBy, JDID, DocumentStatus = "",
            EquipmentQuantity, CheckRate, CustomCheckAmount, BillRate, CustomBillAmount, IvAccessSupply, UpdateCCA, UpdateCBA,
            MarkArrival, City, State, Zip, DocsApprove = "", IsPrint = "",isArchive="";

    public String getIsArchive() {
        return isArchive;
    }

    public void setIsArchive(String isArchive) {
        this.isArchive = isArchive;
    }

    public int job_docs_status = -1;

    public int getJob_docs_status() {
        return job_docs_status;
    }

    public void setJob_docs_status(int job_docs_status) {
        this.job_docs_status = job_docs_status;
    }

    public String getIsPrint() {
        return IsPrint;
    }

    public void setIsPrint(String isPrint) {
        IsPrint = isPrint;
    }

    public String getDocsApprove() {
        return DocsApprove;
    }

    public void setDocsApprove(String docsApprove) {
        DocsApprove = docsApprove;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getMarkArrival() {
        return MarkArrival;
    }

    public void setMarkArrival(String markArrival) {
        MarkArrival = markArrival;
    }

    public String getUpdateCCA() {
        return UpdateCCA;
    }

    public void setUpdateCCA(String updateCCA) {
        UpdateCCA = updateCCA;
    }

    public String getUpdateCBA() {
        return UpdateCBA;
    }

    public void setUpdateCBA(String updateCBA) {
        UpdateCBA = updateCBA;
    }

    public String getIvAccessSupply() {
        return IvAccessSupply;
    }

    public void setIvAccessSupply(String ivAccessSupply) {
        IvAccessSupply = ivAccessSupply;
    }

    public String getCheckRate() {
        return CheckRate;
    }

    public void setCheckRate(String checkRate) {
        CheckRate = checkRate;
    }

    public String getCustomCheckAmount() {
        return CustomCheckAmount;
    }

    public void setCustomCheckAmount(String customCheckAmount) {
        CustomCheckAmount = customCheckAmount;
    }

    public String getBillRate() {
        return BillRate;
    }

    public void setBillRate(String billRate) {
        BillRate = billRate;
    }

    public String getCustomBillAmount() {
        return CustomBillAmount;
    }

    public void setCustomBillAmount(String customBillAmount) {
        CustomBillAmount = customBillAmount;
    }

    public String getIsCheck() {
        return IsCheck;
    }

    public void setIsCheck(String isCheck) {
        IsCheck = isCheck;
    }

    public String getIsBill() {
        return IsBill;
    }

    public void setIsBill(String isBill) {
        IsBill = isBill;
    }

    public String getBillName() {
        return BillName;
    }

    public void setBillName(String billName) {
        BillName = billName;
    }

    public String getFacilityNote() {
        return FacilityNote;
    }

    public void setFacilityNote(String facilityNote) {
        FacilityNote = facilityNote;
    }

    public String getDocumentStatus() {
        return DocumentStatus;
    }

    public void setDocumentStatus(String documentStatus) {
        DocumentStatus = documentStatus;
    }

    public String getJDID() {
        return JDID;
    }

    public void setJDID(String JDID) {
        this.JDID = JDID;
    }

    public String getJobStatusName() {
        return JobStatusName;
    }

    public void setJobStatusName(String jobStatusName) {
        JobStatusName = jobStatusName;
    }

    public String getJobNo() {
        return JobNo;
    }

    public void setJobNo(String jobNo) {
        JobNo = jobNo;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getAssignTo() {
        return AssignTo;
    }

    public void setAssignTo(String assignTo) {
        AssignTo = assignTo;
    }

    public String getFacilityID() {
        return FacilityID;
    }

    public void setFacilityID(String facilityID) {
        FacilityID = facilityID;
    }

    public String getServiceID() {
        return ServiceID;
    }

    public void setServiceID(String serviceID) {
        ServiceID = serviceID;
    }

    public String getBillToID() {
        return BillToID;
    }

    public void setBillToID(String billToID) {
        BillToID = billToID;
    }

    public String getStatusID() {
        return StatusID;
    }

    public void setStatusID(String statusID) {
        StatusID = statusID;
    }

    public String getJobID() {
        return JobID;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getDueTimeDate() {
        return DueTimeDate;
    }

    public void setDueTimeDate(String dueTimeDate) {
        DueTimeDate = dueTimeDate;
    }

    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getJobStatusColor() {
        return JobStatusColor;
    }

    public void setJobStatusColor(String jobStatusColor) {
        JobStatusColor = jobStatusColor;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getJob_logs() {
        return job_logs;
    }

    public void setJob_logs(String job_logs) {
        this.job_logs = job_logs;
    }

    public String getJob_docs() {
        return job_docs;
    }

    public void setJob_docs(String job_docs) {
        this.job_docs = job_docs;
    }

    public String getDocumentPath() {
        return DocumentPath;
    }

    public void setDocumentPath(String documentPath) {
        DocumentPath = documentPath;
    }

    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String documentName) {
        DocumentName = documentName;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getEquipmentQuantity() {
        return EquipmentQuantity;
    }

    public void setEquipmentQuantity(String equipmentQuantity) {
        EquipmentQuantity = equipmentQuantity;
    }

    public ArrayList<LogHistoryModel> logList = new ArrayList<>();
    public  ArrayList<JobDetailsModel> documentList = new ArrayList<>();

}
