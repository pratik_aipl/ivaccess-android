package com.ivaccessbeta.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.AddJobActivity;
import com.ivaccessbeta.fragment.HomeFragmentNew;
import com.ivaccessbeta.listners.DocumentViewClickListener;
import com.ivaccessbeta.listners.HomeListener;
import com.ivaccessbeta.listners.OnActivityResult;
import com.ivaccessbeta.model.JobDetailsModel;
import com.ivaccessbeta.model.JobStatusModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.RecyclerTouchListener;
import com.ivaccessbeta.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.ivaccessbeta.utils.Utils.hasPermissions;

/**
 * Created by User on 28-02-2018.
 */

public class HomeJobDetailAdapter extends RecyclerView.Adapter<HomeJobDetailAdapter.MyViewHolder> implements OnActivityResult, DocumentViewClickListener {

    public ArrayList<JobDetailsModel> list;
    public List<JobDetailsModel> searchJobFilterList = new ArrayList<>();
    public Activity context;
    public HomeFragmentNew homeFragment;
    public HomeListener homeListener;
    public String emailId, token, userId, dob, createdBy, equipmentQTY, statusQTY, billToStatus = "0", checkStatus = "0", noteAmount, amountCheck,
            address, colorCode, statusId = "", jobNo, roleId, notificationMessage, jobId, time, formattedTime, imgPath, timeStamp;
    public final int PICK_IMAGE_CAMERA = 1;
    public int qtyCount = 1;
    public Uri selectedUri;
    public ImageView ivDoc;
    public int imagePosition, click = 1;
    public final static long MILLIS_PER_DAY = 24 * 60 * 60 * 1000L;
    public ArrayList<JobStatusModel> jobStatusList = new ArrayList<>();

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_job_detail, parent, false);

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final JobDetailsModel chijdObj = list.get(position);
        jobId = chijdObj.getJobID();

        // String isDocPaarove = jObject.getString("job_docs_status");
        // System.out.println("status doc===" + isDocPaarove);
        jobNo = list.get(position).getJobNo();


        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            holder.ivDown.setVisibility(View.GONE);
        }
        holder.txtEquipmentQTy.setVisibility(View.VISIBLE);
        holder.rlMinush.setVisibility(View.VISIBLE);
        holder.rlPlush.setVisibility(View.VISIBLE);
        holder.rlPlush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qtyCount = qtyCount + 1;
                equipmentQTY = String.valueOf(qtyCount);
                holder.txtEquipmentQTy.setText(equipmentQTY);
                statusQTY = "plus";
                updateEquipmentQTY(position);
            }
        });
        holder.rlMinush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qtyCount > 0) {
                    qtyCount = qtyCount - 1;
                    equipmentQTY = String.valueOf(qtyCount);
                    holder.txtEquipmentQTy.setText(equipmentQTY);
                    statusQTY = "minus";
                    updateEquipmentQTY(position);
                }
            }
        });
        holder.txtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + address));
                context.startActivity(searchAddress);
            }
        });
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            holder.rlBillTo.setVisibility(View.GONE);
        }


        holder.llHospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click = click + 1;
                if ((click % 2) == 0) {
                    holder.llChildDetail.setVisibility(View.VISIBLE);
                    if (chijdObj.getFacilityNote().equalsIgnoreCase("")) {
                        holder.llNote.setVisibility(View.GONE);
                    } else {
                        holder.llNote.setVisibility(View.VISIBLE);
                    }
                } else {
                    holder.llChildDetail.setVisibility(View.GONE);
                    holder.llNote.setVisibility(View.GONE);
                }
            }
        });
        holder.rlJobStatus.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                //===Manager Can not Change Job Status====//
                if (!roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
                    jobId = chijdObj.getJobID();
                    showJobStatusPopup(holder, x, y, position);
                } else {
                    holder.ivDown.setVisibility(View.GONE);
                }

//                PopupMenu popup = new PopupMenu(getActivity(), v);
//                popup.setOnMenuItemClickListener(instance);
//                popup.inflate(R.menu.job_status);
//                @SuppressLint("RestrictedApi") MenuPopupHelper menuHelper2 = new MenuPopupHelper(getActivity(), (MenuBuilder) popup.getMenu(), v);
//                menuHelper2.setForceShowIcon(true);
//                popup.show();
            }
        });
        if (list.get(position).getIsCheck().equalsIgnoreCase("2") && list.get(position).getIsBill().equalsIgnoreCase("2")) {
            holder.ivAction.setVisibility(View.GONE);
        }
        holder.ivAction.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                jobId = chijdObj.getJobID();
                showJobActionPopup(context, x, y, position);
            }

        });


        /*d-----------------------------------------------*/


        holder.txtJobNo.setText(jobNo);
        holder.txtContractorName.setText(list.get(position).getFirst_name() + " " + list.get(position).getLast_name());
        holder.txtFacilityName.setText(list.get(position).getFacilityName());
        //txtDueDate.setText("Due: " + list.get(position).getDueTimeDate());
        holder.txtRoome.setText(list.get(position).getRoomNumber());
        holder.txtPatientName.setText(list.get(position).getPatientName());
        holder.txtService.setText(list.get(position).getServiceName());
        colorCode = list.get(position).getJobStatusColor();
        holder.txtPhoneNo.setText(list.get(position).getPhone());
        holder.txtPhoneNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + list.get(position).getPhone()));
                context.startActivity(callIntent);
            }
        });
        address = list.get(position).getAddress() + " , " + list.get(position).getState() + ", " + list.get(position).getCity() + ", " + list.get(position).getZip();
        holder.txtAddress.setText(address);
        colorCode = list.get(position).getJobStatusColor();
        statusId = list.get(position).getStatusID();
        holder.rlJobDetail.setBackgroundColor(Color.parseColor(colorCode));
        holder.txtJobAcknologe.setText(list.get(position).getJobStatusName());
        holder.txtEquipmentQTy.setText(list.get(position).getEquipmentQuantity());
        qtyCount = Integer.parseInt((list.get(position).getEquipmentQuantity()));
        String createdDate = list.get(position).getCreatedOn();
        String dueDate = list.get(position).getDueTimeDate();
        createdBy = list.get(position).getCreatedBy();
        holder.txtDOB.setText(list.get(position).getDOB());
        billToStatus = list.get(position).getIsBill();
        checkStatus = list.get(position).getIsCheck();
        String checkRate = list.get(position).getCheckRate();
        String customCheckRate = list.get(position).getCustomCheckAmount();
        String billRate = list.get(position).getBillRate();
        String customBillRate = list.get(position).getCustomBillAmount();
        String updateCCA = list.get(position).getUpdateCCA();
        String updateCBA = list.get(position).getUpdateCBA();

        if (updateCCA.equalsIgnoreCase("1")) {
            holder.checkView.setVisibility(View.VISIBLE);
            holder.txtCheck.setText(" $ " + checkRate + " $ " + customCheckRate);
        } else {
            holder.txtCheck.setText(" $ " + checkRate);
        }

        if (!customBillRate.equalsIgnoreCase(Constant.CUSTOM_BILL_RATE)) {
            holder.billView.setVisibility(View.VISIBLE);
            holder.txtBill.setText(" $ " + billRate + " $ " + customBillRate);
        } else if (updateCBA.equalsIgnoreCase("1")) {
            holder.checkView.setVisibility(View.VISIBLE);
            holder.txtCheck.setText(" $ " + checkRate + " $ " + customCheckRate);
        } else {
            holder.txtBill.setText(" $ " + billRate);
        }


        //=====plush minush quantity========//

        // System.out.println("mark arrival==" + chijdObj.getMarkArrival());
        //  System.out.println("iv access supply==" + chijdObj.getIvAccessSupply());
        //  System.out.println("status==" + statusId);

        if (chijdObj.getIvAccessSupply().equalsIgnoreCase(Constant.IS_IVACCESS_SUPPLY) || statusId.equalsIgnoreCase(Constant.STATUS_COMPLETED) || statusId.equalsIgnoreCase(Constant.STATUS_CREATED)) {
            holder.rlPlush.setVisibility(View.GONE);
            holder.rlMinush.setVisibility(View.GONE);
        } else if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) || roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            holder.llQuantity.setVisibility(View.VISIBLE);
            holder.rlPlush.setVisibility(View.VISIBLE);
            holder.rlMinush.setVisibility(View.VISIBLE);
        }
        Boolean isWithinPrior24Hours = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateFormat.parse(createdDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);

        try {
//                                Instant now = Instant.now();
//                                 isWithinPrior24Hours =
//                                        (!date.toInstant().isBefore(now.minus(24, ChronoUnit.HOURS)))
//                                                &&
//                                                (date.toInstant().isBefore(now)
//                                                );
            String currentDate = String.valueOf(System.currentTimeMillis());

            //  Date strDate = dateFormat.parse(currentDate);

            String strDateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            Date strDate = dateFormat.parse(strDateString);

            String endDateString = new SimpleDateFormat("yyyy-MM-dd").format(date);
            Date endDate = dateFormat.parse(endDateString);
            //  System.out.println("current Date==" + strDate.getTime() + "Created Date==" + date.getTime());
            isWithinPrior24Hours = Math.abs(strDate.getTime() - endDate.getTime()) > MILLIS_PER_DAY;
            //  System.out.println("is24Hours==" + isWithinPrior24Hours);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  Log.i("TAG", "statusId : " + statusId + " = " + Constant.STATUS_CREATED);
        //  Log.i("TAG", "statusId : " + statusId + " = " + Constant.STATUS_ACKNLOLOG);
        //  Log.i("TAG", "roleId : " + roleId + " = " + App.MANAMER_ROLE_ID);

        // Log.i("TAG", "roleId : " + roleId + " = " + App.CONTRACTOR_ROLE_ID);
        // Log.i("TAG", "getJob_docs_status : " + chijdObj.getJob_docs_status() + " = " + Constant.IS_DOC_STATUS);
        //  Log.i("TAG", "isWithinPrior24Hours : " + isWithinPrior24Hours);
        changeJobCheckStatus(holder, chijdObj, position);
        changeJobBillToStatus(holder, chijdObj, position);

        if (statusId.equalsIgnoreCase(Constant.STATUS_CREATED)
                || statusId.equalsIgnoreCase(Constant.STATUS_ACKNLOLOG)
                || roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)
                || roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) ||
                chijdObj.getDocsApprove().equalsIgnoreCase("0")
                || !isWithinPrior24Hours) {
            //  Log.i("TAG", "In IF  : ");
            holder.ivCheck.setClickable(false);
            holder.ivBillTo.setClickable(false);
        } else {
            //  Log.i("TAG", "In else  : ");
            holder.ivCheck.setClickable(true);
            holder.ivBillTo.setClickable(true);
        }


        if (checkStatus.equalsIgnoreCase(Constant.CHECK_CREATED)) {
            holder.ivStatusInfoCheck.setVisibility(View.VISIBLE);
            holder.ivStatusInfoCheck.setImageResource(R.drawable.ic_dot_green);
        }
        if (billToStatus.equalsIgnoreCase(Constant.BILLTO_CREATED)) {
            holder.ivStatusInfoBill.setVisibility(View.VISIBLE);
            holder.ivStatusInfoBill.setImageResource(R.drawable.ic_dot_green);
        }


        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            holder.relativeCheck.setVisibility(View.GONE);
            holder.rlTopBill.setVisibility(View.GONE);
            holder.rlTop.setVisibility(View.GONE);
        }
        holder.txtBillTo.setText("Bill To " + chijdObj.getBillName());
        String note = chijdObj.getFacilityNote();
        if (note.equalsIgnoreCase("")) {
            holder.llNote.setVisibility(View.GONE);
        } else {
            holder.txtNoteName.setText(chijdObj.getFacilityNote());
        }
        if (dueDate.equalsIgnoreCase("0000-00-00 00:00:00") || dueDate.equalsIgnoreCase("1970-01-01 00:00:00")) {
            holder.txtDueDate.setText("");
        } else {
            dateConvertFormate(holder.txtDueDate, dueDate, "Due:");
        }
        if (TextUtils.isEmpty(createdDate)) {
            holder.txtCreatedDate.setText("");
        } else {
            dateConvertFormate(holder.txtCreatedDate, createdDate, "Created:");
        }
        final DocumentListAdapter documentListAdapter = new DocumentListAdapter(list.get(position).documentList, this,context, position);


        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        holder.rvDocument.setHasFixedSize(true);
                        holder.rvDocument.setNestedScrollingEnabled(false);

                        LinearLayoutManager horizontalLayoutManagaer
                                = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                        //  holder.rvDocument.setItemAnimator(new DefaultItemAnimator());
                        holder.rvDocument.setLayoutManager(horizontalLayoutManagaer);
                        holder.rvDocument.setAdapter(documentListAdapter);
                    }
                });
            }
        };
        new Thread(runnable).start();

        if ((list.get(position).getDocsApprove().equalsIgnoreCase(Constant.One))) {
            holder.txtStatusName.setText("DOCS approved");
            holder.ivStatusInfo.setImageResource(R.drawable.ic_right_green);
        } else {
            holder.ivStatusInfo.setImageResource(R.drawable.ic_exclamantion);
            holder.txtStatusName.setText("DOCS Not approved");
        }
        if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {

            holder.ivDocStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list.get(position).getDocsApprove().equalsIgnoreCase(Constant.Zero)) {
                        homeListener.approvDocStatus(emailId, token, userId, chijdObj.getJobID(), position);
                    } else if (list.get(position).getDocsApprove().equalsIgnoreCase(Constant.One)) {
                        homeListener.denyDocStatus(emailId, token, userId, chijdObj.getJobID(), position);
                    }
                }
            });

        }

        if (String.valueOf(chijdObj.getJob_docs_status()).equalsIgnoreCase(Constant.IS_DOC_STATUS)) {
            holder.ivDocStatus.setClickable(true);
        } else if (chijdObj.documentList.size() == 0) {
            holder.ivDocStatus.setClickable(true);

        } else {
            holder.ivDocStatus.setClickable(false);
        }


        LogHistoryAdapter LogListAdapter = new LogHistoryAdapter(chijdObj.logList, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.rvLogHistory.setHasFixedSize(true);
        holder.rvLogHistory.setNestedScrollingEnabled(false);
        holder.rvLogHistory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.rvLogHistory.setLayoutManager(mLayoutManager);
        holder.rvLogHistory.setAdapter(LogListAdapter);
        LogListAdapter.notifyDataSetChanged();


        if (String.valueOf(chijdObj.getJob_docs_status()).equalsIgnoreCase(Constant.IS_DOC_STATUS)) {
            holder.ivDocStatus.setClickable(true);
        } else if (list.get(position).documentList.size() == 0) {
            holder.ivDocStatus.setClickable(true);

        } else {
            holder.ivDocStatus.setClickable(false);
        }


        holder.rlJobNo.setBackgroundResource(R.drawable.gray_sqare_round);
        GradientDrawable gd = (GradientDrawable) holder.rlJobNo.getBackground().getCurrent();
        gd.setColor(Color.parseColor(colorCode));
        gd.setCornerRadii(new float[]{10, 10, 10, 10, 10, 10, 10, 10});
        gd.setStroke(2, Color.parseColor("#000000"), 5, 0);

        if (list.get(position).getIsArchive().equalsIgnoreCase("1")) {
            holder.rlJobStatus.setClickable(false);
            holder.rlPlush.setClickable(false);
            holder.rlMinush.setClickable(false);
            holder.ivAction.setClickable(false);
            holder.ivDocStatus.setClickable(false);
            holder.ivCheck.setClickable(false);
            holder.ivBillTo.setClickable(false);
        } else {
            holder.rlJobStatus.setClickable(true);
            holder.rlPlush.setClickable(true);
            holder.rlMinush.setClickable(true);
            holder.ivAction.setClickable(true);
            holder.ivDocStatus.setClickable(true);
            holder.ivCheck.setClickable(true);
            holder.ivBillTo.setClickable(true);
        }
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchJobFilterList);
        } else {
            for (JobDetailsModel bean : searchJobFilterList) {
                if (bean.getFacilityName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                } else if (bean.getFirst_name().toLowerCase(Locale.getDefault()).contains(charText)) {
                    list.add(bean);
                } else if (bean.getJobNo().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public HomeJobDetailAdapter(Activity context, ArrayList<JobDetailsModel> featuredJobList, HomeFragmentNew homeFragment, ArrayList<JobStatusModel> jobStatusList) {
        this.list = featuredJobList;

        this.context = context;
        this.homeFragment = homeFragment;
        this.searchJobFilterList.addAll(list);
        this.homeListener = (HomeListener) homeFragment;
        this.jobStatusList = jobStatusList;
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(context, App.ROLEID, "");
    }

    @Override
    public void recyclerViewListClicked(View v, int position, int groupPos, int childPos) {
        imagePosition = position;

        dialogDocImage(v, position, groupPos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RecyclerView rvDocument, rvLogHistory, rvJobStatus;
        public LinearLayout llChildDetail, llHospital;
        public RecyclerView.LayoutManager layoutManager;
        public TextView txtJobAcknologe, txtCreatedDate, txtDueDate, txtFacilityName, txtPatientName,
                txtDOB, txtRoome, txtService, txtContractorName, txtJobNo, txtPhoneNo, txtAddress,
                txtBillTo, txtStatusName, txtEquipmentQTy, txtCheck, txtBill, txtNoteName;
        public RelativeLayout rlPlush, rlMinush, rlJobDetail, rlJobStatus, rlBillTo, rlTop, rlTopBill, relativeCheck;
        public CardView rlJobNo;
        public ImageView ivAction, ivDocStatus, ivDoc, ivStatusInfo, ivStatusInfoBill, ivStatusInfoCheck,
                ivBillTo, ivCheck, ivDown;
        public LinearLayout llQuantity, llNote;
        public View checkView, billView;

        public MyViewHolder(View convertView) {
            super(convertView);

            llChildDetail = convertView.findViewById(R.id.llChildDetail);
            llHospital = convertView.findViewById(R.id.llHospital);
            rvDocument = convertView.findViewById(R.id.rvDocList);
            rvLogHistory = convertView.findViewById(R.id.rvLogHistory);
            ivAction = convertView.findViewById(R.id.iv_action);
            txtJobAcknologe = convertView.findViewById(R.id.txtJobAcknologe);
            txtContractorName = convertView.findViewById(R.id.txtContractorName);
            txtDOB = convertView.findViewById(R.id.txtDOB);
            txtCreatedDate = convertView.findViewById(R.id.txtCreatedDate);
            txtFacilityName = convertView.findViewById(R.id.txtFacilityName);
            txtDueDate = convertView.findViewById(R.id.txtDueDate);
            txtRoome = convertView.findViewById(R.id.txtRoome);
            txtPatientName = convertView.findViewById(R.id.txtPatientName);
            txtService = convertView.findViewById(R.id.txtService);
            txtJobNo = convertView.findViewById(R.id.txtJobNo);
            txtPhoneNo = convertView.findViewById(R.id.txtPhoneNo);
            txtAddress = convertView.findViewById(R.id.txtAddress);
            rlJobDetail = convertView.findViewById(R.id.rlJobDetail);
            rlJobNo = convertView.findViewById(R.id.rv_jobNo);
            rlJobStatus = convertView.findViewById(R.id.rlJobStatus);
            rlBillTo = convertView.findViewById(R.id.relativeBill);
            txtBillTo = convertView.findViewById(R.id.txtBillTo);
            ivStatusInfo = convertView.findViewById(R.id.ivStatusInfo);
            txtStatusName = convertView.findViewById(R.id.txtStatusName);
            txtEquipmentQTy = convertView.findViewById(R.id.txtEquipmentQTy);
            rlPlush = convertView.findViewById(R.id.rl_plush);
            rlMinush = convertView.findViewById(R.id.rl_minush);
            ivStatusInfoBill = convertView.findViewById(R.id.ivStatusInfoBill);
            ivStatusInfoCheck = convertView.findViewById(R.id.ivStatusInfoCheck);
            ivCheck = convertView.findViewById(R.id.ivCheck);
            ivBillTo = convertView.findViewById(R.id.ivBillTo);
            llQuantity = convertView.findViewById(R.id.relativeQty);
            txtCheck = convertView.findViewById(R.id.txtCheck);
            txtBill = convertView.findViewById(R.id.txtBill);
            checkView = convertView.findViewById(R.id.checkView);
            billView = convertView.findViewById(R.id.billView);
            ivDown = convertView.findViewById(R.id.ic_down);
            rlTop = convertView.findViewById(R.id.rlTop);
            rlTopBill = convertView.findViewById(R.id.rlTopBill);
            relativeCheck = convertView.findViewById(R.id.relativeCheck);
            llNote = convertView.findViewById(R.id.llNote);
            txtNoteName = convertView.findViewById(R.id.txtNoteName);
            ivDocStatus = convertView.findViewById(R.id.ivdoc);
        }


    }


    private void updateEquipmentQTY(int groupPos) {
        homeListener.updateEquipmentQTY(emailId, token, statusQTY, jobId, groupPos);
    }


    private void showJobStatusPopup(MyViewHolder holder, int x, int y, final int groupPos) {
        final PopupWindow popup = new PopupWindow(context);
        RelativeLayout viewGroup = homeFragment.view.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_custom_job_status, viewGroup);

        JobStatusAdapter jobStatusAdapter;
        jobStatusAdapter = new JobStatusAdapter(context, jobStatusList);
        holder.rvJobStatus = layout.findViewById(R.id.jobStatus);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.rvJobStatus.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.rvJobStatus.setItemAnimator(new DefaultItemAnimator());
        holder.rvJobStatus.setLayoutManager(mLayoutManager);
        holder.rvJobStatus.setAdapter(jobStatusAdapter);
        //jobStatusAdapter.notifyDataSetChanged();
        holder.rvJobStatus.addOnItemTouchListener(new RecyclerTouchListener(context, holder.rvJobStatus, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                int oldStausId = 0, newStatusId = 0;
                oldStausId = Integer.parseInt(statusId);
                statusId = jobStatusList.get(position).getJSID();
                newStatusId = Integer.parseInt(statusId);
                if (statusId.equals(Constant.STATUS_CANCELLED)) {
                    dialogAddJobStatusNote(groupPos, statusId);
                } else if (statusId.equals(Constant.STATUS_SERVICE_NOT_RENDER)) {
                    dialogAddJobStatusNote(groupPos, statusId);
                } else {

                    // if(oldStausId<newStatusId) {
                    homeListener.updateJobStatus(emailId, token, userId, jobId, statusId, "", groupPos);
                    // }
                }
                popup.dismiss();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
    }

    private void dialogAddJobStatusNote(final int groupPos, final String statusId) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString().trim();
                if (note.equals("")) {
                    Utils.showToast("Please add Note", context);
                } else {
                    homeListener.updateJobStatus(emailId, token, userId, jobId, statusId, note, groupPos);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showJobActionPopup(final Context context, int x, int y, final int position) {
        final PopupWindow popup = new PopupWindow(homeFragment.getActivity());
        RelativeLayout viewGroup = homeFragment.getActivity().findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) homeFragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_job_action, viewGroup);
        LinearLayout markArrivalTime = layout.findViewById(R.id.MarkArrivalTime);
        LinearLayout AddNote = layout.findViewById(R.id.AddNote);
        final LinearLayout AddDoc = layout.findViewById(R.id.AddDoc);
        LinearLayout SendNotification = layout.findViewById(R.id.SendNotification);
        LinearLayout EditJob = layout.findViewById(R.id.EditJob);
        LinearLayout Print = layout.findViewById(R.id.Print);
        LinearLayout CheckAmount = layout.findViewById(R.id.CheckAmount);
        LinearLayout InvoiceAmount = layout.findViewById(R.id.InvoiceAmount);
        LinearLayout BillingDate = layout.findViewById(R.id.BillingDate);
        LinearLayout Archive = layout.findViewById(R.id.Archive);
        LinearLayout Delete = layout.findViewById(R.id.Delete);
        LinearLayout de_Archive = layout.findViewById(R.id.de_Archive);


        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) && !createdBy.equalsIgnoreCase(userId)) {
            // EditJob.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
        }


        //======for contractor login=====
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            Print.setVisibility(View.GONE);
            CheckAmount.setVisibility(View.GONE);
            SendNotification.setVisibility(View.GONE);
            //   SendNotification.setVisibility(View.GONE);
            InvoiceAmount.setVisibility(View.GONE);
            BillingDate.setVisibility(View.GONE);
            Archive.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
        }
        // ======for Manager Login======

        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            markArrivalTime.setVisibility(View.GONE);
            AddDoc.setVisibility(View.GONE);
            AddNote.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
            CheckAmount.setVisibility(View.GONE);
            InvoiceAmount.setVisibility(View.GONE);
            EditJob.setVisibility(View.GONE);
            Archive.setVisibility(View.GONE);
            SendNotification.setVisibility(View.GONE);

        }


        //  edit job visibility gone this point is temperory if check or bill is sink then edit job visibility is gone

        if (billToStatus.equalsIgnoreCase(Constant.BILLTO_RIGHT) && checkStatus.equalsIgnoreCase(Constant.CHECK_RIGHT)) {
            EditJob.setVisibility(View.GONE);
        }

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) && billToStatus.equalsIgnoreCase(Constant.BILLTO_CREATED) && checkStatus.equalsIgnoreCase(Constant.CHECK_CREATED)) {
            EditJob.setVisibility(View.GONE);
        }
        if (list.get(position).getIsArchive().equalsIgnoreCase("1")) {
            markArrivalTime.setVisibility(View.GONE);
            AddDoc.setVisibility(View.GONE);
            AddNote.setVisibility(View.GONE);
            SendNotification.setVisibility(View.GONE);
            EditJob.setVisibility(View.GONE);
            Print.setVisibility(View.GONE);
            CheckAmount.setVisibility(View.GONE);
            InvoiceAmount.setVisibility(View.GONE);
            BillingDate.setVisibility(View.GONE);
            Archive.setVisibility(View.GONE);
            Delete.setVisibility(View.GONE);
            de_Archive.setVisibility(View.VISIBLE);
        }

        /*===========when job status is completed then contractor can not be able to add document========*/
//        if (statusId.equalsIgnoreCase(Constant.STATUS_COMPLETED)) {
//            AddDoc.setVisibility(View.GONE);
//        }
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);

        markArrivalTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                selectDateEndTimePicker(position);
            }
        });
        AddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                dialogAddNote(position);
            }
        });
        AddDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgPath = "";
                popup.dismiss();
                dialogAddDoc(position);
            }
        });
        SendNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                dialogSendNotification(position);
            }
        });
        EditJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                editJob(list.get(position));
            }
        });
        CheckAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                checkAmount(position);
            }
        });

        InvoiceAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                invoiceAmount(position);
            }
        });
        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) && createdBy.equalsIgnoreCase(userId)) {
                    popup.dismiss();
                    deleteJob(position);
                } else if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID)) {
                    popup.dismiss();
                    deleteJob(position);
                } else {
                    Utils.showToast("Not Editable", context);
                }
            }
        });
        BillingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                selectBillToDate(position);
            }
        });
        Print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                homeListener.printJob(emailId, token, jobId, position);
            }
        });

        Archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                homeListener.Archive(emailId, token, jobId, position);
            }
        });
        de_Archive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                homeListener.DeArchive(emailId, token, jobId, position);
            }
        });


    }

    public void selectDateEndTimePicker(final int groupPos) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_select_date_and_time);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        final EditText edtDate = dialog.findViewById(R.id.edtDate);
        final EditText edtTime = dialog.findViewById(R.id.edtTime);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = df.format(c);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        String str = sdf.format(new Date());


        edtDate.setText(formattedDate);
        edtTime.setText(str);
        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(homeFragment.view.getWindowToken(), 0);
                Utils.generateDatePicker(context, edtDate);
            }
        });
        edtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTime(edtTime);
                // Utils.generateDatePicker(getActivity(),edtDate);
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String Date = edtDate.getText().toString().trim();
                time = edtTime.getText().toString().trim();

                if (Date.equalsIgnoreCase("")) {
                    Utils.showToast("Select Date ", context);
                } else if (time.equalsIgnoreCase("")) {
                    Utils.showToast("Select Time", context);
                } else {
                    String sendingDate = null;
                    try {
                        StringBuilder dateFormBuilder = new StringBuilder();
                        dateFormBuilder = dateFormBuilder.append(Date.substring(6)).append("-").append(Date, 0, 2).append("-").append(Date, 3, 5);
                        sendingDate = String.valueOf(dateFormBuilder);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String FianlDatAndTime = sendingDate + " " + time;

                    homeListener.markArrivalTime(emailId, token, userId, jobId, FianlDatAndTime, groupPos);
                    // new CallRequest(context).markArrivalTime(emailId, token, userId, jobId, FianlDatAndTime);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    public void getTime(final EditText edt) {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time = selectedHour + ":" + selectedMinute;
                Log.d("time==", time);
                SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
                Date date = null;
                try {
                    date = fmt.parse(time);
                } catch (ParseException e) {

                    e.printStackTrace();
                }
                SimpleDateFormat fmtOut = new SimpleDateFormat("hh:mm aa");
                formattedTime = fmtOut.format(date);
                Log.d("formattedTime==", formattedTime);
                edt.setText(formattedTime);
            }

        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void dialogAddNote(final int groupPos) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString().trim();
                if (note.equals("")) {
                    Utils.showToast("Please add Note", context);
                } else {
                    homeListener.addJobNote(emailId, token, userId, jobId, note, groupPos);
                    //new CallRequest(instance).addJobNote(emailId, token, userId, jobId, note);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dialogDocImage(View v, int position, final int groupPos) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_doc_image);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        RelativeLayout relativeImage = dialog.findViewById(R.id.relativeImage);
        ViewPager mViewPager = dialog.findViewById(R.id.pager);

        ImageView ivApprove = dialog.findViewById(R.id.ivApprove);
        ImageView ivDeny = dialog.findViewById(R.id.ivDeny);
        ImageView ivDelete = dialog.findViewById(R.id.ivDelete);
        ImageView ivPdf = dialog.findViewById(R.id.ivPdf);
        ImageView ivClose = dialog.findViewById(R.id.ivClose);
        ImageView ivLeft = dialog.findViewById(R.id.ivLeft);
        ImageView ivRight = dialog.findViewById(R.id.ivRight);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                imagePosition = position;
            }

            @Override
            public void onPageSelected(int position) {
                imagePosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
            }
        });
        ivApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                homeListener.documentApprove(emailId, token, userId, list.get(groupPos).documentList.get(imagePosition).getJDID(), "Approve", groupPos);
                dialog.dismiss();
            }
        });
        ivDeny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                homeListener.documentDeny(emailId, token, userId, list.get(groupPos).documentList.get(imagePosition).getJDID(), "Deny", groupPos);
                dialog.dismiss();
            }
        });
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (list.get(groupPos).documentList.size() == 0) {
                    dialog.cancel();
                } else {
                    final MaterialDialog dialog = new MaterialDialog.Builder(context)
                            .content("Are you sure you want to Delete?")
                            .positiveText("Ok")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    homeListener.documentDelete(emailId, token, userId, list.get(groupPos).documentList.get(imagePosition).getJDID(), groupPos);
                                    dialog.dismiss();
                                }
                            })
                            .negativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .build();
                    dialog.show();
                }

            }
        });
        ivPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (list.get(groupPos).documentList.get(imagePosition).getIsPrint().equals(Constant.DOCUMENT_ISPRINT)) {
                    homeListener.printDocument(emailId, token, list.get(groupPos).documentList.get(imagePosition).getJDID(), userId, Constant.DOCUMENT_ISNOTPRINT, groupPos);
                    dialog.dismiss();
                } else {
                    homeListener.printDocument(emailId, token, list.get(groupPos).documentList.get(imagePosition).getJDID(), userId, Constant.DOCUMENT_ISPRINT, groupPos);
                    dialog.dismiss();
                }
            }
        });
        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //holder.mViewPager.setCurrentItem(getItem(1), true);
            }
        });


        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            ivApprove.setVisibility(View.GONE);
            ivDeny.setVisibility(View.GONE);
            if (list.get(groupPos).documentList.size() != 0) {
                if (list.get(groupPos).getDocumentStatus().equals(Constant.DOCUMENT_DENY) || list.get(groupPos).getDocumentStatus().equals(Constant.DOCUMENT_APPROVE)) {
                    ivDelete.setVisibility(View.GONE);
                } else {
                    ivDelete.setVisibility(View.VISIBLE);
                }
            }
        } else {
            ivApprove.setVisibility(View.VISIBLE);
            ivDeny.setVisibility(View.VISIBLE);
        }


        //==== Manager Only Views Document====

        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            ivApprove.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
            ivDeny.setVisibility(View.GONE);
            ivPdf.setVisibility(View.GONE);
        }
        //=====contractor can not able to click on blue pdf icon=====
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            ivPdf.setVisibility(View.GONE);
        }
        if (list.get(position).getIsArchive().equalsIgnoreCase("1")) {
            ivApprove.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
            ivDeny.setVisibility(View.GONE);
            ivPdf.setVisibility(View.GONE);
        } else {
            ivApprove.setVisibility(View.VISIBLE);
            ivDelete.setVisibility(View.VISIBLE);
            ivDeny.setVisibility(View.VISIBLE);
            ivPdf.setVisibility(View.VISIBLE);
        }
        DocumentImagePagerAdapter photoViewAdapter = new DocumentImagePagerAdapter(context, list.get(groupPos).documentList);
        mViewPager.setAdapter(photoViewAdapter);
        mViewPager.setCurrentItem(position);


        dialog.show();
    }


    public void dialogAddDoc(final int groupPos) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_doc);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        LinearLayout linearAddDoc = dialog.findViewById(R.id.linearAddDoc);
        //txtFileName = dialog.findViewById(R.id.txtFileName);
        ivDoc = dialog.findViewById(R.id.ivDoc);
        Button dialogButton = dialog.findViewById(R.id.btnOk);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imgPath.equalsIgnoreCase("")) {
                    Utils.showToast("Please Select Image", context);
                } else {
                    dialog.dismiss();
                    homeListener.addJobDoc(emailId, token, jobId, userId, imgPath, groupPos);
                    // new CallRequest(instance).addJobDoc(emailId, token, jobId, userId, imgPath);

                }
            }
        });

        linearAddDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.homeAddDoc = true;
                if (Build.VERSION.SDK_INT >= 23) {
                    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    if (!hasPermissions(context, PERMISSIONS)) {
                        ActivityCompat.requestPermissions(context, PERMISSIONS, PICK_IMAGE_CAMERA);

                    } else {

                        //CropImage.startPickImageActivity(homeFragment.getActivity());
                        selectImage();
                    }
                } else {

                    //CropImage.startPickImageActivity(homeFragment.getActivity());
                    selectImage();
                }


            }
        });
        dialog.show();
    }

    public void selectImage() {
        try {
            //  final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            final CharSequence[] options = {"Take Photo"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
            builder.setTitle("Select Option");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();

                        File f = new File(Environment.getExternalStorageDirectory() + "/IVACESS/images");
                        if (!f.exists()) {
                            f.mkdirs();
                        }
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        timeStamp = String.valueOf(System.currentTimeMillis());
                        File file = new File(Environment.getExternalStorageDirectory(), "IVACESS/images/img_" + timeStamp + ".jpg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        App.homeAddDoc = true;
                        homeFragment.startActivityForResult(intent, PICK_IMAGE_CAMERA);


                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(context, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("TAG", "Adapter : " + resultCode);
        if (requestCode == PICK_IMAGE_CAMERA && resultCode != 0) {
            ByteArrayOutputStream bytes = null;
            try {
                imgPath = getPath(selectedUri);
                //ivDoc.setImageURI(selectedUri);
               /* Bitmap photo = BitmapFactory.decodeFile(imgPath);
                photo = Bitmap.createScaledBitmap(photo, 572, 572, false);
                bytes = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
               /* File f = new File(Environment.getExternalStorageDirectory(), "IVACESS/images/img_" + timeStamp + ".jpg");
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
                fo.close();
                Uri imageUri = Uri.parse(Environment.getExternalStorageDirectory()+ "IVACESS/images/img_" + timeStamp + ".jpg");
*/
                if (CropImage.isReadExternalStoragePermissionsRequired(homeFragment.getActivity(), selectedUri)) {

                    imgPath = selectedUri.getPath();

                    homeFragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                } else {
                    App.homeAddDoc = true;
                    startCropImageActivity(selectedUri);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                // ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(homeFragment.getActivity(), data);

            if (CropImage.isReadExternalStoragePermissionsRequired(homeFragment.getActivity(), imageUri)) {
                selectedUri = imageUri;
                imgPath = selectedUri.getPath();

                homeFragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                App.homeAddDoc = true;
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    App.homeAddDoc = false;
                    ivDoc.setImageURI(result.getUri());
                    selectedUri = result.getUri();
                    imgPath = selectedUri.getPath();
                    Log.i("TAG", "logoPath :-> " + imgPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }


    }

    private void startCropImageActivity(Uri imageUri) {
        App.homeAddDoc = true;
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                //  .setAspectRatio(1, 1)
                .setFixAspectRatio(false)

                .setMultiTouchEnabled(true)
                .start(context);
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    public void dialogSendNotification(final int groupPos) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_send_notification);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button dialogButton = dialog.findViewById(R.id.btnSend);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        TextView txtHeader = dialog.findViewById(R.id.txtHeader);
        txtHeader.setText("Send Notification Job# -" + jobNo);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificationMessage = edtNotes.getText().toString();
                if (notificationMessage.equals("")) {
                    Utils.showToast("Please Add Note", context);
                } else {
                    dialog.dismiss();
                    homeListener.sendJobNotification(emailId, token, userId, jobId, notificationMessage, groupPos);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void editJob(JobDetailsModel obj) {
//        Intent i = new Intent(getContext(), AddJobActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putInt("value", 1);
//        bundle.putSerializable("JobDetailModel", jobDetailsModel);
//        i.putExtras(bundle);
//        startActivity(i);
        AddJobActivity fragment = new AddJobActivity();
        Bundle args = new Bundle();
        args.putInt("value", 1);
        args.putSerializable("JobDetailModel", obj);
        fragment.setArguments(args);
        ((MainActivity)context).changeFragment(fragment, true);
    }

    private void checkAmount(final int groupPos) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_check_amount);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        final EditText edtNotes = dialog.findViewById(R.id.edtNote);
        final EditText edtAmount = dialog.findViewById(R.id.edtAmount);
        TextView txtHeader = dialog.findViewById(R.id.txtHeader);

        txtHeader.setText("Custom Check Amount");
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteAmount = edtNotes.getText().toString();
                amountCheck = edtAmount.getText().toString();
                if (amountCheck.equals("")) {
                    Utils.showToast("Please Add Note", context);
                } else if (noteAmount.equals("")) {
                    Utils.showToast("Please Add Amount", context);
                } else {
                    dialog.dismiss();
                    homeListener.addCheckAmount(emailId, token, userId, jobId, amountCheck, noteAmount, groupPos);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void invoiceAmount(final int groupPos) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_check_amount);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        TextView txtHeader = dialog.findViewById(R.id.txtHeader);
        txtHeader.setText("Custom Invoice Amount");
        final EditText edtNotes = dialog.findViewById(R.id.edtNote);
        final EditText edtAmount = dialog.findViewById(R.id.edtAmount);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteAmount = edtNotes.getText().toString();
                amountCheck = edtAmount.getText().toString();
                if (amountCheck.equals("")) {
                    Utils.showToast("Please Add Note", context);
                } else if (noteAmount.equals("")) {
                    Utils.showToast("Please Add Amount", context);
                } else {
                    dialog.dismiss();
                    homeListener.addInvoiceAmount(emailId, token, userId, jobId, amountCheck, noteAmount, groupPos);
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void deleteJob(final int groupPos) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        homeListener.deleteJob(emailId, token, jobId, userId, groupPos);
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    private void selectBillToDate(final int groupPos) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_select_billing_date);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnSend = dialog.findViewById(R.id.btnSend);
        final EditText edtDate = dialog.findViewById(R.id.edtDate);
        // final EditText edtTime = dialog.findViewById(R.id.edtTime);
        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(homeFragment.view.getWindowToken(), 0);
                Utils.generateDatePicker(context, edtDate);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Date = edtDate.getText().toString().trim();

                if (Date.equalsIgnoreCase("")) {
                    Utils.showToast("Select Date ", context);
                } else {
                    String sendingDate = null;
                    try {
                        StringBuilder dateFormBuilder = new StringBuilder();
                        dateFormBuilder = dateFormBuilder.append(Date.substring(6)).append("-").append(Date, 0, 2).append("-").append(Date, 3, 5);
                        sendingDate = String.valueOf(dateFormBuilder);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    homeListener.billingDate(emailId, token, userId, jobId, sendingDate, groupPos);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


    private void changeJobBillToStatus(final MyViewHolder holder, JobDetailsModel obj, final int groupPos) {

        if (!statusId.equalsIgnoreCase(Constant.STATUS_CREATED) && !statusId.equalsIgnoreCase(Constant.STATUS_ACKNLOLOG)) {
            if (billToStatus.equalsIgnoreCase(Constant.BILLTO_CREATED)) {
                holder.ivStatusInfoBill.setImageResource(R.drawable.ic_dot_green);

                if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {

                    holder.ivBillTo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            billToStatus = Constant.BILLTO_NOT_CREATED;
                            holder.ivStatusInfoBill.setVisibility(View.GONE);

                            jobBillApproved(groupPos);
                        }
                    });
                } else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {

                    holder.ivBillTo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.ivStatusInfoBill.setVisibility(View.VISIBLE);
                            holder.ivStatusInfoBill.setImageResource(R.drawable.ic_right_green);
                            billToStatus = Constant.BILLTO_RIGHT;
                            jobBillApproved(groupPos);
                        }
                    });

                }
            } else if (billToStatus.equalsIgnoreCase(Constant.BILLTO_NOT_CREATED)) {
                holder.ivStatusInfoBill.setVisibility(View.GONE);
                if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {
                    holder.ivBillTo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            billToStatus = Constant.BILLTO_CREATED;
                            holder.ivStatusInfoBill.setVisibility(View.VISIBLE);
                            holder.ivStatusInfoBill.setImageResource(R.drawable.ic_dot_green);
                            jobBillApproved(groupPos);
                        }
                    });
                } else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {
                    holder.ivBillTo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.ivStatusInfoBill.setVisibility(View.VISIBLE);
                            holder.ivStatusInfoBill.setImageResource(R.drawable.ic_right_green);
                            billToStatus = Constant.BILLTO_RIGHT;
                            jobBillApproved(groupPos);
                        }
                    });
                }
            } else {
                holder.ivStatusInfoBill.setVisibility(View.VISIBLE);
                holder.ivStatusInfoBill.setImageResource(R.drawable.ic_right_green);
                holder.ivBillTo.setClickable(false);
            }


        }
    }


    private void changeJobCheckStatus(final MyViewHolder holder, JobDetailsModel obj, final int groupPos) {

        System.out.println("status id==" + statusId);
        if (!statusId.equalsIgnoreCase(Constant.STATUS_CREATED) && !statusId.equalsIgnoreCase(Constant.STATUS_ACKNLOLOG)) {
            if (checkStatus.equalsIgnoreCase(Constant.CHECK_CREATED)) {
                holder.ivStatusInfoCheck.setImageResource(R.drawable.ic_dot_green);
                if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {
                    holder.ivCheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            checkStatus = Constant.CHECK_NOT_CREATED;
                            holder.ivStatusInfoCheck.setVisibility(View.GONE);
                            jobCheckApproved(groupPos);
                        }
                    });
                }/* else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {
                    holder.ivCheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.ivStatusInfoCheck.setVisibility(View.VISIBLE);
                            holder.ivStatusInfoCheck.setImageResource(R.drawable.ic_right_green);
                            checkStatus = Constant.CHECK_RIGHT;
                            jobCheckApproved(groupPos);
                        }
                    });
                }*/
            } else if (checkStatus.equalsIgnoreCase(Constant.CHECK_NOT_CREATED)) {
                holder.ivStatusInfoCheck.setVisibility(View.GONE);
                if (App.multiSelectJobState.equalsIgnoreCase(Constant.CURRENT_JOB_STATE)) {

                    holder.ivCheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            checkStatus = Constant.CHECK_CREATED;
                            holder.ivStatusInfoCheck.setVisibility(View.VISIBLE);
                            holder.ivStatusInfoCheck.setImageResource(R.drawable.ic_dot_green);
                            jobCheckApproved(groupPos);
                        }
                    });
                }/* else if (App.multiSelectJobState.equalsIgnoreCase(Constant.IN_REVIEW_JOB_STATE)) {
                    holder.ivCheck.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.ivStatusInfoCheck.setImageResource(R.drawable.ic_right_green);
                            checkStatus = Constant.CHECK_RIGHT;
                            jobCheckApproved(groupPos);
                        }
                    });
                }*/
            } else {
                holder.ivStatusInfoCheck.setVisibility(View.VISIBLE);
                holder.ivStatusInfoCheck.setImageResource(R.drawable.ic_right_green);
                holder.ivCheck.setClickable(false);
            }
        }
    }

    private void jobCheckApproved(int groupPos) {
        homeListener.jobCheckApprove(emailId, token, userId, jobId, checkStatus, groupPos);
    }


    private void jobBillApproved(int groupPos) {
        homeListener.jobBillToApprove(emailId, token, userId, jobId, billToStatus, groupPos);
    }

    public void dateConvertFormate(TextView txt, String Date, String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(Date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat formatDate = new SimpleDateFormat("dd");
        DateFormat formatMonth = new SimpleDateFormat("MM");
        DateFormat formatYear = new SimpleDateFormat("yyyy");
        DateFormat formatAmPm = new SimpleDateFormat("hh:mm a");
        String finalDate = formatDate.format(date);
        String finalMonth = formatMonth.format(date);
        String finalHour = formatAmPm.format(date);
        String finalYear = formatYear.format(date);
        txt.setText(msg + " " + finalHour + " " + finalMonth + "-" + finalDate + "-" + finalYear);
    }


}
