package com.ivaccessbeta.utils;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by User on 03-05-2018.
 */

public class CustomLinearLayoutManage extends LinearLayoutManager {

    public CustomLinearLayoutManage(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);

    }

    // it will always pass false to RecyclerView when calling "canScrollVertically()" method.
    @Override
    public boolean canScrollVertically() {
        return false;
    }
}