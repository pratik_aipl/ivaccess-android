package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by User on 14-04-2018.
 */
@JsonObject
public class InvoiceGroupModel {
    @JsonField
    public String BillToID, BillName, InvoiceDate,InvoiceNo,Amount,InvoiceStatus,pdf_url;
    public ArrayList<InvoiceChildModel> childList = new ArrayList<InvoiceChildModel>();
    public ArrayList<InvoiceNoteModel> NoteList = new ArrayList<InvoiceNoteModel>();
    public ArrayList<InvoicePaymentNoteModel> NotePaymentList = new ArrayList<InvoicePaymentNoteModel>();

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    public String getInvoiceStatus() {
        return InvoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        InvoiceStatus = invoiceStatus;
    }

    public String getBillToID() {
        return BillToID;
    }

    public void setBillToID(String billToID) {
        BillToID = billToID;
    }

    public String getBillName() {
        return BillName;
    }

    public void setBillName(String billName) {
        BillName = billName;
    }

    public String getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        InvoiceDate = invoiceDate;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public ArrayList<InvoiceChildModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<InvoiceChildModel> childList) {
        this.childList = childList;
    }

    public ArrayList<InvoiceNoteModel> getNoteList() {
        return NoteList;
    }

    public void setNoteList(ArrayList<InvoiceNoteModel> noteList) {
        NoteList = noteList;
    }

    public ArrayList<InvoicePaymentNoteModel> getNotePaymentList() {
        return NotePaymentList;
    }

    public void setNotePaymentList(ArrayList<InvoicePaymentNoteModel> notePaymentList) {
        NotePaymentList = notePaymentList;
    }
}
