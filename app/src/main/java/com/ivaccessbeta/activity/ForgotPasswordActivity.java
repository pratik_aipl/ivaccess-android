package com.ivaccessbeta.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.ivaccessbeta.R;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements AsynchTaskListner {

    public EditText edtEmail;
    public ForgotPasswordActivity instanceLogin;
    public String emailForgot;
    public Button btnSend,btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        instanceLogin = this;

        btnSend=findViewById(R.id.btn_send);
        edtEmail=findViewById(R.id.edt_email);
        btnCancel=findViewById(R.id.btn_cancel);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                emailForgot = edtEmail.getText().toString().trim();
                if(TextUtils.isEmpty(emailForgot)){
                    Utils.showToast("Please Enter Email",getApplicationContext());
                }else {
                    new CallRequest(instanceLogin).forgotPassword(emailForgot);
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);

            switch (request) {
                case forgotPassword:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), instanceLogin);
                            onBackPressed();
                        }
                        else {
                            Utils.handleJSonAPIError(jObj,instanceLogin);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
