package com.ivaccessbeta.fragment.Directory;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class ByNameFragment extends Fragment implements AsynchTaskListner {
    public ArrayList<ByNameModel> list = new ArrayList<>();
    public RecyclerView rvByName;
    public String emailId, token, userId, roleId;
    public ByNameFragment instance;
    public EditText edtSearch;
    ImageView mRefresh;
    public ByNameAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_by_name, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        rvByName = view.findViewById(R.id.rvByName);
        mRefresh = view.findViewById(R.id.mRefresh);
        edtSearch = view.findViewById(R.id.edt_search);
        instance = this;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvByName.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvByName.setLayoutManager(mLayoutManager);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtSearch.setText("");
                getDirectoryByName();
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }
            }
        });

        return view;
    }

    private void getDirectoryByName() {

        new CallRequest(instance).getDirectortyByName(emailId, token, "name");

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getDirectoryByName:
                    list.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            list.clear();
                            list.addAll(LoganSquare.parseList(jObject.toString(), ByNameModel.class));
                            mAdapter = new ByNameAdapter(getActivity(),list);
                            rvByName.setItemAnimator(new DefaultItemAnimator());
                            rvByName.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getDirectoryByName();
    }
}
