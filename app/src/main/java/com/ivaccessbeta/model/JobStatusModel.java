package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 21-03-2018.
 */
@JsonObject
public class JobStatusModel {

    @JsonField
    public String JSID,JobStatusName,JobStatusColor;

    public String getJSID() {
        return JSID;
    }

    public void setJSID(String JSID) {
        this.JSID = JSID;
    }

    public String getJobStatusName() {
        return JobStatusName;
    }

    public void setJobStatusName(String jobStatusName) {
        JobStatusName = jobStatusName;
    }

    public String getJobStatusColor() {
        return JobStatusColor;
    }

    public void setJobStatusColor(String jobStatusColor) {
        JobStatusColor = jobStatusColor;
    }
}
