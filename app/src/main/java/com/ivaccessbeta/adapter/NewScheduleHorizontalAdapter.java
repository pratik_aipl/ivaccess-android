package com.ivaccessbeta.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.AddJobActivity;
import com.ivaccessbeta.fragment.schedule.NewScheduleFragment;
import com.ivaccessbeta.fragment.schedule.EditNotAvailableScheduleFragment;
import com.ivaccessbeta.fragment.schedule.EditScheduleFragment;
import com.ivaccessbeta.listners.NewNotAvailableScheduleListener;
import com.ivaccessbeta.listners.NewScheduleListener;
import com.ivaccessbeta.model.NewScheduleModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class NewScheduleHorizontalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<NewScheduleModel> mList;
    NewScheduleFragment mContext;
    private HorizontalSchedualAdapter.OnItemClickListener mItemClickListener;
    public String roleId;
    public NewScheduleListener scheduleListener;
    public NewNotAvailableScheduleListener scheduleNAListener;
    public String emailId, token, userId, scheduleStatus;
    public NewScheduleModel model;
    public TextView txtApprove, txtDeny, txtTrash, txtCopyToTomorrow;

    public NewScheduleHorizontalAdapter(ArrayList<NewScheduleModel> list, NewScheduleFragment newScheduleFragment) {
        this.mList = list;
        this.mContext = newScheduleFragment;
        this.scheduleListener = newScheduleFragment;
        this.scheduleNAListener = newScheduleFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.layout_schedule_new, parent, false);
                viewHolder = new ViewHolderAvailable(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.layout_not_available_schedule_new, parent, false);
                viewHolder = new ViewHolderNotAvailable(v2);
                break;
        }

        return viewHolder;
    }

    public class ViewHolderNotAvailable extends RecyclerView.ViewHolder {
        TextView txtFacliterName, txtTimeFrom, txtTimeTo, txtSubmitedDate, txtSubmitedBy, txtNote, txtAdminNote, txtNotAvailable;
        ImageView ivAction;
        RelativeLayout relativeSchedule;
        View v;

        ViewHolderNotAvailable(View view) {
            super(view);
            txtNotAvailable = view.findViewById(R.id.txtNotAvailable);
            txtFacliterName = view.findViewById(R.id.txtContractorName);
            txtTimeFrom = view.findViewById(R.id.txtTimeFrom);
            txtTimeTo = view.findViewById(R.id.txtTimeTo);
            relativeSchedule = view.findViewById(R.id.relativeSchedule);
            ivAction = view.findViewById(R.id.ivAction);
            txtSubmitedDate = view.findViewById(R.id.txtSubmitedDate);
            txtSubmitedBy = view.findViewById(R.id.txtSubmitedBy);
            txtNote = view.findViewById(R.id.txtNote);
            txtAdminNote = view.findViewById(R.id.txtAdminNote);
            v = view.findViewById(R.id.view);
        }
    }

    public class ViewHolderAvailable extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public TextView txtHospitalName, txtFacliterName, txtTimeTo, txtTimeFrom, txtJobStatus, txtSubmitedTime, txtNote, txtAdminNote;
        public RelativeLayout relativeSchedule;
        public ImageView ivClock, ivProfile;
        ImageView ivAction;
        View view1;

        ViewHolderAvailable(View itemView) {
            super(itemView);
            txtHospitalName = itemView.findViewById(R.id.txtHospitalName);
            txtFacliterName = itemView.findViewById(R.id.txtFacliterName);
            txtTimeTo = itemView.findViewById(R.id.txtTimeTo);
            txtTimeFrom = itemView.findViewById(R.id.txtTimeFrom);
            txtJobStatus = itemView.findViewById(R.id.txtJobStatus);
            relativeSchedule = itemView.findViewById(R.id.relativeSchedule);
            txtSubmitedTime = itemView.findViewById(R.id.txtSubmitedTime);
            ivClock = itemView.findViewById(R.id.ivClock);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            relativeSchedule = itemView.findViewById(R.id.relativeSchedule);
            ivAction = itemView.findViewById(R.id.ivAction);
            txtNote = itemView.findViewById(R.id.txtNote);
            txtAdminNote = itemView.findViewById(R.id.txtAdminNote);
            view1 = itemView.findViewById(R.id.view);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderAvailable viewHolderAddImage = (ViewHolderAvailable) holder;
                bindAvailableHolder(viewHolderAddImage, position);
                break;
            case 1:
                ViewHolderNotAvailable holderImages = (ViewHolderNotAvailable) holder;
                bindNotAvailableHolder(holderImages, position);
                break;
        }
    }

    private void bindNotAvailableHolder(ViewHolderNotAvailable holder, final int position) {
        roleId = MySharedPref.getString(mContext.getActivity(), App.ROLEID, "");
        model = mList.get(position);
        holder.txtFacliterName.setText(model.getFirst_name() + " " + model.getLast_name());
        holder.txtSubmitedBy.setText("by: " + model.getSubmittedBy());
        String cnote = model.getContractorNotes();
        String anote = model.getAdminNotes();

        if (model.isNotAvailable()) {
            holder.txtNotAvailable.setVisibility(View.VISIBLE);
        } else {
            holder.txtNotAvailable.setVisibility(View.GONE);
        }

        Utils.dateConvertFormate(holder.txtSubmitedDate, model.getCreatedOn(), "Submitted: ");
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {

            holder.txtNote.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(cnote)) {
                holder.txtNote.setText("Note : " + cnote);
                holder.txtNote.setVisibility(View.VISIBLE);
                holder.v.setVisibility(View.VISIBLE);
            } else {
                holder.txtNote.setVisibility(View.GONE);
                holder.v.setVisibility(View.GONE);
            }
        } else if (roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID)) {
            holder.v.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(anote)) {
                holder.txtNote.setText("Note : " + anote);
                holder.txtNote.setVisibility(View.VISIBLE);
                holder.v.setVisibility(View.VISIBLE);
            } else {
                holder.txtNote.setVisibility(View.GONE);
                holder.v.setVisibility(View.GONE);
            }
        } else {
            holder.txtNote.setVisibility(View.GONE);
            holder.v.setVisibility(View.GONE);
        }


        holder.itemView.setOnClickListener(v -> {
            EditNotAvailableScheduleFragment fragment = new EditNotAvailableScheduleFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("not_available_schedule", mList.get(position));
            fragment.setArguments(bundle);
            ((MainActivity) mContext.getActivity()).changeFragment(fragment, true);
        });
        scheduleStatus = model.getNotAvailableStatus();
        if (model.getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_APPROVE)) {
            holder.relativeSchedule.setBackgroundColor(Color.parseColor("#c6f7c6"));
        } else if (model.getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_DENY)) {
            holder.relativeSchedule.setBackgroundColor(Color.parseColor("#e41c1c"));
        } else {
            holder.relativeSchedule.setBackground(ContextCompat.getDrawable(mContext.getActivity(),R.drawable.light_gray_border));
        }

        holder.ivAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                showSortNotAvailablePopup(mContext.getActivity(), x, y, position);
            }
        });
        holder.txtTimeFrom.setText(model.getFromTime() + " To ");
        holder.txtTimeTo.setText(model.getToTime());
    }

    private void bindAvailableHolder(ViewHolderAvailable cellViewHolder, final int position) {

        cellViewHolder.txtHospitalName.setText("" + mList.get(position).getPlacesName());
        cellViewHolder.txtFacliterName.setText("" + mList.get(position).getFirst_name() + " " + mList.get(position).getLast_name());
        cellViewHolder.relativeSchedule.setBackgroundColor(Color.parseColor(mList.get(position).getColorCode()));
        NewScheduleModel model = mList.get(position);
        roleId = MySharedPref.getString(mContext.getActivity(), App.ROLEID, "");
        emailId = MySharedPref.getString(mContext.getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(mContext.getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(mContext.getActivity(), App.LOGGED_USER_ID, "");

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            cellViewHolder.ivAction.setVisibility(View.INVISIBLE);

            cellViewHolder.txtNote.setVisibility(View.VISIBLE);
            cellViewHolder.txtAdminNote.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(mList.get(position).getContractorNotes())) {
                cellViewHolder.txtNote.setText("Note: " + mList.get(position).getContractorNotes());
                cellViewHolder.txtNote.setVisibility(View.VISIBLE);
                cellViewHolder.view1.setVisibility(View.VISIBLE);
            } else {
                cellViewHolder.txtNote.setVisibility(View.GONE);
                cellViewHolder.view1.setVisibility(View.GONE);
            }
        } else if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            cellViewHolder.ivAction.setVisibility(View.VISIBLE);
            cellViewHolder.txtNote.setVisibility(View.VISIBLE);
            cellViewHolder.txtAdminNote.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(mList.get(position).getContractorNotes())) {
                cellViewHolder.txtNote.setText("Contractor Note: " + mList.get(position).getContractorNotes());
                cellViewHolder.txtNote.setVisibility(View.VISIBLE);
                cellViewHolder.view1.setVisibility(View.VISIBLE);
            } else {
                cellViewHolder.txtNote.setVisibility(View.GONE);
                cellViewHolder.view1.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(mList.get(position).getContractorNotes())) {
                cellViewHolder.txtAdminNote.setText("Note: " + mList.get(position).getAdminNotes());
                cellViewHolder.txtAdminNote.setVisibility(View.VISIBLE);
                cellViewHolder.view1.setVisibility(View.VISIBLE);
            } else {
                cellViewHolder.txtAdminNote.setVisibility(View.GONE);
                cellViewHolder.view1.setVisibility(View.GONE);
            }
        } else {
            cellViewHolder.view1.setVisibility(View.GONE);
            cellViewHolder.txtNote.setVisibility(View.GONE);
            cellViewHolder.txtAdminNote.setVisibility(View.GONE);
        }

        final String availableSchedule = mList.get(position).getAvailableSchedule();
        cellViewHolder.ivAction.setOnClickListener(v -> {
            int[] location = new int[2];
            v.getLocationOnScreen(location);
            int x = location[0];
            int y = location[1];
            if (availableSchedule.equals(Constant.AVAILABLE_SCHEDULE)) {
                showSortPopup(mContext.getActivity(), x, y, position);
            }
        });
        cellViewHolder.txtTimeFrom.setText(mList.get(position).getFromTime());
        cellViewHolder.txtTimeTo.setText(mList.get(position).getToTime());

        cellViewHolder.itemView.setOnClickListener(v -> {
            EditScheduleFragment fragment = new EditScheduleFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("schedule", mList.get(position));
            fragment.setArguments(bundle);
            ((MainActivity) mContext.getActivity()).changeFragment(fragment, true);
        });

    }

    @Override
    public int getItemViewType(int position) {

        if (mList.get(position).getAvailableSchedule().equalsIgnoreCase(Constant.AVAILABLE_SCHEDULE)) {
            return 0;
        } else if (mList.get(position).getAvailableSchedule().equalsIgnoreCase(Constant.NOT_AVAILABLE_SCHEDULE)) {
            return 1;
        } else {
            return 0;
        }
    }

    private void showSortNotAvailablePopup(final Activity context, int x, int y, final int position) {

        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_not_available, viewGroup);
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);

        txtApprove = layout.findViewById(R.id.txtApprove);
        txtDeny = layout.findViewById(R.id.txtDeny);
        txtTrash = layout.findViewById(R.id.txtTrash);
        txtCopyToTomorrow = layout.findViewById(R.id.txtCopyToTomorrow);
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtApprove.setVisibility(View.GONE);
            txtDeny.setVisibility(View.GONE);
        } else {
            if (mList.get(position).getNotAvailableStatus().equals(Constant.NA_SCHEDULE_APPROVE)) {
                txtApprove.setVisibility(View.GONE);
                txtDeny.setVisibility(View.GONE);
            } else if (mList.get(position).getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_DENY)) {
                txtDeny.setVisibility(View.GONE);
                txtApprove.setVisibility(View.GONE);
            } else if (mList.get(position).getNotAvailableStatus().equalsIgnoreCase(Constant.NA_SCHEDULE_PENDING)) {
                txtApprove.setVisibility(View.VISIBLE);
                txtDeny.setVisibility(View.VISIBLE);
                txtTrash.setVisibility(View.VISIBLE);
            }
        }

        txtApprove.setOnClickListener(v -> {
            popup.dismiss();
            scheduleNAListener.onApproveNotAvailableSchedule(position, mList.get(position).getScheduleID());

        });
        txtDeny.setOnClickListener(v -> {
            popup.dismiss();
            scheduleNAListener.onDenyNotAvailableSchedule(position, mList.get(position).getScheduleID());
        });

        txtTrash.setOnClickListener(v -> {
            popup.dismiss();
            scheduleNAListener.onDeleteNotAvailableSchedule(position, mList.get(position).getScheduleID());
        });
        txtCopyToTomorrow.setOnClickListener(v -> {
            popup.dismiss();
            scheduleNAListener.onCopyToTomorrowNotAvailableSchedule(position, mList.get(position).getScheduleID());
        });
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    // for both short and long click
    public void SetOnItemClickListener(final HorizontalSchedualAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void convertTo24FormateFrom(TextView txt, String timeStr) {
        SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
        SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
        String fromTime24 = null;
        try {
            fromTime24 = date12Format.format((date24Format.parse(timeStr)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txt.setText(fromTime24);

    }

    private void showSortPopup(Activity context, int x, int y, final int position) {
        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_schedule_action, viewGroup);
        final PopupWindow popup = new PopupWindow(context);
        TextView txtCopyToTomorrow, txtCopyToNextWeek, txtCopyToEntireWeek;
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
        TextView txtAddJob = layout.findViewById(R.id.txtAddJob);
        TextView txtTrash = layout.findViewById(R.id.txtTrash);
        txtCopyToEntireWeek = layout.findViewById(R.id.txtCopyToEntireWeek);
        txtCopyToTomorrow = layout.findViewById(R.id.txtCopyToTomorrow);
        txtCopyToNextWeek = layout.findViewById(R.id.txtCopyToNextWeek);
        txtAddJob.setOnClickListener(v -> {
            popup.dismiss();
            AddJobActivity fragment = new AddJobActivity();
            Bundle bundle = new Bundle();
            bundle.putInt("value", 2);
            bundle.putString("FaciliterName", mList.get(position).getFirst_name() + " " + mList.get(position).getLast_name());
            bundle.putString("date", mList.get(position).getScheduleDate());
            fragment.setArguments(bundle);
            ((MainActivity) mContext.getActivity()).changeFragment(fragment, true);

        });
        txtCopyToTomorrow.setOnClickListener(v -> {
            popup.dismiss();
            //    new CallRequest(NewScheduleFragment.instance).copyToSchedule(emailId,token,userId,mList.get(position).getScheduleID(),Constant.TOMORROW);

            scheduleListener.onCopyToTomorrowSchedule(position, mList.get(position).getScheduleID());
        });
        txtCopyToEntireWeek.setOnClickListener(v -> {
            popup.dismiss();
            scheduleListener.onCopyToEntireWeekSchedule(position, mList.get(position).getScheduleID());
        });
        txtCopyToNextWeek.setOnClickListener(v -> {
            popup.dismiss();
            scheduleListener.onCopyToNextWeekSchedule(position, mList.get(position).getScheduleID());
        });

        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            txtTrash.setVisibility(View.GONE);
        }
        txtTrash.setOnClickListener(v -> {
            popup.dismiss();
            scheduleListener.onDeleteSchedule(position, mList.get(position).getScheduleID());
        });
    }
}
