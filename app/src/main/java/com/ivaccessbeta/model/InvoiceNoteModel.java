package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 10-05-2018.
 */

@JsonObject
public class InvoiceNoteModel {

    @JsonField
    public String InvoiceNoteID,InvoiceNo,Note,CreatedOn,CreatedBy;

    public String getInvoiceNoteID() {
        return InvoiceNoteID;
    }

    public void setInvoiceNoteID(String invoiceNoteID) {
        InvoiceNoteID = invoiceNoteID;
    }

    public String getInvoiceNo() {
        return InvoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        InvoiceNo = invoiceNo;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }
}
