package com.ivaccessbeta.fragment.checks;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.CheckCustomExpandableListAdapter;
import com.ivaccessbeta.adapter.CheckSelectContractorAdapter;
import com.ivaccessbeta.fragment.JobDetailFragment;
import com.ivaccessbeta.fragment.invoice.DownloadInvoiceFragment;
import com.ivaccessbeta.listners.CheckListener;
import com.ivaccessbeta.model.CheckChildModel;
import com.ivaccessbeta.model.CheckContractorModel;
import com.ivaccessbeta.model.CheckGroupModel;
import com.ivaccessbeta.model.CheckNoteModel;
import com.ivaccessbeta.model.CheckPaymentNoteModel;
import com.ivaccessbeta.model.HomeModel;
import com.ivaccessbeta.model.JobDetailsModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.utils.Utils.converMMddyyyyFormate;

public class CheckFragment extends Fragment implements CheckListener, AsynchTaskListner {

    private static final String TAG = "CheckFragment";
    String chkDate = "", chkId = "";
    public ExpandableListView expandableListView;
    public CheckCustomExpandableListAdapter customListAdapter;
    public ArrayList<CheckGroupModel> expandableListGroup = new ArrayList<>();
    public ArrayList<CheckChildModel> expandableListChild = new ArrayList<>();
    public ArrayList<HomeModel> list = new ArrayList<>();
    public ArrayList<String> jobNoStringList = new ArrayList<>();
    public TextView txtSelectContractor, txtCreateCheck;
    public CheckSelectContractorAdapter contractorAdapter;
    public CheckFragment instance;
    public String emailId, token, userId, roleId, jobNo, jobId;
    public ArrayList<CheckContractorModel> contractorFilterList = new ArrayList<>();
    public ArrayList<String> contractorStringList = new ArrayList<>();
    public CheckChildModel groupModel2;
//    public CheckContractorModel contractorModel;
    public ArrayAdapter aaJob;
    public JobDetailsModel jobDetailsModel;
    public TextView txtService, txtFacility, txtAmount, txtNote;
    public EditText edtSearch;
    public String persons;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view;
        view = inflater.inflate(R.layout.fragment_check, container, false);
        instance = this;

        chkDate= getArguments().getString(Constant.chkDate);
        chkId= getArguments().getString(Constant.chkId);

        setHasOptionsMenu(true);
        MainActivity.toolbar_title.setText("CHECK");
        ((MainActivity) getActivity()).showToolbarAction();
        linearJobs.setVisibility(View.GONE);
        expandableListView = view.findViewById(R.id.expandableListView);
        txtSelectContractor = view.findViewById(R.id.txtSelectContractor);
        txtCreateCheck = view.findViewById(R.id.txtCreateCheck);
        edtSearch = view.findViewById(R.id.edt_search);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        txtCreateCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(App.checkContractorId)) {
                    new CallRequest(instance).addCheckToQuickBook(emailId, token, App.checkContractorId);
                } else {
                    Toast.makeText(getActivity(), "Please select contractor.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID) || roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtSelectContractor.setVisibility(View.GONE);
            txtCreateCheck.setVisibility(View.GONE);
        }

        txtSelectContractor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selcectContractor();
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                    customListAdapter.filters(text);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        });

        return view;
    }

    private void getJobList(int groupPos) {
        new CallRequest(instance).getJobListNoCheck(emailId, token, userId, expandableListGroup.get(groupPos).getId());
    }

    private void showEditJobPopup(int groupPos, int childPosition) {

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_edit_job_check);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void selcectContractor() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_contractor_filter_invoice);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        RecyclerView rvSelectContractor = dialog.findViewById(R.id.rv_contractor);
        TextView txtDialogTitle = dialog.findViewById(R.id.txtDialogTitle);
        final CheckBox cbSelectAll = dialog.findViewById(R.id.cbSelectAll);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnClear = dialog.findViewById(R.id.btnClear);
        txtDialogTitle.setText("CUSTOMERS WITH JOBS TO CHECK");

        contractorAdapter = new CheckSelectContractorAdapter(contractorFilterList, cbSelectAll);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rvSelectContractor.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvSelectContractor.setItemAnimator(new DefaultItemAnimator());
        rvSelectContractor.setLayoutManager(mLayoutManager);
        rvSelectContractor.setAdapter(contractorAdapter);
        contractorAdapter.notifyDataSetChanged();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.checkContractorId = android.text.TextUtils.join(",", App.indexCheckContractorList);
                txtCreateCheck.setText("+CREATE CHECK (" + App.indexCheckContractorList.size() + ")");
                dialog.dismiss();
            }
        });
        cbSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isSelectAll = ((CheckBox) v).isChecked();
                App.indexCheckContractorList.clear();
                if (isSelectAll) {
                    for (CheckContractorModel n : contractorFilterList)
                        App.indexCheckContractorList.add(n.getId());
                } else {
                    App.indexCheckContractorList.clear();
                }
                contractorAdapter.selectAll(isSelectAll);
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.checkContractorId = "";
                getCheckList();
                dialog.dismiss();
                txtCreateCheck.setText("+CREATE CHECK ");
                App.checkedArray.clear();
                cbSelectAll.setChecked(false);
                App.indexCheckContractorList.clear();

            }
        });
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getCheckContractor() {

        new CallRequest(instance).getCheckContractor(emailId, token);
    }

    private void popupAddCheck(final Activity context, int x, int y) {

        final PopupWindow popup = new PopupWindow(context);
        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.dialog_add_check, viewGroup);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
    }

    @Override
    public void onOpenPopupListClick(int pos, View v, CheckGroupModel checkGroupModel) {
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        showActionPopup(pos, x, y, checkGroupModel);
    }

    @Override
    public void onEditJobChildListClick(int groupPos, int pos, View v) {
        showEditJobPopup(groupPos, pos);
    }

    @Override
    public void onDeletePerticularChildClick(final int groupPos, final int pos, View v) {
        deletePerticularCheck(groupPos, pos);
    }

    @Override
    public void onAddMemoChildClick(int groupPos, int pos, View v, String memoNote, String memoAmount) {
        String contractorId = null, date = null;
        contractorId = expandableListGroup.get(groupPos).getId();
        date = expandableListChild.get(groupPos).getCheckDate();
        new CallRequest(instance).addCustomMemoCheck(emailId, token, memoAmount, contractorId, date, memoNote, userId);
    }

    @Override
    public void onDeleteNote(final int groupPos, final int pos, View v) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        new CallRequest(instance).deleteCheckNote(emailId, token, expandableListGroup.get(groupPos).getNoteList().get(pos).getCheckNoteID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();


    }

    @Override
    public void onDeletePerticularInvoice(int groupPos, int pos, View v) {

    }

    @Override
    public void onDeletePaymentNote(final int groupPos, final int pos, View v) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteCheckPaymentNote(emailId, token, expandableListGroup.get(groupPos).getNotePaymentList().get(pos).getCHeckPaymentNoteID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();

    }

    @Override
    public void onJobNoClick(int groupPos, int childPos, View v) {
        String jobId = expandableListGroup.get(groupPos).getChildList().get(childPos).getJobID();
        System.out.println("is going in job detail =====");
        JobDetailFragment fragment = new JobDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString("jobNo", jobId);
        fragment.setArguments(bundle);
        ((MainActivity)getActivity()).changeFragment(fragment, true);
    }

    private void deletePerticularCheck(final int groupPos, final int pos) {
        final String CheckId = expandableListGroup.get(groupPos).getChildList().get(pos).getCheckID();
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deletePerticularCheck(emailId, token, CheckId);
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    private void showActionPopup(final int pos, int x, int y, CheckGroupModel checkGroupModel) {

        RelativeLayout viewGroup = getActivity().findViewById(R.id.rvCheckAction);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.dialog_check_action, viewGroup);
        final PopupWindow popUp = new PopupWindow(getActivity());
        popUp.setContentView(layout);
        popUp.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popUp.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popUp.setFocusable(true);
        popUp.setBackgroundDrawable(new BitmapDrawable());
        popUp.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
        TextView txtAddJob = layout.findViewById(R.id.txtAddJob);
        TextView txtDeleteCheck = layout.findViewById(R.id.txtDeleteCheck);
        TextView txtAddNote = layout.findViewById(R.id.txtAddNote);
        TextView txtAddVoucher = layout.findViewById(R.id.txtAddVoucher);
        TextView txtPrint = layout.findViewById(R.id.txtPrint);

        if (TextUtils.isEmpty(expandableListGroup.get(pos).getPdf_url())) {
            txtPrint.setClickable(false);
        }
        if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID)) {
            txtAddVoucher.setVisibility(View.VISIBLE);
        }
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtAddJob.setVisibility(View.GONE);
            txtAddVoucher.setVisibility(View.GONE);
            txtAddNote.setVisibility(View.GONE);
        } else {
            txtAddNote.setVisibility(View.VISIBLE);
        }
        if (checkGroupModel.getQuickBookSync().equalsIgnoreCase("s")) {
            txtAddJob.setVisibility(View.GONE);
        }
        txtAddJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp.dismiss();
                getJobList(pos);
                showAddJobPopup(pos);
            }
        });
        txtDeleteCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp.dismiss();
                deleteCheck(pos);
            }
        });

        txtAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp.dismiss();
                addNoteToCheck(pos);
            }
        });

        txtAddVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp.dismiss();
                addVoucherNumberToCheck(pos);
            }
        });

        txtAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp.dismiss();
                addNoteToCheck(pos);
            }
        });

        txtPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUp.dismiss();

                if (!TextUtils.isEmpty(expandableListGroup.get(pos).getPdf_url())) {
                    Bundle bundle = new Bundle();
                    DownloadInvoiceFragment downlodInvoiceFragment = new DownloadInvoiceFragment();
                    bundle.putString("url", expandableListGroup.get(pos).getPdf_url());
                    downlodInvoiceFragment.setArguments(bundle);
                    ((MainActivity)getActivity()).changeFragment(downlodInvoiceFragment, true);
                }
            }
        });

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID) || roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            txtDeleteCheck.setVisibility(View.GONE);
        }
    }

    private void addPaymentNote(final int groupPos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_payment_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txtAddTitle = dialog.findViewById(R.id.txtAddTitle);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        final EditText edtRef = dialog.findViewById(R.id.edtRef);
        final EditText edtAmount = dialog.findViewById(R.id.edtAmount);

        Button btnOk = dialog.findViewById(R.id.btnOk);
        txtAddTitle.setText(" Add Note  " + expandableListGroup.get(groupPos).getId());
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString();
                String amount = edtAmount.getText().toString();
                String refNo = edtRef.getText().toString();

                if (TextUtils.isEmpty(note)) {
                    Utils.showToast("Please Enter Note", getContext());
                } else if (TextUtils.isEmpty(amount)) {
                    Utils.showToast("Please Enter Amount", getContext());
                } else if (TextUtils.isEmpty(refNo)) {
                    Utils.showToast("Please Enter Ref No", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addPaymentNoteToCheck(emailId, token, expandableListGroup.get(groupPos).getCheckDate(), expandableListGroup.get(groupPos).getId(), note, refNo, amount, userId);
                }
            }
        });
        dialog.show();
    }

    private void addNoteToCheck(final int groupPos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_note);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txtAddTitle = dialog.findViewById(R.id.txtAddTitle);
        final EditText edtNotes = dialog.findViewById(R.id.edtNotes);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        txtAddTitle.setText(" Add Note  " + expandableListGroup.get(groupPos).getId());
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String note = edtNotes.getText().toString();
                if (TextUtils.isEmpty(note)) {
                    Utils.showToast("Please Enter Note", getContext());
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addNoteToCheck(emailId, token, expandableListGroup.get(groupPos).getCheckDate(), expandableListGroup.get(groupPos).getId(), note, userId);
                }
            }
        });
        dialog.show();

    }

    private void addVoucherNumberToCheck(final int groupPos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_voucher);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txtAddTitle = dialog.findViewById(R.id.txtAddTitle);
        final EditText vourcherEdt = dialog.findViewById(R.id.edtVoucher);

        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        txtAddTitle.setText(" Add Voucher Number  ");
        vourcherEdt.setText(!TextUtils.isEmpty(expandableListGroup.get(groupPos).getVoucherNo()) ? expandableListGroup.get(groupPos).getVoucherNo() : "");
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.disableKeyboard(getActivity(), v);
                String voucher = vourcherEdt.getText().toString();
                if (TextUtils.isEmpty(voucher)) {
                    Utils.showToast("Please Enter Voucher Number.", getContext());
//                    vourcherEdt.setError("Please Enter Voucher Number.");
                } else {
                    dialog.dismiss();
                    new CallRequest(instance).addVoucherToCheck(emailId, token, expandableListGroup.get(groupPos).getCheckDate(), expandableListGroup.get(groupPos).getId(), voucher, userId);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.disableKeyboard(getActivity(), v);
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void deleteCheck(final int pos) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(instance).deleteAllCheck(emailId, token, expandableListGroup.get(pos).getId(), expandableListGroup.get(pos).getCheckDate());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    public void showAddJobPopup(final int pos) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_job_check);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        Spinner spJob = dialog.findViewById(R.id.spJob);
        txtService = dialog.findViewById(R.id.txtServiceName);
        txtFacility = dialog.findViewById(R.id.txtFacilityName);
        txtAmount = dialog.findViewById(R.id.txtAmount);
        Button btnOk = dialog.findViewById(R.id.btnOk);
        spJob.setAdapter(aaJob);
        spJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jobNo = list.get(position).getJobNo();
                jobId = list.get(position).getJobID();
                getSelectedJobDetail();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new CallRequest(instance).addJobToCheck(emailId, token, jobNo, expandableListGroup.get(pos).getCheckDate(), userId);
            }
        });
        dialog.show();
    }

    private void getSelectedJobDetail() {
        new CallRequest(instance).getJobDetail(emailId, token, jobId);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {

            switch (request) {
                case getCheckList:
                    Log.i("TAG", "TAG Result : " + result);
                    expandableListGroup.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            CheckGroupModel groupModel = null;
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject ob = jObject.getJSONObject(i);
                                groupModel = new CheckGroupModel();
                                groupModel = LoganSquare.parse(ob.toString(),CheckGroupModel.class);// jParsar.parseJson(ob, new CheckGroupModel());

                                JSONArray noteArray = ob.getJSONArray("Notes");
                                groupModel.NoteList.addAll(LoganSquare.parseList(noteArray.toString(),CheckNoteModel.class));//checkNoteModel);
//                                for (int j = 0; j < noteArray.length(); j++) {
//                                    CheckNoteModel checkNoteModel = new CheckNoteModel();
//                                    checkNoteModel =; (CheckNoteModel) jParsar.parseJson(noteArray.getJSONObject(j), new CheckNoteModel());
//                                }
                                JSONArray notePaymentArray = ob.getJSONArray("PaymentNotes");
                                groupModel.NotePaymentList.addAll(LoganSquare.parseList(notePaymentArray.toString(),CheckPaymentNoteModel.class));//checkNoteModel);

//                                for (int j = 0; j < notePaymentArray.length(); j++) {
//                                    CheckPaymentNoteModel checkPaymentNoteModel = new CheckPaymentNoteModel();
//                                    checkPaymentNoteModel = (CheckPaymentNoteModel) jParsar.parseJson(notePaymentArray.getJSONObject(j), new CheckPaymentNoteModel());
//                                    groupModel.NotePaymentList.add(checkPaymentNoteModel);
//                                }

                                JSONArray childArray = ob.getJSONArray("checks");
//                                expandableListChild.addAll(LoganSquare.parseList(childArray.toString(),CheckChildModel.class));//checkNoteModel);
                                for (int j = 0; childArray.length() > j; j++) {
                                    groupModel2 = new CheckChildModel();
                                    groupModel2 = LoganSquare.parse(childArray.getJSONObject(j).toString(),CheckChildModel.class);
//                                    groupModel2 = (CheckChildModel) jParsar.parseJson(childArray.getJSONObject(j), new CheckChildModel());
                                    groupModel.childList.add(groupModel2);
                                    groupModel.setQuickBookSync(groupModel2.getQuickBookSync());
                                    expandableListChild.add(groupModel2);
                                }
                                expandableListGroup.add(groupModel);
                            }
                            customListAdapter = new CheckCustomExpandableListAdapter(getActivity(), expandableListGroup, instance);
                            expandableListView.setAdapter(customListAdapter);

                            for (int i = 0; i < expandableListGroup.size(); i++) {
                                if (!TextUtils.isEmpty(chkDate) && !TextUtils.isEmpty(chkId)) {
                                    if (chkId.equalsIgnoreCase(expandableListGroup.get(i).getId()) && converMMddyyyyFormate(chkDate).equalsIgnoreCase(converMMddyyyyFormate(expandableListGroup.get(i).getCheckDate()))) {
                                        Log.d(TAG, "onTaskCompleted: expandGroup");
                                        expandableListView.expandGroup(i);
                                        expandableListView.smoothScrollToPosition(i);
                                    }
                                } else
                                    break;
                            }


                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
                case getJobListCheck:
                    list.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                HomeModel homeModel = LoganSquare.parse(jObject.getJSONObject(i).toString(),HomeModel.class);
//                                homeModel = (HomeModel) jParsar.parseJson(jObject.getJSONObject(i), new HomeModel());
                                list.add(homeModel);
                                jobNoStringList.add(homeModel.getJobNo());
                            }
                            aaJob = new ArrayAdapter(getContext(), R.layout.spinner_text, jobNoStringList);
                            aaJob.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteAllCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getCheckContractorList:
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            contractorFilterList.clear();
                            contractorStringList.clear();
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                CheckContractorModel contractorModel = LoganSquare.parse(ob.toString(),CheckContractorModel.class);
                                contractorFilterList.add(contractorModel);
                                contractorStringList.add(contractorModel.getFullname());
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getCheckList();
                    break;
                case addCustomMemoCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getJobDetail:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {

                            JSONObject jObject = jObj.getJSONObject("data");
                            jobDetailsModel = new JobDetailsModel();
                            jobDetailsModel = LoganSquare.parse(jObject.toString(),JobDetailsModel.class);// jParsar.parseJson(, new JobDetailsModel());
                            txtService.setText(jobDetailsModel.getServiceName());
                            txtFacility.setText(jobDetailsModel.getFacilityName());
                            if (jobDetailsModel.getCustomCheckAmount().equalsIgnoreCase("0.00")) {
                                txtAmount.setText(jobDetailsModel.getCheckRate());
                            } else {
                                txtAmount.setText(jobDetailsModel.getCustomCheckAmount());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addJobToCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case addNoteToCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case addVoucherToCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equalsIgnoreCase("ok")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case addPaymentNoteToCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                case deleteCheckNote:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteCheckPaymentNote:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case addCheckQuickBook:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case printCheck:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Bundle bundle = new Bundle();
                            DownloadInvoiceFragment downlodInvoiceFragment = new DownloadInvoiceFragment();
                            bundle.putSerializable("urlModel", "");
                            downlodInvoiceFragment.setArguments(bundle);
                            ((MainActivity)getActivity()).changeFragment(downlodInvoiceFragment, true);
                            // getCheckList();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getCheckContractor();
    }

    public void getCheckList() {
        try {
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                new CallRequest(instance).getCheckList(emailId, token, userId);
            } else {
                new CallRequest(instance).getCheckList(emailId, token, "");
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
