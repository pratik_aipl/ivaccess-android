package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by User on 30-03-2018.
 */
@JsonObject
public class PeofileJobScheduleModel {

    @JsonField
   public String job_date,JobID,JobNo,DueTimeDate,RoomNumber,FacilityName,first_name,last_name,JobStatusColor,CreatedOn,JobStatusName,AvailableSchedule,NotAvailableStatus;

    public String getAvailableSchedule() {
        return AvailableSchedule;
    }

    public void setAvailableSchedule(String availableSchedule) {
        AvailableSchedule = availableSchedule;
    }

    public String getNotAvailableStatus() {
        return NotAvailableStatus;
    }

    public void setNotAvailableStatus(String notAvailableStatus) {
        NotAvailableStatus = notAvailableStatus;
    }

    public ArrayList<PeofileJobScheduleModel> getSubArray() {
        return subArray;
    }

    public void setSubArray(ArrayList<PeofileJobScheduleModel> subArray) {
        this.subArray = subArray;
    }

    public String getJob_date() {
        return job_date;
    }

    public void setJob_date(String job_date) {
        this.job_date = job_date;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getJobNo() {
        return JobNo;
    }

    public void setJobNo(String jobNo) {
        JobNo = jobNo;
    }

    public String getDueTimeDate() {
        return DueTimeDate;
    }

    public void setDueTimeDate(String dueTimeDate) {
        DueTimeDate = dueTimeDate;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }

    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getJobStatusColor() {
        return JobStatusColor;
    }

    public void setJobStatusColor(String jobStatusColor) {
        JobStatusColor = jobStatusColor;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getJobStatusName() {
        return JobStatusName;
    }

    public void setJobStatusName(String jobStatusName) {
        JobStatusName = jobStatusName;
    }

    public ArrayList<PeofileJobScheduleModel>  subArray = new ArrayList<>();


}
