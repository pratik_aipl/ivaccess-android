package com.ivaccessbeta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.View;


import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.activity.SplashActivity;
import com.ivaccessbeta.model.BillToServiceModel;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.model.SchedualDateCalanderModel;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static com.ivaccessbeta.fragment.flotingNotification.services.ChatHeadService.iconView;

public class App extends MultiDexApplication {
    public static App instance;
    public static String IS_LOGGED_IN = "islogedin";
    public SharedPreferences sharedPref;
    public static String userId = "";
    public static String myPref = "IvAccess";
    public static MySharedPref mySharedPref;
    public static String maritualStatus = "";
    public static final String EMAIL = "EMAIL", APP_TOKEN = "APP_TOKEN",
            LOGGED_USER_ID = "LOGGED_USER_ID", PASSWORD = "PASSWORD", ROLEID = "ROLEID", NAME = "NAME", CONTRACTOR_ROLE_ID = "4", ADMIN_ROLE_ID = "2", MANAMER_ROLE_ID = "3", SUPER_ADMIN_ROLE_ID = "1", REGION_ID = "REGION_ID", IS_PASSWORD_CHANGE = "IS_PASSWORD_CHANGE";
    public static String selectedDate = "";
    public static String nextPreDate = "";
    public static String monthString = "";
    public static int monthNo = 0;
    public static String /*uID = "", */password = "", yearMonth = "", passwordChange = "";
    public static String notifyPersonId = "";
    public static List<Integer> indexListJobState = new ArrayList<>();
    public static List<Integer> indexListPerson = new ArrayList<>();
    public static List<Integer> indexListRegion = new ArrayList<>();
    public static List<Integer> indexListFacility = new ArrayList<>();
    public static List<Integer> indexListStatus = new ArrayList<>();
    public static List<Integer> indexListShortBy = new ArrayList<>();
    public static List<Integer> indexListPersonSchedule = new ArrayList<>();
    public static List<Integer> indexListRegionSchedule = new ArrayList<>();
    public static List<Integer> indexListPlaceSchedule = new ArrayList<>();
    public static List<Integer> indexListNotAvailable = new ArrayList<>();
    public static List<String> indexNotifyList = new ArrayList<>();
    public static List<String> indexInvoiceContractorList = new ArrayList<>();
    public static List<String> indexCheckContractorList = new ArrayList<>();
    public static String[] notAvailableList = {"Approve", "Pending", "Deny"};
    public static String  multiSelectJobState = "current", multiSelectPersonId = "", multiSelectRegionId = "",
            multiSelectPlaceId = "", multiSelectedNotAvailable = "Approve,Pending,Deny";
    public static boolean checkState = false;
    public static boolean checkStateEdit = false;
    public static boolean checkStateSchedule = false;
    public static ArrayList<String> contactorStringList = new ArrayList<>();
    public static ArrayList<ContractorModel> personList = new ArrayList<>();
    public static ArrayList<RegionModel> regionList = new ArrayList<>();
    public static ArrayList<String> regionStringList = new ArrayList<>();
    public static ArrayList<PlaceModel> placeList = new ArrayList<>();
    public static ArrayList<String> placeStringList = new ArrayList<>();
    public static ArrayList<SchedualDateCalanderModel> ScheduleFragmentList = new ArrayList<>();
    public static boolean checkEmpDocState = false, homeAddDoc = false;
    public static boolean checkEditProfileState = false;
    public static int CURRENT_SCHEDULE_DETAIL_POSITION = 0;
    public static String description;
    public static ArrayList<BillToServiceModel> serviceRateList = new ArrayList<>();
    public static String billToSreviceRate, invoiceContractorId, checkContractorId;
    public static ArrayList<String> checkedArray = new ArrayList<>();
    public static ArrayList<String> checkedArrayInvoice = new ArrayList<>();
    public static String contractorId = "";
    public static boolean isFromNotificationFragment = false;
    public static boolean isFromChatFragment = false;
    public static String title = "", jobId = "", docId = "", scheduleDate = "", contractorDocId = "", threadID = "";
    public static boolean isDialogOpen = false, isContactorDoc = false, isFilterApply = false;
    public static int tabCount;
    public static boolean isServiceRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());
        instance = this;

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            Log.i("OneSignal", "==>" + result.stringify());
                            JSONObject jsonObject = new JSONObject();
                            jsonObject = result.toJSONObject();
                            try {
                                JSONObject jNotification = jsonObject.getJSONObject("notification");
                                JSONObject jpayload = jNotification.getJSONObject("payload");
                                JSONObject additionalData = jpayload.getJSONObject("additionalData");
                                title = jpayload.getString("title");
                                iconView.setVisibility(View.VISIBLE);
                                if (title.equalsIgnoreCase(Constant.JOB_EDITED) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_NOTE) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_MARKARRIVAL) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_DOC_APPROVE) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_DOC_UPLOAD) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_ADD) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_NOTIFICATION) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_STATUS) ||
                                        App.title.equalsIgnoreCase(Constant.JOB_DOSE_NOT_ACKNLOG)) {
                                    userId = additionalData.getString("user_id");
                                    jobId = additionalData.getString("JobID");
                                    if (App.title.equalsIgnoreCase(Constant.JOB_DOC_APPROVE) || App.title.equalsIgnoreCase(Constant.JOB_DOC_UPLOAD)) {
                                        docId = additionalData.getString("doc_id");
                                    }
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                } else if (title.equalsIgnoreCase(Constant.JOB_EQUIPMENT_STATUS) || App.title.equalsIgnoreCase(Constant.EQUIPMENT_ADD)) {
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                } else if (title.equalsIgnoreCase(Constant.SCHEDULE_ADD) || App.title.equalsIgnoreCase(Constant.SCHEDULE_UPDATE)) {
                                    userId = additionalData.getString("user_id");
                                    scheduleDate = additionalData.getString("schedule_date");
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                } else if (title.equalsIgnoreCase(Constant.DOCUMENT_EXPIRED)) {
                                    contractorId = additionalData.getString("user_id");
                                    //  contractorDocId=additionalData.getString("doc_id");
                                    App.isContactorDoc = true;
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                } else if (title.equalsIgnoreCase(Constant.NOTIFY)) {
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                } else if (title.equalsIgnoreCase(Constant.Message)) {
                                    threadID = additionalData.getString("JobID");
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                    }
                })

                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();
        try {

            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                builder.detectFileUriExposure();
            }
            sharedPref = getSharedPreferences(getPackageName(), 0);
            mySharedPref = new MySharedPref(this);

            if (sharedPref.contains("user")) {
                Gson gson = new Gson();
                //  user = gson.fromJson(sharedPref.getString("user", ""), User.class);
            }
            Class.forName("android.os.AsyncTask");
            MultiDex.install(this);

        } catch (ClassNotFoundException e) {
        }

        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Lato-Medium.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build());

    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static App getInstance() {
        return instance;
    }

}
