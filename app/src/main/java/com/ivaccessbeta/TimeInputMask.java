package com.ivaccessbeta;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.ivaccessbeta.utils.Utils;

import java.util.Calendar;

public class TimeInputMask implements TextWatcher {

    private String current = "";
    private String hhmm = "hhmm";
    private Calendar cal = Calendar.getInstance();
    private EditText input;
    public Context ct;
    public boolean isEditing = true;

    public TimeInputMask(EditText input, Context mContext) {
        this.input = input;
        this.input.addTextChangedListener(this);
        this.ct = mContext;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (!s.toString().equals(current)) {
            String clean = s.toString().replaceAll("[^\\d.]|\\.", "");
            String cleanC = current.replaceAll("[^\\d.]|\\.", "");

//            int cl = clean.length();
//            int sel = cl;
//            for (int i = 2; i <= cl && i < 4; i += 2) {
//                sel++;
//            }
            //Fix for pressing delete next to a forward slash
//            if (clean.equals(cleanC)) sel--;

            if (clean.length() < 5){
                clean = clean + hhmm.substring(clean.length());
            }else{
                int hh  = Integer.parseInt(clean.substring(0,2));
                int mm  = Integer.parseInt(clean.substring(2,4));
//                mm = mm < 1 ? 1 : mm > 60 ? 00 : mm;
//                cal.set(Calendar.MINUTE, mm-1);
//                hh = (hh > cal.getActualMaximum(Calendar.HOUR))? cal.getActualMaximum(Calendar.HOUR):hh;
                clean = String.format("%02d%02d",hh, mm);
            }

            clean = String.format("%s:%s", clean.substring(0, 2),
                    clean.substring(0, 2),
                    clean.substring(2, 4));

//            sel = sel < 0 ? 0 : sel;
            current = clean;
            input.setText(current);
//            input.setSelection(sel < current.length() ? sel : current.length());
        }


    }

    @Override
    public void afterTextChanged(Editable s) {

    }


}