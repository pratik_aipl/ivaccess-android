package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.model.UserProfileModel;
import com.ivaccessbeta.App;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutProfileFragment extends Fragment {
    TextView txtAbout;
    UserProfileModel userProfileModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view= inflater.inflate(R.layout.fragment_about, container, false);
        setHasOptionsMenu(true);
        txtAbout=view.findViewById(R.id.txtAbout);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        userProfileModel=new UserProfileModel();
        txtAbout.setText(App.description);
    }
}
