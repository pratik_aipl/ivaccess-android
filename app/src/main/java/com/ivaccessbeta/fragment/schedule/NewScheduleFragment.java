package com.ivaccessbeta.fragment.schedule;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NewScheduleVerticalAdapter;
import com.ivaccessbeta.fragment.AddPushDeviderFragment;
import com.ivaccessbeta.listners.NewNotAvailableScheduleListener;
import com.ivaccessbeta.listners.NewScheduleListener;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.NewScheduleModel;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.ItemOffsetDecoration;
import com.ivaccessbeta.utils.MultiSelectionSpinnerSchedule;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.ivaccessbeta.App.nextPreDate;
import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.ivPublish;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;
import static com.ivaccessbeta.activity.MainActivity.txtTitle;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.NOT_AVAILABLE;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.PERSONS;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.PLACE;
import static com.ivaccessbeta.utils.Constant.FILTER_TYPE.REGIONS;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewScheduleFragment extends Fragment implements AsynchTaskListner, MultiSelectionSpinnerSchedule.OnMultipleItemsSelectedListener,
        AdapterView.OnItemSelectedListener, NewScheduleListener, NewNotAvailableScheduleListener {

    private static final String TAG = "NewScheduleFragment";
    public ArrayList<NewScheduleModel> subList = new ArrayList<>();
    public RecyclerView rvSchDetail;
    public static NewScheduleFragment instance;
    public String emailId, token, userId;
    public String roleId;
    public static Date myDate = null;
    public static int selectedPosition;
    public ContractorModel contractorModel;
    public static String asubstringEnd;
    public PlaceModel placeModel;
    public RegionModel regionModel;
    public ArrayAdapter aaPersons, aaRegion, aaFacility;
    public Calendar calander;
    public int year;
    public static String typeSchedule;
    public TextView txtJobState, txtShortBy, txtPersons, txtNotAvailable, txtPlace, txtStatuses, txtRegion, txtJobAcknologe, txtNoteAvailable, txtLable, txtNote;
    public int tabPostion = 0;
    public String multiSelecteStatusId;
    public MultiSelectionSpinnerSchedule spRegion, spPerson, spPlace, spNoteAvailable;
    public View viewPerson;
    public int selectedPos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_schedule, container, false);
        rvSchDetail = view.findViewById(R.id.rvSchDetail);
        instance = this;
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.VISIBLE);
        txtTitle.setText("SCHEDULE");
        MainActivity.toolbar_title.setText("");
        ivPublish.setVisibility(View.GONE);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        linearJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showJobSearchDialog();
            }
        });
        final int spacing = getResources().getDimensionPixelOffset(R.dimen.margin_10);

        rvSchDetail.addItemDecoration(new ItemOffsetDecoration(spacing));
        return view;
    }

    public void showJobSearchDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.schedule_search_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button btnOK = dialog.findViewById(R.id.btnOk);
        Button dialogClear = dialog.findViewById(R.id.btnClear);
        final TextView txtMonth = dialog.findViewById(R.id.txt_month);
        LinearLayout llPrevious = dialog.findViewById(R.id.llPrevious);
        TextView txtToday = dialog.findViewById(R.id.txtToday);
        TextView txt_month = dialog.findViewById(R.id.txt_month);
        RelativeLayout relativeNextPre = dialog.findViewById(R.id.relativeNextPre);
        LinearLayout llNext = dialog.findViewById(R.id.llNext);

        //=====contractor login next previous visibility gone=====
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txt_month.setVisibility(View.GONE);
            relativeNextPre.setVisibility(View.GONE);
        }
        //============================================
        long date = System.currentTimeMillis();
        calander = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM  yyyy");
        final SimpleDateFormat month = new SimpleDateFormat("MM");
        final SimpleDateFormat yearFormate = new SimpleDateFormat("yyyy");


        if (App.yearMonth.equalsIgnoreCase("")) {
            year = (calander.get(Calendar.YEAR));
        } else {
            try {
                year = Integer.parseInt(yearFormate.format(dateFormat.parse(App.yearMonth)));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (App.monthNo == 0) {
            App.monthNo = Integer.parseInt(month.format(date));
        }

        if (App.yearMonth.equalsIgnoreCase("")) {
            App.yearMonth = dateFormat.format(date);
            txtMonth.setText(App.yearMonth);
        } else {
            txtMonth.setText(App.yearMonth);
        }
        txtToday.setOnClickListener(v -> {
            App.nextPreDate = "";
            long date1 = System.currentTimeMillis();
            calander = Calendar.getInstance();
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("MMMM  yyyy");
            App.monthNo = Integer.parseInt(month.format(date1));
            year = Integer.parseInt(yearFormate.format(date1));
            App.yearMonth = dateFormat1.format(date1);
            txtMonth.setText(App.yearMonth);
        });
        llPrevious.setOnClickListener(v -> {
            String month12 = null;
            String monthNo = null;
            App.monthNo = App.monthNo - 1;
            if (App.monthNo == 1) {
                monthNo = "01";
                month12 = "January ";
            } else if (App.monthNo == 2) {
                monthNo = "02";
                month12 = "February ";
            } else if (App.monthNo == 3) {
                monthNo = "03";
                month12 = "March ";
            } else if (App.monthNo == 4) {
                monthNo = "04";
                month12 = "April ";
            } else if (App.monthNo == 5) {
                monthNo = "05";
                month12 = "May ";
            } else if (App.monthNo == 6) {
                monthNo = "06";
                month12 = "June ";
            } else if (App.monthNo == 7) {
                monthNo = "07";
                month12 = "July ";
            } else if (App.monthNo == 8) {
                monthNo = "08";
                month12 = "August ";
            } else if (App.monthNo == 9) {
                monthNo = "09";
                month12 = "September ";
            } else if (App.monthNo == 10) {
                monthNo = "10";
                month12 = "October ";
            } else if (App.monthNo == 11) {
                monthNo = "11";
                month12 = "November ";
            } else if (App.monthNo == 12) {
                monthNo = "12";
                month12 = "December ";
            } else {
                App.monthNo = -1;
                monthNo = "12";
                month12 = "December";
            }

            if (App.monthNo == -1) {
                // monthNo="12";
                int minusYear = year - 1;
                System.out.println("minus year===" + minusYear);
                nextPreDate = minusYear + "-" + monthNo;
                year = minusYear;
                // month="December";
                App.yearMonth = month12 + " " + minusYear;
                App.monthNo = 12;
            } else {
                nextPreDate = year + "-" + monthNo;
                App.yearMonth = month12 + " " + year;
            }
            txtMonth.setText(App.yearMonth);
        });
        llNext.setOnClickListener(v -> {
            String month1 = null;
            String monthNo = null;
            App.monthNo = App.monthNo + 1;
            if (App.monthNo == 1) {
                monthNo = "01";
                month1 = "January ";
            } else if (App.monthNo == 2) {
                monthNo = "02";
                month1 = "February ";
            } else if (App.monthNo == 3) {
                monthNo = "03";
                month1 = "March ";
            } else if (App.monthNo == 4) {
                monthNo = "04";
                month1 = "April ";
            } else if (App.monthNo == 5) {
                monthNo = "05";
                month1 = "May ";
            } else if (App.monthNo == 6) {
                monthNo = "06";
                month1 = "June ";
            } else if (App.monthNo == 7) {
                monthNo = "07";
                month1 = "July ";
            } else if (App.monthNo == 8) {
                monthNo = "08";
                month1 = "August ";
            } else if (App.monthNo == 9) {
                monthNo = "09";
                month1 = "September ";
            } else if (App.monthNo == 10) {
                monthNo = "10";
                month1 = "October ";
            } else if (App.monthNo == 11) {
                monthNo = "11";
                month1 = "November ";
            } else if (App.monthNo == 12) {
                monthNo = "12";
                month1 = "December ";
            } else {
                App.monthNo = -1;
                monthNo = "1";
                month1 = "January";
            }
            if (App.monthNo == -1) {
                // monthNo="12";
                int minusYear = year + 1;
                System.out.println("Plush year===" + minusYear);
                nextPreDate = minusYear + "-" + monthNo;
                year = minusYear;
                // month="December";
                App.yearMonth = month1 + " " + minusYear;
                App.monthNo = 1;

            } else {
                nextPreDate = year + "-" + monthNo;
                App.yearMonth = month1 + " " + year;
            }

            txtMonth.setText(App.yearMonth);
        });
        dialogClear.setOnClickListener(v -> {
            long date12 = System.currentTimeMillis();
            calander = Calendar.getInstance();
            SimpleDateFormat dateFormat12 = new SimpleDateFormat("MMMM  yyyy");
            App.yearMonth = dateFormat12.format(date12);
            txtMonth.setText(App.yearMonth);

            if (!App.regionStringList.isEmpty()) {
                spRegion.clearSelection();
                txtRegion.setVisibility(View.VISIBLE);
                App.indexListRegionSchedule.clear();
                App.multiSelectRegionId = "";
            }
            if (!App.placeStringList.isEmpty()) {
                spPlace.clearSelection();
                txtPlace.setVisibility(View.VISIBLE);
                App.indexListPlaceSchedule.clear();
                App.multiSelectPlaceId = "";
            }
            if (!App.contactorStringList.isEmpty()) {
                spPerson.clearSelection();
                txtPersons.setVisibility(View.VISIBLE);
                App.indexListPersonSchedule.clear();
                //contactorArrayList.clear();
                App.multiSelectPersonId = "";
            }
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                txtPersons.setVisibility(View.GONE);
                spPerson.setVisibility(View.GONE);
                viewPerson.setVisibility(View.GONE);
                App.multiSelectPersonId = userId;
            }

            App.multiSelectedNotAvailable = "Approve,Pending,Deny";
            App.indexListNotAvailable.clear();
            spNoteAvailable.clearSelection();
            txtNotAvailable.setVisibility(View.VISIBLE);
            App.nextPreDate = "";
            txtMonth.setText(App.yearMonth);


        });
        btnOK.setOnClickListener(v -> {
            dialog.dismiss();
            getSchedule();
        });

        spPlace = dialog.findViewById(R.id.spPlaces);
        spPerson = dialog.findViewById(R.id.spPerson);
        spRegion = dialog.findViewById(R.id.spRegion);
        spNoteAvailable = dialog.findViewById(R.id.spNoteAvailable);
        txtPersons = dialog.findViewById(R.id.txtPersons);
        txtPlace = dialog.findViewById(R.id.txtPlace);
        txtRegion = dialog.findViewById(R.id.txtRegion);
        viewPerson = dialog.findViewById(R.id.viewPerson);
        txtNotAvailable = dialog.findViewById(R.id.txtNotAvailable);
        dialog.show();

        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            txtPersons.setVisibility(View.GONE);
            spPerson.setVisibility(View.GONE);
            viewPerson.setVisibility(View.GONE);
            App.multiSelectPersonId = userId;
        }
        spPlace.setOnTouchListener((v, event) -> {
            typeSchedule = "place";
            return false;
        });
        spRegion.setOnTouchListener((v, event) -> {
            typeSchedule = "region";
            return false;
        });
        spPerson.setOnTouchListener((v, event) -> {
            typeSchedule = "person";
            return false;
        });
        spNoteAvailable.setOnTouchListener((v, event) -> {
            typeSchedule = "notAvailable";
            return false;
        });

        spPerson.setItems(App.contactorStringList, Constant.FILTER_TYPE.PERSONS);
        spPerson.setSelection(new int[]{0});
        spPerson.setListener(this);
        spPerson.clearSelection();

        spPlace.setItems(App.placeStringList, Constant.FILTER_TYPE.PLACE);
        spPlace.setSelection(new int[]{0});
        spPlace.clearSelection();
        spPlace.setListener(this);

        spRegion.setItems(App.regionStringList, Constant.FILTER_TYPE.REGIONS);
        spRegion.setSelection(new int[]{0});
        spRegion.clearSelection();
        spRegion.setListener(this);

        spNoteAvailable.setItems(App.notAvailableList, Constant.FILTER_TYPE.NOT_AVAILABLE);
        spNoteAvailable.setSelection(new int[]{0});
        spNoteAvailable.setListener(this);
        spNoteAvailable.setSelection(App.notAvailableList);

        if (!TextUtils.isEmpty(App.multiSelectRegionId)) {
            int[] array = new int[App.indexListRegionSchedule.size()];
            int counter = 0;
            for (Integer myInt : App.indexListRegionSchedule) {
                array[counter++] = myInt;
            }
            spRegion.setSelection(array);
            txtRegion.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(App.multiSelectPlaceId)) {
            int[] array = new int[App.indexListPlaceSchedule.size()];
            int counter = 0;
            for (Integer myInt : App.indexListPlaceSchedule) {
                array[counter++] = myInt;
            }
            spPlace.setSelection(array);
            txtPlace.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(App.multiSelectPersonId)) {
            int[] array = new int[App.indexListPersonSchedule.size()];
            int counter = 0;
            for (Integer myInt : App.indexListPersonSchedule) {
                array[counter++] = myInt;
            }
            spPerson.setSelection(array);
            txtPersons.setVisibility(View.GONE);
        }
        if (App.indexListNotAvailable.size() != 0) {
            txtNotAvailable.setVisibility(View.GONE);
            int[] array = new int[App.indexListNotAvailable.size()];
            int counter = 0;
            for (Integer myInt : App.indexListNotAvailable) {
                array[counter++] = myInt;
            }
            spNoteAvailable.setSelection(array);
        }
    }

    private void getConractor() {
        App.personList.clear();
        App.contactorStringList.clear();
        new CallRequest(instance).getConractor(emailId, token, "");
    }

    private void getRegion() {
        try {
            App.regionList.clear();
            App.regionStringList.clear();
            new CallRequest(instance).getRegion(emailId, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPlace() {
        try {
            App.placeList.clear();
            App.placeStringList.clear();
            new CallRequest(instance).getPlace(emailId, token, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.addSchedual) {
            if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                ((MainActivity)getActivity()).changeFragment(new AddNotAvailableFragment(), true);
            } else {
                MainActivity.toolbar_title.setText("Add Not Available Schedule");
                ((MainActivity)getActivity()).changeFragment(new AddAvailableNotAvailableScheduleFragment(), true);
            }
            return true;
        }
        if (id == R.id.drawer) {
            drawer.openDrawer(Gravity.RIGHT);
            return true;
        }
        if (id == R.id.refrash) {
            getSchedule();
            return true;
        }
        if (id == R.id.addPushDevider) {
            ((MainActivity)getActivity()).changeFragment(new AddPushDeviderFragment(), true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getSchedule() {
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            App.multiSelectPersonId = userId;
        }
        new CallRequest(instance).getNewSchedule(emailId, token, App.nextPreDate, App.multiSelectRegionId, App.multiSelectPersonId, App.multiSelectPlaceId, App.multiSelectedNotAvailable);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_schedual, menu);  // Use filter.xml from step 1
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            menu.findItem(R.id.addPushDevider).setVisible(false);
        }
        if (roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            menu.findItem(R.id.addPushDevider).setVisible(false);
            menu.findItem(R.id.addSchedual).setVisible(false);

        }
        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID)) {
            menu.findItem(R.id.addPushDevider).setVisible(false);
            menu.findItem(R.id.addSchedual).setVisible(false);
        }
        getConractor();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getSchedual:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "onTaskCompleted: Schedule --> "+result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            subList.clear();
                            boolean isSet = false;
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject objDate = jObject.getJSONObject(i);
                                NewScheduleModel jobStatusModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), NewScheduleModel.class);
                                Date todayDate = Calendar.getInstance().getTime();
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                String todayDateString = formatter.format(todayDate);

                                JSONArray jArray = objDate.getJSONArray("schedual_list");
                                jobStatusModel.subArray.addAll(LoganSquare.parseList(jArray.toString(), NewScheduleModel.class));

                                if (!jobStatusModel.getSchedual_date().equals(null) && jobStatusModel.getSchedual_date().equalsIgnoreCase(todayDateString)) {
                                    selectedPos = i;
                                }
                                if ((!jobStatusModel.getSchedual_date().equals(null) && getYearMonth(jobStatusModel.getSchedual_date()).equalsIgnoreCase(App.nextPreDate)) && !isSet) {
                                    isSet = true;
                                    selectedPos = i;
                                }
                                if (App.title.equalsIgnoreCase(Constant.SCHEDULE_ADD) || App.title.equalsIgnoreCase(Constant.SCHEDULE_UPDATE) || App.isFromNotificationFragment) {
                                    if ((!jobStatusModel.getSchedual_date().equals(null) && jobStatusModel.getSchedual_date().equalsIgnoreCase(App.scheduleDate))) {
                                        selectedPos = i;
                                    }
                                }
                                subList.add(jobStatusModel);
                            }
                            rvSchDetail.setHasFixedSize(false);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            rvSchDetail.setLayoutManager(layoutManager);
                            NewScheduleVerticalAdapter adapter = new NewScheduleVerticalAdapter(subList, NewScheduleFragment.this);
                            rvSchDetail.setAdapter(adapter);
                            rvSchDetail.getLayoutManager().scrollToPosition(selectedPos);
                            //jobStatusAdapter = new JobStatusAdapter(jobStatusList);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

                case getContractor:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject ob = jObject.getJSONObject(i);
                                contractorModel = new ContractorModel();
                                contractorModel =LoganSquare.parse(ob.toString(),ContractorModel.class);
                                App.personList.add(contractorModel);
                                App.contactorStringList.add(contractorModel.getFullName());
                            }
                            try {
                                aaPersons = new ArrayAdapter(getActivity(), R.layout.spinner_text, App.contactorStringList);
                                aaPersons.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getPlace();
                    break;
                case getPlace:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                placeModel=LoganSquare.parse(ob.toString(),PlaceModel.class);
                                App.placeList.add(placeModel);
                                App.placeStringList.add(placeModel.getPlacesName());
                            }
                            aaFacility = new ArrayAdapter(getContext(), R.layout.spinner_text, App.placeStringList);
                            aaFacility.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getRegion();

                    break;

                case getRegion:
                    //    Utils.hideProgressDialog();
                    try {

                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                regionModel = LoganSquare.parse(ob.toString(),RegionModel.class);
                                App.regionList.add(regionModel);
                                App.regionStringList.add(regionModel.getRegionName());
                            }
                            try {
                                aaRegion = new ArrayAdapter(getContext(), R.layout.spinner_text, App.regionStringList);
                                aaRegion.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Utils.hideProgressDialog();

                    getSchedule();
                    break;

                case deleteSchedule:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getSchedule();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case approveDenyNotAvailableSchedule:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getSchedule();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
                case copyToSchedule:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getSchedule();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

    private String getYearMonth(String date) throws ParseException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Date dt1 = format1.parse(date);
        DateFormat format2 = new SimpleDateFormat("yyyy-MM");
        return format2.format(dt1);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {

        if (filter_type == PERSONS) {

            App.indexListPersonSchedule = indices;
            // App.  multiSelectPersonId = "";
            ArrayList<String> statusIDArray = new ArrayList<>();
            String selectedNames = "";
            for (Integer i : indices) {
                statusIDArray.add(App.personList.get(i).getId());
                selectedNames += App.personList.get(i).getFullName() + ", ";
            }
            txtPersons.setVisibility(View.GONE);

            App.multiSelectPersonId = android.text.TextUtils.join(",", statusIDArray);
            if (App.multiSelectPersonId.equals("")) {
                txtPersons.setVisibility(View.VISIBLE);
            }
        } else if (filter_type == PLACE) {

            App.indexListPlaceSchedule = indices;
            //   App.  multiSelectPlaceId = "";

            ArrayList<String> statusIDArray = new ArrayList<>();
            String selectedNames = "";
            for (Integer i : indices) {
                statusIDArray.add(App.placeList.get(i).getPlacesID());
                selectedNames += App.placeList.get(i).getPlacesName() + ", ";
            }
            txtPlace.setVisibility(View.GONE);
            App.multiSelectPlaceId = android.text.TextUtils.join(",", statusIDArray);
            if (App.multiSelectPlaceId.equals("")) {
                txtPlace.setVisibility(View.VISIBLE);
            }
        } else if (filter_type == REGIONS) {
            App.indexListRegionSchedule = indices;
            ArrayList<String> statusIDArray = new ArrayList<>();
            for (Integer i : indices) {
                statusIDArray.add(App.regionList.get(i).getRegionID());
            }
            txtRegion.setVisibility(View.GONE);
            App.multiSelectRegionId = android.text.TextUtils.join(",", statusIDArray);
            if (App.multiSelectRegionId.equals("")) {
                txtRegion.setVisibility(View.VISIBLE);
            }
        } else if (filter_type == NOT_AVAILABLE) {
            txtNotAvailable.setVisibility(View.GONE);
            App.indexListNotAvailable = indices;
            ArrayList<String> statusIDArray = new ArrayList<>();
            for (Integer i : indices) {
                if (i == 0) {
                    statusIDArray.add("Approve");
                } else if (i == 1) {
                    statusIDArray.add("Pending");
                } else if (i == 2) {
                    statusIDArray.add("Deny");
                } else {
                    statusIDArray.add(String.valueOf(App.notAvailableList));
                }

            }
            //txtRegion.setVisibility(View.GONE);
            App.multiSelectedNotAvailable = android.text.TextUtils.join(",", statusIDArray);

            if (App.multiSelectedNotAvailable.equals("")) {
                txtNotAvailable.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {
        for (int j = 0; j < strings.size(); j++) {
            System.out.println("item:::" + strings.get(j));
        }
    }

    @Override
    public void onDeleteSchedule(final int pos, final String scheduleId) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive((dialog1, which) -> new CallRequest(instance).deleteSchedule(emailId, token, userId, scheduleId))
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    @Override
    public void onCopyToTomorrowSchedule(int pos, String scheduleID) {
        new CallRequest(instance).copyToSchedule(emailId, token, userId, scheduleID, Constant.TOMORROW);
    }

    @Override
    public void onCopyToNextWeekSchedule(int pos, String scheduleId) {
        new CallRequest(instance).copyToSchedule(emailId, token, userId, scheduleId, Constant.NEXT);
    }

    @Override
    public void onCopyToEntireWeekSchedule(int pos, String scheduleId) {
        new CallRequest(instance).copyToSchedule(emailId, token, userId, scheduleId, Constant.ENTIRE);
    }


    @Override
    public void onDeleteNotAvailableSchedule(int pos, final String scheduleID) {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive((dialog1, which) -> new CallRequest(instance).deleteSchedule(emailId, token, userId, scheduleID))
                .negativeText("Cancel")
                .onNegative((dialog12, which) -> dialog12.dismiss())
                .build();
        dialog.show();
    }

    @Override
    public void onApproveNotAvailableSchedule(int pos, String scheduleID) {
        new CallRequest(instance).approveDenyNoteAvailabeSchedule(emailId, token, userId, scheduleID, "Approve");
    }

    @Override
    public void onDenyNotAvailableSchedule(int pos, String scheduleID) {
        new CallRequest(instance).approveDenyNoteAvailabeSchedule(emailId, token, userId, scheduleID, "Deny");
    }

    @Override
    public void onCopyToTomorrowNotAvailableSchedule(int pos, String scheduleID) {
        new CallRequest(instance).copyToSchedule(emailId, token, userId, scheduleID, Constant.TOMORROW);
    }
}
