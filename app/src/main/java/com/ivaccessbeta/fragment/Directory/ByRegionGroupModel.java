package com.ivaccessbeta.fragment.Directory;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;

/**
 * Created by User on 17-04-2018.
 */

@JsonObject
public class ByRegionGroupModel {
    @JsonField
    public String Region;

    public ArrayList<ByRegionChildModel> childList = new ArrayList<ByRegionChildModel>();


    public ByRegionGroupModel() {
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public ArrayList<ByRegionChildModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<ByRegionChildModel> childList) {
        this.childList = childList;
    }
}
