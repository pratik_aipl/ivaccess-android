package com.ivaccessbeta.fragment.profile;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.adapter.AddJobAndNotAvailableTabsPagerAdapter;
import com.ivaccessbeta.adapter.ProfileDocTabsPagerAdapter;
import com.ivaccessbeta.adapter.ProfileEmpDocAdapter;
import com.ivaccessbeta.listners.ProfileDocumentListner;
import com.ivaccessbeta.model.ProfileEmpDocModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.DocChangeEvent;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.ivaccessbeta.utils.Utils.hasPermissions;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeDocProfileFragment extends Fragment implements AsynchTaskListner {
    private static final String TAG = "EmployeeDocProfileFragm";
    public static Dialog dialog;
    public static View layout;

    TabLayout tabLayout;
    ViewPager viewPager;
    boolean callFromEvent = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_employee_doc, container, false);
        setHasOptionsMenu(true);

        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.pager);

        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        callFromEvent = false;
        getEmpDoc();
    }

    private void getEmpDoc() {
        String emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        String token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        String userId;
        if (TextUtils.isEmpty(App.contractorId)) {
            userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        } else {
            userId = App.contractorId;
        }

        new CallRequest(EmployeeDocProfileFragment.this).getEmpDoc(emailId, token, userId);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getEmpDoc:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            ArrayList<ProfileEmpDocModel> list = new ArrayList<>();
                            list.addAll(LoganSquare.parseList(jObject.toString(), ProfileEmpDocModel.class));
                            setTabData(list);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DocChangeEvent event) {
        Log.d(TAG, "onMessageEvent: " + event.isCall());
        callFromEvent = true;
        getEmpDoc();
    }


    private void setTabData(ArrayList<ProfileEmpDocModel> list) {
        tabLayout.removeAllTabs();
        tabLayout.addTab(tabLayout.newTab().setText("Not Expired"));
        tabLayout.addTab(tabLayout.newTab().setText("Expired"));
        viewPager.setAdapter(new ProfileDocTabsPagerAdapter(getFragmentManager(), list));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    @Override
    public void onDestroy() {
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}



