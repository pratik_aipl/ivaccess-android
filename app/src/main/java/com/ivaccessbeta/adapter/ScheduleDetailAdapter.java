package com.ivaccessbeta.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.AddJobActivity;
import com.ivaccessbeta.fragment.schedule.EditScheduleFragment;
import com.ivaccessbeta.fragment.schedule.SchedualDetailFragment;
import com.ivaccessbeta.listners.ScheduleListener;
import com.ivaccessbeta.model.ScheduleDetailModel;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;



public class ScheduleDetailAdapter extends RecyclerView.Adapter<ScheduleDetailAdapter.MyViewHolder> {

    public ArrayList<ScheduleDetailModel> list;
    //public List<ScheduleDetailModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId, roleId;
    public ScheduleDetailModel model;
    public String facilityName;
    public SchedualDetailFragment schedualDetailFragment;
    public ScheduleListener scheduleListener;

    @Override
    public ScheduleDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_schedual, parent, false);
        return new ScheduleDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ScheduleDetailAdapter.MyViewHolder holder, final int position) {
        model = list.get(position);
        holder.txtFacliterName.setText(model.getFirst_name() + " " + model.getLast_name());
        holder.txtHospitalName.setText(model.getPlacesName());
        holder.relativeSchedule.setBackgroundColor(Color.parseColor(model.getColorCode()));
        roleId = MySharedPref.getString(context, App.ROLEID, "");
        facilityName = model.getFirst_name() + " " + model.getLast_name();


        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            holder.ivAction.setVisibility(View.INVISIBLE);

            holder.txtAdminNote.setVisibility(View.GONE);
            holder.txtNote.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(model.getContractorNotes())) {
                holder.view1.setVisibility(View.VISIBLE);
                holder.txtNote.setText("Note : " + model.getContractorNotes());
                holder.txtNote.setVisibility(View.VISIBLE);
            } else {
                holder.txtNote.setVisibility(View.GONE);
                holder.view1.setVisibility(View.GONE);
            }
        } else if (roleId.equalsIgnoreCase(App.SUPER_ADMIN_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            holder.ivAction.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(model.getAdminNotes())) {

                holder.txtNote.setVisibility(View.VISIBLE);
                holder.txtAdminNote.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(model.getContractorNotes())) {
                    holder.txtNote.setText("Contractore : " + model.getContractorNotes());
                    holder.txtNote.setVisibility(View.VISIBLE);
                    holder.view1.setVisibility(View.VISIBLE);
                } else {
                    holder.txtNote.setVisibility(View.GONE);
                    holder.view1.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(model.getAdminNotes())) {
                    holder.view1.setVisibility(View.VISIBLE);
                    holder.txtAdminNote.setText("Note : " + model.getAdminNotes());
                    holder.txtAdminNote.setVisibility(View.VISIBLE);
                } else {
                    holder.view1.setVisibility(View.GONE);
                    holder.txtAdminNote.setVisibility(View.GONE);
                }


            }
        } else {
            holder.view1.setVisibility(View.GONE);
            holder.txtNote.setVisibility(View.GONE);
            holder.txtAdminNote.setVisibility(View.GONE);
        }
        holder.ivAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1];
                showSortPopup(schedualDetailFragment.getActivity(), x, y, position);
            }
        });

        holder.txtTimeFrom.setText(model.getFromTime() + " To ");
        holder.txtTimeTo.setText(model.getToTime());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditScheduleFragment fragment = new EditScheduleFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("schedule", list.get(position));
                fragment.setArguments(bundle);
                ((MainActivity)context).changeFragment(fragment, true);
            }
        });
    }

    private void showSortPopup(final Activity context, int x, int y, final int position) {
        RelativeLayout viewGroup = context.findViewById(R.id.rlEmpDoc);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_schedule_action, viewGroup);
        final PopupWindow popup = new PopupWindow(context);
        TextView txtCopyToTomorrow, txtCopyToNextWeek, txtCopyToEntireWeek;
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, x, y);
        TextView txtAddJob = layout.findViewById(R.id.txtAddJob);
        TextView txtTrash = layout.findViewById(R.id.txtTrash);
        ImageView ivDelete = layout.findViewById(R.id.ivDelete);
        txtCopyToEntireWeek = layout.findViewById(R.id.txtCopyToEntireWeek);
        txtCopyToTomorrow = layout.findViewById(R.id.txtCopyToTomorrow);
        txtCopyToNextWeek = layout.findViewById(R.id.txtCopyToNextWeek);
        txtAddJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
//                Intent i = new Intent(MainActivity.activity, AddJobActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putInt("value", 2);
//                bundle.putString("FaciliterName",model.getFirst_name()+" "+model.getLast_name());
//                i.putExtras(bundle);
                AddJobActivity fragment = new AddJobActivity();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 2);
                bundle.putString("FaciliterName", model.getFirst_name() + " " + model.getLast_name());
                bundle.putString("date", App.ScheduleFragmentList.get(App.tabCount).getDate());
                fragment.setArguments(bundle);
                ((MainActivity)context).changeFragment(fragment, true);
                //  ((MainActivity)getActivity()).startActivity(i);
            }
        });
        txtCopyToTomorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleListener.onCopyToTomorrowSchedule(position);
            }
        });
        txtCopyToEntireWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleListener.onCopyToEntireWeekSchedule(position);
            }
        });
        txtCopyToNextWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleListener.onCopyToNextWeekSchedule(position);
            }
        });

        if (roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID) || roleId.equalsIgnoreCase(App.ADMIN_ROLE_ID)) {
            txtTrash.setVisibility(View.GONE);
            ivDelete.setVisibility(View.GONE);
        }
        txtTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                scheduleListener.onDeleteSchedule(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public ScheduleDetailAdapter(ArrayList<ScheduleDetailModel> List, SchedualDetailFragment schedualDetailFragment) {
        this.list = List;
        this.context=schedualDetailFragment.getActivity();
        this.schedualDetailFragment = schedualDetailFragment;
        this.scheduleListener = schedualDetailFragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtHospitalName, txtFacliterName, txtTimeFrom, txtTimeTo, txtCopyToTomorrow, txtCopyToNextWeek, txtCopyToEntireWeek, txtNote, txtAdminNote;
        ImageView ivAction;
        RelativeLayout relativeSchedule;
        View view1;

        public MyViewHolder(View view) {
            super(view);
            txtHospitalName = view.findViewById(R.id.txtHospitalName);
            txtFacliterName = view.findViewById(R.id.txtFacliterName);
            txtTimeFrom = view.findViewById(R.id.txtTimeFrom);
            txtTimeTo = view.findViewById(R.id.txtTimeTo);
            relativeSchedule = view.findViewById(R.id.relativeSchedule);
            ivAction = view.findViewById(R.id.ivAction);
            txtNote = view.findViewById(R.id.txtNote);
            txtAdminNote = view.findViewById(R.id.txtAdminNote);
            view1 = view.findViewById(R.id.view);

        }
    }
}
