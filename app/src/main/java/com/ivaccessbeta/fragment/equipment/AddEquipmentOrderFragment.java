package com.ivaccessbeta.fragment.equipment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.EquipmentQuantityModel;
import com.ivaccessbeta.model.EquipmentsModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class AddEquipmentOrderFragment extends Fragment implements AsynchTaskListner {
    private static final String TAG = "AddEquipmentOrderFragme";
    public Spinner spinnerContractor, spItem, spQuentity;
    TextView spContractor;
    public ArrayList<ContractorModel> contractorList = new ArrayList<>();
    public ArrayList<String> contractorStringList = new ArrayList<>();
    public AddEquipmentOrderFragment instance;
    public String emailId, token, userId, contractor, equipmentItem, note, quentity, userName, roleId;
    public ContractorModel contractorModel;
    public ArrayList<EquipmentsModel> equipmentList = new ArrayList<>();
    public ArrayList<String> equipmentStringList = new ArrayList<String>();
    public ArrayList<EquipmentQuantityModel> equipmentQuantityList = new ArrayList<>();
    public ArrayList<String> equipmentQuantityStringList = new ArrayList<String>();
    public EquipmentsModel equipmentModel;
    public EquipmentQuantityModel equipmentQuantityModel;
    public EditText edtNote, edtPersoneName;
    public Button btnSave, btnCancel;
    public RelativeLayout relativePerson;
    SpinnerDialog spinnerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_add_equipment_order, container, false);
        MainActivity.toolbar_title.setText("Order Equipments");
        ((MainActivity) getActivity()).showToolbarAction();
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        instance = this;
        spinnerContractor = view.findViewById(R.id.spinnerContractor);
        spContractor = view.findViewById(R.id.spContractor);
        spItem = view.findViewById(R.id.spItem);
        spQuentity = view.findViewById(R.id.spQuentity);
        edtNote = view.findViewById(R.id.edtNote);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        edtPersoneName = view.findViewById(R.id.edtPersoneName);
        relativePerson = view.findViewById(R.id.relativePerson);
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");
        userName = MySharedPref.getString(getActivity(), App.NAME, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        if (roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
            relativePerson.setVisibility(View.GONE);
            edtPersoneName.setVisibility(View.VISIBLE);
            edtPersoneName.setText(userName);
            contractor = userId;
        }
        getContractor();
        btnCancel.setOnClickListener(v -> getActivity().onBackPressed());

        spinnerDialog = new SpinnerDialog(getActivity(), contractorStringList, "Select or Search Person");

        spinnerDialog.setCancellable(true);
        spinnerDialog.setShowKeyboard(false);

        spinnerDialog.bindOnSpinerListener((item, position) -> {
            for (int i = 0; i < contractorList.size(); i++) {
                if (contractorList.get(i).getFullName().equalsIgnoreCase(item)) {
                    contractor = contractorList.get(i).getId();
                    break;
                }
            }
            spContractor.setText(item);
//                facility = facilityList.get(position).getFacilityID();
//                billTo = facilityList.get(position).getDefaultBillTo();
//                getBillList();
//                spBillTo.setEnabled(false);
        });

        spContractor.setOnClickListener(view1 -> spinnerDialog.showSpinerDialog());

//        spinnerContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Utils.disableKeyboard(getContext(), view);
//                if (i != 0)
//                    contractor = contractorList.get(i - 1).getId();
//                else
//                    contractor = "";
//                Log.d(TAG, "onItemSelected: " + i);
//                Log.d(TAG, "onItemSelected: " + contractor);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        spQuentity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.disableKeyboard(getContext(), view);
                if (position != 0)
                    quentity = equipmentQuantityList.get(position - 1).getId();
                else
                    quentity = "";

                Log.d(TAG, "onItemSelected: " + position);
                Log.d(TAG, "onItemSelected: " + quentity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Utils.disableKeyboard(getContext(), view);
                try {
                    equipmentItem = equipmentList.get(position - 1).getEquimentID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSave.setOnClickListener(v -> {
            note = edtNote.getText().toString().trim();

            if (!roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                if (TextUtils.isEmpty(contractor)) {
                    Utils.showToast("Please Select Contractor", getContext());
                } else if (TextUtils.isEmpty(equipmentItem)) {
                    Utils.showToast("Please Select Equipment Item", getContext());
                } else if (TextUtils.isEmpty(quentity)) {
                    Utils.showToast("Please Select Quantity", getContext());
                } else {
                    new CallRequest(instance).addUpdateEquipmentOrder(emailId, token, userId, equipmentItem, contractor, quentity, note);
                }
            } else {
                contractor = userId;
                if (TextUtils.isEmpty(equipmentItem)) {
                    Utils.showToast("Please Select Equipment Item", getContext());
                } else if (TextUtils.isEmpty(quentity)) {
                    Utils.showToast("Please Select Quentity", getContext());
                } else {
                    new CallRequest(instance).addUpdateEquipmentOrder(emailId, token, userId, equipmentItem, contractor, quentity, note);
                }

            }

        });

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
        menu.getItem(2).setVisible(true);
    }

    public void getContractor() {
        new CallRequest(instance).getConractor(emailId, token, "");
    }

    public void getEquipmentsDetail() {
        equipmentList.clear();
        new CallRequest(instance).getEquipment(emailId, token);
    }

    public void getQuantity() {
        equipmentQuantityList.clear();
        new CallRequest(instance).getEquipmentQuantity(emailId, token);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContractor:
                    contractorList.clear();
                    contractorStringList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray ob1 = jObj.getJSONArray("data");
                            for (int i = 0; i < ob1.length(); i++) {
                                JSONObject ob = ob1.getJSONObject(i);
                                contractorModel = new ContractorModel();
                                contractorModel = LoganSquare.parse(ob.toString(), ContractorModel.class);
                                contractorList.add(contractorModel);
                                contractorStringList.add(contractorModel.getFullName());
                            }
//                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, contractorStringList);
//                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spContractor.setAdapter(new NothingSelectedSpinnerAdapter(
//                                    aa, R.layout.contractor_spinner_row_nothing_selected,
//                                    getContext()));
//                            if (contractor != null) {
//                                int spinnerPosition = aa.getPosition(contractor);
//                                spContractor.setSelection(spinnerPosition);
//                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    getEquipmentsDetail();
                    break;
                case getEquipment:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                equipmentModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), EquipmentsModel.class);
                                equipmentList.add(equipmentModel);
                                String roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
                                String equipmentName = "";
                                if (!roleId.equalsIgnoreCase(App.MANAMER_ROLE_ID) && !roleId.equalsIgnoreCase(App.CONTRACTOR_ROLE_ID)) {
                                    equipmentName = equipmentModel.getEquipmentName() + " " + equipmentModel.getEquipmentCode();
                                } else {
                                    equipmentName = equipmentModel.getEquipmentName();
                                }
                                equipmentStringList.add(equipmentName);
                            }
                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, equipmentStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spItem.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.item_nothing_selected,
                                    getContext()));
                            if (equipmentItem != null) {
                                int spinnerPosition = aa.getPosition(equipmentItem);
                                spItem.setSelection(spinnerPosition);
                            }
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    getQuantity();
                    break;

                case getEquipmentQuentity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            for (int i = 0; jObject.length() > i; i++) {
                                equipmentQuantityModel = LoganSquare.parse(jObject.getJSONObject(i).toString(), EquipmentQuantityModel.class);
                                equipmentQuantityList.add(equipmentQuantityModel);
                                equipmentQuantityStringList.add(equipmentQuantityList.get(i).getValue());
                            }
                            ArrayAdapter aa = new ArrayAdapter(getContext(), R.layout.spinner_text, equipmentQuantityStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spQuentity.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.quentity_nothing_selected,
                                    getContext()));
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case addUpdateEquipmentOrder:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast(jObj.getString("message"), getActivity());
                            getActivity().onBackPressed();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
