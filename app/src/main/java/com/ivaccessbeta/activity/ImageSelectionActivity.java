package com.ivaccessbeta.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.multiselectionimage.activities.BaseActivity;
import com.ivaccessbeta.multiselectionimage.activities.GalleryActivity;
import com.ivaccessbeta.multiselectionimage.activities.MultiCameraActivity;
import com.ivaccessbeta.multiselectionimage.adapters.GalleryImagesAdapter;
import com.ivaccessbeta.R;
import com.ivaccessbeta.model.ProfileEmpDocModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.Image;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Params;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ImageSelectionActivity extends BaseActivity implements AsynchTaskListner {
    public ImageSelectionActivity instamce;
    public RecyclerView recyclerView;
    RelativeLayout parentLayout;
    Toolbar toolbar;
    TextView toolbar_title;
    RecyclerView recycler_view;
    AlertDialog alertDialog;
    EditText edtExpiredDate;
    Button dialogButton;
    public String expiredDate = "", emailId, token, userId;
    public ProfileEmpDocModel obj;
    public ArrayList<Image> imagesList;
    public ArrayList<String> imgData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_selection);
        instamce = this;
        parentLayout = findViewById(R.id.parentLayout);
        toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerView = findViewById(R.id.recycler_view);
        dialogButton = findViewById(R.id.btnOk);
        edtExpiredDate = findViewById(R.id.edtExpiredDate);
        emailId = MySharedPref.getString(instamce, App.EMAIL, "");
        token = MySharedPref.getString(instamce, App.APP_TOKEN, "");
        obj = (ProfileEmpDocModel) getIntent().getExtras().getSerializable("ProfileEmpDocModel");
        try {
            if (App.contractorId.equalsIgnoreCase("")) {
                userId = MySharedPref.getString(instamce, App.LOGGED_USER_ID, "");
            } else {
                userId = App.contractorId;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        edtExpiredDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.generateDatePicker(ImageSelectionActivity.this, edtExpiredDate);
            }
        });

        toolbar_title.setText("Select Images");
        initiateMultiPicker();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expiredDate = edtExpiredDate.getText().toString();
                try {
                    StringBuilder dateFormBuilder = new StringBuilder();
                    StringBuilder dateToBuilder = new StringBuilder();
                    dateFormBuilder = dateFormBuilder.append(expiredDate.substring(6)).append("-").append(expiredDate, 0, 2).append("-").append(expiredDate, 3, 5);

                    expiredDate = String.valueOf(dateFormBuilder);
                    System.out.println("expired Date===" + dateToBuilder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (expiredDate.equals("")) {
                        Utils.showToast("Please Select Expired Date", instamce);
                    } else {

                        new CallRequest(instamce).addUserDocActivity(emailId, token, obj.getDOCID(), userId, android.text.TextUtils.join(",", imgData), expiredDate);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            finish();
        }
        switch (requestCode) {
            case Constant.TYPE_MULTI_CAPTURE:
                handleResponseIntent(intent);
                break;
            case Constant.TYPE_MULTI_PICKER:
                try {
                    handleResponseIntent(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private int getColumnCount() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        float thumbnailDpWidth = getResources().getDimension(R.dimen.thumbnail_width) / displayMetrics.density;
        return (int) (dpWidth / thumbnailDpWidth);
    }

    private void handleResponseIntent(Intent intent) {
        imagesList = intent.getParcelableArrayListExtra(Constant.KEY_BUNDLE_LIST);
        recyclerView.setHasFixedSize(true);
        for (int i = 0; i < imagesList.size(); i++) {
            imgData.add(imagesList.get(i).imagePath);
        }

        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(getColumnCount(), GridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setLayoutManager(mLayoutManager);
        GalleryImagesAdapter imageAdapter = new GalleryImagesAdapter(this, imagesList, getColumnCount(), new Params());
        recyclerView.setAdapter(imageAdapter);
    }

    private void initiateMultiCapture() {
        Intent intent = new Intent(this, MultiCameraActivity.class);
        Params params = new Params();
        params.setCaptureLimit(10);
        params.setToolbarColor(R.color.colorPrimary);
        params.setActionButtonColor(R.color.colorPrimary);
        params.setButtonTextColor(R.color.colorPrimary);
        intent.putExtra(Constant.KEY_PARAMS, params);
        startActivityForResult(intent, Constant.TYPE_MULTI_CAPTURE);
    }

    private void initiateMultiPicker() {
        Intent intent = new Intent(this, GalleryActivity.class);
        Params params = new Params();
        params.setCaptureLimit(10);
        params.setPickerLimit(10);
        params.setToolbarColor(R.color.colorPrimary);
        params.setActionButtonColor(R.color.colorPrimary);
        params.setButtonTextColor(R.color.colorPrimary);
        intent.putExtra(Constant.KEY_PARAMS, params);
        startActivityForResult(intent, Constant.TYPE_MULTI_PICKER);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case addUserDoc:
                    try {

                        Utils.hideProgressDialog();
                        try {
                            JSONObject jObj = new JSONObject(result);
                            if (jObj.getString("status").equals("200")) {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                                alertDialogBuilder
                                        .setMessage(jObj.getString("message"))
                                        .setCancelable(false)
                                        .setPositiveButton(Html.fromHtml("<font color='#FF7F27'>OK</font>"),
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.dismiss();
                                                        finish();
                                                    }
                                                });
                                alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                                Utils.showToast("Document Add Successfully", instamce);
                            } else {
                                Utils.handleJSonAPIError(jObj, instamce);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}
