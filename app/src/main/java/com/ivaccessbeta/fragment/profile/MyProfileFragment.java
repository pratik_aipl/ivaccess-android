package com.ivaccessbeta.fragment.profile;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.ProfileTabsPagerAdapter;
import com.ivaccessbeta.model.ContractorModel;
import com.ivaccessbeta.model.UserProfileModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class MyProfileFragment extends Fragment implements AsynchTaskListner {
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public ProfileTabsPagerAdapter adapter;
    public RelativeLayout relativeLeft, relativeRight;
    public CircleImageView profile_image;
    public TextView txtName, txtDesignation, txtEmail, txtAddress;
    public MyProfileFragment instance;
    public String emailId, token, userId;
    public int value;
    public ContractorModel contractorModel;
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        viewPager = view.findViewById(R.id.viewpager);
        MainActivity.toolbar_title.setText("PROFILE");
        ((MainActivity)getActivity()).showToolbarAction();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        instance = this;
        linearJobs.setVisibility(View.GONE);
        tabLayout = view.findViewById(R.id.tabs);
        relativeLeft = view.findViewById(R.id.relative_left);
        relativeRight = view.findViewById(R.id.relative_right);
        profile_image = view.findViewById(R.id.profile_image);
        txtName = view.findViewById(R.id.txtName);
        txtDesignation = view.findViewById(R.id.txtDesignation);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtAddress = view.findViewById(R.id.txtAddress);
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        if (!Utils.hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);

        }
        value = getArguments().getInt("value");
        if (value == 1) {
            userId=getArguments().getString("contractorId");
            App.contractorId=userId;
            //contractorModel = (ContractorModel)getArguments().getSerializable("ContractorModel");
          //  userId=contractorModel.getId();
        }else {
            userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        }
        tabLayout.addTab(tabLayout.newTab().setText("SCHEDULE"));
        tabLayout.addTab(tabLayout.newTab().setText("JOBS "));
        tabLayout.addTab(tabLayout.newTab().setText("HISTORY"));
        tabLayout.addTab(tabLayout.newTab().setText("ABOUT"));
        tabLayout.addTab(tabLayout.newTab().setText("FACILITIES "));
        tabLayout.addTab(tabLayout.newTab().setText("EMPOYEE DOCS "));
        tabLayout.addTab(tabLayout.newTab().setText("EQUIPMENT"));
        relativeLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(-1), true);
            }
        });
        relativeRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(getItem(1), true);
            }
        });
        getUserProfile();
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        adapter = new ProfileTabsPagerAdapter
                (getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        if(App.isContactorDoc){
            viewPager.setCurrentItem(5);
            App.isContactorDoc=false;
        }

        return view;
    }

    private void getUserProfile() {
        new CallRequest(instance).getUserProfile(emailId, token, userId);
    }
    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile_action, menu);
        if(value==1){
            menu.findItem(R.id.edit_profile).setVisible(false);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.edit_profile) {
            ((MainActivity)getActivity()).changeFragment(new EditProfileFragment(), true);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getUserProfile:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONObject jObjData = jObj.getJSONObject("data");
                            UserProfileModel userProfileModel = LoganSquare.parse(jObjData.toString(),UserProfileModel.class);
                            ArrayList<UserProfileModel> list = new ArrayList<>();
                            list.add(userProfileModel);
                            App.description=userProfileModel.getDescription();
                            txtName.setText(list.get(0).getFirst_name() + " " + list.get(0).getLast_name());
                            String imagePath="http://ivaccess.blenzabi.com/"+list.get(0).getProfile_image_path()+list.get(0).getProfile_image_name();
                            if (!imagePath.equalsIgnoreCase("")) {
                                Picasso.with(getContext()).load(imagePath).placeholder(R.drawable.default_user).error(R.drawable.default_user).into(profile_image);
                            }
                            txtEmail.setText(list.get(0).getEmail());
                            txtAddress.setText(list.get(0).getRegionName());
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Utils.hideProgressDialog();
                    break;

            }
        }
    }
}
