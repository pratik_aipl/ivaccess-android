package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 27-03-2018.
 */
@JsonObject
public class PlaceModel implements Serializable {
    @JsonField
    public  String PlacesID,RegionID,PlacesName;

    public String getPlacesID() {
        return PlacesID;
    }

    public void setPlacesID(String placesID) {
        PlacesID = placesID;
    }

    public String getRegionID() {
        return RegionID;
    }

    public void setRegionID(String regionID) {
        RegionID = regionID;
    }

    public String getPlacesName() {
        return PlacesName;
    }

    public void setPlacesName(String placesName) {
        PlacesName = placesName;
    }
}
