package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.EquipmentsAdapter;
import com.ivaccessbeta.model.EquipmentsModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class EquipmentFragment extends Fragment implements AsynchTaskListner {

    public RecyclerView rvPeople;
    public EquipmentsAdapter mAdapter;
    public ArrayList<EquipmentsModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public EditText edtSearch;
    public String emailId, appToken;
    public EquipmentsModel equipmentModel;
    //    public JsonParserUniversal jParser;
    public static EquipmentFragment instance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_equipment, container, false);
        instance = this;

        rvPeople = view.findViewById(R.id.rvEquipments);
        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        appToken = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        linearJobs.setVisibility(View.GONE);
//        emailId = App.uEmail;
//        appToken = App.uToken;
        setHasOptionsMenu(true);

        relativeAdd = view.findViewById(R.id.relative_add);
        relativeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddEquipmentFragment addEquipmentFragment = new AddEquipmentFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("value", 0);
                addEquipmentFragment.setArguments(bundle);
                ((MainActivity)getActivity()).changeFragment(addEquipmentFragment, true);

            }
        });

        edtSearch = view.findViewById(R.id.edt_search);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getEquipmentsDetail();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        //menu.getItem(1).setVisible(false);
    }


    private void getEquipmentsDetail() {
        list.clear();
        new CallRequest(instance).getEquipment(emailId, appToken);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : Equipment " + result);
            list.clear();
            switch (request) {
                case getEquipment:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {

                            JSONArray jObject = jObj.getJSONArray("data");
                            list.clear();
                            list.addAll(LoganSquare.parseList(jObject.toString(), EquipmentsModel.class));
//                            for (int i = 0; jObject.length() > i; i++) {
//                                equipmentModel = new EquipmentsModel();
//                                equipmentModel = (EquipmentsModel) jParser.parseJson(jObject.getJSONObject(i), new EquipmentsModel());
//                                list.add(equipmentModel);
//                            }
                            mAdapter = new EquipmentsAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvPeople.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvPeople.setLayoutManager(mLayoutManager);
                            rvPeople.setItemAnimator(new DefaultItemAnimator());
                            rvPeople.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteEquipment:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Equipment Delete Successfully", getContext());
                            getEquipmentsDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

            }
        }
    }
}
