package com.ivaccessbeta.listners;

/**
 * Created by User on 18-04-2018.
 */

public interface NewScheduleListener {

    void onDeleteSchedule(int pos, String scheduleIscheduleID);
    void onCopyToTomorrowSchedule(int pos, String scheduleID);
    void onCopyToNextWeekSchedule(int pos, String scheduleID);
    void onCopyToEntireWeekSchedule(int pos, String scheduleID);
}
