package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.R;
import com.ivaccessbeta.fragment.AddPushDeviderFragment;
import com.ivaccessbeta.listners.PublishOnClick;
import com.ivaccessbeta.listners.PushDividerListener;
import com.ivaccessbeta.model.PushDeviderModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by User on 10-04-2018.
 */

public class PushDeviderAdapter extends RecyclerView.Adapter<PushDeviderAdapter.MyViewHolder> {

    public ArrayList<PushDeviderModel> list;
    public Context context;
    public String emailId, token, userId;
    public View itemView;
    int defultPos;
    String pushDeviderDate, oldDate,PublishDate;
    AddPushDeviderFragment addPushDeviderFragment;
    PushDividerListener pushDividerListener;
    PublishOnClick publishOnClick;

    @Override
    public PushDeviderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_push_devider, parent, false);
        return new PushDeviderAdapter.MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(PushDeviderAdapter.MyViewHolder holder, final int position) {
        final PushDeviderModel model = list.get(position);
        int weekNo = model.getWeek();
        String startDate = null, endDate = null;
        holder.txtWeekNo.setText(Integer.toString(weekNo));
        startDate = model.getStart_date();
        endDate = model.getEnd_date();
        if (TextUtils.isEmpty(pushDeviderDate)) {
            pushDeviderDate = model.getPushDivider();
        }
        if (endDate.equalsIgnoreCase(pushDeviderDate)) {
            holder.rlPushDevider.setVisibility(View.VISIBLE);
            defultPos = position;
            list.get(position).isVisible = true;
        } else if (position == defultPos && list.get(position).isVisible) {
            holder.rlPushDevider.setVisibility(View.GONE);
            list.get(position).isVisible = true;
        } else {
            holder.rlPushDevider.setVisibility(View.GONE);
            list.get(position).isVisible = false;
        }

        //=======convert start date and end date of dd -MMMM formate ===============

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date formattedSDate = null, formattedEDate = null;
        try {
            formattedSDate = dateFormat.parse(startDate);
            formattedEDate = dateFormat.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat formatDate = new SimpleDateFormat("dd");
        DateFormat formatMonth = new SimpleDateFormat("MMM");
        String finalSDate = formatDate.format(formattedSDate);
        String finalSMonth = formatMonth.format(formattedSDate);
        String finalEDate = formatDate.format(formattedEDate);
        String finalEMonth = formatMonth.format(formattedEDate);
        holder.txtWeekDate.setText(finalSDate + " " + finalSMonth + "-" + finalEDate + "-" + finalEMonth);

        //=================================================================
        holder.rlMinush.setOnClickListener(v -> {
            resetAll();
            Date cDate = new Date();
            String curDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date myPushDeviderDate = null;
            Date curentDate = null;
            try {
                myPushDeviderDate = sdf.parse(pushDeviderDate);
                curentDate = sdf.parse(curDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (curentDate.before(myPushDeviderDate)) {
                if (defultPos > 1) {
                    list.get(defultPos - 1).isVisible = true;
                    pushDeviderDate = list.get(defultPos - 1).getEnd_date();
                    notifyDataSetChanged();
                    pushDividerListener.onSetPreviousPushDivider(position, pushDeviderDate);
                }
            }
        });

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (!TextUtils.isEmpty(PublishDate) && !TextUtils.isEmpty(oldDate)) {
                Date myPushDeviderDate = sdf.parse(PublishDate);
                Date curentDate = sdf.parse(oldDate);

                if (myPushDeviderDate.getTime() == curentDate.getTime()) {
                    holder.mPublish.setEnabled(false);
                    holder.mPublish.setAlpha(0.5f);
                } else {
                    holder.mPublish.setEnabled(true);
                    holder.mPublish.setAlpha(1.0f);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.rlPlush.setOnClickListener(v -> {
            resetAll();
            list.get(defultPos + 1).isVisible = true;
            pushDeviderDate = list.get(defultPos + 1).getEnd_date();
            notifyDataSetChanged();
            pushDividerListener.onSetNextPushDivider(position, pushDeviderDate);
        });
        holder.mPublish.setOnClickListener(v -> publishOnClick.callPublishClick(position));
    }

    public void resetAll() {
        for (PushDeviderModel m : list) {
            m.isVisible = false;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public PushDeviderAdapter(ArrayList<PushDeviderModel> list, AddPushDeviderFragment addPushDeviderFragment, int pos, String pushDeviderDate, String PublishDate) {
        this.list = list;
        this.context = addPushDeviderFragment.getActivity();
        this.addPushDeviderFragment = addPushDeviderFragment;
        this.pushDividerListener = addPushDeviderFragment;
        this.publishOnClick = addPushDeviderFragment;
        this.defultPos = pos;
        this.oldDate = pushDeviderDate;
        this.PublishDate = PublishDate;
        list.get(defultPos).isVisible = true;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlMinush, rlPlush;
        LinearLayout rlPushDevider;
        Button mPublish;
        TextView txtWeek, txtWeekNo, txtWeekDate;

        public MyViewHolder(View view) {
            super(view);

            txtWeek = view.findViewById(R.id.txtWeek);
            txtWeekNo = view.findViewById(R.id.txtWeekNo);
            txtWeekDate = view.findViewById(R.id.txtWeekDate);
            rlMinush = view.findViewById(R.id.rl_minush);
            rlPlush = view.findViewById(R.id.rl_plush);
            mPublish = view.findViewById(R.id.mPublish);
            rlPushDevider = view.findViewById(R.id.rlPushDevider);
        }


    }
}
