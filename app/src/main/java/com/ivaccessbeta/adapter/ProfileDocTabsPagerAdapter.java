package com.ivaccessbeta.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ivaccessbeta.fragment.profile.ExpiredDocFragment;
import com.ivaccessbeta.fragment.profile.NotExpiredDocFragment;
import com.ivaccessbeta.model.ProfileEmpDocModel;

import java.util.ArrayList;

public class ProfileDocTabsPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<ProfileEmpDocModel> list;

    public ProfileDocTabsPagerAdapter(FragmentManager fm, ArrayList<ProfileEmpDocModel> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        switch (i) {
            case 0:
                fragment = NotExpiredDocFragment.newInstance(list);
                break;
            case 1:
                fragment = ExpiredDocFragment.newInstance(list);
                break;

        }
        return fragment;

    }

    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 2;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 2; //No of Tabs
    }


}
