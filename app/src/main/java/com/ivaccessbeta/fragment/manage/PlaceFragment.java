package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.PlaceAdapter;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

public class PlaceFragment extends Fragment implements AsynchTaskListner {

    public RecyclerView rvPeople;
    public PlaceAdapter mAdapter;
    public ArrayList<PlaceModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static PlaceFragment instance;
    public EditText edtSearch;
    public View view;
    public PlaceModel placeModel;
    public String emailId, token, userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_place, container, false);
        linearJobs.setVisibility(View.GONE);
        instance = this;
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");

        setHasOptionsMenu(true);
        rvPeople = view.findViewById(R.id.rvPlace);
        relativeAdd = view.findViewById(R.id.relative_add);
        relativeAdd.setOnClickListener(view -> {
            AddPlacefragment addPeopleFragment = new AddPlacefragment();
            Bundle bundle = new Bundle();
            bundle.putInt("value", 0);
            addPeopleFragment.setArguments(bundle);
            ((MainActivity)getActivity()).changeFragment(addPeopleFragment, true);

        });

        edtSearch = view.findViewById(R.id.edt_search);


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // menu.getItem(1).setVisible(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getPlace();
    }

    private void getPlace() {
        list.clear();
        new CallRequest(instance).getPlace(emailId, token, "");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getPlace:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            list.addAll(LoganSquare.parseList(jObject.toString(), PlaceModel.class));
                            mAdapter = new PlaceAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvPeople.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvPeople.setLayoutManager(mLayoutManager);
                            rvPeople.setItemAnimator(new DefaultItemAnimator());
                            rvPeople.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deletePlace:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Place Delete Successfully", getContext());
                            getPlace();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

}
