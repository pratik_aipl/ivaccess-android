package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

/**
 * Created by User on 25-06-2018.
 */

@JsonObject
public class CheckNoteModel {

    @JsonField
    public String CheckDate,UserID,CreatedOn,Note,CheckNoteID;

    public String getCheckDate() {
        return CheckDate;
    }

    public void setCheckDate(String checkDate) {
        CheckDate = checkDate;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public String getCheckNoteID() {
        return CheckNoteID;
    }

    public void setCheckNoteID(String checkNoteID) {
        CheckNoteID = checkNoteID;
    }
}
