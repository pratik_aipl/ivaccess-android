package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.ServiceTypeAdapter;
import com.ivaccessbeta.model.ServieTypeModel;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceTypeFragment extends Fragment implements AsynchTaskListner {
    public View view;
    public RecyclerView rvPeople;
    public ServiceTypeAdapter mAdapter;
    public ArrayList<ServieTypeModel> list = new ArrayList<>();
    public RelativeLayout relativeAdd;
    public static ServiceTypeFragment instance;
    public EditText edtSearch;
    public ServieTypeModel servieTypeModel;
    public String emailId, token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_service_type, container, false);
        instance = this;
        linearJobs.setVisibility(View.GONE);
        rvPeople = view.findViewById(R.id.rvServiceType);
        relativeAdd = view.findViewById(R.id.relative_add);

        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");

//        emailId = App.uEmail;
//        token = App.uToken;
        setHasOptionsMenu(true);

        relativeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddServiceFragment addServiceFragment=new AddServiceFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("value",0);
                addServiceFragment.setArguments(bundle);
                ((MainActivity)getActivity()).changeFragment(addServiceFragment, true);
            }
        });

        edtSearch = view.findViewById(R.id.edt_search);
        getServiceDetail();

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }

            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getServiceDetail();
    }

    private void getServiceDetail() {
        list.clear();
        new CallRequest(instance).getService(emailId, token);

    }

    @Override
    public void onResume() {
        super.onResume();
        // getServiceDetail();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // menu.getItem(1).setVisible(false);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            list.clear();
            switch (request) {
                case getService:

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {

                            JSONArray jObject = jObj.getJSONArray("data");

                            list.clear();
                            list.addAll(LoganSquare.parseList(jObject.toString(),ServieTypeModel.class));

//                            for (int i = 0; jObject.length() > i; i++) {
//                                servieTypeModel = new ServieTypeModel();
//                                servieTypeModel = (ServieTypeModel) jParser.parseJson(jObject.getJSONObject(i), new ServieTypeModel());
//                            }
                            mAdapter = new ServiceTypeAdapter(getActivity(),list);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                            rvPeople.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            rvPeople.setLayoutManager(mLayoutManager);
                            rvPeople.setItemAnimator(new DefaultItemAnimator());
                            rvPeople.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                        }  else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case deleteService:
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Service Delete Successfully", getContext());
                            getServiceDetail();
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}


