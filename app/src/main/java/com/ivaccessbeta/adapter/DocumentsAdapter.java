package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.fragment.manage.AddDocumentFragment;
import com.ivaccessbeta.fragment.manage.DocumentsFragment;
import com.ivaccessbeta.model.DocumentsModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.MySharedPref;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 01-03-2018.
 */

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.MyViewHolder> {

    public ArrayList<DocumentsModel> list;
    public List<DocumentsModel> searchFilterList = new ArrayList<>();
    public Context context;
    public   String emailId,token,userId;
    @Override
    public DocumentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_region, parent, false);

        return new DocumentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DocumentsAdapter.MyViewHolder holder, final int position) {
        final DocumentsModel model = list.get(position);
        holder.name.setText(model.getDocumentName());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailId= MySharedPref.getString(context, App.EMAIL,"");
                token=MySharedPref.getString(context, App.APP_TOKEN,"");
                userId=MySharedPref.getString(context, App.LOGGED_USER_ID,"");

                doDelete(position);
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDocumentFragment addDocumentFragment=new AddDocumentFragment();
                Bundle b=new Bundle();
                b.putInt("value",1);
                b.putSerializable("DocumentsModel",list.get(position));
                addDocumentFragment.setArguments(b);
                ((MainActivity)context).changeFragment(addDocumentFragment,true);
            }
        });

    }
    private void doDelete(final int position) {

        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .content("Are you sure you want to Delete?")
                .positiveText("Ok")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        new CallRequest(DocumentsFragment.instance).deleteDocument(emailId,token,userId,list.get(position).getDOCID());
                    }
                })
                .negativeText("Cancel")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        dialog.show();
    }

    public void filters(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            list.addAll(searchFilterList);
        } else {
            for (DocumentsModel bean : searchFilterList) {
                if (bean.getDocumentName().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    list.add(bean);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public DocumentsAdapter(Context context, ArrayList<DocumentsModel> List) {
        this.context = context;
        this.list = List;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView ivDelete,ivEdit;

        public MyViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.txtName);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivEdit=view.findViewById(R.id.ivEdit);

        }

    }
    }
