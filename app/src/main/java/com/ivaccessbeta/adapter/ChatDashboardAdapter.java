package com.ivaccessbeta.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.chat.ChatFragment;
import com.ivaccessbeta.model.ChatDashboardModel;
import com.ivaccessbeta.utils.MySharedPref;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatDashboardAdapter extends RecyclerView.Adapter<ChatDashboardAdapter.MyViewHolder> {

    public ArrayList<ChatDashboardModel> list;
    public List<ChatDashboardModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;
    public ChatDashboardModel model;

    @Override
    public ChatDashboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_chat_dashboard, parent, false);
        return new ChatDashboardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChatDashboardAdapter.MyViewHolder holder, final int position) {
        holder.name.setText(list.get(position).getChatName());
        Picasso.with(context).load(list.get(position).getProfileImage()).into(holder.ivProfile);
     //   holder.txtTime.setText(list.get(position).getSendingDate());
        holder.txtCount.setText(list.get(position).getPenddingView());
        emailId = MySharedPref.getString(context, App.EMAIL, "");
        token = MySharedPref.getString(context, App.APP_TOKEN, "");
        userId = MySharedPref.getString(context, App.LOGGED_USER_ID, "");
        if(list.get(position).getPenddingView().equalsIgnoreCase("0")){
            holder.txtCount.setVisibility(View.GONE);
            holder.rlCount.setVisibility(View.GONE);
        }
        getDateMMYYFormate(position,holder.txtTime);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putSerializable("model",list.get(position));
                ChatFragment chatFragment=new ChatFragment();
                chatFragment.setArguments(bundle);
                ((MainActivity)context).changeFragment(chatFragment,true);
            }
        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public ChatDashboardAdapter(ArrayList<ChatDashboardModel> List, Context context) {
        this.list = List;
        this.context = context;
        this.searchFilterList.addAll(list);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name, txtTime, txtCount;
        ImageView ivProfile;
        RelativeLayout rlCount;
        public MyViewHolder(View view) {
            super(view);
            txtCount = view.findViewById(R.id.txtCount);
            txtTime = view.findViewById(R.id.txtTime);
            name = view.findViewById(R.id.txtName);
            ivProfile = view.findViewById(R.id.ivProfile);
            rlCount=view.findViewById(R.id.rlCount);
        }
    }
    private void getDateMMYYFormate(int position ,TextView txt) {
        String CurrentStringEnd =list.get(position).getSendingDate();
        String[] separated = CurrentStringEnd.split("-");
        final String s1End = separated[1];
        String sEndMonth = null;
        System.out.println("spilits" + s1End);

        if (s1End.equalsIgnoreCase("01")) {
            sEndMonth = "January ";
        } else if (s1End.equalsIgnoreCase("02")) {
            sEndMonth = "February ";
        } else if (s1End.equalsIgnoreCase("03")) {
            sEndMonth = "March ";
        } else if (s1End.equalsIgnoreCase("04")) {
            sEndMonth = "April ";
        } else if (s1End.equalsIgnoreCase("05")) {
            sEndMonth = "May ";
        } else if (s1End.equalsIgnoreCase("06")) {
            sEndMonth = "June ";
        } else if (s1End.equalsIgnoreCase("07")) {
            sEndMonth = "July ";
        } else if (s1End.equalsIgnoreCase("08")) {
            sEndMonth = "August ";
        } else if (s1End.equalsIgnoreCase("09")) {
            sEndMonth = "September ";
        } else if (s1End.equalsIgnoreCase("10")) {
            sEndMonth = "October ";
        } else if (s1End.equalsIgnoreCase("11")) {
            sEndMonth = "November ";
        } else if (s1End.equalsIgnoreCase("12")) {
            sEndMonth = "December ";
        }
        String asubstringEnd = CurrentStringEnd.substring(8, 10);

        String date = list.get(position).getSendingDate();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String year = (String) DateFormat.format("yyyy",myDate);
        txt.setText(firstThree(sEndMonth)+" "+asubstringEnd+" "+year);


    }
    public String firstThree(String str) {
        return str.length() < 3 ? str : str.substring(0, 3);
    }
}
