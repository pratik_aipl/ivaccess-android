package com.ivaccessbeta.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ivaccessbeta.R;

import java.util.ArrayList;
import java.util.List;

public class SearchableAdapter extends BaseAdapter implements Filterable {

    private List<String> originalData = null;
    private List<String> filteredData = null;
    private LayoutInflater mInflater;

    public SearchableAdapter(Context context, List<String> data) {
        this.filteredData = data;
        this.originalData = data;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_checkbox, null);
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.text1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        // If weren't re-ordering this you could rely on what you set last time
        holder.text.setText(filteredData.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView text;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredData = (List<String>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<String> FilteredArrayNames = new ArrayList<String>();
                constraint = constraint.toString().toLowerCase();
                if (TextUtils.isEmpty(constraint)) {
                    results.values = originalData;
                    results.count = originalData.size();
                } else {
                    for (int i = 0; i < originalData.size(); i++) {
                        String dataNames = originalData.get(i);
                        if (dataNames.toLowerCase().startsWith(constraint.toString()))  {
                            FilteredArrayNames.add(dataNames);
                        }
                    }
                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;
                }
                return results;
            }
        };

        return filter;
    }

}