package com.ivaccessbeta.model;

/**
 * Created by User on 28-02-2018.
 */

public class ColorPickerModel {
    public   int colorCode;

    public ColorPickerModel(int colorCode) {
        this.colorCode = colorCode;
    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }
}
