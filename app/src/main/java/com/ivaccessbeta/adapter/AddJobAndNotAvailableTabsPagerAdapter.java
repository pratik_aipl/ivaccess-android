package com.ivaccessbeta.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ivaccessbeta.fragment.schedule.AddSchedualFragment;
import com.ivaccessbeta.fragment.schedule.AddNotAvailableFragment;

/**
 * Created by User on 10-04-2018.
 */

public class AddJobAndNotAvailableTabsPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public AddJobAndNotAvailableTabsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                AddSchedualFragment tab1 = new AddSchedualFragment();
                return tab1;
            case 1:
                AddNotAvailableFragment tab2 = new AddNotAvailableFragment();
                return tab2;

        }
        return null;

    }
    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return 2;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return 2; //No of Tabs
    }


}
