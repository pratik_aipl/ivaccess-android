package com.ivaccessbeta.fragment.manage;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.adapter.NothingSelectedSpinnerAdapter;
import com.ivaccessbeta.model.PlaceModel;
import com.ivaccessbeta.model.RegionModel;
import com.ivaccessbeta.App;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.ivaccessbeta.activity.MainActivity.drawer;
import static com.ivaccessbeta.activity.MainActivity.linearJobs;
import static com.ivaccessbeta.activity.MainActivity.toolbar;

public class AddPlacefragment extends Fragment implements AsynchTaskListner{
    public Spinner spRegion;
    public EditText edtPlaceName;
    public String primaryRegion,placeName,placeId;
    public int regionPos=0,value;
    public ArrayList<RegionModel> regionList = new ArrayList<>();
    public ArrayList<String> regionStringList = new ArrayList<>();
    public AddPlacefragment instance;
    public RegionModel regionModel;
    public String emailId, token, userId;
    public Button btnSave,btnCancel;
    public PlaceModel placeModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view;
        view= inflater.inflate(R.layout.fragment_add_placefragment, container, false);
        MainActivity.toolbar_title.setText("Add Place");
        ((MainActivity)getActivity()).showToolbarAction();
        instance = this;
        linearJobs.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        value=getArguments().getInt("value");

        emailId = MySharedPref.getString(getContext(), App.EMAIL, "");
        token = MySharedPref.getString(getContext(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getContext(), App.LOGGED_USER_ID, "");

        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        edtPlaceName = view.findViewById(R.id.edtPlaceName);
        spRegion = view.findViewById(R.id.spinnerPrimariRegion);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);

        if(value==1){
            placeModel = (PlaceModel)getArguments().getSerializable("PlaceModel");
            placeId=placeModel.getPlacesID();
            primaryRegion=placeModel.getRegionID();
            placeName=placeModel.getPlacesName();
            MainActivity.toolbar_title.setText("Edit Place");
            edtPlaceName.setText(placeName);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeName = edtPlaceName.getText().toString().trim();
                if (placeName.equals("")) {
                    Utils.showToast("Enter Place Name", getContext());
                } if (TextUtils.isEmpty(primaryRegion)) {
                    Utils.showToast("Please select Region", getContext());
                }  else {
                    if(value==0) {
                        new CallRequest(instance).addPlace(emailId, token,userId,primaryRegion,placeName);
                    }
                    else if(value==1){
                        new CallRequest(instance).updatePlace(emailId, token, userId,primaryRegion,placeName,placeId);
                    }
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        spRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    Utils.disableKeyboard(getContext(), view);
                    primaryRegion = regionList.get(i-1).getRegionID();
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getRegion();
        return view;
    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Utils.allMenuHide(menu);
    }
    private void getRegion() {
        new CallRequest(instance).getRegion(emailId, token);
    }
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getRegion:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("status").equals("200")) {
                            JSONArray jsonArray = jObj.getJSONArray("data");
                            for (int i = 0; jsonArray.length() > i; i++) {
                                RegionModel roleModel = LoganSquare.parse(jsonArray.getJSONObject(i).toString(),RegionModel.class);
                                regionList.add(roleModel);
                                regionStringList.add(regionList.get(i).getRegionName());
                                if (primaryRegion != null) {
                                    if (primaryRegion.equalsIgnoreCase(roleModel.getRegionID())) {
                                        regionPos = i + 1;
                                    }
                                }
                            }
                            ArrayAdapter aa = new ArrayAdapter(getActivity(), R.layout.spinner_text, regionStringList);
                            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spRegion.setAdapter(new NothingSelectedSpinnerAdapter(
                                    aa, R.layout.resign_spinner_row_nothing_selected,
                                    getActivity()));
                            spRegion.setSelection(regionPos);

                        }
                        else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    break;

                case addPlace:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Place Add Successfully", getContext());
                            getActivity().onBackPressed();
                        }
                        else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updatePlace:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            Utils.showToast("Place Update Succesfully", getContext());
                            getActivity().onBackPressed();
                        }
                        else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
