package com.ivaccessbeta.fragment.Directory;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.ivaccessbeta.App;
import com.ivaccessbeta.R;
import com.ivaccessbeta.activity.MainActivity;
import com.ivaccessbeta.utils.AsynchTaskListner;
import com.ivaccessbeta.utils.CallRequest;
import com.ivaccessbeta.utils.Constant;
import com.ivaccessbeta.utils.MySharedPref;
import com.ivaccessbeta.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ivaccessbeta.activity.MainActivity.linearJobs;

/**
 * A simple {@link Fragment} subclass.
 */
public class ByRegionFragment extends Fragment implements AsynchTaskListner {
    public ExpandableListView expandableListView;
    public ArrayList<ByRegionGroupModel> expandableListGroup = new ArrayList<>();

    public String emailId, token, userId, roleId;
    public EditText edtSearch;
    ImageView mRefresh;
    public ByRegionAdapter mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_by_region, container, false);

        expandableListView = view.findViewById(R.id.expandableListView);
        mRefresh = view.findViewById(R.id.mRefresh);
        setHasOptionsMenu(true);
        linearJobs.setVisibility(View.GONE);
        MainActivity.toolbar_title.setText("DIRECTORY");
        ((MainActivity)getActivity()).showToolbarAction();
        emailId = MySharedPref.getString(getActivity(), App.EMAIL, "");
        token = MySharedPref.getString(getActivity(), App.APP_TOKEN, "");
        userId = MySharedPref.getString(getActivity(), App.LOGGED_USER_ID, "");
        roleId = MySharedPref.getString(getActivity(), App.ROLEID, "");
        edtSearch=view.findViewById(R.id.edt_search);

        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtSearch.setText("");
                getDirectoryByRegionName();
            }
        });
       // mAdapter=new ByRegionAdapter(getContext());

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtSearch != null) {
                    if (mAdapter != null) {
                        String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                        mAdapter.filters(text);
                    }
                }
            }
        });

//        expandableListChild.add(new ByRegionChildModel("123", "2/12/2018", "1200", "Midline: Rolay", "JN:#40405 BENDUCE", "RM:312 DOB : 23/12"));
//        expandableListChild.add(new ByRegionChildModel("123", "2/12/2018", "1200", "Midline: Rolay", "JN:#40405 BENDUCE", "RM:312 DOB : 23/12"));
//
//        expandableListGroup.add(new ByRegionGroupModel("23/12/2018", "Child", "345", expandableListChild));
//        expandableListGroup.add(new ByRegionGroupModel("23/12/2018", "Child", "345", expandableListChild2));
//        expandableListAdapter = new ByRegionAdapter(getActivity(), expandableListGroup,instance);

        return view;
    }

    private void getDirectoryByRegionName() {
        new CallRequest(ByRegionFragment.this).getDirectortyByName(emailId, token, "region");
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {

                case getDirectoryByName:
                    expandableListGroup.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getString("status").equals("200")) {
                            JSONArray jObject = jObj.getJSONArray("data");
                            ByRegionGroupModel groupModel = null;
                            expandableListGroup.clear();
                            for (int i = 0; jObject.length() > i; i++) {
                                JSONObject ob = jObject.getJSONObject(i);
                                groupModel = LoganSquare.parse(ob.toString(),ByRegionGroupModel.class);
                                JSONArray childArray = ob.getJSONArray("Contractor");
                                for (int j = 0; childArray.length() > j; j++) {
                                    ByRegionChildModel groupModel2 = new ByRegionChildModel();
                                    groupModel2 = LoganSquare.parse(childArray.getJSONObject(j).toString(),ByRegionChildModel.class);
                                    groupModel.childList.add(groupModel2);
                                }
                                expandableListGroup.add(groupModel);
                            }
                            mAdapter = new ByRegionAdapter(getActivity(), expandableListGroup);
                            expandableListView.setAdapter(mAdapter);
                        } else {
                            Utils.handleJSonAPIError(jObj, getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getDirectoryByRegionName();
    }
}
