package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by User on 01-03-2018.
 */
@JsonObject
public class DocumentsModel implements Serializable {

    @JsonField
    public String DOCID, DocumentName, ExpiryDay;

    public String getExpiryDay() {
        return ExpiryDay;
    }

    public void setExpiryDay(String expiryDay) {
        ExpiryDay = expiryDay;
    }

    public String getDOCID() {
        return DOCID;
    }

    public void setDOCID(String DOCID) {
        this.DOCID = DOCID;
    }

    public String getDocumentName() {
        return DocumentName;
    }

    public void setDocumentName(String documentName) {
        DocumentName = documentName;
    }
}
