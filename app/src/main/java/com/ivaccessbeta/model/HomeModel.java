package com.ivaccessbeta.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by User on 28-02-2018.
 */

@JsonObject
public class HomeModel implements Serializable{

    @JsonField
    public  String JobID,DueTimeDate,FacilityName,first_name,last_name,JobStatusColor,RoomNumber,JobNo,CreatedOn,IsCheck,IsBill;

    public String getIsCheck() {
        return IsCheck;
    }

    public void setIsCheck(String isCheck) {
        IsCheck = isCheck;
    }

    public String getIsBill() {
        return IsBill;
    }

    public void setIsBill(String isBill) {
        IsBill = isBill;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getJobNo() {
        return JobNo;
    }

    public void setJobNo(String jobNo) {
        JobNo = jobNo;
    }

    public String getRoomNumber() {
        return RoomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        RoomNumber = roomNumber;
    }

    public String getJobID() {
        return JobID;
    }

    public void setJobID(String jobID) {
        JobID = jobID;
    }

    public String getDueTimeDate() {
        return DueTimeDate;
    }

    public void setDueTimeDate(String dueTimeDate) {
        DueTimeDate = dueTimeDate;
    }

    public String getFacilityName() {
        return FacilityName;
    }

    public void setFacilityName(String facilityName) {
        FacilityName = facilityName;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getJobStatusColor() {
        return JobStatusColor;
    }

    public void setJobStatusColor(String jobStatusColor) {
        JobStatusColor = jobStatusColor;
    }
    public ArrayList<JobDetailsModel> ChildJobDetailList = new ArrayList<>();
}
